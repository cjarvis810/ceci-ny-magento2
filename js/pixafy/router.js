/**
 * @param  {[type]} $j        jQuery.noConflict()
 * @param  {[type]} window    instance of window
 * @param  {[type]} document  window.document
 * @param  {[type]} pix       window.pix
 * @param  {[type]} undefined
 * Encapsulate the code in modern standards strict mode self invoking function.
 */
;(function($j, window, document, pix, undefined) {
    "use strict";

    pix = pix || window.pix || {}; //make sure that pix namespace is defined.
    pix.modules = pix.modules || window.pix.modules || {}; //add modules list

    pix.$win = pix.$win || $j(window);
    pix.$doc = pix.$doc || $j(document);
    pix.$body = !!pix.$body &&  !!pix.$body.length ? pix.$body : $j(document.body);
    pix.$doc.ready(docReady);


    pix.router = (function(){
        
        
        var modules = {
                LoginModalWindow:   pix.modules.LoginModalWindow,
                LgSlider:           pix.modules.LgSlider,
                instagramfeed:      pix.modules.instagramfeed,
                MenuTab:            pix.modules.MenuTab,
                GridModalWindow:    pix.modules.GridModalWindow,
                menuColorWheel:     pix.modules.menuColorWheel,
                StickyNav:          pix.modules.StickyNav,
                ScrollLink:         pix.modules.ScrollLink,
                EventSection:       pix.modules.EventSection,
                MiscFunctions:      pix.modules.MiscFunctions,
                checkRadio:         pix.modules.checkRadio,
                quizQuestionaire:   pix.modules.quizQuestionaire,
                jsDropdown:         pix.modules.jsDropdown,
                pixStateSelect:     pix.modules.pixStateSelect,
                expandModalWindow:  pix.modules.expandModalWindow,
                catFilters:         pix.modules.catFilters,
                globalModal:        pix.modules.GlobalModalWindow,
                globalAccordion:    pix.modules.globalAccordion,
                globalTabs:         pix.modules.globalTabs,
                nextAccordion:      pix.modules.nextAccordion,
                newsletterPromo:    pix.modules.newsletterPromo,
                setColorSwatches:   pix.modules.setColorSwatches,
                checkGiftCardBalance:   pix.modules.checkGiftCardBalance,
                quickSwapImages:        pix.modules.quickSwapImages,
                getCurrentColorwheelSettings:   pix.modules.getCurrentColorwheelSettings,
                loadDefaultColorway:            pix.modules.loadDefaultColorway,
                vimeoVideo:         pix.modules.vimeoVideo

            },
            pages = {
                "catalog-product-view": function(){
                    pix.productview.ThumbnailCaroufredsel();
                    pix.productview.miniSlider();
                    pix.productview.quoteSlider();
                }
            },
            pageLoaded = {


            },
            loopOver = function(obj, ignorePageClass){
                for (var prop in obj) {
                    var bodyHasClass = !!ignorePageClass ? true : pix.$body.hasClass(prop);
                    if (obj.hasOwnProperty(prop) && bodyHasClass) {
                        obj[prop]();
                    }
                }
                return obj;
            };
        return {
            modules: modules,
            pages: pages,
            pageLoaded: pageLoaded,
            onready: function(){
                loopOver(modules, true);
                loopOver(pages);
            },
            onload: function(){
                loopOver(pageLoaded, true);
            }
        };
    })();


    /************** On Ready ***************/
    /*************************************/
    function docReady(){
        pix.$body = $j(document.body);
        pix.$win.on("scroll.scrollTopHelper", pix.modules.scrollToTop);
        pix.$body.on("scroll.scrollTopHelper", pix.modules.scrollToTop);
        pix.modules.scrollDownArrow();
        pix.router.onready();
        pix.category.QuickviewModalWindow();
        pix.category.QuickviewCaroufredsel();
        pix.modules.colorPicker();
        pix.modules.contactus();
    }
    
    /************** On Load ***************/
    /**************************************/

    pix.$win.on("load", function(){
        pix.modules.regularGridSlider();
        pix.modules.parallaxSlider();
        pix.modules.quickFadeSlider();
        pix.modules.largeHeaderSlider();
        pix.modules.EventSlider();
        pix.modules.RegularSlider();
        pix.modules.ScrollbarSlider();
        pix.modules.miniSlider();
        pix.modules.generalSlider();
        $j('.v-align').vAlign();
        $j('.home-grid .box').gridHover();
        pix.modules.masonry();
        pix.router.onload();
        pix.modules.TabNavigation();
        pix.modules.ThumbnailCaroufredsel();
        pix.modules.Notify();
        pix.modules.PositionSticky();
        pix.modules.masterfunc();
        pix.modules.curhome();
    });



    window.pix = pix;
})(jQuery.noConflict(), this, this.document, this.pix);