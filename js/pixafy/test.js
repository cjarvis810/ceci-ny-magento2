/**
* Created by jcymerman on 3/27/2015.
*/
(function ($) {
    "use strict";
    $(document).ready(function() {
        $(document).on("click", ".default-shipping", function () {
            //var form = $(this).closest("form");
            console.log($(this).data('val'));
        });
        $(document).on("keyup", ".personalization-text", function () {
            var $this = $(this);
            $('svg').find('.text-' + $this.attr('data-text')).first().text();
            $('svg').pixPersonalizeSetText($this.attr('data-text'), $this.val());
        });
        $("#markasdonebutton").click(function(e){
            e.preventDefault();
            var sku=$('#select-sku').find(":selected").data('sku');
            window.location.href='/index.php/admin/svg/markasdone?sku='+encodeURIComponent(sku);
        });

        $("#exportasepsbutton").click(function(e){
            e.preventDefault();
            var sku=$('#select-sku').find(":selected").data('sku');
            window.location.href='/index.php/admin/svg/downloadeps?sku='+encodeURIComponent(sku);
        });
        $("#bleedLineToggle").change(function() {
            if(this.checked) {
                $("#bleedline_1_"+$( "#select-sku option:selected").text()).show();
            } else {
                $("#bleedline_1_"+$( "#select-sku option:selected").text()).hide();
            }
        });
        $("#innerTrimToggle").change(function() {
            if(this.checked) {
            } else {
            }
        });
        $("#outerTrimToggle").change(function() {
            if(this.checked) {
            } else {
            }
        });
        $('#select-sku').on('change', function() {
            var selected = $(this).val();
            var sku = $(this.options[this.selectedIndex]).attr('data-sku');

            $(this.options[this.selectedIndex]).attr("selected", "selected");
            $("#select-sku").children().each(function() {

                if(this.text != sku) {
                    $(this).removeAttr('selected');
                }
            });

            var $leftPanel = $('#left-panel');
            $leftPanel.find('embed, svg, img').replaceWith('<img src="' + selected + '" />');
            if (selected.match(/\.svg$/)) {
                $leftPanel.find('img').pixPersonalizeInitSvg(function() {
                    var $svg = $('#svg-' + sku);
                    $('select.personalization-color').off('change').on('change', function () {
                        var $this = $(this);
                        var selected = $this.val();
                        var r = parseInt(selected.substring(0, 1), 16);
                        var g = parseInt(selected.substring(1, 2), 16);
                        var b = parseInt(selected.substring(2, 3), 16);
                        var rPct = parseFloat(r) / 15;
                        var gPct = parseFloat(g) / 15;
                        var bPct = parseFloat(b) / 15;
                        $svg.pixPersonalizeSetColorPct($this.attr('data-color'), rPct, gPct, bPct);
                    }).trigger('change');

                    $('select.personalization-font').each(function() {
                        var $this = $(this);
                        $this.val($('#svg-' + sku).find('.font-' + $this.attr('data-font')).first().attr('font-family'));
                    }).off('change').on('change', function () {
                        var $this = $(this);
                        var dataFont = $this.attr('data-font');
                        $svg.pixPersonalizeSetFont(dataFont, $this.val());
                    });

                    $('input[type=text].personalization-text').each(function () {
                        var $this = $(this);
                        $this.val($svg.find('.text-' + $this.attr('data-text')).first().text());
                    }).off('keyup').on('keyup', function () {
                        var $this = $(this);
                        $svg.pixPersonalizeSetText($this.attr('data-text'), $this.val());
                        //console.log("First text: ",  $this('.text-1').val());


                    });

                    $('#right-panel').show();
                });


            } else {
                $('#right-panel').hide();
            }
        }).trigger('change');
    });
})(jQuery.noConflict());