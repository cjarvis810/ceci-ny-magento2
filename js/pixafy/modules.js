/**
 * @param  {[type]} $j        jQuery.noConflict()
 * @param  {[type]} window    instance of window
 * @param  {[type]} document  window.document
 * @param  {[type]} pix       window.pix
 * @param  {[type]} undefined
 * Encapsulate the code in modern standards strict mode self invoking function.
 */
;(function($j, window, document, pix, $f, Enterprise, undefined) {
    "use strict";

    pix = pix || window.pix || {}; //make sure that pix namespace is defined.
    pix.modules = pix.modules || {}; //add modules list

    pix.$win = pix.$win || $j(window);
    pix.$doc = pix.$doc || $j(document);
    pix.$body = !!pix.$body && !!pix.$body.length ? pix.$body : $j(document.body);


    pix.modules.getCurrentColorwheelSettings = function() {

        // Get current colorwheel settings //
        //var swatches.data = ["000000","FFFF00"]
        //pix.modules.changeColorSwatch(swatches);


        /*
        var colorWheelURL = 'pixcore/ajax/getColorwheelSettings/';
        var url = window.getUrl(colorWheelURL);

        $j.ajax({
            url: url,
            type: 'GET'
        }).done(function(data) {
            //$j('.'+data.overlayName).addClass('active-wheel');
            // Set all products to this colorway
        });*/
    };

    pix.modules.curhome = function(){

        //var curalateClone = $j.extend(window.Curalate.FanReels);

        window.Curalate.FanReels.Gallery.init({ 
            code: "cecinewyork",
            filters:{
               tags: "homepage"
        }});
    };


    // Author: Khalid Saleem
    // Description: Instagram Feed using Instafeed Library * Container and Hashtags are in Static Blocks
    //==================================================================================================

    pix.modules.instagramfeed = function(){


        $j(".instawidget").each(function() {

            var hashtagID = this.id;
            var hashTags = $j("#" + hashtagID).attr('data-hashtags');
            var hashTagsSize = $j("#" + hashtagID).attr('data-hashtagssize');
            var hashTagsLimit = $j("#" + hashtagID).attr('data-hashtagslimit');

            var displayInstaBlocks = function(response, params) {

                var data = response.data;
                var blockList = document.createElement('ul');
                for(var i = 0; i < data.length; i++) {
                    // List Item
                    var listItem = document.createElement('li');

                    // Anchor
                    var anchor = document.createElement('a');
                    anchor.setAttribute('target', '_blank');
                    anchor.href = data[i].link;

                    // Image
                    var instaImg = document.createElement('img');
                    instaImg.style.height = hashTagsSize + "px";
                    instaImg.style.width = hashTagsSize + "px";
                    instaImg.src = response.data[i].images.low_resolution.url;

                    anchor.appendChild(instaImg);
                    listItem.appendChild(anchor);
                    blockList.appendChild(listItem);
                }

                document.getElementById(params).appendChild(blockList);
                $j("#"+hashtagID).find('.instagram-preloader').remove();
            };


            var fetcher = new window.Instafetch('0798c403198a4d039869e83f86831838');
            // Fetch results
            fetcher.fetch({
                user: 11628199,
                tag: hashTags,
                limit: hashTagsLimit,
                callback: displayInstaBlocks, 
                params: hashtagID
            });

        });
 
    };

    // Author: ?
    // Description: ?
    //==================================================================================================
    pix.modules.validatweEmail = function(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    };

    // Author: ?
    // Description: ?
    //==================================================================================================
    pix.modules.quickSwapImages = function() {

        /*
        // Listen for click event from side carousel
        $j('#quickview-thumbnail-image a').on('click', function() {

            var ajaxControllerUrl = window.getUrl("pixcore/ajax/getProductImage");

            // AJAX new product image url
            $j.post( ajaxControllerUrl, {product_id: $j(this).attr('product_id'), image_id: $j(this).attr('image_id')} )
                .done(function(res) {
                    $j("#mainProductImage").attr('src', res);

                });

        });*/

    };

    // Author: James
    // Description: ?
    //==================================================================================================
    pix.modules.showSpinner = function() {
        $j("#spinner").show();
    };

    // Author: James
    // Description: ?
    //==================================================================================================
    pix.modules.hideSpinner = function() {
        $j("#spinner").hide();
    };

    // Author: James
    // Description: Globally set color swatches for their respective image
    //==================================================================================================
    pix.modules.setColorSwatches = function() {

        $j(document).on('mouseover', '.product-swatch', function () {
            pix.modules.changeColorSwatch($j(this));
        });

        $j(document).on('mouseout', '.product-swatch', function () {
            pix.modules.changeColorSwatch($j(this));
        });
    };

    // Author: James
    // Description: ?
    //==================================================================================================
    pix.modules.loadDefaultColorway = function() {
        if($j('.product-view-wrapper').length || $j('.main-image-wrapper').length) {
            pix.modules.showSpinner();

            // AJAX get hex colors
            var ajaxUrl = window.getUrl("pixcore/ajax/getHexColor");

            $j.ajax({
                url: ajaxUrl,
                type: 'GET'
            }).done(function(colors) {
                if(colors) {
                    var skuURL = window.getUrl("pixcore/ajax/getProductSkuSupportedByCurrentColorwheel");
                    $j.ajax({
                        url: skuURL,
                        type: 'GET'
                    }).done(function(skus) {
                        var skuObj = $j.parseJSON(skus);
                        var image = $j('.main-image-wrapper svg');

                            // Get current image id/sku
                        if(image.length) {
                            var currentImageId =  image.attr('id').split("-");

                            if(skuObj.indexOf(currentImageId[1]) != -1) {

                                colors = colors.split(",");

                                // fecolormatrix
                                colors.forEach(function (color, index) {
                                    image.pixPersonalizeSetColorHex(index + 1, color);
                                });
                            }
                        }

                        pix.modules.hideSpinner();

                    });
                }
            });
        }
    };

    // Author: James
    // Description: ?
    //==================================================================================================
    pix.modules.changeColorSwatch = function(swatch) {

        if(!swatch.data('colors')){
            return;
        }

        // Lets find the nearest SVG we want to color
        var image = $j(".main-image-wrapper svg");

        // Lets take the data-colors attr and split up the hex code and assign it to a variable
        var colors = swatch.data('colors').split(',');

        // fecolormatrix7
        colors.forEach(function (color, index) {
            image.pixPersonalizeSetColorHex(index + 1, color);
        });
    };

    // Author: James & Khalid Saleem
    // Description: Check Gift Card Balance used in Gift Card Page
    //==================================================================================================
    pix.modules.checkGiftCardBalance = function() {

        /*$j( "#product_addtocart_form" ).submit(function( event ) {

            if(!$j("#giftcard_amount").val()) {
                $j("#validationError").text("One or more fields are missing please ensure all required fields are filled out correctly");
                event.preventDefault();
            }

            $j(".giftcard-form .required-entry").each(function() {

                if(!$j(this).val()) {

                    $j("#validationError").text("One or more fields are missing please ensure all required fields are filled out correctly");
                    event.preventDefault();
                } else {
                    if($j(this).hasClass('validate-email')) {
                        if(!pix.modules.validatweEmail($j(this).val())) {
                            $j("#validationError").text("One or more fields are missing or invalid please ensure all required fields are filled out correctly");
                            event.preventDefault();
                        }
                    }
                }
            });
        });*/

        var giftcardUrl = window.getUrl("pixcore/ajax/getGiftCardAmount");

        $j('#checkGiftCardBalance').on("click", function() {

            $j.post(giftcardUrl, {giftcardCode: $j("#giftcardCode").val()})
                .done(function(data) {
                    $j('.gift-card-balance .validation-advice').remove();
                    if(data) {
                       var floatedPrice = parseInt(data).toFixed(2);
                       $j( "<div class='validation-advice pass-txt'>You have " +"$"+floatedPrice+" remaining.</div>" ).insertAfter( "#giftcardCode" );
                    } else {
                        $j( "<div class='validation-advice'>Card code not found</div>" ).insertAfter( "#giftcardCode" );
                    }

                });

        });
    };

    // Author: James & Khalid Saleem
    // Description: when user land on page it triggers the black sign up modal window 
    //==================================================================================================
    pix.modules.countBlackModal = function() {

        if (document.cookie.indexOf('popupTest') == -1) {
            var secondsToWait = 60*4;

            var ajaxControllerUrl = window.getUrl("pixcore/ajax/getPopupCookie");

            $j.get( ajaxControllerUrl )
                .done(function( newsletterPopup ) {

                    var jsonString = JSON.parse(newsletterPopup);

                    if(jsonString.popupStatus == "black" && jsonString.popupSeconds <= secondsToWait) {

                        var intervalTime = setInterval(function() {
                            // Get the seconds

                            // AJAX state && seconds
                            $j.post(window.getUrl("pixcore/ajax/setPopupCookie"),  { popupStatus: "black", popupSeconds: jsonString.popupSeconds });

                            jsonString.popupSeconds += 1;

                            // Exit condition
                            if(jsonString.popupSeconds === secondsToWait && document.cookie.indexOf('coloredpopup')!=-1){

                                clearInterval(intervalTime);

                                $j.magnificPopup.open({
                                    items: {
                                        src: '#subscribe-modal-black', // can be a HTML string, jQuery object, or CSS selector
                                        type: 'inline'
                                    },
                                    closeBtnInside:true,
                                    midClick: true,
                                    focus: 'select',
                                    closeMarkup: '<button class="mfp-close ion-android-close"></button>',
                                    removalDelay: 500,
                                    mainClass: 'mfp-move-from-top'
                                });

                                $j.get(window.getUrl("pixcore/ajax/setWaitCondition"));

                            }else if(document.cookie.indexOf('coloredpopup')==-1){
                                clearInterval(intervalTime);
                                pix.modules.countColoredModal();
                            }

                        }, 1000);
                    }
                });
        }


    };

    // Author: James
    // Description: Color signup modal window triggers when you cancel the black sign up modal window - yea 2 windows
    //==================================================================================================
    pix.modules.countColoredModal = function() {


        if (document.cookie.indexOf('popupTest') == -1) {
            var secondsToWait = 30;

            var ajaxControllerUrl = window.getUrl("pixcore/ajax/getPopupCookie");

            $j.get( ajaxControllerUrl )
                .done(function( newsletterPopup ) {

                    var jsonString = JSON.parse(newsletterPopup);

                    if(jsonString.popupStatus == "colored" && jsonString.popupSeconds <= secondsToWait) {
                        var intervalTime = setInterval(function () {
                            // Get the seconds

                            // AJAX state && seconds
                            $j.post(window.getUrl("pixcore/ajax/setColoredCookie"), {popupSeconds: jsonString.popupSeconds});

                            jsonString.popupSeconds += 1;

                            // Exit condition
                            if (jsonString.popupSeconds === secondsToWait && document.cookie.indexOf('coloredpopup') == -1) {

                                $j.get(window.getUrl("pixcore/ajax/setBlackCookie"));

                                $j.magnificPopup.open({
                                    items: {
                                        src: '#subscribe-modal-colored', // can be a HTML string, jQuery object, or CSS selector
                                        type: 'inline'
                                    },
                                    closeBtnInside:true,
                                    mainClass: 'mfp-fade',
                                    midClick: true,
                                    focus: 'select',
                                    closeMarkup: '<button class="mfp-close ion-android-close"></button>',
                                    callbacks: {
                                        close: function () {
                                            document.cookie =
                                                'coloredpopup=1; path=/';
                                            pix.modules.countBlackModal();
                                        }
                                    }
                                });
                                clearInterval(intervalTime);
                            }else if(document.cookie.indexOf('coloredpopup') != -1){
                                clearInterval(intervalTime);
                                pix.modules.countBlackModal();
                            }

                        }, 1000);

                    }
                });
        }

    };

    // Author: James
    // Description: Newsletter Promo
    //==================================================================================================
    pix.modules.newsletterPromo = function() {

        if (document.cookie.indexOf('popupTest') == -1) {



            var ajaxControllerUrl = window.getUrl("pixcore/ajax/getPopupCookie");

            $j.get(  ajaxControllerUrl )
                .done(function( newsletterPopup ) {

                    // If newsletter popup is null -- Starting state //
                    if(!newsletterPopup) {
                        // Set newsletterPopup {state: "", seconds: "1"}
                        $j.post(window.getUrl("pixcore/ajax/setPopupCookie"),  { "popupStatus": "colored", "popupSeconds": "1" })
                            .done(function() {
                                pix.modules.countColoredModal();
                            });

                    } else {

                        pix.modules.countColoredModal();
                        pix.modules.countBlackModal();

                    }


                });

        }



    };

    // Author: James
    // Description: My Acccount -> Order History etc
    //==================================================================================================
    pix.modules.HistoryScroll = function() {
        var preloaderImagePath = window.getUrl("../skin/frontend/enterprise/ceciny/images/preloader.gif");
        var preloader = '<img id="preloaderImage" src="'+preloaderImagePath+'" alt="preloader"/>';
        var nextPage = 'a.nextPage:last';

        function appendDetach(firstObj, secondObj) {
            $j(firstObj+" > .jscroll-inner > ul").append($j(secondObj+' ul li').first().detach());
        }

        // ALL ORDERS //
        if($j(nextPage).length) {

            $j('#historyAllOrders').jscroll({
                loadingHtml: preloader,
                nextSelector: nextPage,
                callback: function() {
                    appendDetach('#historyAllOrders', '.jscroll-added');
                    pix.modules.globalAccordion();

                }
            });
        }


        $j('#shippingSections > a').on('click', function() {

                // ALL ORDERS //
            if($j("#allOrdersSection").hasClass('current')) {

                if($j('#historyAllOrders > '+ nextPage).length) {
                    $j('#historyAllOrders').jscroll({
                        loadingHtml: preloader,
                        nextSelector: nextPage,
                        callback: function() {
                        appendDetach('#historyAllOrders', '.jscroll-added');
                        pix.modules.globalAccordion();
                        }
                    });
                }

                // IN PROGRESS //
            } else if($j("#progressSection").hasClass('current')) {
                if($j('#historyProgressOrders > '+ nextPage).length) {
                    $j('#historyProgressOrders').jscroll({
                        loadingHtml: preloader,
                        nextSelector: nextPage,
                        callback: function() {
                            appendDetach('#historyProgressOrders', '#historyProgressOrders');
                            pix.modules.globalAccordion();
                        }
                    });

                }

                // SHIPPED //
            } else if($j("#shippedSection").hasClass('current')) {
                if($j('#historyShippedOrders > '+ nextPage).length) {
                    $j('#historyShippedOrders').jscroll({
                        loadingHtml: preloader,
                        nextSelector: nextPage,
                        callback: function() {
                            appendDetach('#historyShippedOrders', '.jscroll-added');
                            pix.modules.globalAccordion();
                        }
                    });
                }
            }
        });

        return this;
    };

    // Author: Khalid Saleem
    // Description: horizontal scrollbar slider used in company page for CECI History
    //==================================================================================================

    pix.modules.ScrollbarSlider = function(){

        /*
        var slywidth = 0;
        $j('.slidee li').each(function() {
            var $this = $j(this);
            console.log($this.outerWidth());
            slywidth += $this.outerWidth();
        });

        $j('.slidee').css({width:slywidth+'px'});
        */


        var $jframe  = $j('#timeline');
       //var $jslidee = $jframe.children('ul').eq(0);
        var $jwrap   = $jframe.parent();

        // Call Sly on frame
        $jframe.sly({
            speed: 300,
            horizontal: 1,
            easing: 'easeOutExpo',
            pagesBar: $jwrap.find('.pages'),
            activatePageOn: 'click',
            scrollBar: $jwrap.find('.scrollbar'),
            scrollBy: 100,
            dragHandle: 1,
            clickBar: 1,
        });
    };

    // Author: Khalid Saleem
    // Description: Just another slider with different options 'trust me'.
    //==================================================================================================
    pix.modules.FullSlider = function() {
        var ParentClass = $j('.flexslider-global');
        var SlideDelay = 8000;
        var SlideTransitionSpeed = 1500;
        var SecondSlideDelay = SlideDelay - SlideTransitionSpeed;
        if (ParentClass.length > 0) {
         var animation =  function(slider, delay){
            var thisSlide = $j('.flex-control-nav').find('.flex-active');
            GetWidth = 0;
            $j('.gradient-text').css({width:'0'});
            var GetWidth = $j(thisSlide).find('.regular-text').outerWidth();
            $j(thisSlide).find('.gradient-text').animate({width:GetWidth}, delay);
        };

            var argHomeSlider = {
                width: "100%",
                animation: "slide",
                direction: "horizontal",
                directionNav: false,
                slideshow: true,
                slideshowSpeed: SlideDelay,
                animationSpeed: SlideTransitionSpeed,
                controlNav: true,
                pauseOnHover: false,
                manualControls: ".flex-control-nav li",
                start: function(slider) {
                    animation(slider, SlideDelay);
                    $j('.v-align').vAlign();
                },
                after: function(slider){
                    animation(slider,SecondSlideDelay);
                }
            };

            ParentClass.flexslider(argHomeSlider);
        }
        return this;
    };

    // Author: Stephanie
    // Description: The Events Slider in the Artistry Page
    //==================================================================================================
    pix.modules.EventSlider = function() {

        var $myFn = function() {

            var $highlight = function() { 

                if ($j('.open-slideshow').length > 0 ){
                    var $this = $j('.event-slider.open-slideshow .slides');
                  
                    //get all visible items (in this case 3 slides)
                    var items = $this.triggerHandler('currentVisible');
                    
                    // remove all .active classes
                    $this.children().removeClass('active');
                    $this.children().addClass('inactive');
                    
                    //add .active class to 2nd / centered item
                    items.filter(":eq(1)").addClass("active").removeClass("inactive");
                }
                
            }();

            var currPos = $j(this).triggerHandler('currentPosition')+1;
            // console.log(currPos);
            var slideNum = $j(this).children().length;
            var slideCount;
            if(currPos != slideNum) {
                slideCount = "<p>" + (currPos+1) + " of " + slideNum + "</p>";
                $j('.slide-count').html( slideCount );
            } else {
                slideCount = "<p>" + (currPos - (slideNum -1)) + " of " + slideNum + "</p>";
                $j('.slide-count').html( slideCount );
            }


            $j('.close-slide').on('click', function(e){
                e.preventDefault();

                var $this = $j(this).closest('.slides');
                var overlay = $this.data('overlay');
                $this.closest('.open-slideshow').removeClass('open-slideshow');
                // $j('.slider-overlay').removeClass('hide');
                $j('#'+ overlay).removeClass('hide');
                
                return $highlight;
            });

            $j('.open-slideshow .next').off('click.nextSlide');
            $j('.open-slideshow .next').on('click.nextSlide', function(){
                if (currPos === (slideNum-1)) {
                    var $this = $j(this).closest('.event-slider').find('.slides');
                    var overlay = $this.data('overlay');
                    $this.closest('.open-slideshow').removeClass('open-slideshow');
                    $j('#'+ overlay).removeClass('hide');
                }

                var parentSlide = $j(this).closest('.event-slider');
                var parentBottom = parentSlide.offset().top + parentSlide.height();
                var docViewTop = $j(window).scrollTop();
                var docViewBottom = docViewTop + $j(window).height();

                // slide to bottom of event slider if cut off when user clicks next
                if ((parentBottom) >= docViewBottom ) {
                    console.log('height: ' + parentSlide.offset().top );
                    $j('html,body').animate({
                        scrollTop: ( parentSlide.offset().top - 125 )
                    }, 500, function() {
                        console.log('scrolled to bottom of events slider');
                    });
                }
            });
        };

        $j('.global-tabs-wrapper .event-slider .slides').carouFredSel({
            width: '100%',
            height: 700,
            auto: false,
            items: {
                visible: 3,
                start: -1
            },
            scroll: {
                items: 1,
                duration: 600,
                onAfter: $myFn
            },
            prev: {
                button: function() { return $j(this).closest('.event-slider').find('.arrow-wrap .prev');}
            },
            next: {
                button: function() { return $j(this).closest('.event-slider').find('.arrow-wrap .next');}
            }
        });

    };

    // Author: Khalid Saleem
    // Description: Just another slider with different options 'trust me'.
    //==================================================================================================
    pix.modules.RegularSlider = function() {
        var ParentClass = $j('.flexslider-regular');
        if (ParentClass.length > 0) {
            var argRegularSlider = {
                animation: "slide",
                direction: "horizontal",
                animationLoop: true,
                slideshow: false,
                controlNav: false,
                directionNav: true,
                prevText: "",
                nextText: "",
                itemWidth: 290,
                itemMargin: 50,
                slideshowSpeed: 4000,
                animationSpeed: 1500,
                pauseOnHover: true
            };

            ParentClass.flexslider(argRegularSlider);

        }
    };

    // Author: Khalid Saleem
    // Description: Artistry and Philanthropy when click on each slider thumbnail opens up modal window and initiate below slider/function.
    //==================================================================================================
    pix.modules.sliderAfterModalOpens = function() {
            // remove other slider ref and reinitiate
            $j('.flexslider-lg').removeData("flexslider");
            // flexslider
            var ParentClass1 = $j('.flexslider-lg');
                var argCreativeSlider1 = {
                    animation: "slide",
                    direction: "horizontal",
                    animationLoop: false,
                    slideshow: false,
                    controlNav: false,
                    directionNav: true,
                    prevText: "",
                    nextText: "",
                    itemWidth: 1040,
                    maxItems:1,
                    minItems:1,
                    animationSpeed: 1500
                };
                ParentClass1.flexslider(argCreativeSlider1);
          
    };

    // Author: Khalid Saleem
    // Description: Just another slider with different options.
    //==================================================================================================
    pix.modules.LgSlider = function() {
        $j(document).on('click', '.timeline-link', function(){
            var slideTo = $j(this).attr('data-src');
            var ParentClass = $j('.flexslider-lg');
            if (ParentClass.length > 0) {
                var argLgSlider = {
                    startAt: slideTo,
                    animation: "slide",
                    direction: "horizontal",
                    animationLoop: false,
                    slideshow: false,
                    controlNav: false,
                    directionNav: true,
                    prevText: "",
                    nextText: "",
                    itemWidth: 1040,
                    maxItems:1,
                    minItems:1,
                    animationSpeed: 1500
                };
                ParentClass.flexslider(argLgSlider);
                if($j('.timeline-viewport').length){
                    var pos = parseInt(slideTo, 10);
                    if (pos !== $j('.flexslider-lg').data('flexslider').currentSlide) {
                        $j('.flexslider-lg').data('flexslider').flexAnimate(pos, true);
                    }
                }
            }
        });
    };

    // Author: Khalid Saleem
    // Description: On input field focus display mini calendar
    //==================================================================================================

    pix.modules.quizQuestionaire = function(){

        $j('.calendar-input').datetimepicker({
            controlType: 'select',
            timeFormat: 'hh:mm tt'
        });

        $j('.icon-calendar').click(function(){
            $j('.calendar-input').focus();
        });

    };


    // Author: Khalid Saleem
    // Description: Expandable Video Banners - using Vimeo & its froogaloop.min.js library
    //==================================================================================================
    pix.modules.vimeoVideo = function(){

        var videoFrame =$j("#bannervideo")[0];

         // Toggle Play/Stop function
        function onOffVideo() {

            if($j(vidButton).hasClass('icon-play-video') ) {

                $j(vidButton).removeClass('icon-play-video').addClass('icon-pause-video');
                $j('.lead-container').addClass('fullvidheight');
                player.api("play");
                player.api('setVolume', 1);
                //player.api('setCurrentTime', 0);
                player.api('setLoop',true);

            } else {

                $j(vidButton).removeClass('icon-pause-video').addClass('icon-play-video');
                $j('.lead-container').removeClass('fullvidheight');
                player.api('setvolume', 0);
                player.api("pause");
            }
        }

        if ($j(videoFrame).length){

            var player = window.$f(videoFrame);
            var status = $j('.status');
            var vidButton = $j(".video-controller")[0];

            // On pageload options
            player.addEvent('ready', function() {
                status.text('ready');
                player.api('play');
                player.api('setVolume', 0);
                player.api('setLoop',true);
            });

            //On click trigger the above onOffVideo function
            $j(vidButton).on('click', function(e){
                e.preventDefault();
                jQuery('html, body').animate({
                            scrollTop: jQuery(".lead-container").offset().top
                    }, 500);
                onOffVideo();
            });
        }

    };


    pix.modules.pressSlider = function(){
        
        $j('.with-thumbnails').flexslider({
            animation: "slide",
            autoplay: false,
            direction: "horizontal",
            animationLoop: true,
            slideshow: false,
            controlNav: false,
            directionNav: true,
            prevText: "",
            nextText: "",
            itemWidth: 1040,
            maxItems: 1,
            minItems: 1,
            animationSpeed: 1500,
            start: function(){}
        });
    };

    // Author: Khalid Saleem
    // Description: Global Modal Window being used throughout the entire website
    //==================================================================================================

    pix.modules.GlobalModalWindow = function() {

        $j('.open-popup-link').magnificPopup({
            type:'inline',
            removalDelay: 300,
            closeBtnInside:true,
            mainClass: 'mfp-fade',
            midClick: true,
            focus: 'select',
            closeMarkup: '<button class="mfp-close ion-android-close"></button>',
            callbacks: {

                open:function(){

                    /*
                    var slideTo = $j(this).attr('data-src');
                     pix.modules.pressSlider();
                     var slider = $j('.with-thumbnails').data('flexslider');
                     $j('.with-thumbnails').data('flexslider').vars.animationSpeed = 0;
                      slider.flexAnimate(slideTo);
                    $j('.with-thumbnails').data('flexslider').vars.animationSpeed = 1500;
                    */


                    // Initiate jScrollPane if required
                    if($j('.scroll-pane').length){
                        $j('.scroll-pane').jScrollPane({showArrows: true});
                    }

                    // Initiate vAlign if required
                    if($j('.v-align').length){
                        $j('.v-align').vAlign();
                    }

                    if($j('#pdp-zoom').length){
                        var viewportHeight = $j(window).height();
                        $j('.fullwidth-popup').css('min-height', viewportHeight+'px');
                    }

                    // When user click each slider thumbnail it opens modal window and initite below slider if required
                    if($j('.flexslider-lg').length){
                        pix.modules.sliderAfterModalOpens();
                    }

                    // Checkbox/Radiobox initite globally
                    pix.modules.checkRadio();
                    
                }
            }
        });
    };

    // Author: Khalid Saleem
    // Description: On click it slides Modal window from right to left
    //==================================================================================================

    pix.modules.GridModalWindow = function() {

        $j('.slidein-link').on('click', function(e){
            var slideID = $j(this).attr('data-src');
            e.preventDefault();
            $j('.grid-intro').fadeOut('fast');
            $j('#'+slideID).addClass('open');
        });
        
        $j('.grid .close').on('click', function(e){
            e.preventDefault();
            $j('.grid-intro').fadeIn('fast');
            $j('.grid-single-slide').removeClass('open');
        });
    };


    // Author: Khalid Saleem
    // Description: Some random functions
    //==================================================================================================

    $j.fn.vAlign = function() {
        return this.each(function() {
            var ah = $j(this).outerHeight();
            var ph = $j(this).parent().height();
            var mh = Math.ceil((ph - ah) / 2);
            $j(this).css('margin-top', mh).fadeIn(300);
        });
    };

    $j.fn.gridContentHover = function() {
        return this.each(function() {
            var gridContent = $j(this).find('.grid-content');
            var h = gridContent.height();
            gridContent.css('bottom', -h-40);
            $j(this).hover(function(){
                gridContent.animate({bottom: '0'});},
                function(){
                gridContent.animate({bottom: -h-40});
            });
        });
    };

    $j.fn.gridHover = function() {
        return this.each(function() {
            $j(this).hover(function(){
                    $j(this).addClass('hover');
                },
                function(){
                    $j(this).removeClass('hover');
                }
            );
        });
    };


    // Author: Stephanie
    // Description: Displays event calendar used in ceci bio page
    //==================================================================================================

    pix.modules.CalendarShow = function(){
        if (window.Mage.Cookies.secure) {
            return;
        }
        var now = new Date();
        var promise = $j.getJSON(
            window.Mage.Cookies.path.replace(/\/$/, '') + '/pixeventcalendar/calendar/getEventsJson',
            {
                identifier: 'appearances',
                start_date: new Date(now.getFullYear(), 1, 1).getTime(),
                end_date: new Date(now.getFullYear() + 1, 12, 31, 11, 59, 59, 999).getTime()
            }
        );
        return promise.done(function(data) {
            $j('#calendar').fullCalendar({
                defaultDate: window.moment().format('YYYY-MM-DD'),
                contentHeight: 725,
                height: 725,
                fixedWeekCount: false,
                eventLimit: true, // allow "more" link when too many events
                header: false,
                buttonText: {
                    prevYear: now.getFullYear(),
                    nextYear: now.getFullYear() + 1
                },
                columnFormat: {
                    month: 'dddd',
                    day: 'dddd'
                },
                events: data.events,
                /*[
                    {
                        title: 'Ceci speaks at The Knot Conference',
                        start: '2014-10-25T10:30:00',
                        end: '2014-10-25T12:30:00',
                        location: '130 West 23rd Street',
                        description: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                        url: 'knot-event'
                    },
                    {
                        title: 'Morgan Stanley executive womens breakfast',
                        start: '2014-10-12T10:30:00',
                        end: '2014-10-12T12:30:00',
                        location: '1585 Broadway, 27th Floor, New York, New York',
                        description: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                        url: 'pixafy-event'
                    }
                ],*/
                eventRender: function( event, element) {
                    var eventUrl = event.url;
                    var socialLinks = ('<ul class="social"><li><a id="fb" href="http://www.facebook.com/cecinewyork" target="_blank"><i class="icon-facebook"></i></a></li><li><a id="twitter" href="http://twitter.com/cecinewyork" target="_blank"><i class="icon-twitter"></i></a></li><li><a id="pinterest" href="http://pinterest.com/cecinewyork" target="_blank"><i class="icon-pinterest-circled"></i></a></li><li><a id="google" href="https://plus.google.com/+Cecinewyorkdesignfirm" target="_blank"><i class="icon-gplus"></i></a></li></ul>');
                    event.start.format('MMMM DD, YYYY');
                    element.addClass('open-popup-link');
                    element.attr('href', '#'+ eventUrl);
                    element.append('<div id="'+event.url+'" class="event-info white-popup mfp-hide"><h2>' + event.title +'</h2>' + '<div class="event-line"><label class="baron-normal">Date &amp; Time</label>' + '<p>' + event.start.format('dddd, MMMM DD, YYYY') + '</p></div>' +'<div class="event-line event-venue"><label class="baron-normal">Venue</label>' +' <p>' + event.location + '</p></div>' + '<div class="event-line"><p>' + event.description + '</p></div>' + '<div class="event-line"><label class="baron-normal">Register</label><p>' + event.register + '</p></div>' + socialLinks + '</div>');
                    element.on('click', function(e){
                        e.preventDefault();
                    });
                },
                eventClick: function(){
                    pix.modules.GlobalModalWindow();
                },
                eventAfterRender: function( event) {
                    var dateString = window.moment(event.start).format('YYYY-MM-DD');
                    $j('#calendar').find('.fc-day-number[data-date="' + dateString + '"]').addClass('fc-has-event');
                 },
                 viewRender: function(){
                    $j('#calendar-wrapper h3').text(data.calendar.name);
                    var d = $j('#calendar').fullCalendar('getDate');
                    var activeY = d.year();
                    var activeM = d.month()+1;
                    $j('.months-tab a').removeClass('active');
                    $j('.months-tab a[data-month="'+activeM+'"]').addClass('active');
                    $j('.months-tab a').off('click.changeMonth');
                    $j('.months-tab a').on('click.changeMonth', function(e){
                        e.preventDefault();
                        $j('.months-tab a').removeClass('active');
                        $j(this).addClass('active');
                        activeM = $j(this).attr('data-month');
                        var month = window.moment([activeY, activeM-1, 1]);
                        d = $j('#calendar').fullCalendar('gotoDate', month);
                        pix.modules.GlobalModalWindow();
                        //this is the code to remove sneak peek of events the in the following and past months.
                        //$j('.fc-other-month.fc-has-event').each(function(){
                        //    var tdindex=$j(this).index();
                        //    $j(this).parent().parent().parent().find('tbody').find('td').eq(tdindex).hide();
                        //});
                    });

                    $j('.year-tab li').off('click.changeYear');
                    $j('.year-tab li').on('click.changeYear', function(){
                        var currY = window.moment([2015, activeM-1, 1]);
                        var nextY = window.moment([2016, activeM-1, 1]);
                        $j('.year-tab li').removeClass('active');
                        $j(this).addClass('active');
                        if ($j(this).hasClass('left')){
                            d = $j('#calendar').fullCalendar('gotoDate', currY);
                        }
                        if ($j(this).hasClass('right')) {
                            d = $j('#calendar').fullCalendar('gotoDate', nextY);
                        }
                    });
                }
            });
            pix.modules.GlobalModalWindow();
        });
    };

    // Author: Stephanie
    // Description: Additional functionality which goes into The Event Slider
    //==================================================================================================

    pix.modules.EventSection = function() {
        $j('.global-tabs-wrapper .main-col a:not(.open-tab)').on('click', function(e){
            e.preventDefault();
            $j(this).closest('.slider-overlay').addClass('hide');
            $j(this).closest('.global-tabs-wrapper').find('.event-slider').removeClass('open-slideshow');
            var tabUrl = $j(this).attr('href');
            $j(tabUrl).addClass('open-slideshow');
            pix.modules.EventSlider();
        });

        //overlay button can link to other tabs here
        $j('.open-tab').on('click', function(e) {
            e.preventDefault();
            $j(this).closest('.slider-overlay').addClass('hide');
            $j(this).closest('.global-tabs-wrapper').find('.event-slider').removeClass('open-slideshow');
            var open = $j(this).attr('href');
            var tabOpen = $j(open).closest('.global-tabs-wrapper').children('.global-tabs').find('a[href="' + open + '"]');
            tabOpen.click();
        });
    };

    // Author: Khalid Saleem
    // Description: Top Navigation, Tabs functionality under Collections
    //==================================================================================================

    pix.modules.MenuTab = function() {

        function menuTab(){

            /**** Tabs ***/
            $j('.top-navtab li').removeClass('active');
            $j(this).addClass('active');
            $j('.navtab').removeClass('active-navtab');
            var tabUrl = $j(this).find('a').attr('data-tab');
            $j(tabUrl).addClass('active-navtab');

            /**** Indicator ****/
            var thisOffset = $j(this).position().left;
            var thisWidth = $j(this).outerWidth() / 2;
            var thisGetMargin = parseInt($j(this).css('margin-left'));
            var thisFinalOffset = thisOffset + thisGetMargin + thisWidth;
            $j(".navarrow").stop(true, true).animate({left: (thisFinalOffset - 10)+"px"});
        }

        var topNavItems = $j(".top-navtab li").hoverIntent(menuTab);

        //listen for first menu show to adjust the arrow.
        pix.$win.one('mainMenuShow', function(){
            menuTab.call(topNavItems[0]);
        });
    };

    // Author: Khalid Saleem
    // Description: When click on next button it close the current and opens the next used in PPP pages.
    //==================================================================================================

    pix.modules.nextAccordion = function() {

        $j('.next-accordion').click(function(e){
            e.preventDefault();
            $j(this).closest('li').next().find('.accordion-toggle').click();
        });
    };

    // Author: Khalid Saleem
    // Description: Fixed navigation on static pages
    //==================================================================================================
    pix.modules.SubNav = function() {
        if(window.location.hash.indexOf('access_token') !== -1){
            return;
        }
        var urlHash = window.location.hash;
        var scrollAll = '#nav a, .footer-container a, .sub-nav a, .scrollanchor';

        pix.$body.on('click', scrollAll, function(e){
            var hashClick = this.hash.substr(1);
            if($j('#'+hashClick).length) {
                e.preventDefault();
                $j('#'+hashClick).ScrollTo({offsetTop: '62px'});
            }
        });

        if(urlHash){
            $j(urlHash).ScrollTo({offsetTop: '62px'});
        }
        $j('body').scrollspy({
            offset: 64
       });

    };

    // Author: Khalid Saleem
    // Description: Global scroll to top fixed button, appears on the right when you scroll down page
    //==================================================================================================

    pix.modules.scrollToTop = function(){
        var y = $j(this).scrollTop(),
        scrollEle = $j('.scroll-top-link');
        if (y > 500) {
            scrollEle.fadeIn();
        }
        else {
            scrollEle.fadeOut();
        }
        return y;
    };

    // Author: Pat McNamara
    // Description: Global scroll down button, appears on coming soon pages and end of parallax slider
    //==================================================================================================

    pix.modules.scrollDownArrow = function(){
        var scrollDown = $j('.scroll-down-link'); 
        scrollDown.on('click', function(e){
            e.preventDefault();
            $j("html, body").animate({ scrollTop: $j(this).parent().offset().top + $j(this).parent().height() - $j('.nav-fixed').height() });
            $j(this).fadeOut();
        });

        if($j("#parallax-slider .scroll-down-link") > 0){
            scrollDown.css('position', 'fixed');
        }
    };

    // Author: James
    // Description: Color interaction when mouse over, under the top navigation world of ceci
    //==================================================================================================

    pix.modules.menuColorWheel = function(){

        $j('area').mouseenter(function() {
            var dataSrc = $j(this).attr('data-src');
            $j('.color-wheel-wrapper div').removeClass('active-wheel');
            $j('.' + dataSrc).addClass('active-wheel');
            var colorName = $j('.' + dataSrc).html();
            $j('.color-status').text(colorName);
        });


        /*
        $j('map').mouseleave(function() {
            $j('.color-wheel-wrapper div').removeClass('active-wheel');
            $j('.color-status').html('Choose Color');

        });*/


        // AJAX the slice ID to session //
        $j("map[name='colorwheel']").on( "click", "area", function() {
            var overlayName = $j(this).data('src');
            var colorWheelURL = 'pixcore/ajax/setColorwheelSettings/';

            var url = window.getUrl(colorWheelURL);

            $j.ajax({
                url: url,
                type: 'POST',
                data: { overlayName: overlayName }
            }).done(function(data) {
                if(data) {
                    $j('.'+data.overlayName).addClass('active-wheel');
                }
            });

        });


        // Find div with matching data-highlighcolorwheel id and addClass "active-wheel"
        var colorWheelURL = 'pixcore/ajax/getColorwheelSettings/';
        var url = window.getUrl(colorWheelURL);

        $j.ajax({
            url: url,
            type: 'GET'
        }).done(function(data) {
            if(data) {
                $j('.'+data.overlayName).addClass('active-wheel');
            }
        });

    };

    // Author: Stephanie
    // Description: Display sticky nav and hide if the video banner is clicked, example philanthropy page
    //==================================================================================================

    pix.modules.StickyNav = function() {

        if ($j('.sub-nav').length){
            var sticky_navigation_offset_top = $j('.sub-nav').offset().top;
        }

        function sticky_navigation() {

            var scroll_top = $j(window).scrollTop();
            if (scroll_top > sticky_navigation_offset_top) {
                $j('.sub-nav').addClass('nav-fixed');

            } else {
                $j('.sub-nav').removeClass('nav-fixed');
            }

            if($j('.fullvidheight').length){
                var vidHeight = $j('.fullvidheight').height();
                var subnavHeight = $j('.sub-nav').height();
                if(scroll_top > sticky_navigation_offset_top && scroll_top <= vidHeight + (sticky_navigation_offset_top - subnavHeight) ){
                    $j('.sub-nav').addClass('hide');
                } else if (scroll_top > sticky_navigation_offset_top && scroll_top > vidHeight + (sticky_navigation_offset_top - subnavHeight) ){
                    $j('.sub-nav').removeClass('hide');
                }
            } else {$j('.sub-nav').removeClass('hide');}
        }
        sticky_navigation();

        $j(window).scroll(function() {
            sticky_navigation();
        });
    };

    // Author: Pat McNamara
    // Description: obj will become fixed after scrolling past topStart pixels
    // obj will become absolutely positioned when scrolling past bottomObj
    //==================================================================================================

    pix.modules.PositionSticky = function() {

        var once = 1;
        var objHeight, origPos;

        function stickThis(obj, topStart, bottomObj){

            function setOnce(){
                
                if(once==1){
                    objHeight       = obj.outerHeight();
                    origPos         = obj.offset().top;
                    once = once + 1;
                }
            }

            if(obj.length){

                setOnce();
                var bottomObjPos = bottomObj.offset().top + bottomObj.outerHeight(); 
                var currentPos = obj.offset().top;

                if( $j(window).scrollTop() + objHeight > bottomObjPos - topStart){
                    obj.css({
                        'position': 'absolute',
                        'top': bottomObjPos - objHeight - origPos
                    });
                }else if( $j(window).scrollTop() > (origPos - topStart) && bottomObjPos > ( currentPos + objHeight)){
                    obj.css({
                        'position': 'fixed',
                        'top': topStart
                    });
                }else if( origPos > $j(window).scrollTop() - topStart ){
                    obj.css({
                        'position': 'absolute',
                        'top': 0
                    });
                }else if( bottomObjPos > $j(window).scrollTop() + topStart ){
                    obj.css({
                        'position': 'fixed',
                        'top': topStart
                    });
                }
            }
        }

        $j(window).scroll(function() {
            stickThis( $j('.customize-invite-left'), 150, $j('.selections-wrapper') );
            stickThis( $j('.filter-wrapper'), 50, $j('.category-products') );
        });

    };

    // Author: Khalid Saleem
    // Description: Multiple misc functionalities required throughout the entire web
    //==================================================================================================

    pix.modules.MiscFunctions = function(){

        // careers page - clicking view all internship link to open the internship tab.

        $j(".internlink").click(function(e) {
            e.preventDefault();
            var internshipTab = $j('.tab-nav').find("[data-category='careers_internships']");
            var tabintern = $j(internshipTab).find('a');
            $j(tabintern).click();

        });


        // reset forms link
        $j('.resetlink').click(function(e){
            $j('.resetform')[0].reset();
            e.preventDefault();
        });

        // Scroll to top anchor
        $j(".scroll-top-link").click(function(e) {
            $j("html, body").animate({
                scrollTop: "0"
            });
            e.preventDefault();
        });

        //Top menu image overlay, cant use css3 chrome have glitch.
        $j('.nav-btm-container a').hover(function() {
          $j('.img-overlay', this).stop().fadeOut(500);
        }, function() {
            $j('.img-overlay', this).stop().fadeIn(500);
        });

        // upload button get file url

        $j('.upload-btn .button').change(function(){
            var outPut = $j(this).val();
            $j('.upload-txt').val(outPut);
        });

        // +- Qty Increment
        $j(".auto-qty-btn").on("click", function (e) {
            e.preventDefault();
            var  btn = $j(this);
            var  inc_val = parseInt(btn.data('multi'));
            var inpt = btn.closest('.auto-qty-wrapper').find(".qty-nmbr");
            var finalVal = parseInt(inpt.val()) + inc_val;
            if (finalVal > 0 ){
                inpt.val(finalVal);
            }
            

        });

        // On Click banner slide UP/DOWN
        $j(".slideup-btn").on("click", function (e) {
            e.preventDefault();
            $j('.curated-banner').toggleClass('slide-down');
        });


        // top wishlist & my design projects drop down sign in link modal window
        $j(".sign-in").on("click", function (e) {
            e.preventDefault();
            $j('.login-modal-link').click();
        });

        // Footer "my orders" link. On click open login modal, if user is already logged in, show their orders "/sales/order/history/"
        $j(".footer-login-modal-link, .my-order-link").on("click", function (e) {
            e.preventDefault();
            if(!pix.user.isLoggedIn) {
                var timeout=setTimeout(function(){
                    $j('.login-modal-link').click();
                    $j('#login-modal-window').find('form').attr('action', $j('#login-modal-window').find('form').attr('action')+'?refer=orders');
                    clearTimeout(timeout);
                }, 150);

            }else{
                var link = $j(this).attr('href');
                window.location = link;
            }
        });

    };


    // Author: Pat McNamara & Khalid Saleem
    // Description: Provide default masonry grid and advanced masonry grid with reordering of items based on clicking "x" or "+"
    //=========================================================================================================================

    pix.modules.masonry = function(){


        $j('.masonry-selections').masonry({
            itemSelector: '.masonry-item',
            gutter: 8
        });

        pix.$body.off('.btn-close-masonry-item');
        pix.$body.on('click', '.btn-close-masonry-item', function(e) {
            e.preventDefault();
            var thisEle = $j(this);
            var masonryID = '#'+$j(thisEle).closest('.masonry-selections').attr('id');
            var moveItem = thisEle.closest('.masonry-item');
            moveItem.toggleClass('disabled');
            moveItem.detach();
            $j(masonryID).append(moveItem);
            $j(masonryID).masonry('reloadItems');
            $j(masonryID).masonry('layout');
            thisEle.removeClass('btn-close-masonry-item');
            thisEle.addClass('btn-add-masonry-item');
        });

        pix.$body.off('.btn-add-masonry-item');
        pix.$body.on('click', '.btn-add-masonry-item', function(e) {
            e.preventDefault();
            var thisEle = $j( this );
            var masonryID = '#'+$j(thisEle).closest('.masonry-selections').attr('id');
            var moveItem = thisEle.closest('.masonry-item');
            moveItem.toggleClass('disabled');
            moveItem.detach();
            $j(masonryID).prepend( moveItem );
            $j(masonryID).masonry('reloadItems');
            $j(masonryID).masonry('layout');
            thisEle.removeClass('btn-add-masonry-item');
            thisEle.addClass('btn-close-masonry-item');
        });
    };

    // Author: Khalid Saleem
    // Description: Styling global Radio and Check Boxes
    //==================================================================================================

    pix.modules.checkRadio = function(){
        $j( 'input[type="radio"], input[type="checkbox"]' ).each(function() {
            var self = $j(this);
            if (!self.parents('.ez-radio, .ez-checkbox').length) {
                $j(self).ezMark();
            }
        });
    };

    // Author: Khalid Saleem
    // Description: Scroll to ID
    //==================================================================================================
    pix.modules.ScrollLink = function() {
        $j('.scroll-link').on('click', function(e){
            e.preventDefault();
            var navUrl = $j(this).attr('href');
            $j('html, body').animate({
                scrollTop: $j(navUrl).offset().top
            }, 1000);
        });
    };

    // Author: Khalid Saleem
    // Description: Clicking on play button, expands div below and plays video. example artistry page
    //==================================================================================================
    pix.modules.expandModalWindow = function(){

        // Close function
        pix.modules.closeModal = function(){
            var expandModal = $j('.expanded-modal-window');
            var scrollTo = expandModal.closest('.section-scroll');
            expandModal.removeClass('expand-open');
                $j('html, body').animate({
                        scrollTop:$j(scrollTo).offset().top - 63
                }, {
                    duration: 500,
                    complete: function(){
                        expandModal.remove();
                    }
                });
        };

        //Click Open
        $j('.expandModal').on('click', function(e){
            e.preventDefault();

            var thisLink = $j(this);
            var getData = thisLink.attr('data-src');
            var thisSuggestion = thisLink.next('.video-suggestion');
            var afterThis = thisLink.closest('.content-next');
            var dataContainer = $j('<div class="expanded-modal-window"><div class="video-wrap"><iframe id="slideVideo" src="https://'+getData+'?autoplay=1" width="1440" height="805" frameborder="0" d="player1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><div class="video-more"></div><a href="#" class="close-expand"><i class="ion-ios-close-empty"></i></a></div>');
            
            $j('.expanded-modal-window').removeClass('expand-open').remove();
            
            if($j('.expanded-modal-window').length === 0){

                $j(afterThis).after(dataContainer);

                if (thisSuggestion !== 0){
                    $j('.video-more').append(thisSuggestion.html());
                } else {
                    $j('.expanded-modal-window').replaceWith(dataContainer.addClass('expand-open'));
                }

                setTimeout(function(){
                    $j('.expanded-modal-window').addClass('expand-open');
                        $j('html, body').animate({
                                scrollTop: $j('.expanded-modal-window').offset().top - 60
                        }, {
                        duration: 500,
                        complete:function(){
                        }
                    });
                }, 100);

            } else {

                $j('.expanded-modal-window').replaceWith(dataContainer.addClass('expand-open'));
                scrollToVideo();
            }

            //Click Close
            $j('.close-expand').on('click', function(e){
                e.preventDefault();
                pix.modules.closeModal();
            });

            //Click Suggestion
            $j('.video-more .expandModal').on('click', function(e){
                e.preventDefault();

                var thisLink = $j(this),
                    getSuggestion = thisLink.attr('data-src'),
                    iframeContent = $j('<iframe id="slideVideo" src="https://'+getSuggestion+'?autoplay=1" width="1440" height="805" frameborder="0" d="player1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'),
                    iframeVideo = $j('#slideVideo');


                iframeVideo.replaceWith(iframeContent);
                
                scrollToVideo();
            });

            function scrollToVideo() {
                $j('html, body').animate({
                        scrollTop: $j('.expanded-modal-window').offset().top - 50
                }, 500);
            }

        });
    };

    // Author: Khalid Saleem
    // Description: Just another slider
    //==================================================================================================
    pix.modules.miniSlider = function(){
        if($j('.showcase-slider').length){
            $j('.showcase-slider').carouFredSel({
                auto:false,
                items: {
                    visible:4
                },
                prev    : {
                    button  : function(){
                        return $j(this).parents('.showcase-wrapper').find('.prev-slide');
                    },
                    key     : "left"
                },
                next    : {
                    button  : function(){
                        return $j(this).parents('.showcase-wrapper').find('.next-slide');
                    },
                    key     : "right"
                }
            });
        }
    };

    // Author: Khalid Saleem
    // Description: Parallax Slider example World of Ceci Page
    //==================================================================================================
    pix.modules.parallaxSlider = function(){

        if($j('#parallax-slider').length) {
            var slideSpeed = 400;
            var slideDelay = 500;
            var opacityDelay = 300;
            var timeoutDelay = 200;
            var txtCircle = $j('.white-circle');
            var lastArrow = $j('.scroll-down-link');

            var beforeFunc = function(thisObj){
                thisObj.delay(slideDelay);
                txtCircle.animate({opacity:0}, opacityDelay);
                lastArrow.css('opacity', '0');
            };

            var afterFunc = function(){

                setTimeout(function(){
                    txtCircle.animate({opacity:1}, opacityDelay);
                }, timeoutDelay);

                setTimeout(function(){
                    lastArrow.animate({opacity:0.5}, opacityDelay);
                }, 1500);
                
            };

            $j('#parallax-slider').carouFredSel({
                direction: 'up',
                height:'100%',
                auto:false,
                prev: {
                    button  : function(){
                        return $j('.p-slide');
                    },
                    onBefore : function(){
                        beforeFunc($j(this));
                    },
                   onAfter : function(){
                        afterFunc();
                    }
                },
                next: {
                    button  : function(){
                        return $j('.n-slide');
                    },
                    onBefore : function(){
                        beforeFunc($j(this));
                    },
                   onAfter : function(){
                        afterFunc();
                    },
                },
                scroll: { items:1,duration: slideSpeed},
                onCreate: function() {
                    var viewportWidth = $j(window).width();
                    $j('.caroufredsel_wrapper').css({width: viewportWidth});

                }
            });
        }
    };


    // Author: Khalid Saleem
    // Description: Just another slider
    //==================================================================================================
    pix.modules.generalSlider = function(){

        if($j('#regular-caroufredsel-slider').length) {

            $j('#regular-caroufredsel-slider').carouFredSel({
                auto: false,
                width: "100%",
                responsive: true,
                prev: {
                    button: function(){
                        return $j(this).parents('.slide-wrapper').find('.icon-black-prev');
                    },
                    key: "left"
                },
                next: {
                    button: function(){
                        return $j(this).parents('.slide-wrapper').find('.icon-black-next');
                    },
                    key: "right"
                }
            });
        }
    };

    // Author: Pat McNamara
    // Description: Quick fade slider that appears inside of parallax slider on the corporate page
    //==================================================================================================
    pix.modules.quickFadeSlider = function(){

        if($j('#quick-fade-slider').length) {
            jQuery('#quick-fade-slider').carouFredSel({
                auto: true,
                height: 'auto',
                responsive: true,
                scroll: {
                    fx: 'crossfade',
                    items: 1,
                    duration: 600,
                }
            });
        }
    };

    // Author: Pat McNamara
    // Description: Large header slider that appears at top of home & gifts, stationery pages
    //==================================================================================================
    pix.modules.largeHeaderSlider = function(){

        if($j('#large-header-slider').length) {
            jQuery('#large-header-slider').carouFredSel({
                auto: false,
                height: 'auto',
                responsive: true,
                width: '100%',
                scroll: {
                    fx: 'crossfade',
                    items: 1,     
                    duration: 1000,
                    timeoutDuration: 4000
                },
                prev: {
                    button: function(){
                        return $j(this).parents('.slide-wrapper').find('.icon-black-prev');
                    },
                    key: "left"
                },
                next: {
                    button: function(){
                        return $j(this).parents('.slide-wrapper').find('.icon-black-next');
                    },
                    key: "right"
                }
            });
        }
    };

    // Author: Khalid Saleem
    // Description: Just another slider
    //==================================================================================================
    pix.modules.regularGridSlider = function(){

        if($j('.regulargrid-wrapper').length) {

            $j(".thumbnail-carousel").each(function() {
                var SlideContainer = $j(this).attr('id');
                $j('#'+SlideContainer).carouFredSel({
                    auto: false,
                    width: "100%",
                    circular:true,
                    items: {
                        visible:3,
                        start  : $j(".last-item")
                    },
                     scroll: {   
                        items: 1,
                        duration: 1000
                    },             
                    prev: {
                        button: function(){
                            return $j(this).parents('.regulargrid-wrapper').find('.icon-black-small-prev');
                        },
                        key: "left"
                    },
                    next: {
                        button: function(){
                            return $j(this).parents('.regulargrid-wrapper').find('.icon-black-small-next');
                        },
                        key: "right"
                    },
                    
                    onCreate: function() {
                        // Slide the carousel when viewport hit each thumbnail slider - More Fancy?
                        $j('#'+SlideContainer).one('inview', function(event, isInView, visiblePartX, visiblePartY) {
                            if (isInView) {
                            // element is now visible in the viewport
                                if (visiblePartY == 'top') {
                                    $j(this).trigger('slideTo', 0);
                                    $j(this).trigger("configuration", {
                                        circular : false,
                                        infinite : false
                                    });
                                }
                            }
                        });
                    }
                });
            });
        }
    };

    // Author: Khalid Saleem
    // Description: Filters animation in sub categories, mostly CSS3
    //==================================================================================================

    pix.modules.catFilters = function(){
        $j("body").on('click','.teaser-btn', function(e){
            e.preventDefault();
            $j('.teaser-container').toggleClass('open');
            $j('.filter-container ').toggleClass('open');

        });
    };


    // Author: Khalid Saleem
    // Description: Global Accordion, use it anywhere example product page
    //==================================================================================================

    pix.modules.globalAccordion = function(){

        function closeAccordion(){
            $j('.accordion-container').slideUp('normal');
            $j('.accordion-toggle').find('i').removeClass('ion-ios-minus-empty');
            $j('.accordion-toggle').find('i').addClass('ion-ios-plus-empty');
        }

        function openAccordion(target){
            $j(target).next().slideDown('normal', function(){
                // Masonry requires reload when it is wrapped in hidden element
                if($j('.masonry').length){
                    $j('.masonry').masonry('layout');
                }
            });

            $j(target).find('i').toggleClass('ion-ios-plus-empty ion-ios-minus-empty');
        }

        // Reset handlers for jscroll.
        $j(".accordion-toggle").unbind( "click" );

        $j('body').on("click",".accordion-toggle", function(e){
            e.preventDefault();
            var thisContainer = $j(this).next('.accordion-container');

            if($j(thisContainer).is(':visible')){
                closeAccordion();
            } else {
                closeAccordion();
                openAccordion(this);
            }
        });
        $j('.col1-layout').on("click",".avenir-heavy, #clearall", function(e){
            e.preventDefault();
                $j.get($j(this).data('url'),function(data) {
                    $j('.category-view-container').replaceWith(data.producthtml);
                    $j('.filter-wrapper').html(data.filterhtml);
                });
        });
        //tooltip icon inside above anchor to open modal window.
        $j('.icon-help-circled-alt').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
        });
    };

    // Author: Khalid Saleem
    // Description: Global select style
    //==================================================================================================
    pix.modules.jsDropdown = function(items) {
        if(!('.regular-select').length  )
            console.log('selectfired');
        pix.$body = pix.$body && pix.$body.length ? pix.$body : $j(document.body);
        var pixSelect = $j('.pix-selectable');

        if(pixSelect.length > 0){
            var eachSelectable = function() {
                var $t = $j(this);
                var selectable = $t.data('Selectable');
                if (!selectable || !pix.$body.has(selectable.getElement(true)).length)
                    $t.remove();
                else if (selectable){
                    selectable.buildList();
                    selectable.update();
                }
            };
            pixSelect.each(eachSelectable);
        }

        var selects = !!items ? items : $j('select').not('.unused, .disabled-select, .regular-select');
        if(selects.length > 0){
            var addStyling = function() {
                var el = new pix.Selectable({
                    select: this
                }).getElement();
                $j(this).before(el);
            };
            selects.each(addStyling);
        }

    };


    // Author: Khalid Saleem
    // Description: It applies to the country drop down in checkout and cart page to fix magento default functionality
    //==================================================================================================
    pix.modules.pixStateSelect = function(){
        var statecheck = function(){
            var stateInput = $j('.pix-country-select .input-text');
            if(stateInput.is(':visible')){
                $j('.pix-country-select').find('.pix-selectable').hide();
            } else {
                $j('.pix-country-select').find('.pix-selectable').show();
                // re-generate new list as per country
                pix.modules.jsDropdown();
            }
        };
        
        statecheck();

        $j('.int-shipping-select select').change(function(){
            statecheck();
        });
    };

    // Author: Khalid Saleem
    // Description: Global Login Modal Window
    //==================================================================================================
    pix.modules.LoginModalWindow = function() {

        /// Switcher Login/Register
        $j(".checkout-login-list input[name=checkout-method]").change(function() {
            var selectedEl  = $j(this).val();
            $j(".user-type").hide();
            $j("#"+selectedEl).show();
        });

        // Check if the user is not logged in
        if(!pix.user.isLoggedIn) {

            pix.$modalLink = $j('.login-modal-link');

            // Modal Window
            pix.$modalLink.magnificPopup({
                type:'inline',
                removalDelay: 300,
                closeBtnInside:true,
                mainClass: 'mfp-fade',
                midClick: true,
                focus: 'select',
                closeMarkup: '<button class="mfp-close ion-android-close"></button>',
                callbacks: {
                    elementParse: function() {
                            var showForm = $j(this.st.el).attr('data-select');
                            $j('#' + showForm).click();
                        }
                }
            });

            pix.$body.off('click.loginModal', '.login-modal-link');
            pix.$body.on('click.loginModal', '.login-modal-link', function(e){
                e.preventDefault();
                var dataSelect = this.getAttribute('data-select');
                //type = !~dataSelect.indexOf("register") ? 'login' : 'register';
                pix.$modalLink.magnificPopup("open");
                $j('#'+dataSelect).trigger('click');
            });


            if (window.location.search.indexOf("popup=1") > -1) {
                pix.$modalLink.magnificPopup('open');
            }
        } else {
            pix.$body.off('click.loginModal', '.login-modal-link');
            pix.$body.on('click.loginModal', '.login-modal-link', function(){
             //   e.preventDefault();
                var dataSelect = this.getAttribute('data-select');
                console.log(dataSelect);
                if (dataSelect == 'popupcs-logout') {
                    window.amazon.Login.logout();
                    document.cookie = "amazon_Login_accessToken=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
                }

                //type = !~dataSelect.indexOf("register") ? 'login' : 'register';
                $j('#'+dataSelect).trigger('click');
            });
        }
    };


    // Author: Khalid Saleem
    // Description: Global Tabs see artistry page
    //==================================================================================================
    pix.modules.globalTabs = function(){

        //show any tab set as '.current' on load
        $j(function(){
            //get first link not helper modal link
            var tabLink = $j('.global-tabs .current a').first().attr("href");
            $j(tabLink).css("display", "block");
        });

        $j('.global-tabs > a:not(.tabclick)').on('click', function(e){

            e.preventDefault();
            $j(this).addClass("current");
            $j(this).siblings().removeClass("current");
            $j(this).closest('.global-tabs-wrapper').find('.tab-container').css("display", "none");
            var tab = $j(this).attr("href");
            $j(tab).fadeIn();

            if ($j(tab).hasClass('event-slider-wrapper')){
                $j(tab).closest('.global-tabs-wrapper').find('.event-slider').removeClass('open-slideshow');
                $j(tab).closest('.global-tabs-wrapper').find('.event-slider .slides').carouFredSel('destroy');
                $j(tab).find('.slider-overlay').addClass('hide');
                $j(tab).find('.event-slider').first().addClass('open-slideshow');
                pix.modules.EventSlider();
            }
        });

        //only run this for tab nav slider on click
        $j(".global-tabs .tabclick").on('click', function(e){
            e.preventDefault();

            $j('.global-tabs li').removeClass("current");
            $j(this).parent().addClass("current");
            
            var tab = $j(this).attr("href");
            $j(".tab-container").not(tab).css("display", "none");
            $j(tab).fadeIn();
            
            var currentTabLeft  = $j( '.current' ).position().left;
            var currentTabRight = $j( '.current' ).position().left + $j( '.current' ).outerWidth();

            var totalWidth  = 0;
            $j( '.global-tabs .tabclick' ).siblings().each(function() {
                totalWidth = totalWidth + $j( this ).outerWidth();
            });

            var parentWidth = $j( '.global-tabs' ).parent().outerWidth();
            var btnWidth    = $j( '.scroll-left' ).outerWidth();
            var tabNum      = $j( '.global-tabs li.current' ).index() + 1;
            var tabChildren = $j( '.global-tabs' ).children().length;

            if( tabNum == tabChildren ){
                $j('.tab-scroll .scroll-right').css("display", "none");
            }else if(tabNum == 1){
                $j('.tab-scroll .scroll-left').css("display", "none");
            }else if( tabNum > 1 ){
                $j('.tab-scroll .scroll-left').css("display", "block");
            }

            if ( currentTabRight > ( parentWidth - btnWidth ) && parentWidth >= currentTabLeft ) {
                $j( '.global-tabs' ).animate({
                    left: -( currentTabLeft - btnWidth )
                });
            }
            else if ( currentTabRight < ( parentWidth - btnWidth ) ) {
                $j( '.global-tabs' ).animate({
                    left: ( 0 )
                });
            }
        });
    };


    // Author: Pat McNamara
    // Description: Add scroll functionality to global tabs with ".tab-scroll"
    //==================================================================================================

    pix.modules.TabNavigation = function () {

        $j('.tab-scroll .scroll-right').on('click', function(e){
            e.preventDefault();
            $j('.global-tabs').find('.current').next('li').children('a.tabclick').click();
            $j('.tab-scroll .scroll-left').css("display", "block");
        });

        $j('.tab-scroll .scroll-left').on('click', function(e){
            e.preventDefault();
            $j('.global-tabs').find('.current').prev('li').children('a.tabclick').click();
            $j('.tab-scroll .scroll-right').css("display", "block");
        });
    };


    // Author: Pat McNamara
    // Description: Default vertical thumbnail slider
    //==================================================================================================

    pix.modules.ThumbnailCaroufredsel = function() {
        
        //general vertical thumbnail slider
        if($j('.vertical-scroll .thumbnail-wrapper ul').length){
            
            $j('.vertical-scroll .thumbnail-wrapper ul').carouFredSel({
                direction: 'up',
                auto: false,
                prev: '.prev',
                next: '.next',
                items: {
                    visible:5
                },
                onCreate: function() {
                    $j('.vertical-scroll .thumbnail-wrapper ul a').click(function(e){
                        e.preventDefault();
                        $j( '.vertical-scroll .thumbnail-wrapper ul li' ).removeClass('selected');
                        $j( this ).parent().addClass('selected');
                    });
                }
            });

        }

        if($j('#invite-large').length){
        
            $j('#invite-large').carouFredSel({
                auto: false,
                align: 'center',
                height: 'variable',
                scroll: {
                    fx: 'directscroll',
                    onAfter: function(){
                        inviteSlide();
                    }
                },
                items: {
                    height: 'variable'
                },
                prev    : {
                    button  : function(){
                        return $j(this).parents('.slide-wrapper').find('.icon-black-prev');
                    },
                    key     : 'left'
                },
                next    : {
                    button  : function(){
                        return $j(this).parents('.slide-wrapper').find('.icon-black-next');
                    },
                    key     : 'right'
                },
                synchronise: ['#invite-thumbs', true, true, 0]//string selector, boolean inheritOptions, boolean sameDirection, number deviation
            });

        }

        if($j('#invite-thumbs').length){
            $j('#invite-thumbs').carouFredSel({
                auto: false,
                width: '100%',
                direction: 'down',
                prev: {
                    button: '#prev2'
                },
                next: {
                    button: '#next2'
                },
                items: {
                    visible: 5
                },
                scroll : {
                    items: 3,
                    fx: 'directscroll',
                    onAfter: function(){
                        inviteSlide();
                    }
                },
                onCreate: function() {
                    $j('#invite-thumbs a').click(function(e){
                        e.preventDefault();
                        $j( this ).parent().siblings().removeClass('selected');
                        $j( this ).parent().addClass('selected');
                        $j('#invite-large').trigger('slideTo', $j(this).attr("href"));
                    });
                },
                synchronise: ['#invite-large', true, true, 0]//string selector, boolean inheritOptions, boolean sameDirection, number deviation
            });
        }

        function inviteSlide(){
            $j('#invite-thumbs li').removeClass('selected');
            $j('#invite-thumbs li:nth-child(1)').addClass('selected');
        }

    };


    // Author: Khalid Saleem
    // Description: Resolving Race Condition with click to anchor page load -  call it Master Function
    //==================================================================================================
    pix.modules.masterfunc = function() {
        pix.modules.HistoryScroll();
        pix.modules.FullSlider();
        pix.modules.CalendarShow().done(function(){
            pix.modules.SubNav();
        });
    };


    // Author: Pat McNamara
    // Description: This notification function will work on any element with the class of 'notify'
    // it also requires the notification text to be placed inside a data attribute as below
    // <button class="button notify notify-left" data-notification="Your Text Here">Save</button>
    //==================================================================================================

    pix.modules.Notify = function() {

        var notifyWidth = 150;//global notify box width

        pix.$body.on('click', '.notify', function(e){
            e.preventDefault();
            addBox( $j(this) );
        });

        function addBox(notifyLink){

            var prevElement = notifyLink.prev();

            if( !prevElement.hasClass( 'notify-wrapper' ) ){

                //position box relative to the center of the element
                var clickHeight     = notifyLink.outerHeight();
                var clickWidth      = notifyLink.outerWidth();
                var notifyBox  = {
                    top:        'inherit',
                    left:       -( ( notifyWidth / 2) - ( clickWidth / 2 ) - 1 ) + 'px',
                    bottom:     '12px',
                    arrowClass: 'notify-arrow-top'
                };

                if( notifyLink.hasClass( 'notify-left' ) ){
                    notifyBox.top           = (( clickHeight - 20 ) / 2 ) + 'px';
                    notifyBox.left          = -( notifyWidth + 15 ) + 'px';
                    notifyBox.bottom        = 'inherit';
                    notifyBox.arrowClass    = 'notify-arrow-left';
                }else if( notifyLink.hasClass( 'notify-right' ) ){
                    notifyBox.top           = (( clickHeight - 20 ) / 2 ) + 'px';
                    notifyBox.left          = ( clickWidth + 15 ) + 'px';
                    notifyBox.bottom        = 'inherit';
                    notifyBox.arrowClass    = 'notify-arrow-right';
                }else if( notifyLink.hasClass( 'notify-bottom' ) ){
                    notifyBox.top           = ( clickHeight + 14 ) + 'px';
                    notifyBox.left          = ( clickWidth/2 - notifyWidth/2 ) + 'px';
                    notifyBox.bottom        = 'inherit';
                    notifyBox.arrowClass    = 'notify-arrow-bottom';
                }

                notify( notifyLink, notifyBox);

            }else{
                hoverShow(prevElement);
            }

        }

        //insert notification box before element
        function notify(obj, notifyBox) {

            obj.before('<div class="notify-wrapper ' + notifyBox.arrowClass + ' "><div class="notify-container"><div class="notify-arrow"></div><div class="notify-inner"><span class="message">' + obj.attr("data-notification") + '</span></div></div></div></div>');
            
            obj.prev().children('.notify-container').css({
                'top': notifyBox.top,
                'left': notifyBox.left,
                'bottom': notifyBox.bottom,
                'width': notifyWidth
            });
            
            hoverShow(obj.prev());
        }

        var zLevel = 999;

        function notifyMessage(obj, message) {
            
            if( !obj.prev().hasClass( 'notify-wrapper' ) ){
                addBox( obj );
            }

            var notifyText = message;
            obj.prev().children('.notify-container').find('.message').html(notifyText);
            obj.prev().removeClass('fade');
            zLevel = zLevel + 1;
            obj.prev().css('z-index', ( zLevel ) );
            hoverShow(obj.prev());
        }

        function wishlistexcuteurl(url, element){
            $j.get(url, function(data){
                if(data.status===1) {
                    if(data.content!=='') {
                        $j('#topwishlist').replaceWith(data.content);
                    }else{
                        $j('#topwishlist').find('.block-title').find('span').html('0');
                        $j('#topWishlistContent').find('.inner-wrapper').find('#wishlist-sidebar').replaceWith('<p class="cart-empty">You have no items in your wishlist.</p>');
                    }
                    window.Enterprise.TopWishlist.initialize('topWishlistContent');
                    window.Enterprise.TopWishlist.showWishlist();
                    if(element!==null) {
                        $j(element).data('url', data.link);
                        $j(element).attr('href', data.link);
                    }
                }
            });
        }

        function hoverShow(showObj){

            var hoverObj = showObj.next();
            hoverObj.mouseover(function(){
                showObj.removeClass('hidden');
            });
            hoverObj.mouseout(function(){
                showObj.addClass('hidden');
            });

        }

        // Description: Items below are for hover and click notification messages
        //==================================================================================================
        pix.$body.on('mouseover', '.link-wishlist, .link-wishlist-large', function(e){
            e.preventDefault();
            var link = $j(this);
            if (!link.hasClass('active')) {
                notifyMessage( link, 'Add to<br>My Favorites');
            }else{
                notifyMessage( link, 'Remove from<br>My Favorites');
            }
        });

        pix.$body.on('click','.minilist-link-wishlist',function(e){
            e.preventDefault();
            wishlistexcuteurl($j(this).data('url'), null);
        });

        //changes text on heart icon click
        pix.$body.on('click', '.link-wishlist:not(.login-modal-link), .link-wishlist-large:not(.login-modal-link)', function(e) {
            e.preventDefault();
            var link = $j(this);
            wishlistexcuteurl($j(this).data('url'), this);
            link.toggleClass('active');
            if (!link.hasClass('active')) {
                notifyMessage( link, 'Removed from<br>My Favorites');
            }else{
                notifyMessage( link, 'Added to<br>My Favorites');
            }
        });

        //changes hover text on plus icons
        pix.$body.on('mouseover', '.link-plus, .link-plus-large', function(e){
            e.preventDefault();
            var link = $j(this);
            if (!link.hasClass('active')) {
                notifyMessage( link, 'Add to<br>My Selections');
            }else{
                notifyMessage( link, 'Remove from<br>My Selections');
            }
        });

        //changes text on plus icon click
        pix.$body.on('click', '.link-plus, .link-plus-large', function(e) {
            e.preventDefault();
            var link = $j(this);
            link.toggleClass('active');
            if (!link.hasClass('active')) {
                notifyMessage( link, 'Removed from<br>My Selections');
            }else{
                notifyMessage( link, 'Added to<br>My Selections');
            }
            link.toggleClass('ion-android-close');
        });

    };

    // Author: Pat McNamara
    // Description: Dynamically change color picker font color based on the brightness of it's background
    //==================================================================================================

    pix.modules.colorPicker = function(){

        if($j('.color-picker').length){

            $j('.color-picker li a').each(function(){
                var el = $j(this);
                //get background in rgb then parse into array and map as numbers
                var rgb = el.css('backgroundColor').replace('rgb(', '').replace(')','' ).split(',').map(Number);
                //color brightness formula: http://www.w3.org/TR/AERT#color-contrast
                var o = Math.round(((parseInt(rgb[0]) * 299) + (parseInt(rgb[1]) * 587) + (parseInt(rgb[2]) * 114)) /1000);
                if(o > 125){//125 is the middle of the range
                    el.css('color', 'rgba(0, 0, 0, 0.6)');//dark font
                }
            });

            //add selected style class to clicked swatch
            $j('.color-picker li').on('click', function(e) {
                e.preventDefault();
                $j('.color-picker>li.selected').removeClass('selected');
                $j(this).addClass('selected');
            });

        }

    };

    // Author: John Forte
    // Description: Handles all the hiding and showing of elements on the contact us page.
    //==================================================================================================

    pix.modules.contactus = function(){

        // Author: Pat M
        // Description: Get hash value in url to open specific contact form
        if($j('.contact-us-container').length > 0){
            
            $j("select[name='contactus-template']").change(function(){
                hideshow($j(this).val());
                window.location.hash = "#"+$j(this).val();
            });

            $j(".change-contact-form").on('click', function(){
                var url = $j(this).attr('href');
                var hash = url.substring(url.indexOf("#")+1).substring(0);
                hideshow(hash);
                window.location.hash = "#"+hash;
                switchForm();
            });

            switchForm();
        }

        function hideshow(elementclass){
            $j('#contactForm').find('ul.form-list > li.'+elementclass).show();
            $j('#contactForm').find('ul.form-list > li:not(.'+elementclass+')').hide();
        }

        function switchForm (){

            var hashVal = window.location.hash.split("#")[1];

            if( $j('.'+hashVal).length > 0 ) {
                hideshow(hashVal);
                $j("option[value='"+hashVal+"']").addClass('active');

                var getActiveItem = $j("option[value='"+hashVal+"']").index();
                $j('.pix-selectable-list').children().removeClass('active');
                
                var listItem = $j('.pix-selectable-list li:eq('+getActiveItem+')');
                listItem.addClass('active');
                $j('.pix-selectable-trigger').text(listItem.html());
            }
            else{
                hideshow("sayhello");
            }
        }
    };

    window.pix = pix;

})(jQuery.noConflict(), this, this.document, this.pix, this.enterprise, this.$f);
