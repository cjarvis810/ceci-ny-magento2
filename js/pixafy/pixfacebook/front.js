;(function(window, document, undefined){
    "use strict";
    var Pixfacebook = function(){
        return this.initialize.call(this);
    };

    Pixfacebook.prototype = {
        initialize: function(){
        	
        	this.fb_class 			= "facebook_login";
        	this.fb_call_event 		= "click";
        	this.fb_scope			= "email";
        	this.fb_get_fields		= "id, email, name, picture";
        	this.fb_site_url_full	= window.fb_site_url_full;
        	this.fb_controller		= "pixfacebook/account/";
        	this.fb_login_action	= "soclogin";
        	this.redirect_input_id	= "afterlogin_redirect";
        	this.fb_error_label		= "Facebook error: ";
        	this.fb_cancel_label	= "";
        	this.fb_general_error	= "Sorry, some error happened. Please try again.";
    		this.fb_init();
        	this.bindButtons();
        },
        
        bindButtons: function(){
        	document.on(this.fb_call_event, "."+this.fb_class, function(event) {
        		FB.login(this.authFbUser.bind(this), {scope: this.fb_scope});
        	}.bind(this));
        },
        
        authFbUser: function(response) {
        	if (response.authResponse) {
            	FB.api('/me',{fields: this.fb_get_fields, scope: this.fb_scope}, this.handleFbResponce.bind(this));
        	} else{
    			if(this.fb_cancel_label)
    			{
    				alert(this.fb_cancel_label);
    			}
        	}
        },
        
        handleFbResponce: function(response) {
    		if(response.error != undefined) {
    			alert(this.fb_error_label + response.error);
    		} else{
                var elem = $$('.'+ this.fb_class);
                if(elem && elem.length)
                    fireEvent(elem[0], 'beforeAjax');

    			var facebook_redirect = "pixfacebook/account/loginredirect";	
    			var afterlogin_redirect = $(this.redirect_input_id)?$(this.redirect_input_id).value:false; 
    			new Ajax.Request(this.getAction(this.fb_login_action), {
    				method: 'POST',
    				parameters: {email: response.email, facebook_acc: response.id, name: response.name, image: response.picture.data.url, afterlogin_redirect: afterlogin_redirect, redirect_uri: facebook_redirect, display : 'touch'},
    				onSuccess: this.ajax_login_fb_success.bind(this),
    				onFailure: this.ajax_login_fb_error.bind(this)
    			});
    		}
    	},
        
        getAction: function(action){
        	return this.fb_site_url_full+this.fb_controller+action;
        },
        
        ajax_login_fb_success: function(transport){
        	var data = transport.responseText.evalJSON(true);
        	if(data.success == "true"){}
        	if(data.redirect){
                console.log(data);
        		window.location = data.redirect; 
        	}
        },
        
        ajax_login_fb_error: function(transport){
        	alert(this.fb_general_error);
            var fbButton = $$('.this.fb_class');
            if(fbButton.length)
                fireEvent($$('.this.fb_class')[0], 'afterAjax');
        },
    	fb_init: function(){
    		(function(d){

    			var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    			if (d.getElementById(id)) {return;}
    			js = d.createElement('script'); js.id = id; js.async = true;
    			js.src = "//connect.facebook.net/en_US/all.js";
    			ref.parentNode.insertBefore(js, ref);
    		}(document));



    		window.fbAsyncInit = function () {
    			FB.init({
                    appId:fb_app_id,
    				status:true,
    				cookie:true,
    				oauth:true,
    				xfbml:true,
    				next: window.fb_site_url_full
    			});
    		};
    	}
    }


    if(!!document.addEventListener){
        document.addEventListener("DOMContentLoaded", function() {
            var pix_fb = new Pixfacebook();
        });
    }
    else if(!!document.attachEvent){
        document.attachEvent("onreadystatechange", function() {
            if( !pix_fb && !!~['loaded', 'complete'].indexOf(this.readyState) )
            var pix_fb = new Pixfacebook();
        });
    }

})(this, this.document);
