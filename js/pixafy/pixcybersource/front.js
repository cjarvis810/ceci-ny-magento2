var csProfile = Class.create();
csProfile.prototype = {
	initialize: function(){
		this.select_id				= "cc_profile";
		this.list_class				= "form-list";
		this.list_item_tag			= "li";
		this.never_hide_class		= "never_hide";
		this.hide_class				= "hide_block";
		this.never_disable_class	= "never_disable";
		this.disable_class			= "disable_block";
		this.original_reqired_class	= "required-entry";
		this.disabled_reqired_class	= "required-entry_";
		
		this.init_select();
	},
	
	init_select: function(){
		this.bind_change();
		this.set_form_status();
	},
	
	bind_change: function(){
		$(this.select_id).observe("change", function(){
			this.set_form_status();
		}.bind(this));
	},
	
	set_form_status: function(){
		var value = $(this.select_id).value;
		if(value){
			this.hide_payment_form();
		}else{
			this.show_payment_form();
		}
	},
	
	hide_payment_form: function(){
		var self = this;
		$(this.select_id).up("."+this.list_class).getElementsBySelector("li").each(function(item){
			if(!item.hasClassName(self.never_hide_class)&&item.getStyle("display") != "none"){
				item.style.display = "none";
				item.addClassName(self.hide_class);
				item.getElementsBySelector("select, input").each(function(_item){
					if(_item.hasClassName(self.original_reqired_class)){
						_item.removeClassName(self.original_reqired_class);
						_item.addClassName(self.disabled_reqired_class);
					}
					_item.disabled = true;
				})
			}
		})
	},
	
	show_payment_form: function(){
		var self = this;
		$(this.select_id).up("."+this.list_class).getElementsBySelector("li").each(function(item){
			if(!item.hasClassName(self.never_hide_class)&&item.hasClassName(self.hide_class)){
				item.removeClassName(self.hide_class);
				item.style.display = "block";
				item.getElementsBySelector("select, input").each(function(_item){
					if(_item.hasClassName(self.disabled_reqired_class)){
						_item.removeClassName(self.disabled_reqired_class);
						_item.addClassName(self.original_reqired_class);
					}
					_item.disabled = false;
				})
			}
		})
	}
}