/**
 * @param  {[type]} $j        jQuery.noConflict()
 * @param  {[type]} window    instance of window
 * @param  {[type]} document  window.document
 * @param  {[type]} pix       window.pix
 * @param  {[type]} productPainter
 * Encapsulate the code in modern standards strict mode self invoking function.
 */
var productPainter = productPainter || {};
;(function($j, window, document, pix, $f, productPainter) {
    "use strict";

    pix = pix || window.pix || {}; //make sure that pix namespace is defined.
    pix.category = pix.category || {}; //add modules list

    pix.$win = pix.$win || $j(window);
    pix.$doc = pix.$doc || $j(document);
    pix.$body = !!pix.$body &&  !!pix.$body.length ? pix.$body : $j(document.body);
    pix.category.qvproductId = 0;
    pix.category.fbdata = [];


    /**************** Quickview Modal Window ***************/

    pix.category.QuickviewModalWindow = function() {
        $j('.open-popup-link-qv').magnificPopup({
            type:'inline',
            removalDelay: 300,
            mainClass: 'mfp-fade',
            midClick: true,
            focus: 'select',
            closeMarkup: '<button class="mfp-close ion-android-close"></button>',
            callbacks: {
                open:function(){

                    if(('#category-product-quickview').length){

                        var productId = $j(this).get(0).st.el.attr('data-product-id');

                        pix.category.qvproductId = productId;

                        var quickviewUrl = 'pixcatalog/product/quickview/id/'+productId,
                            loaderHtml = '<div class="quickview-preloader"><img src="'+pix.SKIN_URL+'images/preloader.gif" /></div>',
                            url = window.getUrl(quickviewUrl);

                        var promise = $j.ajax({
                            url: url,
                            type: 'GET',
                            dataType:'json'
                        });

                        $j('#category-product-quickview-li-'+productId).html(loaderHtml);
                        pix.category.QuickviewCaroufredsel();

                        promise.done(function(data) {
                            if(!!data.qv)
                            {
                                $j('#category-product-quickview-li-'+productId).html(data.qv);
                                pix.category.fbdata[productId] = data.fb;
                                pix.category.updateFbMetaTags(productId);
                                pix.category.ThumbnailCaroufredsel(productId);
                                pix.category.QuickviewAccordion(productId);
                            }
                            productPainter.setColorFromSwatch($j('#category-product-quickview-li-'+productId+' .default'));
                        });


                    }
                },
                beforeClose: function() {
                    console.log('Modal close has been initiated');
                }
            }
        });
    };

    /****Set carousel li content *****************************/

    pix.category.SetCarouselLiContent = function(e) {
        var productId = e.attr('qv-carousel-li'),
        quickviewUrl = 'pixcatalog/product/quickview/id/'+productId,
        loaderHtml = '<div class="quickview-preloader"><img src="'+pix.SKIN_URL+'images/preloader.gif" /></div>',
        url = window.getUrl(quickviewUrl);
        var promise = $j.ajax({
            url: url,
            type: 'GET',
            dataType:'json'
        });
        $j('#category-product-quickview-li-'+productId).html(loaderHtml);
        promise.done(function(data) {
            if(!!data.qv)
            {
                $j('#category-product-quickview-li-'+productId).html(data.qv);
                pix.category.fbdata[productId] = data.fb;
                pix.category.updateFbMetaTags(productId);
                pix.category.ThumbnailCaroufredsel(productId);
                pix.category.QuickviewAccordion(productId);

            }
        });
    };

    /** Thumbnail caroufredsel for each product ***/

    pix.category.ThumbnailCaroufredsel = function(productId) {
        $j('#quickview-thumbnail-image-'+productId).carouFredSel({
            direction: 'up',
            auto: false,
            // prev: '.prev',
            // next: '.next',
            prev: '#prev2-'+productId,
            next: '#next2-'+productId,
            items: {
                visible: 5,
                height: 104,
                width: 100
            },
            onCreate: function() {
                $j('.vertical-scroll .thumbnail-wrapper ul a').click(function(e){
                    e.preventDefault();
                    $j( '.vertical-scroll .thumbnail-wrapper ul li' ).removeClass('selected');
                    $j( this ).parent().addClass('selected');
                });
            }
        });
    };

    /****Category Product Quick View Caroufredsel *****************************/

    pix.category.QuickviewCaroufredsel = function(){
        var quickviewParentSlide = $j('#quickview-parent-slide');
        if (quickviewParentSlide.length > 0) {
            $j('#quickview-parent-slide').carouFredSel({
                prev: {
                    button: '#prev3',
                    onBefore: function (data) {
                        if (!$j(data.items.visible).html().length) {
                            pix.category.SetCarouselLiContent($j(data.items.visible));
                        }
                    }
                },
                next: {
                    button: '#next3',
                    onBefore: function (data) {
                        if (!$j(data.items.visible).html().length) {
                            pix.category.SetCarouselLiContent($j(data.items.visible));
                        }
                    },
                    onAfter: function () {
                        //  alert('after');
                    }
                },
                auto: false,
                items: {
                    start: "#category-product-quickview-li-" + pix.category.qvproductId,
                    visible: 1
                }
            });

            $j('.box-shadow').on('click', function (e) {
                var currentItem = $j(this).prop('rel');
                $j(".main-image-wrapper").children().fadeOut('slow').promise().done(function () {
                    var visibleItem = $j(".quickimage-" + currentItem);
                    $j(visibleItem).fadeIn("slow");
                });
                e.preventDefault();
            });
        }
    };

    pix.category.QuickviewAccordion = function(productId){

        $j('#accordion-title-'+productId).click(function(e){

            var currentClick = $j(this);
            var currentIcon = $j(currentClick).find('i');
            var accordionContainer = $j(currentClick).next();
            accordionContainer.slideToggle('medium', function(){
                if(accordionContainer.is(':visible')){
                    currentIcon.removeClass().addClass('ion-ios-minus-empty');
                } else {
                    currentIcon.removeClass().addClass('ion-ios-plus-empty');
                }
            });
            e.preventDefault();
        });
    };

    pix.category.updateFbMetaTags = function(productId){
       var source = pix.category.fbdata[productId];
       // console.log(source);
       // $j('#og_title').html(source.title);
        document.getElementById("og_title").content = source.title;
        document.getElementById("og_url").content = source.productUrl;
        document.getElementById("og_image").content = decodeURI(source.imageUrl);
        document.getElementById("og_desc").content = source.title;
    };

    window.pix = pix;
})(jQuery.noConflict(), this, this.document, this.pix, this.$f, productPainter);
