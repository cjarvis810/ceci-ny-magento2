var productPainter = productPainter || {};

(function ($) {
    var self = this;
    function onProductsLoad() {

        $('svg g.mainContainer').attr('transform', 'scale(.85) skewX(-5) skewY(5) translate(30, 40)');
        $('a.product-swatch.default').each(function () {
           self.setColorFromSwatch($(this));
        });
        
//        $('a.product-swatch.default').each(function () {
//            var $this = $(this);
//            var sku = $this.attr('data-sku');
//            var $svg = $('#svg-' + sku);
//            var colors = $this.attr('data-colors').split(',');
//            colors.forEach(function (color, index) {
//                $svg.pixPersonalizeSetColorHex(index + 1, color);
//            });
//        });
//        var $swatches = $('a.product-swatch.normal');
//        $swatches.off('mouseover').on('mouseover', function (e) {
//            var $this = $(this);
//            var sku = $this.attr('data-sku');
//            var $svg = $('#svg-' + sku);
//            var colors = $this.attr('data-colors').split(',');
//            colors.forEach(function (color, index) {
//                $svg.pixPersonalizeSetColorHex(index + 1, color);
//            });
//        });
//        $swatches.off('mouseout').on('mouseout', function (e) {
//            var $this = $(this);
//            var sku = $this.attr('data-sku');
//            var $svg = $('#svg-' + sku);
//            var $default = $('#colorway-default-' + sku);
//            var colors = $default.attr('data-colors').split(',');
//            colors.forEach(function (color, index) {
//                $svg.pixPersonalizeSetColorHex(index + 1, color);
//            });
//        });
    }
    $(document).ready(function () {
        setPreselectedColorwheelSettings();
        var preloaderImagePath =  window.getUrl('../skin/frontend/enterprise/ceciny/images/preloader.gif');

        if($('.category-products a.nextPage').length) {
            $('.category-products').jscroll({
                debug: true,
                loadingHtml: '<img src="'+preloaderImagePath+'" alt="loading" />',
                contentSelector: '.products-grid > li, .products-list > li, .quickview-slide > li, a.nextPage:last',
                nextSelector: 'a.nextPage:last',
                callback: function() {
                    $('.quickview-slide').append($('.jscroll-added li.quickview-li').detach());
                    $('.products-list, .products-grid').append($('.jscroll-added li').detach());
                    pix.category.QuickviewModalWindow();
                    setPreselectedColorwheelSettings();
                    onProductsLoad();
                    $('.v-align').vAlign();
                }
            });


            onProductsLoad();
            handleQuickView();

        }
    });

    function setPreselectedColorwheelSettings() {
        var images = $("ul li.item svg");

        // Get the colors stored in session //
        var ajaxUrl = window.getUrl("pixcore/ajax/getHexColor");

        $.ajax({
            url: ajaxUrl,
            type: 'GET'
        }).done(function(colors) {
            if(colors) {
                var colors = colors.split(",");

                var skuURL = window.getUrl("pixcore/ajax/getProductSkuSupportedByCurrentColorwheel");
                $.ajax({
                    url: skuURL,
                    type: 'GET'
                }).done(function(skus) {
                    var skuObj = $.parseJSON(skus);

                    // Loop through all images to ensure each image is being applied with supported colorway
                    images.each(function() {

                        // Get current image id/sku
                        var currentImageId =  $(this).attr('id');
                        var currentSkuParts = currentImageId.split("-");

                        if(skuObj.indexOf(currentSkuParts[1]) != -1) {

                            colors.forEach(function (color, index) {
                                $("#"+currentImageId).pixPersonalizeSetColorHex(index + 1, color);
                            });

                        }

                    });

                });
            }


        });

    }

    function handleQuickView() {
        $(document).on('mouseover', '.product-swatch', function () {
            self.setColorFromSwatch($(this));
        });
        $(document).on('mouseout', '.product-swatch', function () {
            self.setColorFromSwatch($(this).siblings('.default'));          
        });
    }


    this.setColorFromSwatch = function(swatch) {
        if(!swatch.data('colors')){
            return;
        }
        var image = swatch.closest('li').find('svg');  
        var colors = swatch.data('colors').split(',');        
        colors.forEach(function (color, index) {
            image.pixPersonalizeSetColorHex(index + 1, color);
        });
    };

}).call(productPainter,jQuery);

/**
 * Created by jcymerman on 3/27/2015.
 */
