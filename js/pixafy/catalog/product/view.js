/**
 * Created by jcymerman on 3/27/2015.
 */

(function ($) {

    $(document).ready(function () {
        $('img[src$=".svg"]').pixPersonalizeInitSvg();


        /*
            Listen for click event and increase by multiples of 5, if number is not even, round up to to next 5
            (PDP - wedding)
          */
        $( "#printQuantityIncrease" ).click(function() {
            var increaseQuantityAmount = parseInt($('input#printQuantity').val());

            if(increaseQuantityAmount % 5 != 0) {
                $('input#printQuantity').val( Math.ceil(increaseQuantityAmount/5)*5 );
            } else {
                $('input#printQuantity').val(increaseQuantityAmount + 5);
            }

        });

        /*
            Listen for click event and decrease by multiples of 5, if number is not even, round down to next 5.
            (PDP - wedding)
          */
        $( "#printQuantityDecrease" ).click(function() {
            var decreasedQuantityAmount = parseInt($('input#printQuantity').val());

            if( decreasedQuantityAmount > 5) {

                if(decreasedQuantityAmount % 5 != 0) {
                    $('input#printQuantity').val( Math.floor(decreasedQuantityAmount/5)*5 );
                } else {
                    $('input#printQuantity').val(decreasedQuantityAmount - 5);
                }

            }

        });
    });
})(jQuery.noConflict());
