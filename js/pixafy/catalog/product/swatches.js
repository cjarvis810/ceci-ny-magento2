// Author:      James Jones
// Description: Used to handle all swatch logic such as swapping images and more!
(function ($) {

    $(document).on('click', '.product-thumb', function() {

        $("#errorMessage").empty();
        
        // Set color in add to cart button //
        if($(this).data('attributeid')) {
            $('.hiddenColorAttribute').val($(this).data('attributeid'));
        } else {

            $('.hiddenColorAttribute').removeAttr('value');

        }

        // Ensure the price is greater than 0 before setting it
        if($(this).data('attrprice')) {

            $('.productPrice').html($(this).data('attrprice'));

        } else {

            $('.productPrice').empty();

        }


        if($(this).data('issaleable') || !$(this).data('attributeid')) {
            $("#product-addtocart-button span span").text('Add to Cart');
            $("#product-addtocart-button").attr('title', 'Add to Cart');
            $("#product-addtocart-button").removeClass('sold-out');
            $("#product-addtocart-button").removeAttr('disabled');


        } else if(!$(this).data('issaleable') && $(this).data('attributeid')) {

            $("#product-addtocart-button span span").text('Sold Out');
            $("#product-addtocart-button").attr('title', 'Sold Out');
            $("#product-addtocart-button").addClass('sold-out');
            $("#product-addtocart-button").attr('disabled', 'disabled');
            
        }


        // Set the attribute image url
        $('#quick-image').attr('src', $(this).data('attrimageurl'));

        // deselect all others
        $('a[selected="selected"]').removeAttr('selected');

        // Make this product image active
        $(this).attr('selected', 'selected');

    });

    // Author:      James Jones
    // Description: Used to swap the images for a configurable product with different simple colored products
    function swapQuickViewSwatchImages() {

        $(document).on('mouseout', '.product-swatch', function() {
            console.log("QuickView mouse out");
            var productSku = $(this).data('productsku');

            $("#"+productSku+"-image").attr("src", $("#"+productSku+"-image").attr('configimage'));
            $('#quick-image').attr("src", $('a[selected="selected"]').data('attrimageurl'));


        });

        $(document).on('mouseover', '.product-swatch', function() {

            if (!$(this).hasClass("rainbow")) {

                // PDP/QuickView page //
                if($('#quick-image').length) {

                    $('#quick-image').attr('src', $(this).data('attrimageurl'));
                   // $('.productPrice').html($(this).data('attrprice'));

                } else {
                    // List/search view //
                    // Set the old image so we can switch back if needed
                    $('#'+$(this).data('productsku')+"-image").attr('configimage', $('#'+$(this).data('productsku')+"-image").attr('src'));

                    // apply the new image
                    $('#'+$(this).data('productsku')+"-image").attr('src', $(this).data('attrimageurl'));


                }

                // Update the hidden field to the value of the attribute id
                // $('.hiddenColorAttribute').val($(this).data('attributeid'));

            } else {

                $(this).closest('.item').find('.rainbow-overlay').fadeTo('fast', 1);

            }

        });


        $(document).on('mouseout', '.rainbow', function() {
            $(this).closest('.item').find('.rainbow-overlay').fadeTo('fast', 0);
        });

    }

    swapQuickViewSwatchImages();

})(jQuery.noConflict());