/**
 * Created by jcymerman on 3/25/2015.
 * jQuery functions for personalizing SVGs
 */

(function ($) {
    $.fn.lineinputs=function(){
        var inputs=$("svg").find("text").length;
        $("#lineinputs").html("");
        for(var i=1; i<inputs; i++){
            $('<li class="label">Line '+(i)+'</li><li class="data"><input type="text" data-sku="test" data-text="'+i+'" class="personalization-text" value="'+$("svg").find(".text-"+i).text()+'" />').appendTo("#lineinputs");
        }
    };
    $.fn.pixPersonalizeSetColorHex = function (colorIndex, hex) {
        var r = parseInt(hex.substring(0, 2), 16);
        var g = parseInt(hex.substring(2, 4), 16);
        var b = parseInt(hex.substring(4, 6), 16);
        var rPct = parseFloat(r) / 255;
        var gPct = parseFloat(g) / 255;
        var bPct = parseFloat(b) / 255;
        this.pixPersonalizeSetColorPct(colorIndex, rPct, gPct, bPct);
        return this;
    };

    $.fn.pixPersonalizeSetColorPct = function(colorIndex, rPct, gPct, bPct) {
        this.find('.color-' + colorIndex).attr('values',
            '0 0 0 0 ' + rPct + '\n' +
            '0 0 0 0 ' + gPct + '\n' +
            '0 0 0 0 ' + bPct + '\n' +
            '0 0 0 1 0'
        );
        return this;
    };

    $.fn.pixPersonalizeSetText = function(textIndex, str) {
        //var $svg = this;

        var $text = $(".text-" + textIndex).html( str );
        //var $text = this.find('.text-');

        if ($text.length != 1) return this;

        //$text.html(str);
        if ($text[0].toString() == '[object SVGTSpanElement]') {
            var $gp = $text.parent().parent();
            $text.parent().detach().appendTo($gp);
        }

        // If shrink-fit class is found resize text
        if($text.hasClass('shrink-fit')) {
            return this.pixPersonalizeResizeText($text);

        } else {
            // Otherwise put a limit the size of the box container
            return this.pixPersonalizeLimitText($text);

        }
    };

    $.fn.pixPersonalizeLimitText = function ($elements) {
        var $svg = this;
        $elements.each(function () {
            var $child = $(this);
            var classes = $child.attr('class').split(' ');
            $.each(classes, function (index, className) {
                var matches = className.match(/^group-(\d+)$/);
                if (matches) {
                    var curWidth = 0;
                    var groupIndex = parseInt(matches[1]);
                    var maxWidth = $svg.attr('data-group-' + groupIndex + '-max-width');
                    if (!maxWidth) return;
                    var $groupMembers = $child;
                    if ($svg.attr('data-group-' + groupIndex + '-share-size') == 'yes') {
                        $groupMembers = $svg.find('text.' + className);
                        $groupMembers.each(function() {
                            var bbox = this.getBBox();
                            if (bbox.width > curWidth) {
                                curWidth = bbox.width;
                            }
                        });
                    } else {
                        var bbox = $child[0].getBBox();
                        curWidth = bbox.width;
                    }
                    if (curWidth > maxWidth) {
                        //var resize = maxWidth / curWidth;
                        //$groupMembers.attr('transform', 'scale(' + resize + ')');
                        var textInput = $child.attr('class').match(/text-(\d+)/)[1];
                        $("input[data-text='"+textInput+"'").attr('maxlength', $child[0].textContent.length);
                        $("input[data-text='"+textInput+"'").attr('title', "Limit reached");

                        var tooltips = $( "input[data-text='"+textInput+"'" ).tooltip({
                            position: {
                                my: "left top",
                                at: "right+5 top-5"
                            },
                            open: function(event, ui) {

                                console.log("event: ", event);

                            }
                        });

                        tooltips.tooltip( "open" );
                        //alert("Limit reached");

                    }

                }
            });
        });
        return this;
    };

    $.fn.pixPersonalizeResizeText = function ($elements) {
        var $svg = this;
        $elements.each(function () {
            var $child = $(this);
            var classes = $child.attr('class').split(' ');
            var curWidth = 0;
            $.each(classes, function (index, className) {
                var matches = className.match(/^group-(\d+)$/);
                if (matches) {
                    var curWidth = 0;
                    var groupIndex = parseInt(matches[1]);
                    var maxWidth = $svg.attr('data-group-' + groupIndex + '-max-width');
                    if (!maxWidth) return;
                    var $groupMembers = $child;
                    if ($svg.attr('data-group-' + groupIndex + '-share-size') == 'yes') {
                        $groupMembers = $svg.find('text.' + className);
                        $groupMembers.each(function() {
                            var bbox = this.getBBox();
                            if (bbox.width > curWidth) {
                                curWidth = bbox.width;
                            }
                        });
                    } else {
                        var bbox = $child[0].getBBox();
                        curWidth = bbox.width;
                    }
                    if (curWidth > maxWidth) {
                        var resize = maxWidth / curWidth;
                        $groupMembers.attr('transform', 'scale(' + resize + ')');
                    } else {
                        $groupMembers.attr('transform', '');
                    }
                }
            });
        });
        return this;
    };

    $.fn.pixPersonalizeSetFont = function(fontIndex, fontFamily) {
        var $elements = this.find('.font-' + fontIndex);
        $elements.attr('font-family', fontFamily);
        this.pixPersonalizeResizeText($elements);
        return this;
    };

    $.fn.pixPersonalizeInitSvg = function(callback) {
        this.each(function () {
            var $this = $(this);
            $.get(
                this.src,
                function (data) {
                    $this.replaceWith(data);
                    if (!!callback) {
                        callback();
                    }
                    $this.lineinputs();
                },
                'html'
            );
        });
        return this;
    };

})(jQuery.noConflict());