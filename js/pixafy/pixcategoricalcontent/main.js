"use strict";

/**
 * Add categorical content to pix.modules, making sure we're instantiated on the way * 
 */
window.pix = window.pix || {};
window.pix.modules = window.pix.modules || {};
window.pix.modules.CategoricalContent = window.pix.modules.CategoricalContent || {};

/**
 * categorical content
 * 
 * The way this works is the following. On initial page load, we'll aleady have the content 
 * for one page. Everytime someone goes to another page, or changes category, the content
 * for that page will be loaded in. 
 * 
 */
(function ($, document, modules) {

    /**
     * Aliasing ourselves for using our public methods inside jquery
     * @type main_L13
     */
    var that = this;

    /**
     * Jquery document 
     * @type @call;$
     */
    var document = $(document); //we'll be using this one alot 

    /**
     * When ready initialize this module
     */
    document.ready(function () {
        that.initialize();
    });

    /**
     * Initialize our event handlers
     * @returns {undefined}
     */
    this.initialize = function () {
        registerTabHandler();
        registerPaginationHandler();
        registerArchiveHandler();
        registerThumbClickHandler();
        registerNextButtonHandler();
        registerMiniCarouselClick();
        initNextButton();
    };

    /**
     * When clicking on a tab, load that tab
     * @returns {undefined}
     */
    function registerTabHandler() {
        /**
         * What to click to switch tabs
         * @type String
         */
        var tabNavigation = '.tab-nav li';
        document.on('click', tabNavigation, function (event) {
            $(tabNavigation).removeClass('active');
            $(this).addClass('active');
            loadTab($(this).data('category'), null, 'ALL');
        });
    }

    /**
     * When choosing a different page, load it in
     * @returns {undefined}
     */
    function registerPaginationHandler() {
        /**
         * The dropdown to change pages
         * @type String
         */
        var categoryPagerDropdown = "select.cat-pager";
        document.on('change', categoryPagerDropdown, function () {
            scrollTopOfDiv();
            var archive = $(this).parent('.cat-pager-container').children('select.archive-dropdown').val();
            loadTab($(this).data('category'), $(this).val(), archive);
        });
    }

    /**
     * Register the handler for clicking next
     * @returns {undefined}
     */
    function registerNextButtonHandler() {
        document.on('click', '.next-page-button', function (event) {
            event.preventDefault();
            scrollTopOfDiv();
            var dropdown = $(this).parent('.cat-pager-container').children('select.cat-pager');
            dropdown.val(parseInt(dropdown.val()) + 1);
            dropdown.trigger('change');
        });
    }

    /**
     * Initialize next buttons
     * @returns {undefined}
     */
    function initNextButton() {
        //this might not be the best way to do it but it's what I thought of    
        $('.next-un-initialized').each(function () {
            var dropdown = $(this).parent('.cat-pager-container').children('select.cat-pager');
            var nextButton = $(this);
            if (parseInt(dropdown.val()) < parseInt(nextButton.data('total-pages'))) {
                nextButton.removeClass('hidden');
            }
            nextButton.removeClass('next-un-initialized');
            dropdown.change(function () {
                if (parseInt(dropdown.val()) < parseInt(nextButton.data('total-pages'))) {
                    nextButton.removeClass('hidden');
                } else if (!nextButton.hasClass('hidden')) {
                    nextButton.addClass('hidden');
                }
            });
        });
    }

    /**
     * The dropdown for switching archives 
     * @returns {undefined}
     */
    function registerArchiveHandler() {
        var archiveDropdown = "select.archive-dropdown";
        var lastValue = null; //previous value that we'll switch back to    

        //on focus capture value so we can swicthc back
        document.on('focus', archiveDropdown, function () {
            lastValue = $(this).val();
        });

        //on change load the next tab
        document.on('change', archiveDropdown, function () {
            scrollTopOfDiv();
            var newValue = $(this).val();
            if (lastValue) {
                $(this).val(lastValue); //switch back to old page for when this is returned to
            }
            loadTab($(this).data('category'), null, newValue);
        });
    }

    /**
     * Scroll to top of dive
     * @returns {undefined}
     */
    function scrollTopOfDiv() {
        var scrolltopNav = jQuery('.tabs-nav').closest('.section-scroll').attr('id');
        jQuery('#'+scrolltopNav).ScrollTo({offsetTop: '62px'});
    }

    /**
     * On thumb click open the flex slider
     * @returns {undefined}
     */
    function registerThumbClickHandler() {
        document.on('click', '.cat-content-link', function () {

            var slideTo = parseInt($(this).attr('data-src'));
            var ParentClass = $('.flexslider-lg');
            if (ParentClass.length > 0) {
                var argLgSlider = {
                    animation: "slide",
                    direction: "horizontal",
                    animationLoop: true,
                    slideshow: false,
                    controlNav: false,
                    directionNav: true,
                    prevText: "",
                    nextText: "",
                    itemWidth: 1040,
                    maxItems: 1,
                    minItems: 1,
                    animationSpeed: 1500,
                    start: function(){
                        thumbsCarousel();
                    }
                };
                ParentClass.flexslider(argLgSlider);
                //Onclick jump to specific slide
                var slider = $('.flexslider-lg').data('flexslider');
                var animationSpeed = slider.vars.animationSpeed;    //save animation speed to reset later
                slider.vars.animationSpeed = 0;
                slider.flexAnimate(slideTo);                  //position index for desired slide goes here
                slider.vars.animationSpeed = animationSpeed;
            }
        });
    }

    /**
     * 
     * @returns {undefined}
     */
    function registerMiniCarouselClick() {
        document.on('click', '.miniCarouselThumb', function (e) {
            console.log('sdfsdf');
            e.preventDefault();
            var img = $(this);
            var double = img.data('double');
            var src = img.attr('src');
            var parentContainer = img.closest('.press-wrapper');
            $(this).parent().append('<div class="gold-border"></div>');
            parentContainer.children('.img-container').removeClass('hidden');
            img.parents('.thumbnails-wrapper').find('.thumbnails').removeClass('activated');
            img.parents('.thumbnails').addClass('activated');
            if (double) {
                parentContainer.children('.double-container').find('img').attr('src', src);
                parentContainer.children('.single-container').addClass('hidden');
            } else {
                parentContainer.children('.single-container').find('img').attr('src', src);
                parentContainer.children('.double-container').addClass('hidden');
            }
        });
    }

    /**
     * Loads the extra images in
     * @returns {unresolved}
     */
    function thumbsCarousel() {

        $('.press-thumbs').carouFredSel({
        auto:false,
        align: 'center',
        infinite: false,
        circular: false,
        scroll: 1,
        onCreate: function(){
            $( ".press-thumbs div:nth-child(2) .miniCarouselThumb" ).click();
        },
            prev: {
                button: function () {
                    return $(this).parents('.thumbnails-wrapper').find('.prev-slide');
                },
                key: "left"
            },
            next: {
                button: function () {
                    return $(this).parents('.thumbnails-wrapper').find('.next-slide');
                },
                key: "right"
            }
        });

    }


    /**
     * Query Server and load tab
     * @param {type} category
     * @returns {undefined}
     */
    function loadTab(category, page, archive) {
        /**
         * Container just for tabs
         * @type String
         */
        var tabContainer = "#tabs-container";
        /**
         * Container just for pager
         * @type String
         */
        var pagerContainer = "#cat-pagers";

        /**
         * 
         * @type String|String
         */
        if (typeof archive === 'undefined') {
            archive = 'ALL';
        }

        /**
         * The pager we'll switch to 
         * @type String
         */
        var thePager = '#' + category + "-pager-" + archive;

        /**
         * If paged not passed in, figure it out 
         */
        if (!page) {
            var pagerSelectBox = $(thePager + " select.cat-pager");
            if (pagerSelectBox.length > 0) {
                page = pagerSelectBox.val();
            } else {
                page = 1;
            }
        }
        /**
         * The tab we'll be switching to
         * @type String
         */
        var theTab = '#' + category + '-tab-' + page + '-' + archive;

        if ($(theTab).length > 0) {
            switchTab(theTab);
            switchPager(thePager);
        } else {
            tabServer(category, page, archive, function (data) {
                $(tabContainer).append(data.content);
                $(pagerContainer).append(data.pager);
                initNextButton();
                switchTab(theTab);
                switchPager(thePager);
                modules.GlobalModalWindow();
            });
        }
    }

    /**
     * Switch tab
     * @param {type} id
     * @returns {undefined}
     */
    function switchTab(id) {
        $('.tab').removeClass('active-tab');
        $(id).addClass('active-tab');
    }

    /**
     * Switch the pager
     * @param {type} id
     * @returns {undefined}
     */
    function switchPager(id) {
        $('.cat-pager-container').addClass('hidden');
        $(id).removeClass("hidden");
    }


    /**
     * Our tab server
     * @param {type} category
     * @param {type} page
     * @param {type} success
     * @returns {undefined}
     */
    function tabServer(category, page, archive, success) {
        /**
         * The main container for evertything
         * @type String
         */
        var container = $("#category-tabs-container").data('container');
        $.ajax({
            beforeSend: function () {
                $('.tab').removeClass('active-tab');
                $('#loading-tab').addClass('active-tab');
            },
            url: 'pixcategoricalcontent/categorytabs',
            type: 'GET',
            data: {
                'container': container,
                'category': category,
                'p': page || 1,
                'y': archive || 'ALL'
            },
            dataType: 'json',
            'success': success
        });
    }

}).call(window.pix.modules.CategoricalContent, jQuery, document, window.pix.modules);



