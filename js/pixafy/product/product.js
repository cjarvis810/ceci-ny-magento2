/**
 * @param  {[type]} $j        jQuery.noConflict()
 * @param  {[type]} window    instance of window
 * @param  {[type]} document  window.document
 * @param  {[type]} pix       window.pix
 * @param  {[type]} undefined
 * Encapsulate the code in modern standards strict mode self invoking function.
 */
;(function($j, window, document, pix, $f, undefined) {
    "use strict";

    pix = pix || window.pix || {}; //make sure that pix namespace is defined.
    pix.productview = pix.productview || {}; //add modules list

    pix.$win = pix.$win || $j(window);
    pix.$doc = pix.$doc || $j(document);
    pix.$body = !!pix.$body &&  !!pix.$body.length ? pix.$body : $j(document.body);

    /**************** Thumbnail Carousel ***************/

    pix.productview.ThumbnailCaroufredsel = function() {
        $j('#quickview-thumbnail-image').carouFredSel({
            direction: 'up',
            auto:false,
            prev: '#prev2',
            next: '#next2',
            items: {
                visible:5
            }
        });
        $j('.product-thumb').click(function(){
            $j('#pdp-zoom').find('img').attr('src',$j(this).data('attrimageurl'));
        });
    };

    /******************* More Designs Slider ***********************/

    /*************** List Item Slider *****************/

    pix.productview.miniSlider = function(){
        $j('.list-item-design-slider').carouFredSel({
            auto:false,
            infinite : false,
            circular: false,
            items: {
                visible:4
            },
            prev    : {
                button  : function(){
                    return $j(this).parents('.list-item-design-slider-wrapper').find('.prev-slide');
                },
                key     : "left"
            },
            next    : {
                button  : function(){
                    return $j(this).parents('.list-item-design-slider-wrapper').find('.next-slide');
                },
                key     : "right"
            }
        });
    };

    /**************** Quote Slider *************************/

    pix.productview.quoteSlider = function(){
        $j('.quote-slider').carouFredSel({
            auto:false,
            items: {
                visible:1
            },
            prev    : {
                button  : function(){
                    return $j(this).parents('.black-box-container').find('.prev-slide');
                },
                key     : "left"
            },
            next    : {
                button  : function(){
                    return $j(this).parents('.black-box-container').find('.next-slide');
                },
                key     : "right"
            }
        });
    };
    window.pix = pix;
})(jQuery.noConflict(), this, this.document, this.pix, this.$f);
