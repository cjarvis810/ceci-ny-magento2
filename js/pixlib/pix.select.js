/*
 * @PixSelectable - Custom Select Elements
 * Authors: Joram Kwak
 * Date: Aug 1st, 2014
 * Description: A jQuery custom select plugin.
 */

;(function( $, win, doc, undefined ){
	window.pix = window.pix || {};

	var domHelper = {
		getDom: function(item, parent){
			if(typeof item == 'string') {
				if(parent)
					return parent.find(item);
				else
					return $(item);
			} else if(item instanceof jQuery)
				return item;
			else
				return $(item);
		}
	};

	window.pix.Selectable = (function(domHelper){
        var ACTIVE = 'active';
        var UNUSED = 'unused';
        var FOCUS = 'focus';

        var STATE_CLOSED = 1;
        var STATE_OPEN = 2;

        var KEYCODE_ENTER = 13;
        var KEYCODE_SPACE = 32;
        var KEYCODE_ESC = 27;
        var KEYCODE_UP = 38;
        var KEYCODE_DOWN = 40;
        var KEYCODE_LEFT = 37;
        var KEYCODE_RIGHT = 39;
        var KEYCODE_TAB = 9;

        function Selectable(options){
            this._dom = {
                base: null,
                list: null,
                items: null,
                select: null,
                selectOptions: null,
                trigger: null
            };
            this._data = {
                initialized: false,
                state: STATE_CLOSED,
                selecting: null,
                selected: null,
                changed: {
                    state: false,
                    selecting: false,
                    selected: false
                },
                documentEventHandler: null,
                documentEventHandlerAttached: false
            };

            this.init(options);
        }
        Selectable.prototype = {
            init: function(options){
                if(this._data.initialized)
                    return this;

                // Check to see if the multiple class is added if so treat it as multiselect option
                if(options.select.className != 'multiple') {
                    var opt = $.extend({
                        select: null,
                        cssClass: 'pix-selectable'
                    }, options);

                    if(!opt.select)
                        return this;

                    this._data.cssClass = opt.cssClass;

                    this._dom.select = domHelper.getDom(opt.select);
                    this.build();
                    this.attachEvents();

                    this._data.initialized = true;

                    this._dom.base.data('Selectable', this);
                    return this;
                }

            },
            kill: function(){
                this.detachEvents();
                this.destroy();
                this._data.initialized = false;

                return this;
            },
            isInitialized: function(){
                return this._data.initialized;
            },
            build: function(){
                this.destroy();

                this._dom.select.addClass(UNUSED);
                this._dom.base = $('<div tabindex="0"></div>').addClass(this._data.cssClass);
                this._dom.trigger = $('<a></a>').addClass(this._data.cssClass+'-trigger').appendTo(this._dom.base);
                this._dom.list = $('<ul></ul>').addClass(this._data.cssClass+'-list').appendTo(this._dom.base);

                this.buildList();
                return this;
            },
            destroy: function(){
                this._dom.select.removeClass(UNUSED);

                if(this._dom.base)
                    this._dom.base.remove();

                this._dom.base = null;
                this._dom.list = null;
                this._dom.items = null;
                return this;
            },
            buildList: function(){
                if(this._dom.items)
                    this._dom.items.remove();

                var that = this;
                var items;

                this._dom.selectOptions = this._dom.select.find('option').each(function(){
                    var el = $(this);
                    var item = $('<li></li>').addClass(that._data.cssClass+'-item').data('option', this);
                    item.html(el.html());
                    item.data('value', el.val());
                    item.appendTo(that._dom.list);
                    if(items)
                        items = items.add(item);
                    else
                        items = item;
                });

                this._dom.items = items;
                this.update();

                return this;
            },
            update: function(){
                var selected = this._dom.selectOptions.filter(':checked');
                if(selected.length) {
                    this._data.selected = this._dom.items.filter(function(){
                        return ($(this).data('option') == selected[0])
                    });
                    this._data.changed.selected = true;
                    this.render();
                }
                return this;
            },
            openSelect: function(){
                this._data.state = STATE_OPEN;
                this._data.changed.state = true;

                this.render()
                    .gotoSelecting();

                this.attachDynamicEvents();
                return this;
            },
            closeSelect: function(){
                this._data.state = STATE_CLOSED;
                this._data.selecting = this._data.selected;
                this.setSelection();

                this._data.changed.state = true;
                this.render();

                this.detachDynamicEvents();
                return this;
            },
            toggleSelect: function(){
                if(this._data.state == STATE_CLOSED)
                    this.openSelect();
                else
                    this.closeSelect();

                return this;
            },
            setSelection: function(item){
                item = this._dom.items.filter(item);
                if(item.length) {
                    this._data.selecting = item;
                    this._data.changed.selecting = true;
                    return this.render();
                }
                return this;
            },
            setSelectionFromChar: function(char){
                var selection = (this._data.selecting) ? this._data.selecting : this._data.selected;
                var index = selection.index();
                var newSelection;

                char = char.toLowerCase();
                for(var i = index + 1, len = this._dom.items.length; i < len; i++){
                    if(this._dom.items.eq(i).text().substr(0, 1).toLowerCase() == char) {
                        newSelection = this._dom.items.eq(i);
                        break;
                    }
                }
                if(!newSelection) {
                    for(var i = 0, len = index; i < len; i++) {
                        if(this._dom.items.eq(i).text().substr(0, 1).toLowerCase() == char) {
                            newSelection = this._dom.items.eq(i);
                            break;
                        }
                    }
                }
                if(newSelection)
                    this.setSelection(newSelection);

                return this;
            },
            selectItem: function(item){
                if(item && this._data.selected && item !== this._data.selected[0]){
                    item = this._dom.items.filter(item);
                } else if(this._data.selecting) {
                    item = this._data.selecting;
                }

                if(item && item.length) {
                    this._dom.select.val($(item.data('option')).val());
                    this.triggerChange();
                }

                this.closeSelect();
                return this;
            },
            triggerChange: function(){
                var evt;
                if (document.createEvent) {
                    // dispatch for all browsers except IE before version 9
                    evt = document.createEvent("HTMLEvents");
                    evt.initEvent('change', true, true ); // event type, bubbling, cancelable
                    this._dom.select[0].dispatchEvent(evt);
                } else {
                    // dispatch for IE before version 9
                    evt = document.createEventObject();
                    this._dom.select[0].fireEvent('onchange', evt);
                }

                return this;
            },
            focus: function(){
                this._dom.base.addClass(FOCUS);
            },
            blur: function(){
                if(this._data.state == STATE_CLOSED)
                    this._dom.base.removeClass(FOCUS);
            },
            render: function(/*all*/){
                if(arguments[0] || this._data.changed.state) {
                    if(this._data.state == STATE_CLOSED)
                        this._dom.base.removeClass(ACTIVE);
                    else if(this._data.state == STATE_OPEN)
                        this._dom.base.addClass(ACTIVE);

                    this._data.changed.state = false;
                }
                if(arguments[0] || this._data.changed.selecting) {
                    if(this._data.selecting) {
                        this._dom.items.removeClass(ACTIVE);
                        this._data.selecting.addClass(ACTIVE);
                    }
                    this._data.changed.selection = false;
                }
                if(arguments[0] || this._data.changed.selected) {
                    this._dom.items.removeClass(ACTIVE);
                    this._data.selected.addClass(ACTIVE);

                    this._dom.trigger.html(this._data.selected.html());
                    this._data.changed.selected = false;
                }

                return this;
            },
            gotoSelecting: function(){
                if(this._data.state == STATE_OPEN) {
                    var selection = (this._data.selecting) ? this._data.selecting : this._data.selected;
                    var selectionTop = selection.position().top;
                    var selectionBottom = selectionTop + selection.outerHeight();
                    //var listScrollTop = this._dom.list.scrollTop();
                    var listBottom = this._dom.list.height();

                    if(selectionTop < 0) {
                        this._dom.list.scrollTop(this._dom.list.scrollTop() + selectionTop);
                    } else if(selectionBottom > listBottom){
                        this._dom.list.scrollTop(this._dom.list.scrollTop() + (selectionBottom - listBottom));
                    }
                }

                return this;
            },
            attachEvents: function(){
                var that = this;

                this._dom.base.on('click.Selectable', function(e){
                    that.events(e);
                }).on('keydown.Selectable', function(e){
                    if(that._data.state == STATE_OPEN) {
                        if(e.keyCode == KEYCODE_TAB) {
                            that.closeSelect();
                            that.blur();
                            return;
                        }

                        e.preventDefault();
                        if(e.keyCode == KEYCODE_ESC) {
                            that.closeSelect();
                        } else if(e.keyCode == KEYCODE_UP || e.keyCode == KEYCODE_LEFT){
                            if(!that._data.selecting)
                                that._data.selecting = that._data.selected;

                            that.setSelection(that._data.selecting.prev()[0]);
                        } else if(e.keyCode == KEYCODE_DOWN || e.keyCode == KEYCODE_RIGHT) {
                            if(!that._data.selecting)
                                that._data.selecting = that._data.selected;

                            that.setSelection(that._data.selecting.next()[0]);
                        } else if(e.keyCode == KEYCODE_ENTER) {
                            that.selectItem();
                        }

                        //check if char
                        var char = String.fromCharCode(e.keyCode);
                        if(char) {
                            that.setSelectionFromChar(char)
                                .gotoSelecting();
                        }

                    } else {
                        if(e.keyCode == KEYCODE_ENTER || e.keyCode == KEYCODE_SPACE) {
                            e.preventDefault();
                            that.openSelect();
                        }
                        else if(e.keyCode == KEYCODE_UP || e.keyCode == KEYCODE_LEFT){
                            e.preventDefault();
                            that.selectItem(that._data.selected.prev());
                        } else if(e.keyCode == KEYCODE_DOWN || e.keyCode == KEYCODE_RIGHT) {
                            e.preventDefault();
                            that.selectItem(that._data.selected.next());
                        }
                    }
                });
                
                if(!!this._dom.items){
                    this._dom.items.on('mouseenter.Selectable', function(e){
                        that.focus();
                        that.setSelection(this);
                    });
                }

                if(!!this._dom.select){
                    this._dom.select.on('change.Selectable', function(){
                        that.update();
                    }).on('rebuild.Selectable', function(){
                        that.buildList();
                    });
                }

                return this;
            },
            detachEvents: function(){
                this._dom.select.off('.Selectable');
                this._dom.base.off('.Selectable');
                this._dom.items.off('.Selectable');

                this.detachDynamicEvents();
                return this;
            },
            attachDynamicEvents: function(){
                if(!this._data.documentEventHandler) {
                    var that = this;
                    this._data.documentEventHandler = function(e){
                        if(that._dom.base[0] !== e.target && !that._dom.base.has(e.target).length)
                            that.events(e);
                    };
                }

                if(!this._data.documentEventHandlerAttached) {
                    $(document).on('click.Selectable', this._data.documentEventHandler);
                    this._data.documentEventHandlerAttached = true;
                }

                return this;
            },
            detachDynamicEvents: function(){
                if(this._data.documentEventHandlerAttached) {
                    $(document).off('click.Selectable', this._data.documentEventHandler);
                    this._data.documentEventHandlerAttached = false;
                }
            },
            getElement: function(select){
                if(select)
                    return this._dom.select;

                return this._dom.base;
            },
            events: function(e){
                var type = e.type;
                var target = e.target;

                if(type == 'click') {
                    if(this._dom.base[0] === target || this._dom.base.has(target).length) {
                        //e.stopPropagation();
                        if(this._dom.list.has(target).length)
                            this.selectItem(target);
                        else
                            this.toggleSelect();
                    } else {
                        this.closeSelect();
                        this.blur();
                    }
                }
            }
        };

        return Selectable;
    })(domHelper);
})( jQuery, window, document );