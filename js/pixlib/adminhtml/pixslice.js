window.onload = function()
{
    //get the elements
    var hiddenInput = document.createElement('input');
    var nextDiv = document.getElementById('form_tabs_form_section_content');

    //set attributes to the hidden element
    hiddenInput.setAttribute('type','hidden');
    hiddenInput.setAttribute('name','is_in_slice');
    hiddenInput.setAttribute('id', 'is_in_slice');

    //hiddenObject = document.getElementById('in_promotion');

    function getJson() {
        var checkboxes = document.getElementsByClassName('checkbox');
        var position = document.getElementsByName('position');
        var jSonValue = new Object();
        // loop over them all
        for (var i=0; i<checkboxes.length; i++) {
            // And stick the checked ones onto an array...
            if (checkboxes[i].checked) {
                jSonValue[checkboxes[i].value] = position[i].value;
            }
        }
        // Return the array if it is non-empty, or null
        return jSonValue;
    }
    checkedBoxes = getJson();
    var pixSlicesQuestions = $H(checkedBoxes);
    hiddenInput.value = pixSlicesesQuestionValue();

    //append element in the form
    document.getElementById('edit_form').appendChild(hiddenInput);

    function pixSlicesesQuestionValue()
    {
        return JSON.stringify(pixSlicesQuestions);
    }

    function registerSlicesquestion(grid, element, checked){
        if(checked){
            if(element.positionElement){
                element.positionElement.disabled = false;
                pixSlicesQuestions.set(element.value, element.positionElement.value);
            }
        }
        else{
            if(element.positionElement){
                element.positionElement.disabled = true;
            }
            pixSlicesQuestions.unset(element.value);
        }
        hiddenInput.value = pixSlicesesQuestionValue();
        grid.reloadParams = {'selected_products[]':pixSlicesQuestions.keys()};
    }

    function slicesQuestionRowClick(grid, event){
        var trElement = Event.findElement(event, 'tr');
        var isInput   = Event.element(event).tagName == 'INPUT';
        if(trElement){
            var checkbox = Element.getElementsBySelector(trElement, 'input');
            if(checkbox[0]){
                var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                colorway_idJsObject.setCheckboxChecked(checkbox[0], checked);
            }
        }
    }

    function positionChange(event){
        var element = Event.element(event);
        if(element && element.checkboxElement && element.checkboxElement.checked){
            pixSlicesQuestions.set(element.checkboxElement.value, element.value);
            console.log(pixSlicesQuestions);
            hiddenInput.value = pixSlicesesQuestionValue();
        }
    }
    var tabIndex = 1000;

    function slicesQuestionRowInit(grid, row){
        var checkbox = $(row).getElementsByClassName('checkbox')[0];
        var position = $(row).getElementsByClassName('input-text')[0];
        if(checkbox && position){
            checkbox.positionElement = position;
            position.checkboxElement = checkbox;
            position.disabled = !checkbox.checked;
            position.tabIndex = tabIndex++;
            Event.observe(position,'keyup',positionChange);
        }
    }

    colorway_idJsObject.rowClickCallback = slicesQuestionRowClick;
    colorway_idJsObject.initRowCallback = slicesQuestionRowInit;
    colorway_idJsObject.checkboxCheckCallback = registerSlicesquestion;
    colorway_idJsObject.rows.each(function(row){slicesQuestionRowInit(colorway_idJsObject, row)});

}

