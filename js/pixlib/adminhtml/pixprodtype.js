window.onload = function()
{
    //get the elements
    var hiddenInput = document.createElement('input');
    var nextDiv = document.getElementById('form_tabs_form_section_content');

    //set attributes to the hidden element
    hiddenInput.setAttribute('type','hidden');
    hiddenInput.setAttribute('name','is_in_group');
    hiddenInput.setAttribute('id', 'is_in_group');

    //hiddenObject = document.getElementById('in_promotion');

    function getJson() {
        var checkboxes = document.getElementsByClassName('checkbox');
        var position = document.getElementsByName('position');
        var jSonValue = new Object();
        // loop over them all
        for (var i=0; i<checkboxes.length; i++) {
            // And stick the checked ones onto an array...
            if (checkboxes[i].checked) {
                jSonValue[checkboxes[i].value] = position[i].value;
            }
        }
        // Return the array if it is non-empty, or null
        return jSonValue;
    }
    checkedBoxes = getJson();
    var pixGroupQuestions = $H(checkedBoxes);
    hiddenInput.value = pixGroupesQuestionValue();

    //append element in the form
    document.getElementById('edit_form').appendChild(hiddenInput);

    function pixGroupesQuestionValue()
    {
        return JSON.stringify(pixGroupQuestions);
    }

    function registerGroupquestion(grid, element, checked){
        if(checked){
            if(element.positionElement){
                element.positionElement.disabled = false;
                pixGroupQuestions.set(element.value, element.positionElement.value);
            }
        }
        else{
            if(element.positionElement){
                element.positionElement.disabled = true;
            }
            pixGroupQuestions.unset(element.value);
        }
        hiddenInput.value = pixGroupesQuestionValue();
        grid.reloadParams = {'selected_products[]':pixGroupQuestions.keys()};
    }

    function groupQuestionRowClick(grid, event){
        var trElement = Event.findElement(event, 'tr');
        var isInput   = Event.element(event).tagName == 'INPUT';
        console.log('was clicked');
        if(trElement){
            var checkbox = Element.getElementsBySelector(trElement, 'input');
            console.log('was clicked 2');
            if(checkbox[0]){
                var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                group_idJsObject.setCheckboxChecked(checkbox[0], checked);
                console.log('was clicked 3');
            }
        }
    }

    function positionChange(event){
        var element = Event.element(event);
        if(element && element.checkboxElement && element.checkboxElement.checked){
            pixGroupQuestions.set(element.checkboxElement.value, element.value);
            console.log(pixGroupQuestions);
            hiddenInput.value = pixGroupesQuestionValue();
        }
    }
    var tabIndex = 1000;

    function groupQuestionRowInit(grid, row){
        var checkbox = $(row).getElementsByClassName('checkbox')[0];
        var position = $(row).getElementsByClassName('input-text')[0];
        if(checkbox && position){
            checkbox.positionElement = position;
            position.checkboxElement = checkbox;
            position.disabled = !checkbox.checked;
            position.tabIndex = tabIndex++;
            Event.observe(position,'keyup',positionChange);
        }
    }

    group_idJsObject.rowClickCallback = groupQuestionRowClick;
    group_idJsObject.initRowCallback = groupQuestionRowInit;
    group_idJsObject.checkboxCheckCallback = registerGroupquestion;
    group_idJsObject.rows.each(function(row){groupQuestionRowInit(group_idJsObject, row)});

}

