window.onload = function()
{
	//get the elements
	var hiddenInput = document.createElement('input');
	var nextDiv = document.getElementById('form_tabs_form_section_content');

	//set attributes to the hidden element
	hiddenInput.setAttribute('type','hidden');
	hiddenInput.setAttribute('name','is_in_quiz');
	hiddenInput.setAttribute('id', 'is_in_quiz');

	//hiddenObject = document.getElementById('in_promotion');

	function getJson() {
	  var checkboxes = document.getElementsByClassName('checkbox');
	  var position = document.getElementsByName('position');
	  var jSonValue = new Object();
	  // loop over them all
	  for (var i=0; i<checkboxes.length; i++) {
	     // And stick the checked ones onto an array...
	     if (checkboxes[i].checked) {
	        jSonValue[checkboxes[i].value] = position[i].value;
	     }
	  }
	  // Return the array if it is non-empty, or null
	  return jSonValue;
	}
	checkedBoxes = getJson();
	var pixQuizzQuestions = $H(checkedBoxes);
    hiddenInput.value = pixQuizzesQuestionValue();

	//append element in the form
	document.getElementById('edit_form').appendChild(hiddenInput);

	function pixQuizzesQuestionValue()
	{
		return JSON.stringify(pixQuizzQuestions); 
	}

	function registerQuizzquestion(grid, element, checked){
        if(checked){
            if(element.positionElement){
                element.positionElement.disabled = false;
                pixQuizzQuestions.set(element.value, element.positionElement.value);
            }
        }
        else{
            if(element.positionElement){
                element.positionElement.disabled = true;
            }
            pixQuizzQuestions.unset(element.value);
        }
        hiddenInput.value = pixQuizzesQuestionValue();
        grid.reloadParams = {'selected_products[]':pixQuizzQuestions.keys()};
    }

    function quizzQuestionRowClick(grid, event){
        var trElement = Event.findElement(event, 'tr');
        var isInput   = Event.element(event).tagName == 'INPUT';
        if(trElement){
            var checkbox = Element.getElementsBySelector(trElement, 'input');
            if(checkbox[0]){
                var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                quiz_idJsObject.setCheckboxChecked(checkbox[0], checked);
            }
        }
    }

    function positionChange(event){
        var element = Event.element(event);
        if(element && element.checkboxElement && element.checkboxElement.checked){
            pixQuizzQuestions.set(element.checkboxElement.value, element.value);
            console.log(pixQuizzQuestions);
            hiddenInput.value = pixQuizzesQuestionValue();
        }
	}
    var tabIndex = 1000;

    function quizzQuestionRowInit(grid, row){
        var checkbox = $(row).getElementsByClassName('checkbox')[0];
        var position = $(row).getElementsByClassName('input-text')[0];
        if(checkbox && position){
            checkbox.positionElement = position;
            position.checkboxElement = checkbox;
            position.disabled = !checkbox.checked;
            position.tabIndex = tabIndex++;
            Event.observe(position,'keyup',positionChange);
        }
    }

    quiz_idJsObject.rowClickCallback = quizzQuestionRowClick;
    quiz_idJsObject.initRowCallback = quizzQuestionRowInit;
    quiz_idJsObject.checkboxCheckCallback = registerQuizzquestion;
    quiz_idJsObject.rows.each(function(row){quizzQuestionRowInit(quiz_idJsObject, row)});

}

