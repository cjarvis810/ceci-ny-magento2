var Curalate = Curalate || {};
(function() {
    var c = document.createElement("style"),
        d = '#curalate-fan-reel{width:1002px}.curalate-top-heading{background-color:#fff;height:110px;margin:auto;text-align:center;width:500px; border-top:1px solid #d7d7d7; border-right:1px solid #d7d7d7; border-left:1px solid #d7d7d7;}.curalate-top-heading h3{font-size:15px;margin:0;padding-top:20px; text-transform:none;}.curalate-top-heading h2{color:#af975f;font-size:24px;letter-spacing:.2em;margin:15px 0;padding:0;}#curalate-fan-reel{display:block;vertical-align:middle}.curalate-viewport{background-color:#fff;display:inline-block;height:590px;position:relative;vertical-align:middle}.curalate-thumbs{display:block;margin:0 auto;text-align:center;overflow:hidden}.curalate-hidden{display:none}.curalate-image-container{position:relative}.curalate-thumbnail{display:block;float:left;height:250px;margin:0;width:250px}.curalate-image-container img{width:250px}.curalate-social-sharing{float:right;margin:0 36px;width:200px}.curalate-social-button{background-color:#c9c9c9;background:-webkit-linear-gradient(#fff,#c9c9c9);background:-o-linear-gradient(#fff,#c9c9c9);background:-moz-linear-gradient(#fff,#c9c9c9);background:linear-gradient(#fff,#c9c9c9);border-radius:100%;box-shadow:0 0 5px 1px #999;display:inline-block;float:right;height:30px;margin:32px 10px 0;width:30px}.curalate-social-button img{display:block;margin:auto;width:15px}.curalate-social-button .curalate-instagram{margin-top:7.5px}.curalate-social-button .curalate-pinterest{margin-top:6.5px;width:12px}.curalate-social-button .curalate-twitter{margin-top:8.4px}.curalate-social-button .curalate-facebook{margin-top:6px;width:8px}';
    c.setAttribute("type", "text/css");
    if (c.styleSheet) {
        c.styleSheet.cssText = d;
    } else {
        c.innerHTML = d;
    }
    document.getElementsByTagName("head")[0].appendChild(c);
    document.getElementById("curalate-fan-reel-wrapper").innerHTML = '<div id="curalate-fan-reel"> <div class="curalate-top-heading"> <h3 class="caslon-book">Social Graces</h3> <h2>share the love</h2> </div> <div class="curalate-viewport box-shadow"> <div class="curalate-thumbs"> </div> <div class="curalate-social-sharing"> <a href="" class="curalate-social-button"><img class="curalate-instagram" src="//d116tqlcqfmz3v.cloudfront.net/cecinewyork-125/assets/instagram-share.png?cache=1437082186200"/></a> <a href="" class="curalate-social-button"><img class="curalate-pinterest" src="//d116tqlcqfmz3v.cloudfront.net/cecinewyork-125/assets/pinterest-share.png?cache=1437082186200"/></a> <a href="" class="curalate-social-button"><img class="curalate-twitter" src="//d116tqlcqfmz3v.cloudfront.net/cecinewyork-125/assets/twitter-share.png?cache=1437082186200"/></a> <a href="" class="curalate-social-button"><img class="curalate-facebook" src="//d116tqlcqfmz3v.cloudfront.net/cecinewyork-125/assets/facebook-share.png?cache=1437082186200"/></a> </div> </div> </div> <div class="curalate-hidden"> <div data-curalate-photo-template> <a href="#"> <div class="curalate-image-container"> <img data-curalate-image="medium_square" class="curalate-image"/> </div> </a> </div> </div>';
    var e = window.Curalate;
    e.FanReels = e.FanReels || {};
    e.FanReels.Gallery = e.FanReels.Gallery || {};
    var b = function(g, f) {
            var i, h = document.createElement(g);
            for (i in f) {
                h.setAttribute(i, f[i]);
            }
            return h;
        },
        a = function(f) {
            return b("script", {
                type: "text/javascript",
                charset: "UTF-8",
                src: f
            });
        };
    e.FanReels.Gallery.init = function(h) {
        var j, g = a("//api.curalate.com/js-min/fanreel-gallery-impl.min.js"),
            i;
        var f = {
            pageSize: 2,
            navigation: {
                infiniteScroll: false
            }
        };
        for (i in h) {
            f[i] = h[i];
        }
        j = function() {
            e.FanReels.Gallery.load(f);
        };
        if (typeof g.onload === "object") {
            g.onload = j;
        } else {
            g.onreadystatechange = function() {
                if (this.readyState === "loaded" || this.readyState === "complete") {
                    j();
                }
            };
        }
        document.body.appendChild(g);
    };
}());
console.log('lib loaded');