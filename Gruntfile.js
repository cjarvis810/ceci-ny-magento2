(function (module, undefined) {
    "use strict";

    /**
     * In our grunt file we will be performing our minifications and what not
     */
    module.exports = function (grunt) {

        var configFileName = 'pix_env_config.json';

        //Here we are gathering information we will need from the pix environment config
        //js folders and files to be minified should be added there
        var pixConfig = grunt.file.readJSON(configFileName);

        //all our js files in an array
        var allJsFiles = (function getAllJavascriptFiles() {
            return pixConfig.js.reduce(function (running, current) {
                return running.concat(combineFolderAndFilesFromConfig(current, "js"));
            }, []);
        }());

        //all our css files in an array 
        var allCssFiles = (function getAllCssFiles() {
            return pixConfig.css.reduce(function (running, current) {
                return running.concat(combineFolderAndFilesFromConfig(current, "css"));
            }, []);
        }());

        /**
         * Helper function for config parsing
         * @param {type} config
         * @param {type} extension
         * @returns {unresolved}
         */
        function combineFolderAndFilesFromConfig(config, extension) {
            return config.folders.map(function (currentValue) {
                return currentValue + "*." + extension;
            }).concat(config.files);
        }

        /**
         * jshint config object
         */
        var hintOpts = {
            options: {
                jshintrc: '.jshintrc'
            },
            all: allJsFiles.concat(['Gruntfile.js'])
        };


        /**
         * watchList configuration for watching
         * This is still here as an option, don't know if people will still use it though
         */
        var watchList = {
            js: {
                files: allJsFiles,
                tasks: ['jshint', 'uglify', 'updateconfig'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: allCssFiles,
                tasks: ['csslint', 'cssmin', 'updateconfig'],
                options: {
                    livereload: true
                }
            }
        };

        /**
         * uglify config object.
         */
        var uglify = {
            options: {
                mangle: {
                    except: ['jQuery', 'Prototype']
                }
            },
            my_target: {
                options: {
                    sourceMap: true //enables source map creation
                },
                files: (function getSourcesAndDestinations() {
                    return pixConfig.js.reduce(function (running, current) {
                        return running.concat({
                            src: combineFolderAndFilesFromConfig(current, "js"),
                            dest: current.destination
                        });
                    }, []);
                }())
            }
        };

        /**
         * If readable flag is sent don't mangle
         */
        if( grunt.option('readable') ){
            uglify.options.mangle = false;
            uglify.options.beautify = true;
        }

        /**
         * csslint config object
         */
        var lintOpts = {
            files: allCssFiles,
            options: {
                'adjoining-classes': false, //allow adjoining classes
                'box-model': false, //don't throw error on box-sizing
                'box-sizing': false, //don't throw error on box-sizing
                'qualified-headings': false, //we are using a CMS, client is always liable to add heading tags in odd places.
                'unique-headings': false, //cannot assume all headings are the same because clients use CMS blocks
                'universal-selector': false, //ignore star pop hacks
                'star-property-hack': false, //ignore star pop hacks
                'underscore-property-hack': false, //ingore this one too
                'regex-selectors': false, //regex selectors are used in font css files
                'floats': false, //no float limit
                'rules-count': false, //no rule limit
                'vendor-prefix': false, //do not require all vender prefixes
                'selector-max': 4 //5 still seems high, but this is a starting point.
            }
        };

        /**
         * cssmin config object
         */
        var cssMin = {
            combine: {
                files: (function getSourcesAndDestinations() {
                    return pixConfig.css.reduce(function (running, current) {
                        return running.concat({
                            src: combineFolderAndFilesFromConfig(current, "css"),
                            dest: current.destination
                        });
                    }, []);
                }())
            },
            options: {
                compatibility: 'ie7' //removes spacing issues in compressed css for ie8
            }
        };


        /**
         * githooks config object
         */
        var githooks = {
            all: {
                // Will run the jshint at every commit
                'pre-commit': 'jshint csslint',
            }
        };

        grunt.log.writeln(["Commence hinting, linting, combing, and minifying!"]);



        /**
         * Grunt Initilazation
         * Pixafy Default Configuration
         */
        grunt.initConfig({
            watch: watchList,
            jshint: hintOpts,
            uglify: uglify,
            csslint: lintOpts,
            cssmin: cssMin,
            githooks: githooks,
            updateconfig: function(){
                grunt.file.write('test.json', JSON.stringify(pixConfig, null, 4) );
            }
        });

        // Load the Grunt plugins.
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-contrib-jshint');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-cssmin');
        grunt.loadNpmTasks('grunt-contrib-csslint');
        grunt.loadNpmTasks('grunt-githooks');


        //these are our public tasks tasks        
        grunt.registerTask('default', watchList.js.tasks.concat(watchList.css.tasks));
        grunt.registerTask('w', ['watch', 'githooks']);

        //this is used internally to update the json file
        grunt.registerTask('updateconfig', 'Updates the config file to include the time modified. Used for cache busting.', function(){
            pixConfig['last-updated'] = new Date().getTime();
            grunt.file.write(configFileName, JSON.stringify(pixConfig, null, 4) );
        });
    };
})(module);


