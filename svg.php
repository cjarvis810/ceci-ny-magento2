<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 3/25/2015
 * Time: 3:48 PM
 */

require_once 'app/Mage.php';
umask(0);
Mage::app('admin');
Mage::getSingleton('core/session', array('name' => 'frontend'));
header('Location: /index.php/admin/svg/index/');
exit();
$fonts = array();
if (is_dir('media/fonts')) {
    $dir = opendir('media/fonts');
    while ($filename = readdir($dir)) {
        if (in_array($filename, array('.', '..'))) continue;
        $fonts[] = pathinfo('media/fonts/' . $filename, PATHINFO_FILENAME);
    }
}

$svgs = array();
$helper = Mage::helper('pixcatalog/svg');
$productsStagingPath = $helper->getStagingProductsDir();
echo $productsStagingPath;
if (is_dir($productsStagingPath)) {
    $productsStagingDir = opendir($productsStagingPath);
    while ($sku = readdir($productsStagingDir)) {
        if (in_array($sku, array('.', '..'))) continue;
        $skuStagingPath = $productsStagingPath . $sku;
        if (is_dir($skuStagingPath)) {
            $skuStagingDir = opendir($skuStagingPath);
            while ($filename = readdir($skuStagingDir)) {
                if (in_array($filename, array('.', '..'))) continue;
                $source = $skuStagingPath . DS . $filename;
                $pathinfo = pathinfo($source);
                if ($pathinfo['extension'] != 'svg') continue;
                $svgs[$sku] = $helper->getStagingUrl("products/$sku") . "$filename";
            }
        }
    }
}

//var_dump($svgs);

?>
<html>
<head>
    <script>
        var matches = window.location.href.match(/^(.*\/)[^\/]*$/);
        var Mage = {
            Cookies: {
                path: matches[1]
            }
        };
    </script>
    <script type="application/javascript" src="<?php echo Mage::getBaseUrl('js'); ?>pixlib/jquery.js"></script>
    <script type="application/javascript" src="<?php echo Mage::getBaseUrl('js'); ?>pixafy/catalog/product/personalization.js"></script>
    <script type="application/javascript" src="<?php echo Mage::getBaseUrl('js'); ?>pixafy/test.js"></script>
    <link rel="stylesheet" href="<?php echo Mage::getBaseUrl('media'); ?>css/fonts.css" />
    <style>
        ul li {
            list-style-type: none;
            margin-top: 10px;
        }
        ul li.label {
            width: 100px;
            float: left;
            clear: left;
        }
        ul li.data {
            float: left;
        }
        input, select {
            width: 500px;
        }
        div.panel {
            width: 50%;
        }
        #left-panel {
            float: left;
        }
        #right-panel {
            float: right;
        }
    </style>
</head>
<body>
    <div id="left-panel" class="panel">
        <select id="select-sku">
            <?php $i = 1; ?>
            <?php foreach ($svgs as $sku => $filename): ?>
                <option data-sku="<?php echo $sku; ?>" value="<?php echo $filename ?>" <?php if ($i++ == 1): ?><?php endif; ?>><?php echo $sku; ?></option>
            <?php endforeach; ?>
        </select>
        Bleed lines: <input type="checkbox" id="bleedLineToggle" value="1">

        <br /><br />
        <img />
    </div>
    <div id="right-panel" class="panel">
        <ul>
            <?php for ($i = 1; $i <= 20; ++$i): ?>
                <li class="label">Line <?php echo $i; ?></li>
                <li class="data"><input type="text" data-sku="test" data-text="<?php echo $i; ?>" class="personalization-text" /></li>
            <?php endfor; ?>
            <?php for ($i = 0; $i <= 4; ++$i): ?>
                <li class="label">Color <?php echo $i; ?></li>
                <li class="data">
                    <select data-sku="test" data-color="<?php echo $i; ?>" class="personalization-color">
                        <?php for ($r = 0; $r <= 15; $r += 3): ?>
                            <?php for ($g = 0; $g <= 15; $g += 3): ?>
                                <?php for ($b = 0; $b <= 15; $b += 3): ?>
                                    <?php $hex = sprintf('%x%x%x', $r, $g, $b); ?>
                                    <option><?php echo $hex; ?></option>
                                <?php endfor; ?>
                            <?php endfor; ?>
                        <?php endfor; ?>
                    </select>
                </li>
            <?php endfor; ?>
            <?php for ($i = 1; $i <= 5; ++$i): ?>
                <li class="label">Font <?php echo $i; ?></li>
                <li class="data">
                    <select data-sku="test" data-font="<?php echo $i; ?>" class="personalization-font">
                        <?php foreach ($fonts as $font): ?>
                            <option><?php echo $font; ?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
            <?php endfor; ?>
        </ul>
    </div>
</body>
</html>