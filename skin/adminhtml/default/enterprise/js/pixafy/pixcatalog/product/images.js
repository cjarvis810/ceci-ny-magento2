/**
 * Created by jcymerman on 3/23/2015.
 * Change img to embed tags when rendering SVGs by overriding the loadImage function
 */

(function ($) {
    Product.Gallery.prototype.loadImage = function(file) {
        var image = this.getImageByFile(file);
        this.getFileElement(file, 'cell-image img').src = image.url;
        this.getFileElement(file, 'cell-image img').show();
        this.getFileElement(file, 'cell-image .place-holder').hide();
        if (image.url.match(/\.svg$/)) {
            var el = $(this.getFileElement(file, 'cell-image img'))[0];
            el.outerHTML = el.outerHTML.replace('<img', '<embed');
        }
    };
})(jQuery.noConflict());