<?php

/**
 * Imageworkshop_Exception
 * 
 * Manage ImageWorkshop exceptions
 * 
 * @link http://phpimageworkshop.com
 * @author Sybio (Clément Guillemain  / @Sybio01)
 * @license http://en.wikipedia.org/wiki/MIT_License
 * @copyright Clément Guillemain
 */

class Iw_Exception extends Zend_Exception
{
    /**
     * __toString method
     *
     * @return string
     */
    public function __toString()
    {
        return __CLASS__.": [{$this->code}]: {$this->message}\n";
    }
}