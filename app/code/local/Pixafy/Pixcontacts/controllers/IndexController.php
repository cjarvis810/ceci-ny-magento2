<?php
require_once 'app' . DS . 'code' . DS. 'core' . DS . 'Mage' . DS . 'Contacts' . DS . 'controllers' . DS . 'IndexController.php';
class Pixafy_Pixcontacts_IndexController extends Mage_Contacts_IndexController
{
   public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);
                $error = false;
                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['telephone']), 'NotEmpty')) {
                    $error = true;
                }
                $emailbody='';
                if($post['contactus-template']=='sayhello'){
                    if (!Zend_Validate::is(trim($post['message']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Message: '.$post['message'].'<br />';
                    }
                }else if($post['contactus-template']=='askquestion'){
                    if (!Zend_Validate::is(trim($post['message']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Message: '.$post['message'].'<br />';
                    }
                }else if($post['contactus-template']=='makeappointment'){
                    if (!Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Comment: '.$post['comment'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['event-data']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Event Date: '.$post['event-date'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['type-of-event']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Type of Event: '.$post['type-of-event'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['event-venue-location']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Event Venue Location: '.$post['event-venue-location'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['number-guest']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Number of Guest: '.$post['number-guest'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['many-colors']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'How Many Colors: '.$post['many-colors'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['letterpress-flat']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Letterpress or Flat: '.$post['letterpress-flat'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['othercomment']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Other Comment: '.$post['othercomment'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['details']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Details: '.$post['details'].'<br />';
                    }
                    if(!count($_POST['interestedin'])>0){
                        $error = true;
                    }else{
                        $emailbody.= 'Interested In: <br />';
                        foreach($_POST['interestedin'] as $value){
                            $emailbody.='&nbsp;&nbsp;&nbsp;&nbsp;'.$value.'<br />';
                        }
                    }
                    if(!count($_POST['iam'])>0){
                        $error = true;
                    }else{
                        $emailbody.= 'I\'m a: <br />';
                        foreach($_POST['iam'] as $value){
                            $emailbody.='&nbsp;&nbsp;&nbsp;&nbsp;'.$value.'<br />';
                        }
                    }
                }else if($post['contactus-template']=='requestestimate'){
                    if (!Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Comment: '.$post['comment'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['event-date']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Event Date'.$post['event-date'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['type-of-event']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Type of Event: '.$post['type-of-event'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['event-venue-location']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Event Venue Location: '.$post['event-venue-location'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['details']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Details: '.$post['details'].'<br />';
                    }
                    if(!count($_POST['interestedin'])>0){
                        $error = true;
                    }else{
                        $emailbody.= 'Interested In: <br />';
                        foreach($_POST['interestedin'] as $value){
                            $emailbody.='&nbsp;&nbsp;&nbsp;&nbsp;'.$value.'<br />';
                        }
                    }
                    if(!count($_POST['iam'])>0){
                        $error = true;
                    }else{
                        $emailbody.= 'I\'m a: <br />';
                        foreach($_POST['iam'] as $value){
                            $emailbody.='&nbsp;&nbsp;&nbsp;&nbsp;'.$value.'<br />';
                        }
                    }
                }else if($post['contactus-template']=='consultation'){
                    if (!Zend_Validate::is(trim($post['details']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Details: '.$post['details'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Comment: '.$post['comment'].'<br />';
                    }
                }else if($post['contactus-template']=='feature'){
                    if (!Zend_Validate::is(trim($post['interestedintext']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Interested In: '.$post['interestedintext'].'<br />';
                    }
                }else if($post['contactus-template']=='contribute'){
                    if (!Zend_Validate::is(trim($post['website']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Website: '.$post['website'].'<br />';
                    }
                    if (!Zend_Validate::is(trim($post['details']), 'NotEmpty')) {
                        $error = true;
                    }else{
                        $emailbody.= 'Details: '.$post['details'].'<br />';
                    }
                }else if($post['contactus-template']=='mailinglist'){
                    if(!count($_POST['iam'])>0){
                        $error = true;
                    }else{
                        $emailbody.= 'I\'m a: <br />';
                        foreach($_POST['iam'] as $value){
                            $emailbody.='&nbsp;&nbsp;&nbsp;&nbsp;'.$value.'<br />';
                        }
                    }
                }else{
                    $error=true;
                }
                if($error) {
                    throw new Exception();
                }
                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        null,
                        array(
                            'name'=>$post['name'],
                            'email'=>$post['email'],
                            'telephone'=>$post['telephone'],
                            'emailbody'=>$emailbody
                        )
                    );
                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('contacts/index/');

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('contacts/index/');
                return;
            }

        } else {
            $this->_redirect('contacts/index/');
        }
    }

}
?>