<?php
class Pixafy_PixCybersource_Helper_Retrieve extends Mage_Core_Helper_Abstract
{
	
	protected $_profile_cc_info = array();
	protected $_options_html = false;
	
	function getTokens($original = false){
		if($this->getCustomer()){
			$tokens = $this->getCustomer()->getData("cybersource_subscription_id");
			$deprecated_tokens = $this->getCustomer()->getData("deprecated_tokens");
			if($original){
				return $tokens;
			}else{
				return $this->prepareTokens($tokens, $deprecated_tokens);
			}
		}else{
			return false;
		}
	}
	
	protected function prepareTokens($tokens, $deprecated_tokens = false){
		$_deprecated_tokens = array();
		if($deprecated_tokens){
			$_deprecated_tokens = explode(",", $deprecated_tokens);
		}
		if(!empty($tokens)){
			$tokens_str = preg_replace("/order\:[0-9]*\=token\:/", "", $tokens);
			if(!empty($tokens_str)){
				$_tokens = explode(",", $tokens_str);
			}
			foreach($_deprecated_tokens as $deprecated_token){
				if(in_array($deprecated_token, $_tokens)){
					unset($_tokens[array_search($deprecated_token, $_tokens)]);
				}
			}
		}
		return !empty($_tokens)?$_tokens:false;
	}
	
	function getCustomer(){
		return Mage::getSingleton('customer/session')->getCustomer();
	}
	
	function getOptionsHtml($profiles){
		if($this->_options_html === false){
			$this->_options_html = "";
			if(!empty($profiles)){
				foreach($profiles as $profile){
					$this->drawOption($profile);
				}
			}
		}
		return !empty($this->_options_html)?$this->_options_html.$this->drawFirstOption():false;
	}
	
	protected function drawFirstOption(){
		return "<option value=''>".$this->__("New credit card")."</option>";
	}
	
	protected function drawOption($profile){
		$data = $profile->paySubscriptionRetrieveReply;
		if(is_object($data)&&$data->reasonCode == $this->getConstant()->SUCCESS_CODE){
			$this->_profile_cc_info[$this->getEncodedProfileId($data)] = $this->getCardCodeById($data->cardType);
			//var_dump($this->getEncodedProfileId($data));
			$this->_options_html .= "<option value='".$this->getEncodedProfileId($data)."'>";
			$this->_options_html .= $this->getOptionTitle($data);
			$this->_options_html .= "</option>";
		}
		return $this->_options_html;
	}
	
	function getOptionTitle($data){
		$html = "";
		$html .= $this->getCardTypeById($data->cardType);
		$html .= " ".$this->__("XXXX-XXXX-XXXX-").substr($data->cardAccountNumber, -4, 4);
		return $html;
	}
	
	function getEncodedProfileId($data){
		return Mage::helper('core')->encrypt($data->subscriptionID);
	}
	
	function getCardTypeById($id){
		return array_key_exists($id, $this->getConstant()->CC_CARD_TYPES)?$this->getConstant()->CC_CARD_TYPES[$id]:false;
	}

	function getCardCodeById($id){
		return array_key_exists($id, $this->getConstant()->CC_CARD_CODES)?$this->getConstant()->CC_CARD_CODES[$id]:false;
	}
	
	function getConstant(){
		return Mage::helper("pixcybersource/constants");
	}

	function getProfileCards(){
		$data = array();
		foreach($this->_profile_cc_info as $key => $val){
			$data[$val][] = $key;
		}
		return $data;
	}
	
}
