<?php 
class Pixafy_PixCybersource_Helper_Constants extends Mage_Core_Helper_Abstract
{
	
	public $CC_CARD_TYPES = array(
		"001" => "Visa",
		"002" => "MasterCard",
		"003" => "American Express",
		"004" => "Discover",
		"005" => "Diners Club",
		"006" => "Carte Blanche",
		"007" => "JCB",
		"014" => "EnRoute",
		"021" => "JAL",
		"024" => "Maestro",
		"031" => "Delta",
		"033" => "Visa Electron",
		"034" => "Dankort",
		"035" => "Laser",
		"036" => "Carte Bleu",
		"037" => "Carta Si",
		"042" => "Maestro",
		"043" => "GE Money UK"
	);
	public $CC_CARD_CODES = array(
		"001" => "VI",
		"002" => "MC",
		"003" => "AE",
		"004" => "DI",
		"005" => "DC",
		"006" => "CBLANCHE",
		"007" => "JCB",
		"014" => "ER",
		"021" => "JAL",
		"024" => "SM",
		"031" => "DE",
		"033" => "VE",
		"034" => "DA",
		"035" => "LA",
		"036" => "CBLEUE",
		"037" => "CS",
		"042" => "SM",
		"043" => "GE"
	);
	
	public $SUCCESS_CODE = 100;
	
}


