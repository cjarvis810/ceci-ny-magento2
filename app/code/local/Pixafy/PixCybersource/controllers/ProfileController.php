<?php 
class Pixafy_PixCybersource_ProfileController extends Mage_Core_Controller_Front_Action
{
	public function listAction(){
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$headBlock = $this->getLayout()->getBlock('head');
		if ($headBlock) {
			$headBlock->setTitle(Mage::helper('pixcybersource')->__('My Payment Profiles'));
		}
		$this->renderLayout();
	}
	
	public function deleteAction(){
		if($this->_getSession()->isLoggedIn()){
			$profile_id = $this->getProfileId();
			if($profile_id){
				$customer = Mage::getModel("customer/customer")->load(Mage::getSingleton("customer/session")->getCustomer()->getId());
				//var_dump($customer);
				//exit;
				$deprecated_tokens = $customer->getData("deprecated_tokens");
				if(empty($deprecated_tokens)){
					$deprecated_tokens = $profile_id;
				}else{
					$deprecated_tokens .= ",".$profile_id;
				}
				$customer->setData("deprecated_tokens", $deprecated_tokens);
				try{
					$customer->save();
					$this->_getSession()->addSuccess($this->__('The profile has been removed.'));
					$this->_redirectSuccess(Mage::getUrl('*/*/list', array('_secure'=>true)));
					return;
				} catch (Mage_Core_Exception $e) {
					$this->_getSession()->addError($e->getMessage());
            	} catch (Exception $e) {
            		$this->_getSession()->addError($this->__('Cannot delete profile.'));
            	}
			}
		}
		return $this->_redirectError(Mage::getUrl('*/*/list'), array('_secure'=>true));
	}
	
	private function getProfileId(){
		$encoded_id = urldecode($this->getRequest()->getParam("profile"));
		if($encoded_id){
			$id = Mage::helper("core")->decrypt($encoded_id);
			return $id;
		}
		return false;
	}
	
	protected function _getSession()
	{
		return Mage::getSingleton('customer/session');
	}
}
?>