<?php
	$installer = $this;
    $installer->startSetup();
    
    $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
    
    $entityTypeId     = $setup->getEntityTypeId('customer');
    $attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
    $attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
    
    $setup->addAttribute('customer','deprecated_tokens',
		array(
			'type'          => 'text',
			'label'         => 'Deprecated tokens',
			'input'         => 'text',
			'visible'       => true,
			'required'      => false,
			'user_defined'  => false,
			'default'       => ""
		)
	);
    
    $setup->addAttributeToGroup(
    		$entityTypeId,
    		$attributeSetId,
    		$attributeGroupId,
    		'deprecated_tokens',
    		'999'  //sort_order
    );
    
    $installer->endSetup();