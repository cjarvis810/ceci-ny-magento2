<?php

$installer = $this;
$installer->startSetup();
// Add tables for logging customer payment info
$installer->run("
DROP TABLE IF EXISTS `{$this->getTable('pixcybersource_paymentprofile')}`;
CREATE TABLE `{$this->getTable('pixcybersource_paymentprofile')}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `subscription_id` varchar(20) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `cc_lastfour` varchar(4) NOT NULL,
  `cc_exp_month` varchar(2) NOT NULL,
  `cc_exp_year` varchar(4) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();
