<?php
$installer = $this;
 
$installer->startSetup();
 
$installer->run("
	ALTER TABLE {$this->getTable('sales_flat_quote_payment')} ADD  `cc_profile` VARCHAR( 50 ) NULL DEFAULT NULL
");
 
$installer->endSetup();
