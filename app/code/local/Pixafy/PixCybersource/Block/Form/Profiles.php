<?php 
class Pixafy_PixCybersource_Block_Form_Profiles extends Mage_Core_Block_Template
{
	protected function _construct()
	{
		$this->setTemplate('pixcybersource/form/profiles.phtml');
		$this->getProfiles();
		//var_dump($this->getProfileCards());
		//var_dump(Mage::registry("payment_profiles"));
	}
	
	protected function getProfiles(){
		if(!Mage::registry("payment_profiles")){
			$tokens = $this->_getHelper()->getTokens();
			$data = array();
			if($tokens){
				foreach($tokens as $token){
					$profile = $this->setProfile($token);
					if($profile->reasonCode == $this->getConstant()->SUCCESS_CODE){
						$data[] = $this->setProfile($token);
					}
				}
			}
			Mage::unregister("payment_profiles");
			Mage::register("payment_profiles", $data);
		}
		return Mage::registry("payment_profiles");
	}
	
	
	protected function setProfile($token){
		$request = Mage::getModel("pixcybersource/soap_request_retrieve");
		$request->setSubscriptionId($token);
		$response = $this->getSoapClient()->getResponse($request);
		return $response->getData("response");
	}
	
	public function _getHelper(){
		return Mage::helper("pixcybersource/retrieve");
	}
	
	function getConstant(){
		return Mage::helper("pixcybersource/constants");
	}
	
	function getProfilesSelectHtml(){
		return $this->_getHelper()->getOptionsHtml($this->getProfiles());
	}
	
	protected function getSoapClient()
	{
		return Mage::getModel('cybersource/soap_client');
	}
	
	public function getProfile($token){
		if(!Mage::registry("current_profile")){
			$profile = $this->setProfile($token);
			Mage::unregister("current_profile");
			Mage::register("current_profile", $profile);
		}
		return Mage::registry("current_profile");
	}
	
	
	function getProfileCards(){
		return json_encode($this->_getHelper()->getProfileCards());
	}
	
}