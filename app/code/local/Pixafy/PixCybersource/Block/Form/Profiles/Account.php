<?php
class Pixafy_PixCybersource_Block_Form_Profiles_Account extends Pixafy_PixCybersource_Block_Form_Profiles
{
	protected $_current_profile = false;
	
	function getCardType($profile = false){
		return $this->_getHelper()->getCardTypeById($this->getCurrentProfile()->paySubscriptionRetrieveReply->cardType);
	}
	
	function getShortCardNumber(){
		return $this->__("XXXX-XXXX-XXXX-").substr($this->getCurrentProfile()->paySubscriptionRetrieveReply->cardAccountNumber, -4, 4);
	}
	
	function getProfileData($field){
		return (property_exists($this->getCurrentProfile()->paySubscriptionRetrieveReply, $field))?$this->getCurrentProfile()->paySubscriptionRetrieveReply->$field:false;
	}
	
	function setCurrentProfile($profile){
		$this->_current_profile = $profile;
	}
	
	function getCurrentProfile(){
		return $this->_current_profile;
	}
	
	function getViewUrl(){
		return $this->getUrl("pixcybersource/profile/view", array("profile" => urlencode(Mage::helper('core')->encrypt($this->getCurrentProfile()->paySubscriptionRetrieveReply->subscriptionID))));
	}
	
	function getDeleteUrl(){
		return $this->getUrl("pixcybersource/profile/delete", array("profile" => urlencode(urlencode(Mage::helper('core')->encrypt($this->getCurrentProfile()->paySubscriptionRetrieveReply->subscriptionID)))));
	}
	
}