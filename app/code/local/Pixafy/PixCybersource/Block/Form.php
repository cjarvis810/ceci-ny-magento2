<?php
class Pixafy_PixCybersource_Block_Form extends Cybersource_PaymentManagement_Block_Form
{
	
	protected function _construct()
	{
		parent::_construct();
		if(!Mage::app()->getStore()->isAdmin())
		{
			$this->setTemplate('pixcybersource/form.phtml');
			//$this->setChild("cybersource_form", Mage::app()->getLayout()->createBlock("pixcybersource/form_profiles"));
		}
	}
	
}