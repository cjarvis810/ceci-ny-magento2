<?php

/**
 * 
 */
class Pixafy_PixCybersource_Model_Cybersource extends Cybersource_PaymentManagement_Model_Cybersource {

    /**
     * 
     * @param type $data
     * @return \Pixafy_PixCybersource_Model_Cybersource
     */
    public function assignData($data) {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setCcType($data->getCcProfile() ? $data->getCcCardProfile() : $data->getCcType())
                ->setCcOwner($data->getCcOwner())
                ->setCcLast4(substr($data->getCcNumber(), -4))
                ->setCcNumber($data->getCcNumber())
                ->setCcCid($data->getCcCid())
                ->setCcExpMonth($data->getCcExpMonth())
                ->setCcExpYear($data->getCcExpYear())
                ->setCcSsIssue($data->getCcSsIssue())
                ->setCcSsStartMonth($data->getCcSsStartMonth())
                ->setCcSsStartYear($data->getCcSsStartYear())
                ->setCcProfile(Mage::helper("core")->decrypt($data->getCcProfile()))
        ;

        return $this;
    }

    /**
     * 
     * @return \Pixafy_PixCybersource_Model_Cybersource
     */
    public function validate() {
        if (!Mage::registry("is_validated")) {
            Mage::register("is_validated", true);
            $info = $this->getInfoInstance();
            if ($info->getCcProfile()) {
                return $this;
            } else {
                return parent::validate();
            }
        }
        return $this;
    }

    /**
     * 
     * @param Varien_Object $payment
     * @param type $amount
     * @return type
     */
    public function authorize(Varien_Object $payment, $amount) {
        if (!$this->canAuthorize()) {
            Mage::throwException(Mage::helper('payment')->__('Authorize action is not available.'));
        }
        if ($this->isProfileExist($payment)) {
            return $this->authorizeProfile($payment, $amount);
        } else {
            return parent::authorize($payment, $amount);
        }
    }

    /**
     * 
     * @param type $payment
     * @return type
     */
    protected function isProfileExist($payment) {
        $profile = $payment->getCcProfile();
        return !empty($profile) ? true : false;
    }

    /**
     * 
     * @param Varien_Object $payment
     * @param type $amount
     * @return type
     */
    public function capture(Varien_Object $payment, $amount) {
        if (!$this->canCapture()) {
            Mage::throwException(Mage::helper('payment')->__('Capture action is not available.'));
        }
        if ($this->isProfileExist($payment)) {
            return $this->captureProfile($payment, $amount);
        } else {
            return parent::capture($payment, $amount);
        }
    }

    /**
     * 
     * @param type $payment
     * @param type $amount
     * @return \Pixafy_PixCybersource_Model_Cybersource
     */
    protected function captureProfile($payment, $amount) {
        try {
            $error = false;
            $request = Mage::getModel('cybersource/payment')->assembleCaptureProfileRequest($payment, $amount);
            //$request->appendMerchantDefinedDataFields($payment);
            $response = $this->getSoapClient()->getResponse($request);
            Mage::getModel('cybersource/payment')->saveTransactionProfileDetails($payment, $response);

            Mage::helper('cybersource/payment')->addComment($payment, $response);

            /**
             * Clarification:
             * response-> reasonCode is returned by CyberSource and used here
             * response -> ccAuthReply -> reasonCode is returned by the processor and NOT used here
             *
             * */
            $cybersourceReasonCode = $response->getReasonCode();

            if ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_SUCCESS) {
                
            } elseif ($cybersourceReasonCode == 242) {
                $error = $response->getReasonMessage();
            } elseif ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_CVN_DECLINE) {
                $error = Mage::helper('cybersource')->__('Payment rejected. Please correct your Card Verification Number and try again.');
            } elseif ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_AVS_DECLINE) {
                $error = Mage::helper('cybersource')->__('Payment rejected. Please correct your billing address and try again.');
            } else {
                if ($this->getConfigData('dm_enabled')) {
                    //Run Service: Decision Manager
                    $error = Mage::getModel('cybersource/decision')->manageDecision($response, $payment, 'capture');
                } else {
                    $error = Mage::helper('cybersource')->__('There is an error in processing the payment. Please try again.');
                }
            }

            // Service: Payment Tokenization
            //Mage::getModel('cybersource/tokenization')->tokenizePayment($response, $request, $payment);
            // Service: Delivery Address Verification
            Mage::getModel('cybersource/dav')->verifyDeliveryAddress($payment);
        } catch (Exception $e) {
            $error = $this->__('Gateway request error: ' . $e->getMessage() . Mage::helper('cybersource')->prepareSignature()); //@todo TALK-MESSAGE
        }
        if ($error) {
            Mage::throwException($error); //@internal TALK-MESSAGE
        }

        return $this;
    }

    /**
     * 
     * @param type $payment
     * @param type $amount
     * @return \Pixafy_PixCybersource_Model_Cybersource
     */
    protected function authorizeProfile($payment, $amount) {
        try {
            $error = false;
            // Run Service Credit Card Processing
            $request = Mage::getModel('cybersource/payment')->assembleAuthorizeProfileRequest($payment, $amount);
            //$request->appendMerchantDefinedDataFields($payment);
            $response = $this->getSoapClient()->getResponse($request);
            Mage::getModel('cybersource/payment')->saveTransactionProfileDetails($payment, $response);
            Mage::helper('cybersource/payment')->addComment($payment, $response);

            /**
             * Clarification:
             * response-> reasonCode is returned by CyberSource and used here
             * response -> ccAuthReply -> reasonCode is returned by the processor and NOT used here
             *
             * */
            $cybersourceReasonCode = $response->getReasonCode();
            if ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_SUCCESS) {
                
            } elseif ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_CVN_DECLINE) {
                Mage::helper('cybersource/payment')->addCommentForSoftReject($payment, $response, $cybersourceReasonCode);
                Mage::register('soft-reject', 'cvn-reject');
            } elseif ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_AVS_DECLINE) {
                Mage::helper('cybersource/payment')->addCommentForSoftReject($payment, $response, $cybersourceReasonCode);
                ;
                Mage::register('soft-reject', 'avs-reject');
            } else {
                //Run Service: Decision Manager
                if ($this->getConfigData('dm_enabled')) {
                    //Run Service: Decision Manager
                    $error = Mage::getModel('cybersource/decision')->manageDecision($response, $payment, 'authorize');
                } else {
                    $error = Mage::helper('cybersource')->__('Please review your address and credit card information.');
                }
            }

            // Service: Payment Tokenization
            //Mage::getModel('cybersource/tokenization')->tokenizePayment($response, $request, $payment);
            // Service: Delivery Address Verification
            if ($payment->getOrder()->getShippingAddress()) {
                Mage::getModel('cybersource/dav')->verifyDeliveryAddress($payment);
            }
        } catch (Exception $e) {
            $error = $this->__('Gateway request error: ' . $e->getMessage() . Mage::helper('cybersource')->prepareSignature()); //@internal TALK-MESSAGE
            //$error = $this->__('Payment gateway  error. Please try again.');
        }
        if ($error) {
            Mage::throwException($this->__('Payment error. ') . $error); //@internal TALK-MESSAGE
        }
        return $this;
    }

    //REFUND PROCESS FOR CYBERSOURCE=================//

    public function refund(Varien_Object $payment, $requestedAmount) {
        if (!$this->canRefund()) {
            Mage::throwException(Mage::helper('payment')->__('Refund action is not available.'));
        } else {
            return $this->refundProfile($payment, $requestedAmount);
        }
    }

    /**
     * Refund the amount with transaction id
     *
     * @param Mage_Payment_Model_Info $payment
     * @param decimal $amount
     * @return Mage_Paygate_Model_Authorizenet
     * @throws Mage_Core_Exception
     */
    public function refundProfile(Varien_Object $payment, $requestedAmount) {
        //parent::refund($payment, $amount);
        $error = false;
        if ($payment->getParentTransactionId() && $payment->getCybersourceRequestToken()) {
            $request = Mage::getModel('cybersource/soap_request_credit', array(
                        Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID => $payment->getParentTransactionId()
            ));
            
            //$request->addAmountData($requestedAmount);
            //$request->addCurrencyData($payment);
            $request->getRequest()->purchaseTotals->grandTotalAmount = $requestedAmount;
            //$request->getRequest()->purchaseTotals->grandTotalAmount = $requestedAmount;
            $request->getRequest()->purchaseTotals->currency = $payment->getOrder()->getData('order_currency_code');
            $request->getRequest()->ccCreditService->captureRequestID = $payment->getParentTransactionId();
            //$request->getRequest()->purchaseTotals->currency = $payment->getOrder()->getData('order_currency_code');
            //$request->addItemsData($payment->getData('creditmemo')->getAllitems());
//            $request->getRequest()->card->cardType = 'VI';
//            	
//$request->getRequest()->billTo->city = 'New York';
//
//$request->getRequest()->billTo->country = 'US';
//
//$request->getRequest()->billTo->email = 'awahid@pixafy.com';
//
//$request->getRequest()->billTo->firstName = 'Abu';
//
//$request->getRequest()->billTo->lastName = 'Wahid';
//
//$request->getRequest()->billTo->postalCode = '10019';
//
//$request->getRequest()->billTo->state = 'NY';
//
//$request->getRequest()->card->accountNumber = '4111111111111111';
//
//$request->getRequest()->card->expirationMonth = '03';
//
//$request->getRequest()->card->expirationYear = '2017';
           // echo "<pre>";
           // var_dump($request->getRequest());
            //$request->addItemsData($payment->getData('creditmemo')->getAllitems());

        } else {
            // @todo implement if transaction ID or cybersource token is not set
        }

        try {
            $response = $this->getSoapClient()->getResponse($request);
            //echo "<pre>";
            //var_dump($response);
            //die();
            if ($response->isSuccess()) {
                $payment->setLastTransId($response->getRequestId());
                $payment->setLastCybersourceRequestToken($response->getRequestToken());
                $payment->setCcTransId($response->getRequestId());
                $payment->setTransactionId($response->getRequestId());
                $payment->setIsTransactionClosed(0);
                $payment->setCybersourceRequestToken($response->getRequestToken());
            } else {
                $error = $response->getMessage(); // @internal TALK-MESSAGE
            }
        } catch (Exception $e) {
            $error = $this->__('Gateway request error: ' . $e->getMessage()); // @internal TALK-MESSAGE
        }

        if ($error) {
            // @internal TALK-MESSAGE
            Mage::throwException($error);
        }

        return $this;
    }

    //REFUND PROCESS FOR CYBERSOURCE=================//
}
