<?php
class Pixafy_PixCybersource_Model_Soap_Request_Authorize
extends Cybersource_PaymentManagement_Model_Soap_Request_Authorize
{
    /*
     * Override the addPayment method,  adding the the flag to check if
     * the order contains shipping address info, process with or without the
     * shipping address info to void CyberSource error
     */
    public function addPayment(Varien_Object $payment)
    {
        $data = $this->createBillingAddressData($payment);
        $this->addBillingAddressData($data);

        //check if shipping address info exists
        if($payment->getOrder()->getShippingAddress()){
            $data = $this->createShippingAddressData($payment);
            $this->addShippingAddressData($data);
        }

        $data = $this->createCreditCardData($payment);
        $this->addCreditCardData($data);

        $data = $this->createPurchaseTotalsData($payment);
        $this->addPurchaseTotalsData($data);

        $data = $this->createBusinessRulesData();
        $this->addBusinessRulesData($data);

        //if customer logged added it to request
        $customerId = $payment->getOrder()->getCustomerID();
        if (isset($customerId) && !empty($customerId)) {
            $request = $this->getRequest();
            $request->billTo->customerID = $customerId;
        }

        parent::addPayment($payment);
        return $this;
    }

	function addProfilePayment($payment){
		$data = $this->createPurchaseTotalsData($payment);
		$this->addPurchaseTotalsData($data);
		$this->setData('payment', $payment);
		return $this;
	}
	
	function addProfile($payment){
		$request = $this->getRequest();
		$request->recurringSubscriptionInfo = new stdClass();
		$request->recurringSubscriptionInfo->frequency = 'on-demand';
        $request->recurringSubscriptionInfo->subscriptionID = $payment->getCcProfile();
        return $this;
	}
	
}