<?php
class Pixafy_PixCybersource_Model_Soap_Request_Retrieve extends Cybersource_PaymentManagement_Model_Soap_Request
{
	
	protected function _construct()
	{
		$request = $this->getRequest();
		$request->paySubscriptionRetrieveService = new stdClass();
		$request->paySubscriptionRetrieveService->run = 'true';
	}
	
	function setSubscriptionId($token){
		$request = $this->getRequest();
		$request->recurringSubscriptionInfo = new stdClass();
		$request->recurringSubscriptionInfo->subscriptionID = $token;
	}
	
	
}