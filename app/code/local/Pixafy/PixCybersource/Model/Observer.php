<?php 
class Pixafy_PixCybersource_Model_Observer extends AnotherClass {
	
	function checkOrders() {
		$salesModel=Mage::getModel("sales/order");
        $salesCollection = $salesModel->getCollection()
                            //->addAttributeToFilter('customer_id', array('eq' => 63))
                            ->addFieldToFilter('status',array('eq' => 'payment_review'))
                            ;
        foreach($salesCollection as $sC)
        {
            try {
                $orderId = (int) $sC->getEntityId();
                if($orderId)
                {
                    $order = $sC;
                    if($orderId) {
                        Mage::getModel('cybersource/conversion')->getDecisionConversion($orderId);
                        
                        if($order->getState() !==  'payment_review')
                        {
                            Mage::log("order:" . $orderId . " - updated",null,'cybersource.log');
                        }
                        else {
                            Mage::log("order:" . $orderId . " - not updated",null,'cybersource.log');
                        }
                    }
                        
                    
                    $order->save();
                }
                    
                    
                
            } catch (Mage_Core_Exception $e) {
               Mage::log($e->getMessage() . "order:" .  $orderId,null,'cybersource.log');
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Failed to download the conversion report.'));
                Mage::log($e->getMessage() . "order:" .  $orderId,null,'cybersource.log');
            }
        }
	}
}


?>