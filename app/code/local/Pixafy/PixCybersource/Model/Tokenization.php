<?php

/**
 * CyberSource PaymentManagement Tokenization Model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Pixafy_PixCybersource_Model_Tokenization extends Cybersource_PaymentManagement_Model_Tokenization {

    /**
     * Save payment token (Subscription ID ) at both customer-level and order-level
     *
     * @param $payment
     * @param $responseProfile
     *
     * @return Cybersource_PaymentManagement_Model_Cybersource
     */
    public function saveSubscripitonId($payment, $responseProfile) {

        if (Mage::getStoreConfig('payment/cybersource_payment_tokenization/enabled') == 1) {
            $newSubscriptionId = $responseProfile->getResponse()->paySubscriptionCreateReply->subscriptionID;
            $orderNumber = $payment->getOrder()->getData('increment_id');
            $customerId = $payment->getOrder()->getData('customer_id');
            $customer = Mage::getModel('customer/customer')->load($customerId);
            $customerSubscriptionId = $customer->getcybersource_subscription_id();

            $ccParams = Mage::app()->getFrontController()->getRequest()->getParams();
            $paymentprofile = Mage::getModel('pixcybersource/paymentprofile');
            if ($ccParams['payment']['cc_profile'] == '') {
                $cybersourceprofile = array(
                    "customer_id" => $customerId,
                    "subscription_id" => $newSubscriptionId,
                    "cc_type" => $ccParams['payment']['cc_type'],
                    "cc_lastfour" => substr($ccParams['payment']['cc_number'], -4),
                    "cc_exp_month" => $ccParams['payment']['cc_exp_month'],
                    "cc_exp_year" => $ccParams['payment']['cc_exp_year']
                );
                //Save payment info in pixcybersource_paymentprofile table
                $paymentprofile->addData($cybersourceprofile)->save();
            }
            if (empty($customerSubscriptionId)) {
                $customerSubscriptionId = $customerSubscriptionId . 'order:' . $orderNumber . '=token:' . $newSubscriptionId;
            } else {
                $customerSubscriptionId = $customerSubscriptionId . ',order:' . $orderNumber . '=token:' . $newSubscriptionId;
            }
            $customer->setcybersource_subscription_id($customerSubscriptionId)->save();
        }

        return $this;
    }

}
