<?php

class Pixafy_PixCybersource_Model_Payment extends Cybersource_PaymentManagement_Model_Payment {

    public function assembleAuthorizeProfileRequest(Varien_Object $payment, $amount) {
        $request = Mage::getModel('cybersource/soap_request_authorize');
        $request->addProfilePayment($payment);
        $request->addProfile($payment);
        $request->addAmountData($amount);
        $request->addItemsData($payment->getOrder()->getAllVisibleItems());
        return $request;
    }

    public function assembleCaptureProfileRequest(Varien_Object $payment, $amount) {
        
        /**
         * Clarification:
         * Request Token is a required API field that you will  receive
         * in the reply of all services and send in the API request of most
         * follow-on services. It has nothing to do with payment tokenization.
         * The data used for payment tokenization is called Subscription ID.
         * */
        
        $cybersourceRequestToken = $payment->getCybersourceRequestToken();
        $parentTransactionId = $payment->getParentTransactionId();
        
        /**
         * If there is already an authorization
         * Else if there is not any authorization
         * */
        if ($parentTransactionId && $cybersourceRequestToken) {
            $request = Mage::getModel('cybersource/soap_request_capture', array(
                        Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID => $parentTransactionId,
                        Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN => $cybersourceRequestToken,
                        'order_id' => $payment->getOrder()->getIncrementId()
            ));
            $request->addCurrencyData($payment);
        } else {           
            $request = Mage::getModel('cybersource/soap_request_capture');            
            $request->addProfilePayment($payment);
            $request->addProfile($payment);
            $request->addPayment($payment);
            $request->addItemsData($payment->getOrder()->getAllVisibleItems());
        }
        $request->addAmountData($amount);
        return $request;
    }

    public function saveTransactionProfileDetails($payment, $response) {
        parent::saveTransactionDetails($payment, $response);
    }

}
