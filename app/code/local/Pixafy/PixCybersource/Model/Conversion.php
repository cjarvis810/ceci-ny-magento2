<?php

class Pixafy_PixCybersource_Model_Conversion extends Cybersource_PaymentManagement_Model_Conversion
{
	
	    /**
     * Download conversion report for all payment_review orders and
     * update orders accordingly
     *
     * @see reviewPaymentAction method of the core OrderController class
     *
     *
     * @return void
     */
    public function getAllDecisionConversions() {
        $conversionReport = Mage::getModel('cybersource/reports')->fetchOnDemandConversionReport();
        if($conversionReport) {
            if(isset($conversionReport->Conversion) ) {
                try {
                    foreach ($conversionReport->Conversion as $conversion) {
                        $tid = explode(" ", (string) $conversion['RequestID']);
                        $transactionId = (string) $tid[0];
                        $newDecision = (string) $conversion->NewDecision;
                        $mRn = explode(" ", (string) $conversion['MerchantReferenceNumber']);
                        $incrementId = (string) $mRn[0];
                        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
                        $payment = $order->getPayment();
                        $paymentMethod = $payment->getMethod();
                        $status = $order->getStatus();
                        if($status == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
                            $IsAlreadyProcessed = Mage::registry($order->getId());
                            if(!$IsAlreadyProcessed) {
                                $this->reflectConversion($newDecision, $payment, $transactionId);
                            }
                            Mage::unregister($order->getId());
                        }
                    }
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
    }

    /**
     * Process the conversion decision
     *
     * @param $newDecision
     * @param $payment
     * @param $transactionId
     *
     * @return void
     */
    protected function reflectConversion($newDecision, $payment, $transactionId)
    {
        if($newDecision == Cybersource_PaymentManagement_Helper_Decision::ACCEPT) {
              $this->acceptReviewedPayment($payment, $transactionId);
        } elseif($newDecision == Cybersource_PaymentManagement_Helper_Decision::REJECT) {
            $this->rejectReviewedPayment($payment, $transactionId);
        }
    }

    /**
     * Reject the payment and update order details
     *
	 * Updated to add payment fail email
     * @param $payment
     * @param $transactionId
     *
     * @return void
     */
    protected function rejectReviewedPayment($payment, $transactionId) {
        $order = $payment->getOrder();
        $transaction = $payment->getTransaction($transactionId);
        if($this->paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE) {
            $transaction->setIsClosed(true);
            $order->addRelatedObject($transaction);
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true);
            $order->save();
        } elseif($this->paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
            $transaction->setAdditionalInformation($this->_isTransactionFraud, true);
            $transaction->setIsClosed(true);
            $order->addRelatedObject($transaction);
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true);
            $order->save();
        } else {
            $issue = Mage::helper('cybersource')->__(' Unexpected payment action method. ') ;
            throw new Cybersource_PaymentManagement_Exception($issue);
        }
		$quote = $order->getQuote() ?: Mage::getModel("sales/quote")->getCollection()->addFieldToFilter('entity_id', $order->getQuoteId())->getFirstItem();
		Mage::helper('pixcybersource')->sendPaymentFailedEmail(($quote ?: $order), Mage::helper('cybersource')->__('Payment rejected. Please try again after reviewing your address and credit card information.'), 'onepage', $order->getIncrementId());
    }
}
