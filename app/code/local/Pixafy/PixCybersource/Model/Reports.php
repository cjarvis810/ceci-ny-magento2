<?php
/**
 * Reporting API Model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Pixafy_PixCybersource_Model_Reports extends Cybersource_PaymentManagement_Model_Reports
{
    /**
     * Prepares the query for POST request
     *
     * @return bool|object
     */
    protected function formulateQuery($type, $date = null) {
		if (Mage::getStoreConfig('cybersource/decision_manager/reporting_api_username')) {
            $reportingApiUser = Mage::getStoreConfig('cybersource/decision_manager/reporting_api_username');
        }
        if (Mage::getStoreConfig('cybersource/decision_manager/reporting_api_password')) {
            $reportingApiPassword = Mage::getStoreConfig('cybersource/decision_manager/reporting_api_password');
        }
        if (Mage::getStoreConfig('cybersource/decision_manager/reporting_api_mode') == 'production') {
            $reportingApiModeServer = 'https://ebc.cybersource.com/ebc/';
        } elseif (Mage::getStoreConfig('cybersource/decision_manager/reporting_api_mode') == 'test') {
            $reportingApiModeServer = 'https://ebctest.cybersource.com/ebctest/';
        }
        $params = array('merchantID' => Mage::getStoreConfig('cybersource/account/merchant'),
                'format' => 'xml',
                'username' => $reportingApiUser,
                'password' => $reportingApiPassword);
        switch ($type) {
            case self::REPORT_TYPE_ONDEMAND:
                // The interval must not exceed 24 hours, and
                // you must use the UTC (GMT) time
                $gmtNow = Mage::getModel('core/date')->gmtDate();
                $nowDateTime = new DateTime($gmtNow);
                $params['endDate'] = $nowDateTime->format("Y-m-d");
                $params['endTime'] = $nowDateTime->format("H:i:s");
                $yesterdayDateTime = $nowDateTime->modify('-23 hour')->modify('+1 minute');
                $params['startDate'] = $yesterdayDateTime->format("Y-m-d");
                $params['startTime'] = $yesterdayDateTime->format("H:i:s");
                $serverAddress = $reportingApiModeServer . 'ConversionDetailReportRequest.do';
                $uri = Zend_Uri::factory($serverAddress);
                $uri->setQuery($params);
                $httpClient = new Varien_Http_Client($uri);
                return $httpClient;
                break;
            case self::REPORT_TYPE_DAILY:
                if (is_null($date)) {
                    $date = date('Y/m/d', strtotime('yesterday'));
                }
                $serverAddress = $reportingApiModeServer . 'DownloadReport' . DS . $date . DS
                . trim($_merchant_id) . DS . 'ConversionDetailReport.xml';
                $uri = Zend_Uri::factory($serverAddress);
                $httpClient = new Varien_Http_Client($uri);
                return $httpClient;
                break;
            default:
                return false;
                break;
        }

    }
}