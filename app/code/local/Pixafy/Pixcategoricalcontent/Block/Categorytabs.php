<?php

class Pixafy_Pixcategoricalcontent_Block_CategoryTabs extends Mage_Core_Block_Template {

    /**
     * The media url for images
     * @var type 
     */
    protected $mediaUrl;

    /**
     * The content limit.
     * @var int 
     */
    protected $contentLimit;

    /**
     * Our category Constant
     * @var type 
     */
    protected $categoryConstant = false;

    /**
     * Our content collection
     * @var type 
     */
    protected $contentCollection;

    /**
     * The container settings 
     * @var type 
     */
    protected $containerSettings = false;

    /**
     * The year we are filtering for
     * @var type 
     */
    protected $yearFilter = null;
    
    /**
     * Content images sorted by id
     * @var type 
     */
    protected $contentImages = array();

    public function _construct() {
        $this->mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        parent::_construct();
    }

    /**
     * Set ,pde
     * @param type $mode
     */
    public function setMode($mode) {
        $this->mode = $mode;
    }

    /**
     * Add our default values
     */
    protected function _prepareLayout() {
        $this->setTemplate('pixafy/pixcategoricalcontent/categorytabs.phtml');
        $this->getLayout()->getBlock('head')->addJs('pixafy/pixcategoricalcontent/main.js');
        $this->getYearFilter();
        parent::_prepareLayout();
    }

    /**
     * Gets content categories.
     * Careful, will return all if not filtered.  
     * @return Pixafy_Pixcategoricalcontent_Model_Resource_Content_Collection
     */
    public function getContentCategories() {
        $container = $this->getContainer();
        $collection = Mage::getResourceModel('pixcategoricalcontent/category_collection');
        if ($container) {
            $collection->filterByContainerConstant($container); //this will also join container
            $collection->orderForDisplay();
        }
        return $collection;
    }

    /**
     * Get the current page
     * @return type
     */
    public function getCurrentPage() {
        return $this->getRequest()->getParam('p') ? : '1';
    }

    /**
     * Get a page worth of content
     * @return type
     */
    public function getContentCollection() {
        if ($this->contentCollection) {
            return $this->contentCollection;
        }
        $this->contentCollection = Mage::getResourceModel('pixcategoricalcontent/content_collection');
        $this->setupFilterByContainer();
        $this->setupFilterByCategory();
        $this->setupFilterByYears();
        $this->setupExtraImages();
        $this->setupPager();
        return $this->contentCollection;
    }

    /**
     * Filter by container if present
     */
    protected function setupFilterByContainer() {
        $container = $this->getContainer();
        if ($container) {
            $this->contentCollection->filterByContainerConstant($container);
        }
    }

    /**
     * If we have category filter
     */
    protected function setupFilterByCategory() {
        $category = $this->getCategoryConstant();
        if ($category && $category !== 'ALL') {
            $this->contentCollection->filterByCategoryConstant($category);
        }
    }

    /**
     * list of years for archving by year
     */
    protected function setupFilterByYears() {
        $settings = $this->getContainerSettings();
        if (!$settings["archive_by_year"]) {
            return;
        }
        $this->contentCollection->filterByYear($this->getYearFilter());
    }

    protected function setupExtraImages() {
        $settings = $this->getContainerSettings();
        if ($settings["use_extra_images"]) {
            $this->contentCollection->joinExtraContentInfo();
        }
    }

    /**
     * We're going to make a small query to get the sttings so as not to disturb the pager
     * @staticvar boolean $settings
     * @return boolean
     */
    public function getContainerSettings() {
        if ($this->containerSettings !== false) {
            return $this->containerSettings;
        }        
        $this->containerSettings = Pixafy_Pixcategoricalcontent_Model_ContentSettings::getContainerSettingsByConstant($this->getContainer());         
        return $this->containerSettings;        
        
    }
    
    /**
     * Gets category
     * @return type
     */
    public function getCategoryConstant() {
        if ($this->categoryConstant !== false) {
            return $this->categoryConstant;
        }
        $this->categoryConstant = $this->determineCategoryConstant();
        return $this->categoryConstant;
    }

    /**
     * Will attempt to retrieve category first from xml params, 2nd from url
     * @return type
     */
    protected function determineCategoryConstant() {
        $cat = $this->getCategory();
        if ($cat) {
            return $cat;
        }
        return $this->getRequest()->getParam('category');
    }

    /**
     * Inits pager
     */
    public function setupPager() {
        $limit = (int) $this->getContentLimit() ? : 6;
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setTemplate("pixafy/pixcategoricalcontent/pager.phtml");
        $pager->setAvailableLimit(array($limit => $limit));
        $pager->setCollection($this->getContentCollection());
        $this->setChild('pager', $pager);
    }

    /**
     * Get year, first from the request, then try from the xml config, then this year. 
     * @staticvar boolean $year
     * @return boolean
     */
    public function getYearFilter() {
        if ($this->yearFilter) {
            return $this->yearFilter;
        }
        $this->yearFilter = $this->getRequest()->getParam('y', null);
        if ($this->yearFilter) {
            return $this->yearFilter;
        }
        $this->yearFilter = parent::getYearFilter();
        if ($this->yearFilter) {
            return $this->yearFilter;
        }
        $this->yearFilter = 'ALL';
        return $this->yearFilter;
    }
    
    public function preloadContentImages($contentIds){        
        foreach( Mage::getResourceModel('pixcategoricalcontent/content_image_collection')->filterByContentIds($contentIds) as $image){
            if( !isset( $this->contentImages[$image->getContentId()] )){
                $this->contentImages[$image->getContentId()] = array();
            }
            $this->contentImages[$image->getContentId()][] = $image;
        }        
    }
    
    public function getCurrentContentImages(){
        $contentId = $this->getContentId();
        $return = isset($this->contentImages[$contentId]) ? $this->contentImages[$contentId] : array();
        return $return;
    }

}
