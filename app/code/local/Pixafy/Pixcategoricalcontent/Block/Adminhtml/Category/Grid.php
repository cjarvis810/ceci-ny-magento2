<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('category_id');
        $this->setDefaultSort('category_constant');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $this->setCollection(Mage::getResourceModel('pixcategoricalcontent/category_collection')->joinContainer());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
//we don't need idf        
//        $this->addColumn('category_id', array(
//            'header' => 'Category ID',
//            'align' => 'right',
//            'width' => '50px',
//            'index' => 'category_id',
//        ));
        $this->addColumn('name', array(
            'header' => 'Category Name',
            'align' => 'left',
            'index' => 'category_name',
        ));
        $this->addColumn('container_name', array(
            'header' => 'Container Name',
            'align' => 'left',
            'index' => 'container_name',
        ));        
        $this->addColumn('category_constant', array(
            'header' => 'Category Constant',
            'align' => 'left',
            'index' => 'category_constant',
        ));
        $this->addColumn('category_position', array(
            'header' => 'Category Position',
            'width' => '50px',
            'align' => 'left',
            'index' => 'category_position',
        ));
        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row){        
        return $this->getUrl('*/*/edit', array('category_id'=>$row->getId()));
    }
}