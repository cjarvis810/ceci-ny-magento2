<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function _construct() {
        parent::_construct();
        $editMode = Mage::registry('category_model');        
        $this->_objectId = 'category_id';
        $this->_blockGroup = 'pixcategoricalcontent';
        $this->_controller = 'adminhtml_category';
        $this->_mode = 'edit';
        $this->_headerText = $editMode ? 'Edit Category' : "New Category";
        $this->_updateButton('save', 'label', 'Save Category');
        $this->_updateButton('delete', 'label', 'Delete Category');
    }

}
