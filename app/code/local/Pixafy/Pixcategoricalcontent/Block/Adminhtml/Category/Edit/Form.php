<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Category_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * If we have a model here, it means we are editing / if not it is 'new'
     * @var type 
     */
    protected $model = null;

    public function __construct(array $args = array()) {
        if ($model = Mage::registry('category_model')) {
            $this->model = $model->getData();
        }
        parent::__construct($args);
    }

    protected function _prepareForm() {

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('category_id' => $this->getRequest()->getParam('category_id'))),
            'method' => 'post',
                )
        );

        $legend = $this->model ? "Edit information for Category: " . $this->model["category_name"] : "Enter information for new Category";

        $fieldset = $form->addFieldset('category_form', array('legend' => $legend));
        $fieldset->addField('category_name', 'text', array(
            'label' => 'Category Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'category_name',
        ));
        
        $options = array();
        
        foreach (Mage::getResourceModel("pixcategoricalcontent/container_collection") as $c) {
            $options[$c->getId()] = $c->getContainerName();
        }
        
        $fieldset->addField('container_id', 'select', array(
            'label' => 'Container Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'container_id',
            'values' => $options,
        ));
        
        $fieldset->addField('category_constant', $this->model ? 'label' : 'text', array(
            'label' => 'Category Constant',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'category_constant'
        ));       
        
        $fieldset->addField('category_position', 'text', array(
            'label' => 'Category Sort Position',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'category_position'            
        ));       
        
        

        $form->setUseContainer(true);
        
        $this->setForm($form);
        if ($this->model) {
            $form->setValues($this->model);
        }

        return parent::_prepareForm();
    }
    
    

}
