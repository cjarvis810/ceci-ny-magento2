<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Container_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('container_id');
        $this->setDefaultSort('container_constant');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $this->setCollection(Mage::getResourceModel('pixcategoricalcontent/container_collection'));
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        //No need for id here
//        $this->addColumn('container_id', array(
//            'header' => 'Container ID',
//            'align' => 'right',
//            'width' => '50px',
//            'index' => 'container_id',
//        ));
        $this->addColumn('container_name', array(
            'header' => 'Container Name',
            'align' => 'left',
            'index' => 'container_name',
        ));
        $this->addColumn('container_constant', array(
            'header' => 'Container Constant',
            'align' => 'left',
            'index' => 'container_constant',
        ));               
        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row){        
        return $this->getUrl('*/*/edit', array('container_id'=>$row->getId()));
    }
}