<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Container_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function _construct() {
        parent::_construct();
        $editMode = Mage::registry('container_model');        
        $this->_objectId = 'container_id';
        $this->_blockGroup = 'pixcategoricalcontent';
        $this->_controller = 'adminhtml_container';
        $this->_mode = 'edit';
        $this->_headerText = $editMode ? 'Edit Container' : "New Container";
        $this->_updateButton('save', 'label', 'Save Container');
        $this->_updateButton('delete', 'label', 'Delete Container');
    }

}
