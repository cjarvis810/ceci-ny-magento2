<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Container_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * If we have a model here, it means we are editing / if not it is 'new'
     * @var type 
     */
    protected $model = null;

    public function __construct(array $args = array()) {
        if ($model = Mage::registry('container_model')) {
            $this->model = $model->getData();
        }
        parent::__construct($args);
    }

    protected function _prepareForm() {

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('container_id' => $this->getRequest()->getParam('container_id'))),
            'method' => 'post',
                )
        );

        $legend = $this->model ? "Edit information for Container: " . $this->model["container_name"] : "Enter information for new Container";

        $fieldset = $form->addFieldset('container_form', array('legend' => $legend));
        $fieldset->addField('container_name', 'text', array(
            'label' => 'Container Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'container_name',
        ));

        $fieldset->addField('container_constant', $this->model ? 'label' : 'text', array(
            'label' => 'Container Constant',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'container_constant'
        ));

        $fieldset->addField('container_settings', 'textarea', array(
            'label' =>'Container Settings',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'container_settings',                                                
            'after_element_html' => '<small>This is a json encoded list. Please see http://www.w3schools.com/json/ regarding formatting json</small>'
        ));

        $form->setUseContainer(true);

        $this->setForm($form);
        if ($this->model) {
            $form->setValues($this->model);
        }

        return parent::_prepareForm();
    }

}
