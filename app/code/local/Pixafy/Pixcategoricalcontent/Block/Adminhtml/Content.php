<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Content extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_content';
        $this->_blockGroup = 'pixcategoricalcontent';
        $this->_headerText = 'Content';
        $this->_addButtonLabel = 'Add Content';
        parent::__construct();        
    }
   
}
