<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_category';
        $this->_blockGroup = 'pixcategoricalcontent';
        $this->_headerText = 'Content Categories';
        $this->_addButtonLabel = 'Add Category';
        parent::__construct();
    }
   
}
