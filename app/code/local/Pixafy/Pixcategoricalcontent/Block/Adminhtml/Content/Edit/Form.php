<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Content_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * If we have a model here, it means we are editing / if not it is 'new'
     * @var type 
     */
    protected $model = null;

    public function __construct(array $args = array()) {
        if ($model = Mage::registry('content_model')) {
            $this->model = $model->getData();
        }
        parent::__construct($args);
    }

    protected function _prepareForm() {


        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('content_id' => $this->getRequest()->getParam('content_id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
                )
        );

        $legend = $this->model ? "Edit information for Content: " . $this->model["content_title"] : "Enter information for new Content";

        $fieldset = $form->addFieldset('category_form', array('legend' => $legend));

        $fieldset->addField('content_title', 'text', array(
            'label' => 'Content Title',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'content_title',
        ));

        $fieldset->addField('content_mini_title', 'text', array(
            'label' => 'Content Mini Title',
            'required' => true,
            'name' => 'content_mini_title',
        ));
        
        $options = array();

        foreach (Mage::getResourceModel("pixcategoricalcontent/container_collection") as $c) {
            $options[$c->getId()] = $c->getContainerName();
        }

        $fieldset->addField('container_id', 'select', array(
            'label' => 'Container Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'container_id',
            'values' => $options,
        ));

        $options = array();

        foreach (Mage::getResourceModel("pixcategoricalcontent/category_collection") as $c) {
            $options[$c->getId()] = $c->getCategoryName();
        }

//        $fieldset->addField('category_id', 'select', array(
//            'label' => 'Category Name',
//            'class' => 'required-entry',
//            'required' => true,
//            'name' => 'category_id',
//            'values' => $options,
//        ));
        $options = array();
        foreach (Mage::getResourceModel("pixcategoricalcontent/category_collection") as $c) {
            array_push($options, array('value'=>$c->getId(),'label'=>$c->getCategoryName()));
        }
        foreach (Mage::getResourceModel("pixcategoricalcontent/content_category_collection")    ->filterbyContentId($this->getRequest()->getParam('content_id')) as $c) {
            $values[]=$c->getCategoryId();
        }
        $fieldset->addField('category_id', 'multiselect', array(
            'label' => 'Category Name',
            'class' => 'required-entry',
            'required' => true,
            'name' =>'category_id[]',
            'values' =>$options,
            'value'=>$values
        ));

        $fieldset->addField('content_date', 'date', array(
            'label' => 'Content Date',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'content_date',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' =>  Varien_Date::DATETIME_INTERNAL_FORMAT,
            'format' => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'time' => true
        ));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

        $wysiwygConfig->addData(array(
            'add_variables' => false,
            'add_widgets' => true,
            'add_images' => true,
            'directives_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
            'directives_url_quoted' => preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive')),
            'widget_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/widget/index'),
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
            'files_browser_window_width' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_width'),
            'files_browser_window_height' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_height')
        ));

        $fieldset->addField('content_description', 'editor', array(
            'label' => 'Content Description',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'content_description',
            'config' => $wysiwygConfig,
            'style' => 'width:800px; height:50px;',
        ));
        
                
        $fieldset->addField('content_short_description', 'editor', array(
            'label' => 'Content Short Description',
            'required' => false,
            'config' => $wysiwygConfig,                       
            'name' => 'content_short_description',
            'style' => 'width:800px; height:50px;',
        ));

        $fieldset->addField('content_image', 'image', array(
            'label' => 'Content Image',
            'required' => false,
            'name' => 'content_image'
        ));

        $fieldset->addField('content_thumbnail', 'image', array(
            'label' => 'Content Thumbnail',
            'required' => false,
            'name' => 'content_thumbnail'
        ));

        $fieldset->addType('additional_images', 'Pixafy_Pixcategoricalcontent_Block_Adminhtml_Content_Edit_Images');        
        
        $fieldset->addField('additional_images', 'additional_images', array(
            'label' => 'Additional Images',            
            'required' => false,            
            'bold' => true,
            'label_style' => '',            
            'model_id' => $this->model ? $this->model['content_id'] : false
        ));

        $form->setUseContainer(true);

        $this->setForm($form);
        if ($this->model) {
            $form->setValues($this->model);
        }

        return parent::_prepareForm();
    }

}
