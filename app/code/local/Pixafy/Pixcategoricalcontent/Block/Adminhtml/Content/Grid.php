<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Content_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('content_id');
        $this->setDefaultSort('content_date');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $this->setCollection(Mage::getResourceModel('pixcategoricalcontent/content_collection')->joinContainer());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
// we don't need the id        
//        $this->addColumn('content_id', array(
//            'header' => 'Content ID',
//            'align' => 'right',
//            'width' => '50px',
//            'index' => 'content_id',
//        ));
        
        $this->addColumn('container_name', array(
            'header' => 'Container Name',
            'align' => 'left',
            'index' => 'container_name',
        ));
        
//        $this->addColumn('category_name', array(
//            'header' => 'Category Name',
//            'align' => 'left',
//            'index' => 'category_name',
//        ));
                
        $this->addColumn('content_title', array(
            'header' => 'Content Title',
            'align' => 'left',
            'index' => 'content_title',
        ));
        
        $this->addColumn('content_mini_title', array(
            'header' => 'Content Mini Title',
            'align' => 'left',
            'index' => 'content_mini_title',
        ));
        
        $this->addColumn('content_date', array(
            'header' => 'Content Date',
            'align' => 'left',
            'index' => 'content_date',
            "type" => 'date',
            'format' => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM)
        ));
        
//        $this->addColumn('created_date', array(
//            'header' => 'Created Date',
//            'align' => 'left',
//            'index' => 'created_at',
//        ));
//        
//        $this->addColumn('updated_date', array(
//            'header' => 'Updated Date',
//            'align' => 'left',
//            'index' => 'updated_at',
//        ));
//        
        

        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row){        
        return $this->getUrl('*/*/edit', array('content_id'=>$row->getId()));
    }
}