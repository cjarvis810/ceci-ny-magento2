<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Content_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function _construct() {
        parent::_construct();
        $editMode = Mage::registry('content_model');
        $this->_objectId = 'content_id';
        $this->_blockGroup = 'pixcategoricalcontent';
        $this->_controller = 'adminhtml_content';
        $this->_mode = 'edit';
        $this->_headerText = $editMode ? 'Edit Content' : "New Content";
        $this->_updateButton('save', 'label', 'Save Content');
        $this->_updateButton('delete', 'label', 'Delete Content');
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $head = $this->getLayout()->getBlock('head');
            $head->setCanLoadExtJs(true);
            $head->setCanLoadTinyMce(true);
            $head->addItem("js", "mage/adminhtml/variables.js");
            $head->addItem("js", "mage/adminhtml/wysiwyg/widget.js");
            $head->addItem("js", "lib/flex.js");
            $head->addItem("js", "lib/FABridge.js");
            $head->addItem("js", "mage/adminhtml/flexuploader.js");            
            $head->addItem("js", "mage/adminhtml/browser.js");
            $head->addItem("js", "prototype/window.js");
            $head->addItem("js_css", "prototype/windows/themes/default.css");
            $head->addCss('lib/prototype/windows/themes/magento.css');
        }
    }

}
