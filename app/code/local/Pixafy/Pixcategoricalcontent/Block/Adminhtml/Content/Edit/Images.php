<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Content_Edit_Images extends Varien_Data_Form_Element_Abstract {

    public function __construct($attributes = array()) {
        parent::__construct($attributes);
    }

    public function getElementHtml() {       
       return $this->buildForm()->toHtml();       
    }

    public function buildForm() {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_image_form',
            'method' => 'post',
            'enctype' => 'multipart/form-data'
                )
        );

        $topLevel = $form->addFieldset('content_images', array('legend' => 'Add additional Images'));

        $fieldset2 = $topLevel->addFieldset('new_image_fields', array('legend' => 'New Image'));
         
        $fieldset2->addField('new_content_image', 'image', array(
            'label' => 'New Content Image',
            'required' => false,            
            'name' => 'content_extra_images[0]'
        ));
        
        $fieldset2->addField('content_image_position', 'text', array(
            'label' => 'Content Image Display Position',
            'required' => false,            
            'name' => "content_image_position[0]"
        ));


        foreach (Mage::getResourceModel('pixcategoricalcontent/content_image_collection')->filterByContentId($this->getModelId()) as $image) {
            $fieldset2 = $topLevel->addFieldset('image' . $image->getId(), array('legend' => 'Edit Image'));
            $fieldset2->addField('content_image_' . $image->getId(), 'image', array(
                'label' => 'Content Image',
                'required' => false,
                'value' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $image->getContentImage(),
                'name' => 'content_extra_images[' . $image->getId() . ']'
            ));
            $fieldset2->addField('content_image_position_' . $image->getId(), 'text', array(
                'label' => 'Content Image Display Position',
                'required' => false,
                'value' => $image->getContentImagePosition(),
                'name' => "content_image_position[{$image->getId()}]"
            ));
        };
        return $form;
    }

    public function getLabelHtml($idSuffix = '') {
        if (!is_null($this->getLabel())) {
            $html = '<label for="' . $this->getHtmlId() . $idSuffix . '" style="' . $this->getLabelStyle() . '">' . $this->getLabel()
                    . ( $this->getRequired() ? ' <span class="required">*</span>' : '' ) . '</label>' . "\n";
        } else {
            $html = '';
        }
        return $html;
    }

}
