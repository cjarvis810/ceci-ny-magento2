<?php

class Pixafy_Pixcategoricalcontent_Block_Adminhtml_Container extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_container';
        $this->_blockGroup = 'pixcategoricalcontent';
        $this->_headerText = 'Content Containers';
        $this->_addButtonLabel = 'Add Container';
        parent::__construct();
    }
   
}
