<?php

$installer = $this;
$installer->startSetup();


$installer->run("
    CREATE TABLE `pixcategoricalcontent_container` (
    `container_id`  int NOT NULL AUTO_INCREMENT ,
    `container_name`  varchar(255) NOT NULL ,
    `container_constant`  varchar(255) NOT NULL ,
    `container_settings`  varchar(255) NULL ,    
    PRIMARY KEY (`container_id`),
    UNIQUE INDEX `constant` (`container_constant`) 
    );
    
    CREATE TABLE `pixcategoricalcontent_content` (
    `content_id`  int NOT NULL AUTO_INCREMENT ,
    `container_id`  int NOT NULL ,
    `category_id`  int NULL ,
    `content_status`  tinyint NOT NULL DEFAULT 1 ,
    `content_title`  varchar(255) NOT NULL ,
    `content_mini_title`  varchar(255) NULL ,
    `content_date`  timestamp NOT NULL ,
    `content_description`  varchar(255) NULL ,
    `content_image`  varchar(255) NULL ,
    `content_thumbnail`  varchar(255) NULL ,
    `created_at`  timestamp NOT NULL ,
    `updated_at` timestamp NOT NULL ,
    PRIMARY KEY (`content_id`)
    );
    
    CREATE TABLE `pixcategoricalcontent_category` (
    `category_id` int(11) NOT NULL AUTO_INCREMENT,
      `container_id` int(11) NOT NULL,
      `category_name` varchar(255) NOT NULL,
      `category_constant` varchar(255) NOT NULL,
      `category_position` varchar(255) NOT NULL,
      PRIMARY KEY (`category_id`),
      UNIQUE KEY `cont` (`category_constant`)
    );
    
    CREATE TABLE `pixcategoricalcontent_content_image` (
    `content_image_id`  int NOT NULL AUTO_INCREMENT ,
    `content_id`  int NOT NULL, 
    `content_image`  varchar(255) NOT NULL ,
    `content_image_position`  int(11) NOT NULL DEFAULT 1,
    PRIMARY KEY (`content_image_id`)
    );

        
");


$installer->endSetup();

