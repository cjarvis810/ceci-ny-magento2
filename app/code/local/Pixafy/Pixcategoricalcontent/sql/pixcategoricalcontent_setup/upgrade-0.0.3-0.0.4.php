<?php

$installer = $this;
$installer->startSetup();


$installer->run("CREATE TABLE `pixcategoricalcontent_content_to_categories` (
    `id`  int NOT NULL AUTO_INCREMENT ,
    `content_id`  int NOT NULL,
    `category_id`  int NOT NULL ,
    PRIMARY KEY (`id`)
    );");
$installer->run("ALTER TABLE `pixcategoricalcontent_content` DROP `category_id`;");

$installer->endSetup();
?>