<?php

$installer = $this;
$installer->startSetup();


$installer->run("
   ALTER TABLE `pixcategoricalcontent_content`
   ADD COLUMN `content_short_description`  varchar(255) NULL AFTER `content_description`;
");


$installer->endSetup();



