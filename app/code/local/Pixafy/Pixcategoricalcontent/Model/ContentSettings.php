<?php

/**
 * May not be the best way, bgut it's what I can do in 5 minutes without changing outher stuff
 */
class Pixafy_Pixcategoricalcontent_Model_ContentSettings{
    
    /**
     * Our default settings
     * @var type 
     */
    private static $defaults = array(
        "thumbnail_width" => 280,
        "thumbnail_height" => 374,
        "image_width" => 472,
        "image_height" => 631,
        "archive_by_year" => false,
        "use_extra_images" => false,
        "inner_carousel_height" => 145        
    );
    
    /**
     * Our resolved settings
     * @var type 
     */
    private static $resolved = array();
    
    
    public static function getContainerSettingsByConstant( $constant ) {
        if ( isset( self::$resolved[$constant] ) ) {
            return self::$resolved[$constant];
        }
        
        $db = Mage::getSingleton("core/resource")->getConnection("core_read");
        self::$resolved[$constant] = json_decode($db->fetchOne("SELECT container_settings FROM pixcategoricalcontent_container WHERE container_constant = ?", array($constant)), true);        
       
        self::$resolved[$constant] = self::$resolved[$constant] ? array_merge( self::$defaults, self::$resolved[$constant]) : self::$defaults;
        
        return self::$resolved[$constant];
    }
    
    public static function getContainerSettingsById( $id ) {
          if ( isset( self::$resolved[$id] ) ) {
            return self::$resolved[$id];
        }
        
        $db = Mage::getSingleton("core/resource")->getConnection("core_read");
        self::$resolved[$id] = json_decode($db->fetchOne("SELECT container_settings FROM pixcategoricalcontent_container WHERE container_id = ?", array($id)), true);        
       
        self::$resolved[$id] = self::$resolved[$id] ? array_merge( self::$defaults, self::$resolved[$id]) : self::$defaults;
        
        return self::$resolved[$id];
    }
            
    
    
}