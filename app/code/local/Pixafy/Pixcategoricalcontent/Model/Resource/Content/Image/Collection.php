<?php

class Pixafy_Pixcategoricalcontent_Model_Resource_Content_Image_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected function _construct() {
        $this->_init('pixcategoricalcontent/content_image');
        $this->addOrder('content_image_position', 'ASC');
    }

    public function filterByContentId($id) {
        $this->addFieldToFilter('main_table.content_id', array('eq' => $id));
        return $this;
    }
    
    public function filterByContentIds(array $ids) {
        $this->addFieldToFilter('main_table.content_id', array('in' => $ids));
        return $this;
    }

}
