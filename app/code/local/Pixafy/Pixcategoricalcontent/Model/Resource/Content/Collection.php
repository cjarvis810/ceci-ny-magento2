<?php

class Pixafy_Pixcategoricalcontent_Model_Resource_Content_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    /**
     * Nothing special here
     */
    protected function _construct() {
        $this->_init('pixcategoricalcontent/content');
        $this->setOrder('content_date', 'DESC');
    }

    /**
     * If the cateogry type is already joined
     * @var type 
     */
    protected $categoryJoined = false;

    /**
     * Whether the container is joined
     * @var bool 
     */
    protected $containerJoined = false;

    /**
     * Whether the extra content ifo has already been joined
     * @var type 
     */
    protected $extraContentInfoJoined = false;

    /**
     * If we filter by a category constant, we'll set it here
     * @var type 
     */
    public $categoryConstant = 'ALL';

    /**
     * Years array here to assist pager
     * @var type 
     */
    public $yearsArray = array();

    /**
     * Year filter hear
     * @var type 
     */
    public $yearFilter = 'ALL';
    
    /**
     * The size of the collection
     * @var type 
     */
    public $size = false;

    /**
     * Join the even type so we can get the name
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Content_Collection
     */
    public function joinCategory() {
        if (!$this->categoryJoined) {
            $this->getSelect()
                ->join(array("contenttocategory" => "pixcategoricalcontent_content_to_categories"), "main_table.content_id = contenttocategory.content_id", "contenttocategory.*")
                ->join(array("category" => "pixcategoricalcontent_category","contenttocategory" => "pixcategoricalcontent_content_to_categories"), "contenttocategory.category_id = category.category_id", "category.*");
             $this->categoryJoined = true;
         }
        return $this;
    }

    /**
     * Join container if not yet joined
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Content_Collection
     */
    public function joinContainer() {
        if (!$this->containerJoined) {
            $this->getSelect()
                    ->join(array("container" => "pixcategoricalcontent_container"), "main_table.container_id = container.container_id", "container.*");
            $this->containerJoined = true;
        }
        return $this;
    }

    /**
     * Filter by Container Id 
     * @param type $id
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Content_Collection
     */
    public function filterByContainerId($id) {
        $this->addFieldToFilter('main_table.container_id', array('eq' => $id));
        return $this;
    }

    /**
     * Filter by $constant name
     * @param string $constant
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Content_Collection
     */
    public function filterByContainerConstant($constant) {
        $this->joinContainer()->addFieldToFilter("container.container_constant", $constant);
        return $this;
    }

    /**
     * Filter by the category constant
     * @param type $constant
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Content_Collection
     */
    public function filterByCategoryConstant($constant) {
        $this->joinCategory()->addFieldToFilter("category.category_constant", $constant);
        $this->categoryConstant = $constant;
        return $this;
    }

    /**
     * There's no times on these things, so I don't think time zone is really an issue. Might actually cause problems. 
     */
    public function filterByYear($year) {
        $this->setYearsArray();
        $this->yearFilter = $year;
        if ($year === 'ALL') {
            return $this;
        }
        $dateModel = Mage::getSingleton('core/date');
        $this->addFieldToFilter("content_date", array('gteq' => $dateModel->date("$year-01-01 00:00:00")));
        ++$year;
        $this->addFieldToFilter("content_date", array('lt' => $dateModel->date(null, "$year-01-01 00:00:00")));
        return $this;
    }

    /**
     * Set the possible years for collection
     */
    public function setYearsArray() {
        $clone = clone $this->getSelect();
        $clone->reset(Zend_Db_Select::COLUMNS);
        $clone->columns('DISTINCT( YEAR(content_date)) as years');
        $this->yearsArray = $clone->query()->fetchAll(Zend_Db::FETCH_COLUMN, 'years');
    }

    /**
     * Get extra info included
     */
    public function joinExtraContentInfo() {
        if (!$this->extraContentInfoJoined) {
            $this->getSelect()
                    ->joinLeft(array("content_images" => "pixcategoricalcontent_content_image"), "content_images.content_id = main_table.content_id", "COUNT(content_images.content_image_id) as num_extra_images")
                    ->group("main_table.content_id");
        }
        return $this;
    }

    /**
     * The group by statement ruined the pagination, so I had to add this
     * 
     * @return type
     */
    public function getSize() {         
        if( $this->size !== false ){
            return $this->size;
        }
        $this->size = sizeof($this->getAllIds());
        return $this->size;
    }

}
