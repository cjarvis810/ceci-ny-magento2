<?php
class Pixafy_Pixcategoricalcontent_Model_Resource_Content_Category_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected function _construct() {
        $this->_init('pixcategoricalcontent/content_contenttocategory');
    }
    public function filterbyContentId($id){
        $this->addFieldToFilter('main_table.content_id', array('eq' => $id));
        return $this;
    }
    public function filterbyCategoryId($id){
        $this->addFieldToFilter('main_table.category_id', array('eq' => $id));
        return $this;
    }
    public function filterbyContentIdAndCategory($id, $catid){
        $this->addFieldToFilter('main_table.content_id', array('eq' => $id));
        $this->addFieldToFilter('main_table.category_id', array('eq' => $catid));
        return $this;
    }
}
?>