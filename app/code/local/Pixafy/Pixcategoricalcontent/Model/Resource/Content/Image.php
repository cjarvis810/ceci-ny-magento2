<?php

class Pixafy_Pixcategoricalcontent_Model_Resource_Content_Image extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct() {
        $this->_init('pixcategoricalcontent/content_image', 'content_image_id');
    }
}