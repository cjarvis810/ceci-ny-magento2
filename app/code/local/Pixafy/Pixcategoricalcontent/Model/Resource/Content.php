<?php

class Pixafy_Pixcategoricalcontent_Model_Resource_Content extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct() {
        $this->_init('pixcategoricalcontent/content', 'content_id');
    }
}