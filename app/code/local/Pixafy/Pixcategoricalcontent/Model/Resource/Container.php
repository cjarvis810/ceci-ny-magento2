<?php

class Pixafy_Pixcategoricalcontent_Model_Resource_Container extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct() {
        $this->_init('pixcategoricalcontent/container', 'container_id');
    }
}