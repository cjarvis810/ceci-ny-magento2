<?php

class Pixafy_Pixcategoricalcontent_Model_Resource_Container_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected function _construct() {
        $this->_init('pixcategoricalcontent/container');
    }

}
