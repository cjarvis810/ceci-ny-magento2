<?php

class Pixafy_Pixcategoricalcontent_Model_Resource_Category_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    /**
     * nothing special here
     */
    protected function _construct() {
        $this->_init('pixcategoricalcontent/category');
    }
    
    //whether container is joined
    protected $containerJoined = false;
        
    /**
     * Filter by Container Id 
     * @param type $id
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Category_Collection
     */
    public function filterByContainerId( $id ){
        $this->addFieldToFilter('main_table.container_id', array('eq' => $id));
        return $this;
    }
   
    /**   
     * Join container if not yet joined
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Category_Collection
     */
    public function joinContainer() {
        if (!$this->containerJoined) {
            $this->getSelect()
                    ->join(array("container" => "pixcategoricalcontent_container"), "main_table.container_id = container.container_id", "container.*");
            $this->containerJoined = true;
        }
        return $this;
    }
        
    /**
     * 
     * @param type $constant
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Category_Collection
     */    
    public function filterByContainerConstant($constant) {
        $this->joinContainer()->addFieldToFilter("container.container_constant", $constant);        
        return $this;
    }
    
    /**
     * Add display order 
     * @return \Pixafy_Pixcategoricalcontent_Model_Resource_Category_Collection
     */
    public function orderForDisplay(){
        $this->addOrder("main_table.category_position", "ASC");
        return $this;
    }

}
