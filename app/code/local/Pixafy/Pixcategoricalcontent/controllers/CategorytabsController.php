<?php

class Pixafy_Pixcategoricalcontent_CategorytabsController extends Mage_Core_Controller_Front_Action {
    
    /**
     * This is only used as an ajax server
     * Renders a special version of the catergory tabs block by specifying
     * which templates to use. 
     */
    public function indexAction() {
         //if this isn't an ajax request, send them away 
        if (!$this->getRequest()->isXmlHttpRequest() && !Mage::getIsDeveloperMode()) {
            $this->_redirect('');
        }
        $this->loadLayout();        
        $container = $this->getRequest()->getParam('container');
        $category = $this->getRequest()->getParam('category');
        $categoryTabs = $this->getLayout()->createBlock('pixcategoricalcontent/categorytabs');               
        $categoryTabs->setContainer($container);
        $categoryTabs->setCategory($category);        
        $categoryTabs->setTemplate('pixafy/pixcategoricalcontent/tab.phtml');                            

        echo json_encode( array(
            'content' => $categoryTabs->toHtml(),
            'pager' => $categoryTabs->getChildHtml('pager'),            
        ));
    }
    
    public function extraImagesAction(){
        //if this isn't an ajax request, send them away 
        if (!$this->getRequest()->isXmlHttpRequest() && !Mage::getIsDeveloperMode()) {
            $this->_redirect('');
        }
        $this->loadLayout();        
        $contentIds = $this->getRequest()->getParam('extra');        
        $categoryTabs = $this->getLayout()->createBlock('pixcategoricalcontent/categorytabs');                               
        $categoryTabs->setTemplate('pixafy/pixcategoricalcontent/extraimages.phtml');                                    
        $response = array();
        $categoryTabs->preLoadContentImages($contentIds);
        foreach( $contentIds as $id ){            
            $categoryTabs->setContentId($id);
            $response[$id] = $categoryTabs->toHtml();
        }
        if( $response ){            
            echo json_encode($response);
        }
        
    }

}
