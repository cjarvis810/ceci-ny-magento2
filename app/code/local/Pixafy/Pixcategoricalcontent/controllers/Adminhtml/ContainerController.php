<?php
require_once Mage::getBaseDir('code') . '/local/Pixafy/Pixhelper/Adminhtml/controllers/BasicCrudController.php';

class Pixafy_Pixcategoricalcontent_Adminhtml_ContainerController extends Pixafy_Pixhelper_Adminhtml_BasicCrudController {

    public function modelName() {
        return 'container';
    }

    public function moduleShortName() {
        return 'pixcategoricalcontent';
    }

    /**
     * Check to make sure settings is valid json
     * If its not don't save it
     * @param type $model
     * @param type $data
     */
    protected function setModelDataFromFormData($model, $data) {       
        if ( !empty($data['container_settings']) && !json_decode($data['container_settings'])) {
            unset($data['container_settings']);
            $this->_getSession()->addWarning('Container settings did not validate as proper json data and was not saved.');
        }
        parent::setModelDataFromFormData($model, $data);
    }

}
