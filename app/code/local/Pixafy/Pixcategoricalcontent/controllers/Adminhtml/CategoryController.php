<?php
require_once Mage::getBaseDir('code') . '/local/Pixafy/Pixhelper/Adminhtml/controllers/BasicCrudController.php';

class Pixafy_Pixcategoricalcontent_Adminhtml_CategoryController extends Pixafy_Pixhelper_Adminhtml_BasicCrudController {

    public function modelName() {
        return 'category';
    }

    public function moduleShortName() {
        return 'pixcategoricalcontent';
    }

}
