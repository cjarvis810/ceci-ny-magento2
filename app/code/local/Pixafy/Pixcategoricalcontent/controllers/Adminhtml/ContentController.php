<?php
require_once Mage::getBaseDir('code') . '/local/Pixafy/Pixhelper/Adminhtml/controllers/BasicCrudController.php';

class Pixafy_Pixcategoricalcontent_Adminhtml_ContentController extends Pixafy_Pixhelper_Adminhtml_BasicCrudController {

    public function modelName() {
        return 'content';
    }

    public function moduleShortName() {
        return 'pixcategoricalcontent';
    }

    /**
     * Overrinding the set model data to include our custom logic
     * @param type $model
     * @param type $data
     */
    protected function setModelDataFromFormData($model, $data) {
        $settings = Pixafy_Pixcategoricalcontent_Model_ContentSettings::getContainerSettingsById($data['container_id']);
        foreach (array("content_image" => array($settings['image_width'], $settings['image_height'], true), "content_thumbnail" => array($settings['thumbnail_width'], $settings['thumbnail_height']), false) as $qualifier => $dimensions) {
            $imgUpload = $this->handleImage($qualifier, 'wysiwyg/contentimages', $dimensions);
            if ($imgUpload || !empty($data[$qualifier]['delete'])) { //if file is being uploaded or delete checked
                $data[$qualifier] = $imgUpload;
            } else {
                unset($data[$qualifier]); //make sure we don't mess with what is there
            }
        }
       // $data['content_date'] = Mage::getModel('core/date')->timestamp(strtotime($data['content_date']));
        $model->setData($data);
        if (!$model->getId()) {
            $model->setCreatedAt(Mage::getSingleton("core/date")->gmtTimestamp());
        }
        $model->setUpdatedAt(Mage::getSingleton("core/date")->gmtTimestamp());
        $this->handleExtraImages($model, $data);
        $set='(';
        foreach($this->getRequest()->getParam('category_id') as $value){
            if(!Mage::getResourceModel('pixcategoricalcontent/content_category_collection')->filterbyContentIdAndCategory($model->getId(),$value)->getFirstItem()->getId()) {
                $category = Mage::getModel('pixcategoricalcontent/content_contenttocategory');
                $category->setContentId($model->getId());
                $category->setCategoryId($value);
                $category->save();
            }
            $set.=$value.',';
        }
        $set=substr($set,0,strlen($set)-1).')';
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $connection->query('DELETE FROM `pixcategoricalcontent_content_to_categories` WHERE `content_id`=\''.$model->getId().'\' AND `category_id` NOT IN '.$set);
    }

    /**
     * Handle the extra imafes
     * this is too long, but won't ever be extended or anything so I'll live iwth it
     * @param type $model
     * @param type $data
     */
    public function handleExtraImages($contentModel, $data) {

        if (!$contentModel->getId()) {
            $contentModel->save();
        }

        $formName = 'content_extra_images';
        $copyDir = 'wysiwyg/contentimages/subimages/' . $contentModel->getId();
        $fullCopyDir = Mage::getBaseDir('media') . '/' . $copyDir;
        if (!file_exists($copyDir)) {
            mkdir($fullCopyDir, 0777, true);
        }
        $uploadedFiles = array();

        $settings = Pixafy_Pixcategoricalcontent_Model_ContentSettings::getContainerSettingsById($data['container_id']);

        $maxPosition = function($id) {
            static $pos = false;
            if ($pos !== false) {
                return ++$pos;
            }
            $db = Mage::getSingleton("core/resource")->getConnection("core_read");
            $pos = $db->fetchOne("SELECT max(content_image_position) FROM pixcategoricalcontent_content_image WHERE content_id = ?", array($id));
            $pos = $pos ? $pos + 1 : 1;
            return $pos;
        };

        //upload any new images
        if (isset($_FILES['content_extra_images']) && is_array($_FILES[$formName]['tmp_name'])) {
            foreach ($_FILES[$formName]['tmp_name'] as $id => $name) {
                if (file_exists($_FILES[$formName]['tmp_name'][$id])) {
                    try {
                        $imageObj = new Varien_Image($_FILES[$formName]['tmp_name'][$id]);
                        $imageObj->constrainOnly(false);
                        $imageObj->keepAspectRatio(true);
                        $imageObj->keepFrame(false);
                        $newWidth = $imageObj->getOriginalWidth() >= $imageObj->getOriginalHeight() ? $settings["full_spread_width"] : $settings["image_width"];
                        $newHeight = $imageObj->getOriginalWidth() >= $imageObj->getOriginalHeight() ? $settings["full_spread_height"] : $settings["image_height"];
                        $imageObj->resize($newWidth, $newHeight);
                        $imageObj->save($_FILES[$formName]['tmp_name'][$id]);
                        $uploader = new Varien_File_Uploader(array(
                            'name' => $_FILES[$formName]['name'][$id],
                            'type' => $_FILES[$formName]['type'][$id],
                            'tmp_name' => $_FILES[$formName]['tmp_name'][$id],
                            'error' => $_FILES[$formName]['error'][$id],
                            'size' => $_FILES[$formName]['size'][$id]
                        ));
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png')); // or pdf or anything
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = $fullCopyDir;
                        $uploader->save($path, $_FILES[$formName]['name'][$id]);
                        $uploadedFiles[$id] = "$copyDir/{$_FILES[$formName]['name'][$id]}";
                    } catch (Exception $e) {
                        $this->_getSession()->addError("There was an errror saving one of the images: " . $e->getMessage());
                    }
                }
            }
        }

        //update existing images
        foreach (Mage::getResourceModel('pixcategoricalcontent/content_image_collection')->filterByContentId($contentModel->getId()) as $existingImage) {
            $id = $existingImage->getId();
            if (!empty($data['content_extra_images'][$id]['delete'])) {
                $existingImage->delete();
                continue;
            }
            $position = !empty($data['content_image_position'][$id]) ? $data['content_image_position'][$id] : $maxPosition($contentModel->getId());
            $existingImage->setContentImagePosition($position);
            if (isset($uploadedFiles[$id])) {
                $existingImage->setContentImage($uploadedFiles[$id]);
            }
            $existingImage->save();
        }

        //create new image if exists
        if (isset($uploadedFiles[0])) {
            $contentImage = Mage::getModel('pixcategoricalcontent/content_image');
            $contentImage->setContentImage($uploadedFiles[0]);
            $position = $data['content_image_position'][0] ? $data['content_image_position'][0] : $maxPosition($contentModel->getId());
            $contentImage->setContentImagePosition($position);
            $contentImage->setContentId($contentModel->getId());
            $contentImage->save();
        }
    }
    protected function tryToSaveModel($model) {
        try {
            $model->save();
        } catch (Exception $e) {
            $this->_getSession()->addError(ucfirst($this->modelName) . ' could not be saved, please check form values.');
            if ($id = $model->getId()) {
                return $this->_redirect('*/*/edit',array('content_id'=>$id));
            }
            return $this->_redirectReferer();
        }
        $this->_getSession()->addSuccess(ucfirst($this->modelName) . ' saved succesfully');
        return $this->_redirect('*/*/edit',array('content_id'=>$model->getId()));
    }

}
