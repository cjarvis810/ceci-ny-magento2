<?php
class Pixafy_Pixgiftcard_Block_Adminhtml_Renderer_Amount
extends Enterprise_GiftCard_Block_Adminhtml_Renderer_Amount
    implements Varien_Data_Form_Element_Renderer_Interface
{

    public function getValues()
    {
        $values = array();
        $data = $this->getElement()->getValue();
        if (is_array($data) && count($data)) {
            usort($data, array($this, '_sortValues2'));
            $values = $data;
        }
        return $values;
    }

    protected function _sortValues2($a, $b)
    {
        if ($a['website_value']!=$b['website_value']) {
            return $a['website_value']<$b['website_value'] ? -1 : 1;
        }
        return 0;
    }
}