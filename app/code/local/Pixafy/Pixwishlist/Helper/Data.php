<?php

/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Enterprise
 * @package     Enterprise_Wishlist
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Multiple wishlist helper
 *
 * @category    Enterprise
 * @package     Enterprise_Wishlist
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixwishlist_Helper_Data extends Enterprise_Wishlist_Helper_Data {

	/**
	 * Retrieve url for adding product to wishlist with params
	 *
	 * @param Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $item
	 * @param array $params
	 *
	 * @return  string|bool
	 */
	public function getAddUrlWithParams($item, array $params = array()) {
		$productId = null;
		if ($item instanceof Mage_Catalog_Model_Product) {
			$productId = $item->getEntityId();
		}
		if ($item instanceof Mage_Wishlist_Model_Item) {
			$productId = $item->getProductId();
		}

		if ($productId) {
			$params['product'] = $productId;
			$params[Mage_Core_Model_Url::FORM_KEY] = $this->_getSingletonModel('core/session')->getFormKey();
			return $this->_getUrlStore($item)->getUrl('wishlist/index/ajaxadd', $params);
		}

		return false;
	}
    public function isProductInWishlist($product){
        $customerId = Mage::getSingleton('customer/session')->getCustomerId();
        if(!$customerId){
            return false;
        }
        $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customerId, true);
        $collection = Mage::getModel('wishlist/item')->getCollection()
            ->addFieldToFilter('wishlist_id', $wishlist->getId())
            ->addFieldToFilter('product_id', $product->getId());
        $item = $collection->getFirstItem();
        return !!$item->getId();
    }
    public function getWishlistUrl($product){
        $isInWishlist=Mage::helper('pixwishlist')->isProductInWishlist($product);
        $wishlisturl=(!$isInWishlist ? Mage::helper('wishlist')->getAddUrl($product): Mage::helper('wishlist')->getRemoveUrl(Mage::getModel('wishlist/item')->loadByProductWishlist(
            Mage::helper('wishlist')->getWishlist()->getId(),
            $product->getId(),
            $product->getStoreId()
        )));
        if(isset($_SERVER['HTTPS'])) {
            if($_SERVER['HTTPS']=='on') {
                return str_replace(array('http://','wishlist'), array('https://','pixwishlist'), $wishlisturl);
            }else{
                return str_replace(array('https://','wishlist'), array('http://','pixwishlist'), $wishlisturl);
            }
        }else{
            return str_replace(array('https://','wishlist'), array('http://','pixwishlist'), $wishlisturl);
        }

    }

}
