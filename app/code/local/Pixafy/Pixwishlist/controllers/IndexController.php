<?php
require_once 'Enterprise/Wishlist/controllers/IndexController.php';

class Pixafy_Pixwishlist_IndexController extends Enterprise_Wishlist_IndexController {

	const RESULT_STATUS_FAIL = 0;
	const RESULT_STATUS_SUCCESS = 1;
	const RESULT_STATUS_REQUIRE_LOGIN = -1;
	const PROTOCOL = 'HTTP/1.1';

	protected $_statusCodeMapping = array(
			self::RESULT_STATUS_FAIL => array(
					'code' => '400',
					'message' => '400 Bad request'
			),
			self::RESULT_STATUS_SUCCESS => array(
					'code' => '200',
					'message' => '200 OK'
			),
			self::RESULT_STATUS_REQUIRE_LOGIN => array(
					'code' => '401',
					'message' => '401 Unauthorized'
			)
	);

	protected function _sendJson($data = array()) {
		$statusCode = $this->_statusCodeMapping[self::RESULT_STATUS_SUCCESS];
		if (array_key_exists('status', $data)) {
			$statusCode = $this->_statusCodeMapping[$data['status']];
		}
		if (is_array($data)) {
			$data = Mage::helper('core')->jsonEncode($data);
		}
		http_response_code($statusCode['code']);
		header('Content-type:application/json');
		echo $data;
		exit;
	}

	/**
	 * Get html by give handler name
	 * @param $name
	 * @return string
	 */
	protected function _getPartialHtml($name) {
		$layout = $this->getLayout();
		$update = $layout->getUpdate();
		$update->load($name);
		$layout->generateXml();
		$layout->generateBlocks();
		$output = $layout->getOutput();
		Mage::getSingleton('core/translate_inline')->processResponseBody($output);
		return $output;
	}

	/**
	 * Add item to wishlist
	 * Create new wishlist if wishlist params (name, visibility) are provided
	 */
	public function addAction()
    {
       if (Mage::app()->getRequest()->isAjax()) {
        $customerId = $this->_getSession()->getCustomerId();
        $name = $this->getRequest()->getParam('name');
        $visibility = ($this->getRequest()->getParam('visibility', 0) === 'on' ? 1 : 0);
        if ($name !== null) {
            try {
                $wishlist = $this->_editWishlist($customerId, $name, $visibility);
                $this->_getSession()->addSuccess(
                    Mage::helper('enterprise_wishlist')->__('Wishlist "%s" was successfully saved', Mage::helper('core')->escapeHtml($wishlist->getName()))
                );
                $this->getRequest()->setParam('wishlist_id', $wishlist->getId());
                $this->_sendJson(
                    array(
                        'status' => 1,
                        'message' => Mage::helper('pixwishlist')->__('Wishlist "%s" was successfully saved', Mage::helper('core')->escapeHtml($wishlist->getName())),
                        'content' => $this->_getPartialHtml('loadajaxwishlist')
                    )
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addException(
                    $e, Mage::helper('enterprise_wishlist')->__('Error happened during wishlist creation')
                );
            }
        } else {
            $wishlist = $this->_getWishlist();
            if (!$wishlist) {
                return $this->norouteAction();
            }

            $session = Mage::getSingleton('customer/session');

            $productId = (int)$this->getRequest()->getParam('product');
            if (!$productId) {
                $this->_sendJson(
                    array(
                        'status' => 1,
                        'message' => Mage::helper('pixwishlist')->__('Error no id'),
                        'content' => $this->_getPartialHtml('wishlist_index_loadajaxwishlist')
                    )
                );
            }

            $product = Mage::getModel('catalog/product')->load($productId);
            if (!$product->getId() || !$product->isVisibleInCatalog()) {
                $this->_sendJson(
                    array(
                        'status' => 1,
                        'message' => $this->__('Cannot specify product.'),
                        'content' => $this->_getPartialHtml('wishlist_index_loadajaxwishlist')
                    )
                );
            }

            try {
                $requestParams = $this->getRequest()->getParams();
                if ($session->getBeforeWishlistRequest()) {
                    $requestParams = $session->getBeforeWishlistRequest();
                    $session->unsBeforeWishlistRequest();
                }
                $buyRequest = new Varien_Object($requestParams);

                $result = $wishlist->addNewItem($product, $buyRequest);
                if (is_string($result)) {
                    Mage::throwException($result);
                }
                $wishlist->save();

                Mage::dispatchEvent(
                    'wishlist_add_product', array(
                        'wishlist' => $wishlist,
                        'product' => $product,
                        'item' => $result
                    )
                );

                $referer = $session->getBeforeWishlistUrl();
                if ($referer) {
                    $session->setBeforeWishlistUrl(null);
                } else {
                    $referer = $this->_getRefererUrl();
                }

                /**
                 *  Set referer to avoid referring to the compare popup window
                 */
                $session->setAddActionReferer($referer);

                Mage::helper('wishlist')->calculate();

                $message = $this->__('%1$s has been added to your wishlist. Click <a href="%2$s">here</a> to continue shopping.', $product->getName(), Mage::helper('core')->escapeUrl($referer));
                $this->_sendJson(
                    array(
                        'status' => 1,
                        'message' => $message,
                        'content' => $this->_getPartialHtml('wishlist_index_loadajaxwishlist'),
                        'link'=>Mage::helper('pixwishlist')->getWishlistUrl($product)
                    )
                );
                $session->addSuccess($message);
            } catch (Mage_Core_Exception $e) {
                $this->_sendJson(
                    array(
                        'status' => 0,
                        'message' => $this->__('An error occurred while adding item to wishlist: %s', $e->getMessage()),
                        'content' => $this->_getPartialHtml('wishlist_index_loadajaxwishlist')
                    )
                );
                $session->addError($this->__('An error occurred while adding item to wishlist: %s', $e->getMessage()));
            } catch (Exception $e) {
                $this->_sendJson(
                    array(
                        'status' => 0,
                        'message' => $this->__('An error occurred while adding item to wishlist.'),
                        'content' => $this->_getPartialHtml('wishlist_index_loadajaxwishlist')
                    )
                );
                $session->addError($this->__('An error occurred while adding item to wishlist.'));
            }
        }
        } else {
                parent::addAction();
        }
    }
    public function removeAction()
    {
        if (Mage::app()->getRequest()->isAjax()) {
            $id = (int)$this->getRequest()->getParam('item');
            $item = Mage::getModel('wishlist/item')->load($id);
            if (!$item->getId()) {
                $this->_sendJson(
                    array(
                        'status' => 0,
                        'message' => 'No Product!',
                        'content' => $this->_getPartialHtml('wishlist_index_loadajaxwishlist'),
                        'link'=>Mage::helper('pixwishlist')->getWishlistUrl($item->getProduct())
                    )
                );
                return;
            }
            $wishlist = $this->_getWishlist($item->getWishlistId());
            if (!$wishlist) {
                $this->_sendJson(
                    array(
                        'status' => 0,
                        'message' => 'No wishlist!',
                        'content' => $this->_getPartialHtml('wishlist_index_loadajaxwishlist'),
                        'link'=>Mage::helper('pixwishlist')->getWishlistUrl($item->getProduct())
                    )
                );
                return;
            }
            try {
                $item->delete();
                $wishlist->save();
                Mage::helper('wishlist')->calculate();
                $message=$this->__('The product has been remove from your wishlist.');
                $this->_sendJson(
                    array(
                        'status' => 1,
                        'message' => $message,
                        'content' => $this->_getPartialHtml('wishlist_index_loadajaxwishlist'),
                        'link'=>Mage::helper('pixwishlist')->getWishlistUrl($item->getProduct())
                    )
                );
                $this->_getSession()->addSuccess($message);
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('customer/session')->addError(
                    $this->__('An error occurred while deleting the item from wishlist: %s', $e->getMessage())
                );
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError(
                    $this->__('An error occurred while deleting the item from wishlist.')
                );
            }
        } else {
            parent::removeAction();
        }
    }
}
