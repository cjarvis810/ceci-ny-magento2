<?php

class Pixafy_Pixwishlist_Block_Customer_Top extends Enterprise_Wishlist_Block_Customer_Sidebar {

	/**
	 * Retrieve block title
	 *
	 * @return string
	 */
	public function getTitle() {
		$count = $this->getItemCount(true);
		return $this->__('You have <span>%d item%s</span> in your wishlist', $count, ($count == 1) ? '' : 's');
	}

	/**
	 * Check whether user has items in his wishlist
	 *
	 * overridden to work with changes in @getItemCount
	 * 
	 * @return bool
	 */
	public function hasWishlistItems() {
		return $this->getItemCount(true) > 0;
	}

	/**
	 * Add sidebar conditions to collection
	 *
	 * Overriden to change Page size from 3 to 2
	 * @param  Mage_Wishlist_Model_Resource_Item_Collection $collection
	 * @return Mage_Wishlist_Block_Customer_Wishlist
	 */
	protected function _prepareCollection($collection) {
		$collection->setCurPage(1)
				->setPageSize(2)
				->setInStockFilter(true)
				->setOrder('added_at');

		return $this;
	}

}
