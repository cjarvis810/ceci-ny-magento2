<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 5/6/2015
 * Time: 11:39 AM
 */

require_once(Mage::getModuleDir('controllers','Mage_Sales').DS.'OrderController.php');

class Pixafy_Pixsales_OrderController extends Mage_Sales_OrderController
{

    public function historyInprogressAction()
    {
        echo $this->getLayout()->createBlock('pixsales/order_history_inprogress')
            ->setTemplate('sales/order/orders_list.phtml')
            ->toHtml();
    }

    public function historyShippedAction()
    {
        echo $this->getLayout()->createBlock('pixsales/order_history_shipped')
            ->setTemplate('sales/order/orders_list.phtml')
            ->toHtml();
    }

    public function historyAllAction()
    {
        echo $this->getLayout()->createBlock('pixsales/order_history_all')
            ->setTemplate('sales/order/orders_list.phtml')
            ->toHtml();
    }

    public function addGiftCardAction()
    {
        $params = $this->getRequest()->getParams();
        print_r($params);
        $url = Mage::getBaseUrl()."api/v2_soap/?wsdl";
        die('You made it!');

        $proxy = new SoapClient($url); // TODO : change url
        $sessionId = $proxy->login('apiUser', 'apiKey'); // TODO : change login and pwd if necessary

        $result = $proxy->shoppingCartGiftcardAdd($sessionId, 'giftcardAccountCode', '15');
        var_dump($result);
    }
}