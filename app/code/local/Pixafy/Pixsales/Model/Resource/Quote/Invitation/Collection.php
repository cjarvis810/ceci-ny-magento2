<?php

class Pixafy_Pixsales_Model_Resource_Quote_Invitation_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    /**
     * Whether to join invitation recipients
     * @var bool 
     */
    protected $_joinedInvitationRecipients;

    protected function _construct() {
        $this->_init('pixsales/quote_invitation');
    }

    /**
     * Joins the recipients
     * @return \Pixafy_Pixsales_Model_Resource_Quote_Invitation
     */
    public function joinInvitationRecipients() {
        if (!$this->_joinedInvitationRecipients) {
            $this->_joinedInvitationRecipients = true;
            $this->getSelect()
                    ->join(array("invitationRecipients" => "pixsales_quote_invitation_recipient"), "main_table.id = invitationRecipients.invitation_id");
        }
        return $this;
    }

}
