<?php

class Pixafy_Pixsales_Model_Resource_Quote_Invitation extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('pixsales/quote_invitation', 'id');
    }

    /**
     * Get Collection of given type filtered by quote id
     * @param type $collectionName
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     * @throws LogicException
     */
    public function getInvitationRecipients() {
        $invitationId = $this->getId();
        if (!$invitationId) {
            throw new LogicException("Must have id to retrieve collection.");
        }
        //coments
        $collection = Mage::getModel('pixsales/quote_invitation_recipient')->getCollection();
        return $collection->addFieldToFilter('invitation_id', $invitationId);
    }

}
