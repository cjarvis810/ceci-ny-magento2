<?php

class Pixafy_Pixsales_Model_Resource_Quote_Comment extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('pixsales/quote_comment', 'id');
    }

}
