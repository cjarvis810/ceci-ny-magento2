<?php

class Pixafy_Pixsales_Model_Resource_Quote_Invitation_Recipient_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected function _construct() {
        $this->_init('pixsales/quote_invitation_recipient');
    }

}
