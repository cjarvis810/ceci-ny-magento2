<?php

class Pixafy_Pixsales_Model_Quote_Invitation_Recipient extends Mage_Core_Model_Abstract {

    public function _construct() {
        $this->_init('pixsales/quote_invitation_recipient');
    }

}
