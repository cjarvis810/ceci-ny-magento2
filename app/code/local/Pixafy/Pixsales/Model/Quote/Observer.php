<?php

/**
 * Pixafy_Pixsales_Model_QuoteObserver responds to events occuring on the Mage_Sales_Model_Quote object
 */
// quote_item_load_after - qty
class Pixafy_Pixsales_Model_Quote_Observer {

    protected $quoteID;
    protected $customerId;
    protected $quoteItem;
    protected $quoteItemId;
    protected $designProjectId;

    /**
     * Quote
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote;

    /**
     * Customer
     * @var Mage_Customer_Model_Customer
     */
    protected $_customer;

    /**
     * The name of the design project
     */
    const DEFAULT_DESIGN_PROJECT_NAME = "Untitled Project ";

    public function checkoutCartUpdateItemsAfter(Varien_Event_Observer $observer)
    {
        $session = Mage::getSingleton('customer/session');

        // Ensure they are logged in before creating design projects
        if ($session->isLoggedIn()) {

            $items = $observer->getInfo();
            foreach ($items as $itemId => $item) {

                $this->quoteItemId = $itemId;
                $totalSumQty = $this->_totalSumOfQtyForQuoteItem($itemId);

                // Check qty with pivot table
                if($totalSumQty < $item['qty']) {
                    // Qty change has increased

                    $this->_increaseQuantityCurrentDesignProject($item['qty'], $totalSumQty);

                } elseif($totalSumQty > $item['qty']) {
                    // Qty change has decreased

                    $this->_decreaseQuantityAcrossAllDesignProjects($item['qty'], $totalSumQty);

                }

            }

        }

    }


    public function salesQuoteRemoveItem(Varien_Event_Observer $observer)
    {

        $session = Mage::getSingleton('customer/session');

        if($session->isLoggedIn()) {

            // Get active design project
            if($this->designProjectId = $this->_getActiveDesignProject()) {

                $this->quoteItemId    = $observer->getQuoteItem()->getItemId();
                $this->_removeRecordPivotTable();

            }

        }

    }

    public function checkoutOnepageSuccess(Varien_Event_Observer $observer)
    {
        $order = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());

        // Query sales_flat_quote for design projects associated with this quote id
        $designProjectId = $this->_getQuotesAssociatedDesignProject($order['quote_id']);

        // Delete all references to design project on pivot table
        $this->_deleteAllDesignProjectRecordsFromPivotTable($designProjectId);

    }

    protected function _deleteAllDesignProjectRecordsFromPivotTable($designProjectId)
    {
        $records =  Mage::getModel("pixdesignprojects/pixdesigns")->getCollection()
            ->addFieldToFilter("design_project_id", $designProjectId);

        foreach($records as $record) {
            $record->delete();
        }

    }


    protected function _getQuotesAssociatedDesignProject($quoteId)
    {
        return Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('quote_design_id', $quoteId)
            ->getFirstItem()
            ->getId();

    }

    protected function _increaseQuantityCurrentDesignProject($qty, $totalPivotTableSum)
    {
        // Get the current active design project
        $this->designProjectId = $this->_getActiveDesignProject();

        if($this->designProjectId && $this->quoteItemId) {

            $designProject =  Mage::getModel("pixdesignprojects/pixdesigns")->getCollection()
                ->addFieldToFilter("design_project_id", $this->designProjectId)
                ->addFieldToFilter("quote_item_id", $this->quoteItemId)
                ->getFirstItem();

            $qty = ($qty - $totalPivotTableSum) + $designProject->getData('qty');

            // Pivot table reference doesn't exist create one
            if(!$designProject->getData()) {

                $record = array(
                    'customer_id'       => Mage::getSingleton('customer/session')->getId(),
                    'quote_item_id'     => $this->quoteItemId,
                    'qty'               => $qty,
                    'design_project_id' => $this->designProjectId
                );

                Mage::getModel("pixdesignprojects/pixdesigns")->setData($record)->save()->getId();

            } else {
                // Update quantity //
                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                $write->query("UPDATE pixdesignprojects_pixdesigns SET qty=".intval($qty)." WHERE quote_item_id=".$this->quoteItemId." AND design_project_id=".$this->designProjectId." AND customer_id=".Mage::getSingleton('customer/session')->getId());

            }


        }

    }


    protected function _decreaseQuantityAcrossAllDesignProjects($qty, $totalSumQty)
    {
        // Get the current active design project
        $this->designProjectId = $this->_getActiveDesignProject();

        if($this->quoteItemId && $this->designProjectId) {
            $qty = $totalSumQty - $qty;
            // If the quote item id count is equal to one then just
            $collection = Mage::getModel("pixdesignprojects/pixdesigns")->getCollection()
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
                ->addFieldToFilter('quote_item_id', $this->quoteItemId)
                ->setOrder("last_updated", "DESC");

            $pivotId = 0;
            if(count($collection->getData()) == 1) {
                // Update this record only

                $finalQty = $collection->getColumnValues('qty')[0];
                $pivotId = $collection->getColumnValues('sales_grid_id')[0];


            } else {
                $arrayOfIds = $collection->getAllIds();
                $absoluteCount = 0;

                // Loop through quote items's qty
                foreach($collection as $designProject) {

                    for($i=1; $i <= $designProject->getQty(); $i++) {

                        if($absoluteCount == $qty) {
                            break;
                        }

                        $absoluteCount++;
                    }

                    if($absoluteCount != $qty) {

                        unset($arrayOfIds[array_search($designProject->getId, $arrayOfIds)]);
                        // Delete record from pivot table

                        $collection = Mage::getModel('pixdesignprojects/pixdesigns')->getCollection()
                            ->addFieldToFilter('sales_grid_id', $designProject->getId());
                        $collection->walk('delete');

                    } else {

                        $finalQty = $designProject->getQty();
                        break;

                    }

                }

                $pivotId = end($arrayOfIds);

            }


            // Set the qty to last array
            // Update quantity //
            $qty = $finalQty - $qty;
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $write->query("UPDATE pixdesignprojects_pixdesigns SET qty=".intval($qty)." WHERE sales_grid_id=".$pivotId);

        }

    }

    protected function _removeRecordPivotTable()
    {

        if($this->quoteItemId && $this->designProjectId) {

            $collection = Mage::getModel('pixdesignprojects/pixdesigns')->getCollection()
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
                ->addFieldToFilter('quote_item_id', $this->quoteItemId)
                ->addFieldToFilter('design_project_id', $this->designProjectId);
            $collection->walk('delete');

        } else {

            Mage::throwException(__METHOD__.": One or more id's are missing");

        }

    }


    public function checkoutCartAddProductComplete(Varien_Event_Observer $observer)
    {

        $session = Mage::getSingleton('customer/session');

        // Ensure they are logged in before creating design projects
        if ($session->isLoggedIn()) {

            $this->_quote       = Mage::getSingleton('checkout/session')->getQuote();
            $this->quoteID      = $this->_quote->getId();

            $this->_getMostRecentQuoteItem();

            // IF an active design project exist
            if($this->designProjectId = $this->_getActiveDesignProject()) {
                // Active design project found //

                // Save pivot table
                $this->_saveNewDesignProjectPivotTable();

            } else {
                // No active design projects found //

                // create new design project
                $this->designProjectId = $this->_createDesignProject( $this->quoteID );

                // Save pivot table
                $this->_saveNewDesignProjectPivotTable();
            }


        }

    }

    protected function _getMostRecentQuoteItem()
    {

        $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
        $max = 0;
        $lastItem = null;

        foreach ($items as $item) {
            if ($item->getId() > $max) {
                $max = $item->getId();
                $lastItem = $item;
            }
        }

        if ($lastItem) {
            $this->quoteItem = $item;
        }

    }

    protected function _saveNewDesignProjectPivotTable()
    {

        if($this->_checkIfAllIdsExist()) {

            // No record found insert into pivot table
            $collection = Mage::getModel("pixdesignprojects/pixdesigns")->getCollection()
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
                ->addFieldToFilter('quote_item_id', $this->quoteItem->getId())
                ->addFieldToFilter('design_project_id', $this->designProjectId)
                ->getFirstItem();

            $absoluteQty = $this->_getCurrentItemsTrueQuantity() + $collection->getData('qty');

            $record = array(
                'customer_id'       => Mage::getSingleton('customer/session')->getId(),
                'quote_item_id'     => $this->quoteItem->getId(),
                'qty'               => $absoluteQty,
                'design_project_id' => $this->designProjectId
            );

            if($collection->getData()) {

                $model = Mage::getModel('pixdesignprojects/pixdesigns')->load($collection->getId())->addData($record);

                try {

                    $recordId = $model->setId($collection->getId())->save();

                } catch(Exception $e) {
                    echo $e->getMessage();
                }

            } else {

                $recordId = Mage::getModel("pixdesignprojects/pixdesigns")->setData($record)->save()->getId();

            }

            return ($recordId) ? true : false;

        } else {

            Mage::throwException(__METHOD__.": One or more record ids was not retrieved for this quote");

        }

    }

    /*
     * @Desc    This will determine the quantity for the current item being saved to the pivot table
     */
    protected function _getCurrentItemsTrueQuantity()
    {
        $collection = Mage::getModel("pixdesignprojects/pixdesigns")->getCollection()
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter('quote_item_id', $this->quoteItem->getId());
            //->addFieldToFilter('design_project_id', $this->designProjectId)


        // If this quote item doesn't exist at all assume whatever quantity being set is the true quantity
        if(!$collection->getData()) {

            return $this->quoteItem->getQty();

        } else {
            // Quote item exist

            // If more than one record exist then calculate the absolute quantity
            if(count($collection->getData()) >= 1) {

                // Lets calculate the sum of quantity for all the other design projects then subtract
                $absoluteQty = (($this->quoteItem->getQty()) - $this->_totalSumOfQtyForQuoteItem($this->quoteItem->getId()));

                // Quantity should always be positive since we are adding to the cart
                if($absoluteQty > 0) {
                    // If the absolute quantity is greater than 0 assume increasing quantity by absolute amount
                    return $absoluteQty;

                }

                // that total from incoming quantity
            } else {
                // If only this record exist just return quote's org quantity
                return $this->quoteItem->getQty();
            }

        }

    }


    protected function _totalSumOfQtyForQuoteItem($quoteItemId)
    {
        $quoteCollection = Mage::getModel("pixdesignprojects/pixdesigns")->getCollection()
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter('quote_item_id', $quoteItemId);

        $quantitySum=0;
        foreach($quoteCollection as $quoteItem) {
           $quantitySum += $quoteItem->getQty();
        }

        return ($quantitySum) ? (int)$quantitySum : false;
    }

    protected function _checkIfAllIdsExist()
    {

        if($this->quoteItem && $this->designProjectId) {

            return true;

        } else {

            Mage::throwException(__METHOD__.": One or more record ids was not retrieved for this quote");

        }
    }

    /*
     * @Desc    This will check the design projects pivot table to ensure all these columns match. (customer id, quote item id, design project id)
     * @Return  (Boolean)   Returns true if a record is found matching all 3 columns, otherwise false.
     */
    protected function _checkDesignProjectsPivotTable()
    {

        if($this->_checkIfAllIdsExist()) {
            $pivotTable = Mage::getModel("pixdesignprojects/pixdesigns")->getCollection()
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
                ->addFieldToFilter('quote_item_id', $this->quoteItemId)
                ->addFieldToFilter('design_project_id', $this->designProjectId)
                ->getFirstItem();


            return ($pivotTable) ? (int)$pivotTable->getData('sales_grid_id') : false;

        }

    }

    protected function _getMostRecentDesignProject()
    {
        $designProjects = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('customer_id',Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter('design_project_name', array('notnull' => true))
            ->setOrder('created_at', 'DESC')
            ->getFirstItem();

        return ($designProjects->getData('entity_id')) ? (int)$designProjects->getData('entity_id') : false;
    }

    protected function _getUnassignedDesignProjects()
    {

        $designProjects = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter('quote_design_id', 0)
            ->getFirstItem();


        return ($designProjects->getData('entity_id')) ? (int)$designProjects->getData('entity_id') : false;

    }

    protected function _getActiveDesignProject()
    {
        $designProjects = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter('design_project_name', array('notnull' => true))
            ->addFieldToFilter('design_project_status', 1)
            ->getFirstItem();

        return ($designProjects->getData('entity_id')) ? (int)$designProjects->getData('entity_id') : false;
    }



    /*
     *  Create new design project
     *
     * @Param   (int)   Quote ID you wish to associate this design project with otherwise 0 will assume next quote created
     *                  is the parent quote.
     * @Return  ID
     */
    protected function _createDesignProject($quoteId)
    {
        $store_id = Mage::app()->getStore()->getId();
        $customerObj = Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getId());
        $quoteObj = Mage::getModel('sales/quote')->assignCustomer($customerObj);
        $storeObj = $quoteObj->getStore()->load($store_id);
        $quoteObj->setStore($storeObj);
        $quoteObj->setStoreId($store_id);
        $quoteObj->setDesignProjectStatus(1);

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $lastTitleIdUsed = (!$customer->getLastUntitledProjectNumber()) ? 1 : $customer->getLastUntitledProjectNumber();

        $quoteObj->setDesignProjectName(self::DEFAULT_DESIGN_PROJECT_NAME . $lastTitleIdUsed);
        $quoteObj->setQuoteDesignId($quoteId);

        if($quoteObj->save()) {
            $lastTitleIdUsed++;
            $customer->setLastUntitledProjectNumber($lastTitleIdUsed);

            return (int)$quoteObj->getId();

        }

        return false;
    }

    /*
     * @Return  ID of the most recent project
     */
    protected function _getMostRecentDesignProjectId()
    {
       $designProjects = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter('design_project_name', array('notnull' => true))
            ->addFieldToFilter('design_project_status', 1) //
            ->setOrder('created_at', 'DESC')
            ->getFirstItem();

       // If a design project exist that is more recent AND

        return ($designProjects->getData('entity_id')) ? (int)$designProjects->getData('entity_id') : false;
    }


    /**
     * Called on clear_expired_quotes_before event
     * Adding our own custom rules to expiring quotes so that the ones with names do not get expired
     * @param Varien_Event_Observer $observer
     */
    public function beforeClearingExpiredQuotes(Varien_Event_Observer $observer) {
        $salesObserver = $observer->getData('sales_observer');
        $fields = array_merge($salesObserver->getExpireQuotesAdditionalFilterFields(), array('design_project_name' => array('null' => true))); // since we can only set, not add, get first since there are other attributes on there
        $salesObserver->setExpireQuotesAdditionalFilterFields($fields);
    }

    /**
     * Checks if a name is needed. If it is needed sets customer and quote
     * @param Varien_Event_Observer $observer
     * @return boolean
     */
    protected function _isNameNeeded(Varien_Event_Observer $observer) {
        $session = Mage::getSingleton('customer/session');
        if (!$session->isLoggedIn()) { //if they aren't logged in take no action
            return false;
        }
        //$q = $observer->getEvent()->getQuote();

        $customerObj = Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getId());
        $q = Mage::getModel('sales/quote')->assignCustomer($customerObj);

        if ($q->getDesignProjectName() !== null) { //checking null incase someone wants to name their project 0
            return false;
        }
        $this->_customer = $session->getCustomer();
        $this->_quote = $q;
        return true;
    }

    /**
     * Auto names project and increments users project number
     * @todo: I think this should be rethought, 1) user's projcet numbers will just keep increasing. 2) Users can have multiple projects with the same name.
     * @param Mage_Sales_Model_Quote $quote
     * @param Mage_Customer_Model_Customer $customer
     */
    protected function _autoNameUnNamedProject(Mage_Sales_Model_Quote $quote, Mage_Customer_Model_Customer $customer) {
        $last = $customer->getLastUntitledProjectNumber();
        if (!$last) { //make sure if last isn't there we make it zero before incrementing
            $last = 0;
        }
        $last++;
        $quote->setDesignProjectName(self::DEFAULT_DESIGN_PROJECT_NAME . $last);
        $customer->setLastUntitledProjectNumber($last);
        $customer->save();
    }

}
