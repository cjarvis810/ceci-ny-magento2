<?php

class Pixafy_Pixsales_Model_Quote_Invitation extends Mage_Core_Model_Abstract {

    public function _construct() {
        $this->_init('pixsales/quote_invitation');
    }

    /**
     * Get Collection of given type filtered by quote id
     * @param type $collectionName
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     * @throws LogicException
     */
    public function getInvitationRecipientCollection() {
        $invitationId = $this->getId();
        if (!$invitationId) {
            throw new LogicException("Must have id to retrieve collection.");
        }
        //coments
        return Mage::getResourceModel('pixsales/quote_invitation_recipient_collection')->addFieldToFilter('invitation_id', $invitationId);
    }

}
