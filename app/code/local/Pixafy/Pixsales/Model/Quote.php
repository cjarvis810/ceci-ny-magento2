<?php

/**
 * Pixafy_Pixsales_Model_Quote Overrides the default magento  Mage_Sales_Model_Quote
 * We'll be implementing some custom methods
 */
class Pixafy_Pixsales_Model_Quote extends Mage_Sales_Model_Quote {

    /**
     * Get comment collection returns a collection of comments matching this quote id. 
     * @return Pixafy_Pixsales_Model_Resource_Quote_Comment  
     * @throws Exception
     */
    public function getCommentCollection() {
        return $this->_getChildCollection('pixsales/quote_comment_collection');
    }

    /**
     * Get invitation collection matching quote id
     * @return Pixafy_Pixsales_Model_Resource_Quote_Invitation
     * @throws LogicException
     */
    public function getInvitationCollection() {
        return $this->_getChildCollection('pixsales/quote_invitation_collection');
    }

    /**
     * @return Pixafy_Pixsales_Model_Resource_Quote_Invitation_Recipient_Collection
     * @throws LogicException
     */
    public function getInvitationRecipientCollection() {
        return $this->_getChildCollection('pixsales/quote_invitation_recipient_collection');
    }

    /**
     * Get Collection of given type filtered by quote id
     * @param type $collectionName
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     * @throws LogicException
     */
    protected function _getChildCollection($collectionName) {
        $quoteId = $this->getId();
        if (!$quoteId) {
            throw new LogicException("Must have quote id to retrieve collection.");
        }        
        return Mage::getResourceModel($collectionName)->addFieldToFilter('main_table.quote_id', $quoteId);         
    }

}
