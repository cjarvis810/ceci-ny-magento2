<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 5/4/2015
 * Time: 4:57 PM
 */

class Pixafy_Pixsales_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $customerId;


    public function __construct()
    {
        $this->customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function getAllHistoryCollection()
    {

        return Mage::getModel("sales/order")->getCollection()
            ->addAttributeToSelect('*')
            ->setOrder('created_at', 'DESC')
            ->addFieldToFilter('customer_id', $this->customerId);


    }

    public function getShippedOrders()
    {
        return Mage::getModel("sales/order")->getCollection()
            ->addAttributeToSelect('*')
            ->setOrder('created_at', 'DESC')
            ->addFieldToFilter('customer_id', $this->customerId)
            ->addFieldToFilter('status', array('in'=>array(
                'complete',
                'cancel'
            )));

    }

    public function getOrdersInProgress()
    {

        return Mage::getModel("sales/order")->getCollection()
            ->addAttributeToSelect('*')
            ->setOrder('created_at', 'DESC')
            ->addFieldToFilter('customer_id', $this->customerId)
            ->addFieldToFilter('status', array('in'=>array(
                'pending',
                'pending_payment',
                'payment_review',
                'fraud',
                'pending_paypal',
                'holded'
            )));
    }
}