<?php

$installer = $this;
$installer->startSetup();

/**
 * Add the design project name 
 */
$salesQuoteTable = $installer->getTable("sales/quote");
$installer->run("
    ALTER TABLE `{$salesQuoteTable}`
    ADD COLUMN `design_project_name`  varchar(255) NULL;
");

/**
 * This adds and integer attribute to customer, default null
 */
$installer->addAttribute('customer', 'last_untitled_project_number', array(    
    'type'          => 'int',
    'label'         => 'This is the number of the last untitled design project.'    
));
 
$installer->endSetup();

