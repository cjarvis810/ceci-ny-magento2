<?php

$installer = $this;
$installer->startSetup();

$installer->run(" 

CREATE TABLE `pixsales_quote_invitation` (
`id`  int NOT NULL AUTO_INCREMENT ,
`quote_id`  int NOT NULL COMMENT 'the quote id' ,
`custom_message`  text NULL COMMENT 'custom message to send' ,
`designer_feedback`  tinyint NOT NULL DEFAULT 0 ,
`created_on`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'when the invitation was created' ,
PRIMARY KEY (`id`)
)
;

CREATE TABLE `pixsales_quote_invitation_recipient` (
`id`  int NOT NULL AUTO_INCREMENT ,
`invitation_id`  int NOT NULL ,
`quote_id`  int NOT NULL COMMENT 'not 3nf but convenient',
`email_address`  varchar(255) NOT NULL COMMENT 'an email address that received this invitation' ,
`verification_code`  varchar(255) NOT NULL COMMENT 'a unique code' ,
`sent_on`  timestamp NULL COMMENT 'when this message was sent, for created check invitation' ,
`customer_id` int NULL COMMENT 'the customer id who will comment',
`accepted_on` timestamp NULL COMMENT 'the time the invitation was accepted' ,
`revoked_on` timestamp NULL COMMENT 'if invitation is revoked',
PRIMARY KEY (`id`)
)
;


CREATE TABLE `pixsales_quote_comment` (
`id`  int NOT NULL AUTO_INCREMENT,
`quote_id`  int NOT NULL ,
`customer_id`  int NOT NULL ,
`message`  text NOT NULL ,
`created_on`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`)
)
;
"
);

$installer->endSetup();



