<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 5/28/2015
 * Time: 12:33 PM
 */

class Pixafy_Pixsales_Block_Giftcards_Giftcards extends Mage_Core_Block_Template
{
    protected $customerId;

    // {{block type="pixsales/giftcards_giftcards" productId="31" template="customercare/giftcards/giftcards.phtml"}}
    public function __construct()
    {
       // Get collection
        $this->customerId = Mage::getSingleton('customer/session')->getId();

        $this->collection = Mage::getModel('sales/quote')->getCollection()
            //->addFieldToSelect('entity_id') // this is important
            ->addFieldToFilter('customer_id', $this->customerId)
            ->addFieldToFilter('design_project_name', array('notnull' => true));
    }

    public function getProductId()
    {
        // Search for product named gift cards

        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', "gift-card");

        return $product->getId();
    }
}