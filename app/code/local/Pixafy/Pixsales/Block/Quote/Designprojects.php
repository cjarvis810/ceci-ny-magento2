<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 5/7/2015
 * Time: 5:53 PM
 */

class Pixafy_Pixsales_Block_Quote_Designprojects extends Mage_Core_Block_Template
{
    private $collection;

    public function __construct()
    {

        $this->collection = Mage::getModel('sales/quote')->getCollection()
            //->addFieldToSelect('entity_id') // this is important
            ->addFieldToFilter('customer_id',  Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter('design_project_name', array('notnull' => true))
            ->setOrder("created_at", "DESC");
    }

    public function getItemCount()
    {
        return $this->collection->count();
    }

    public function getCustomerDesignProjects()
    {
        return $this->collection;
    }

    public function getTotalQuoteItemsForDesignProject($designProjectId)
    {
        return Mage::getModel("pixdesignprojects/pixdesigns")->getCollection()
            ->addFieldToFilter("customer_id", Mage::getSingleton('customer/session')->getId())
            ->addFieldToFilter("design_project_id", $designProjectId)
            ->count();

    }

}