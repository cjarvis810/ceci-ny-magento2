<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 5/4/2015
 * Time: 3:47 PM
 */
class Pixafy_Pixsales_Block_Order_History extends Mage_Sales_Block_Order_History
{
    public function __construct()
    {
        Mage_Core_Block_Template::__construct();
        $this->setTemplate('sales/order/history.phtml');

       /* $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
            ->setOrder('created_at', 'desc')
        ;*/

        //$this->setOrders($orders);
        if (Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')) {
            Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('sales')->__('My Orders'));
        }
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

    }

}