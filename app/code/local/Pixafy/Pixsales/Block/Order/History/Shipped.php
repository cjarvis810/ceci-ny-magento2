<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 5/4/2015
 * Time: 3:47 PM
 */

class Pixafy_Pixsales_Block_Order_History_Shipped extends Pixafy_Pixsales_Block_Order_History
{
    protected function _construct()
    {
        Mage_Sales_Block_Order_History::_construct();
        $this->setOrders(Mage::helper('pixsales')->getShippedOrders());
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        print $this->getData('pager_type');
        $pager = $this->getLayout()->createBlock('pixsales/order_history_toolbar_pager_shipped', 'sales.order.history.pager.shipped')
            ->setTemplate('sales/order/history/autoscroll.phtml')
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
    }

}