<?php

abstract class Pixafy_Pixhelper_Adminhtml_BasicCrudController extends Mage_Adminhtml_Controller_Action {

    /**
     * The short name of the the module eg: pixeventcalendar 
     * @var string 
     */
    protected $moduleShortName;

    /**
     * The model name we are CRUDDING on 
     * @var string 
     */
    protected $modelName;

    /**
     * This will contain out blocks, which will be rendered
     * @var type 
     */
    protected $blocks;

    /**
     * This method MUST be called by the extending class
     * @param type $moduleShortName
     * @param type $modelName
     */
    public function _construct() {
        $this->moduleShortName = $this->moduleShortName();
        $this->modelName = $this->modelName();
        $this->setBlocks();
        parent::_construct();
    }

    /**
     * These blocks are rendered from the handle layout method
     * If you need additional blocks rendered, modify the array
     */
    protected function setBlocks() {
        $this->blocks = array(
            'index' => array(
                $this->moduleShortName . '/adminhtml_' . $this->modelName
            ),
            'edit' => array(
                "{$this->moduleShortName}/adminhtml_{$this->modelName}_edit"
            ),
        );
    }

    /**
     * This method is used to configure the shortName
     * Called from construct
     * This is abstract to remind developers to implement
     * Ex: function moduleShortName(){ return pixeventcalendar; }
     * @return string The Module ShortName
     */
    abstract function moduleShortName();

    /**
     * This method is used to configure the shortName
     * Called from construct
     * This is abstract to remind developers to implement
     * Ex: function modelName(){ return calendar; }
     * @return string The Module ShortName
     */
    abstract function modelName();

    /**
     * Returns the index for the model  
     */
    public function indexAction() {
        $this->handleLayout('index');
    }

    /**
     * Renders the layout adding block appropriately
     */
    protected function handleLayout($blockType) {
        $layout = $this->loadLayout()->_setActiveMenu('pixafy/' . $this->moduleShortName);
        foreach ($this->blocks[$blockType] as $block) {
            $layout->_addContent($this->getLayout()->createBlock($block));
        }
        $layout->renderLayout();
    }

    /**
     * Our edit view
     */
    public function editAction() {
        //load our model
        $model = $this->loadModelFromRequest();
        if (!$model) {
            $this->_getSession()->addError('This model does not exist');
            return $this->_redirect('*/*/');
        }
        //store the model in registry for the block to retrieve
        Mage::register($this->modelName . '_model', $model);
        $this->handleLayout("edit");
        return $this;
    }

    /**
     * Sets the model data from the form data
     * If not following the same conventions or need to map data manually, override this method
     * @param type $model
     * @param type $data
     */
    protected function setModelDataFromFormData($model, $data) {
        $model->setData($data);
    }

    /**
     * Our new view
     */
    public function newAction() {
        $this->handleLayout("edit");
        return $this;
    }

    /**
     *  Try to save the model
     *  If editing, we will have an id and try to load it i
     * @return type
     */
    public function saveAction() {
        $params = $this->getRequest()->getParams();
        $model = $this->loadModelFromRequest(); //if we don't have a model we'll try to make a new one
        if (!$model) {
            $model = Mage::getModel("{$this->moduleShortName}/{$this->modelName}");
        }
        $this->setModelDataFromFormData($model, $params);
        return $this->tryToSaveModel($model);
    }

    /**
     * Delete the model
     * @return type
     */
    public function deleteAction() {
        $model = $this->loadModelFromRequest();
        if (!$model) {
            $this->_getSession()->addError('Could not delete, this ' . ucfirst($this->modelName) . ' does not exist.');
            return $this->_redirect('*/*/');
        }
        $model->delete();
        $this->_getSession()->addSuccess(ucfirst($this->modelName) . ' deleted succesfully');
        return $this->_redirect('*/*/');
    }

    /**
     * Try to save the model, if it does not work redirect
     * @param type $eventType
     * @return type
     */
    protected function tryToSaveModel($model) {
        try {
            $model->save();
        } catch (Exception $e) {
            $this->_getSession()->addError(ucfirst($this->modelName) . ' could not be saved, please check form values.');
            if ($id = $model->getId()) {
                return $this->_redirectReferer();
            }
            return $this->_redirectReferer();
        }
        $this->_getSession()->addSuccess(ucfirst($this->modelName) . ' saved succesfully');
        return $this->_redirectReferer();
    }

    /**
     * Attempt to load the model from request
     * @return boolean
     */
    protected function loadModelFromRequest() {
        $id = $this->getRequest()->getParam($this->modelName . '_id', false);
        if (!$id) {
            return false;
        }
        $model = Mage::getModel("{$this->moduleShortName}/{$this->modelName}")->load($id);
        if (!$model->getId()) {
            return false;
        }
        return $model;
    }

    /**
     * 
     * @param type $formName
     * @param type $copyDir
     * @return file path or null
     */
    protected function handleImage($formName, $copyDir, $dimensions = array(null, null)) {
        if (!(isset($_FILES[$formName]) && file_exists($_FILES[$formName]['tmp_name']))) {
            return null;
        }
        //if this fails, we'll jsut add the message to session, but we'll allow the saving to continue
        try {
            $imageObj = new Varien_Image($_FILES[$formName]['tmp_name']);
            $imageObj->constrainOnly(false);
            $imageObj->keepAspectRatio(true);
            $imageObj->keepFrame(false);
            if (isset($dimensions[2]) && $dimensions[2] && $imageObj->getOriginalWidth() >= $imageObj->getOriginalHeight()) { //dimension 2 allow double wide
                $dimensions[0] *= 2;
            }
            $imageObj->resize($dimensions[0], $dimensions[1]);
            $imageObj->save($_FILES[$formName]['tmp_name']);
            $uploader = new Varien_File_Uploader($formName);
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png')); // or pdf or anything
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $path = Mage::getBaseDir('media') . '/' . $copyDir;
            $unique = uniqid();
            $uploader->save($path, $unique . $_FILES[$formName]['name']);
            return "$copyDir/$unique{$_FILES[$formName]['name']}";
        } catch (Exception $e) {
            $this->_getSession()->addError("There was an errror saving the image: " . $e->getMessage());
            return null;
        }
    }

}
