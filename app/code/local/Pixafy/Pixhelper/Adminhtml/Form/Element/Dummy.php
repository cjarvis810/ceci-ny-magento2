<?php

/**
 * Just displays given content
 * Content given by user_html and user_label
 */
class Pixafy_Pixhelper_Adminhtml_Form_Element_Dummy extends Varien_Data_Form_Element_Abstract {

    public function __construct($attributes = array()) {
        parent::__construct($attributes);
    }

    public function getElementHtml() {
        return $this->getUserHtml();
    }

    public function getLabelHtml($idSuffix = '') {        
        return $this->getUserLabel();
    }

}
