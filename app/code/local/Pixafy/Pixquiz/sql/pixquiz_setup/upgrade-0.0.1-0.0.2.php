<?php

try {
    
    $installer = $this;
    $installer->startSetup();
    
    // edit question table
    $table = $installer->getTable('pixquiz/question');
    $installer->getConnection()
        ->addColumn($table, 'type', 'TINYINT(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT \'Type\'');

    // create question option table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixquiz/question_option'))
        ->addColumn('option_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
            ), 'Option Id')
        ->addColumn('question_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            ), 'Question Id')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            ), 'Position')
        ->addColumn('value', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Name')
        ->addIndex($installer->getIdxName('pixquiz/question_option', array('option_id', 'position'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('option_id', 'position'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->addForeignKey($installer->getFkName('pixquiz/question_option', 'question_id', 'pixquiz/question', 'question_id'),
            'question_id', $installer->getTable('pixquiz/question'), 'question_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Question Options');
    $installer->getConnection()->createTable($table);

    $installer->endSetup();
    
    $installer->createSampleQuestionOptions();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}

