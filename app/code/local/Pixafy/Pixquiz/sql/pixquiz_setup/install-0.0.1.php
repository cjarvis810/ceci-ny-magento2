<?php

try {
    
    $installer = $this;
    $installer->startSetup();

    // create quiz table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixquiz/quiz'))
        ->addColumn('quiz_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
            ), 'Quiz Id')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Name')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Creation Time')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Update Time')
        ->setComment('Quizzes');
    $installer->getConnection()->createTable($table);

    // create question table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixquiz/question'))
        ->addColumn('question_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Question Id')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default' => '0'
            ), 'Status')
        ->addColumn('question', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Question')
        ->addColumn('template_var', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Template Variable')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Creation Time')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Update Time')
        ->setComment('Questions');
    $installer->getConnection()->createTable($table);
    
    // create quiz question table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixquiz/quiz_question'))
        ->addColumn('quiz_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
            ), 'Quiz Id')
        ->addColumn('question_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Question Id')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default' => '0'
        ), 'Position')
        ->addIndex($installer->getIdxName('pixquiz/quiz_question', array('quiz_id', 'position'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('quiz_id', 'position'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->addForeignKey($installer->getFkName('pixquiz/quiz_question', 'quiz_id', 'pixquiz/quiz', 'quiz_id'),
            'quiz_id', $installer->getTable('pixquiz/quiz'), 'quiz_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName('pixquiz/quiz_question', 'question_id', 'pixquiz/question', 'question_id'),
            'question_id', $installer->getTable('pixquiz/question'), 'question_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Quiz Questions');
    $installer->getConnection()->createTable($table);

    $installer->endSetup();
    
    
    
    $installer = Mage::getResourceModel('catalog/setup','catalog_setup');
    $installer->startSetup();
    
    $table = $installer->getTable('catalog/category');
    
    $installer->getConnection()
        ->addColumn($table, 'quiz_id', array(
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'unsigned'  => true,
            'comment' => 'Quiz Id'
        ));
    $installer->getConnection()
        ->addConstraint($installer->getFkName('catalog/category', 'quiz_id', 'pixquiz/quiz', 'quiz_id'),
            $installer->getTable('catalog/category'), 'quiz_id', $installer->getTable('pixquiz/quiz'), 'quiz_id');

    $installer->endSetup();
    
    $installer->startSetup();
    
    $installer->addAttribute('catalog_category', 'quiz_id', array(
        'label' => 'Quiz',
        'input' => 'select',
        'type' => 'static',
        'source' => 'pixquiz/attribute_source_quiz',
        'required' => false,
        'group' => 'General Information',
        'position' => 100
    ));
    
    $installer->endSetup();
    
    $installer = $this;
    $installer->createSampleQuiz();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}

