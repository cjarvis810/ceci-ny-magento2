<?php

try {
    $installer = Mage::getResourceModel('catalog/setup','catalog_setup');
    $installer->startSetup();

    $table = $installer->getTable('catalog/category');

    $installer->run("ALTER TABLE $table DROP FOREIGN KEY " . $installer->getFkName('catalog/category', 'quiz_id', 'pixquiz/quiz', 'quiz_id'));
    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}

