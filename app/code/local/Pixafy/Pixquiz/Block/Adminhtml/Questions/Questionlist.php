<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Questions_Questionlist extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
    {
	    $this->_blockGroup = 'pixquiz';
	    $this->_controller = 'adminhtml_questions_questionlist';
        parent::__construct();
    }

    public function getHeaderText()
    {
        return Mage::helper('adminhtml')->__('Question Management');
    }
}


?>