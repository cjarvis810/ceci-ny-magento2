<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Questions_Questionform_Tab_Questioninfo extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		if (Mage::getSingleton('adminhtml/session')->getFormData())
		{
			$data = Mage::getSingleton('adminhtml/session')->getFormData();
			Mage::getSingleton('adminhtml/session')->setFormData(null);
		}
		elseif(Mage::registry('question'))
		{
			$data = Mage::registry('question')->getData();
		}

		$form = new Varien_Data_Form();
        $this->setForm($form);

		$fieldset = $form->addFieldset('registry_form',array('legend'=>Mage::helper('pixquiz')->__('Question Information')));

		$fieldset->addField(
			'template_var',
			'text',
			array(
				'label' => Mage::helper('pixquiz')->__('Template Var'),
				'name'	=> 'template_var',
				'required' => TRUE
				)
			);

		$fieldset->addField(
			'question',
			'textarea',
			array(
				'label' => Mage::helper('pixquiz')->__('Question'),
				'name'	=> 'question',
				'required' => TRUE
				)
			);
		$fieldset->addField(
			'status',
			'select',
			array(
				'label' => Mage::helper('pixquiz')->__('Active'),
				'name'	=> 'status',
				'values' => array('1' => 'Yes', '0' => 'No'),
				'required' => TRUE
				)
			);
		$fieldset->addField(
			'type',
			'select',
			array(
				'label' => Mage::helper('pixquiz')->__('Type'),
				'name'	=> 'type',
				'values' => array( '0' => 'Select','1' => 'Text'),
				'required' => TRUE
				)
			);
				
		$form->setValues($data);

		return parent::_prepareForm();
	}
}



?>