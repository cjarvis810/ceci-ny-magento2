<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Questions_Questionform_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
 
  public function __construct()
  {
      parent::__construct();
      $this->setId('form_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('pixquiz')->__('Question Information'));
  }
 
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('pixquiz')->__('Question Information'),
          'title'     => Mage::helper('pixquiz')->__('Question Information'),
          'content'   => $this->getLayout()->createBlock('pixquiz/adminhtml_questions_questionform_tab_questioninfo')->toHtml()
      ));

      $this->addTab('form_section2', array(
          'label'     => Mage::helper('pixquiz')->__('Question Options'),
          'title'     => Mage::helper('pixquiz')->__('Question Options'),
          'content'   => $this->getLayout()->createBlock('pixquiz/adminhtml_questions_questionform_tab_questionoption')->toHtml()
      ));
      
      return parent::_beforeToHtml();
  }
}


?>