<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Questions_Questionform extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'question_id';
        $this->_blockGroup = 'pixquiz';
        $this->_controller = 'adminhtml_questions';
        $this->_mode = 'questionform';
        $this->_updateButton('save', 'label', Mage::helper('pixquiz')->__('Save Question'));
    }

    public function getHeaderText()
    {
        if(Mage::registry('question') && Mage::registry('question')->getId())
        {
            return Mage::helper('pixquiz')->__('Edit Question');
        }
        return Mage::helper('pixquiz')->__('New Question');
    }
}


?>