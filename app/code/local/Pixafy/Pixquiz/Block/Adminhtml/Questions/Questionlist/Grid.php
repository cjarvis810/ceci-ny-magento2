<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Questions_Questionlist_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
        parent::__construct();
        $this->setId('question_id');
        $this->setDefaultSort('question_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    protected function _prepareCollection()
    {
    	$collection = Mage::getResourceModel('pixquiz/question_collection');
    	$this->setCollection($collection);
    	return $collection;
    }
	protected function _prepareColumns()
    {
        //question_id, question, template_var, status, created_at, updated_at
    	$helper = Mage::helper('pixquiz');

    	$this->addColumn('question_id', array(
    			'header' => $helper->__('Question Id'),
    			'type' => 'number',
    			'index'  => 'question_id'
    		));
    	$this->addColumn('question', array(
    			'header' => $helper->__('Question'),
    			'index'  => 'question'
    		));
    	$this->addColumn('template_var', array(
    			'header' => $helper->__('Template Varible Name'),
    			'index'  => 'template_var'
    		));
    	$this->addColumn('status', array(
    			'header' => $helper->__('Status'),
                'type'  => 'options',
    			'index'  => 'status',
                'options' => array('0' => 'No', '1' => 'Yes')
    		));
    	$this->addColumn('created_at', array(
    			'header' => $helper->__('Created At'),
    			'type'	 => 'date',
    			'index'  => 'created_at'
    		));
    	$this->addColumn('updated_at', array(
    			'header' => $helper->__('Updated At'),
    			'type'	 => 'date',
    			'index'  => 'updated_at'
    		));

    	return parent::_prepareColumns();
    }
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getQuestionId()));
    }
}


?>