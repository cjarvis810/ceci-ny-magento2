<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Questions_Questionform_Tab_Questionoption extends Mage_Adminhtml_Block_Widget
{
	public function __construct()
    {
        parent::__construct();
        $this->setTemplate('pixafy/pixquiz/questionoptions.phtml');
    }
    
    protected function _prepareLayout()
    {
        $this->setChild('delete_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => Mage::helper('eav')->__('Delete'),
                    'class' => 'delete delete-option'
                )));

        $this->setChild('add_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => Mage::helper('eav')->__('Add Option'),
                    'class' => 'add',
                    'id'    => 'add_new_option_button'
                )));
        return parent::_prepareLayout();
    }

    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    public function getAddNewButtonHtml()
    {
        return $this->getChildHtml('add_button');
    }

    public function getOptionValues()
    {
		$values = $this->getData('option_values');
		if(is_null($values))
		{
			$question_id = $this->getRequest()->getParam('id',null);
    		$question_options = Mage::getModel('pixquiz/question_option')
    							->getCollection()
    							->addFieldToFilter('question_id',$question_id);
			$values = array();
	    	foreach($question_options as $q_opt)
	    	{
	    		$value = array();
	    		$value['intype'] = '';
	    		$value['id'] = $q_opt->getOptionId();
	        	$value['sort_order'] = $q_opt->getPosition();
	        	$value['position'] = $q_opt->getPosition();
	        	$value['value'] = $q_opt->getValue();
	    		$values[] = new Varien_Object($value);
	    	}
	    	$this->setData('option_values', $values);
	    	return $values;
		}
     
    }

}


?>