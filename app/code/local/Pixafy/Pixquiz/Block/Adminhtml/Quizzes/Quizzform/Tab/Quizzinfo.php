<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Quizzes_Quizzform_Tab_Quizzinfo extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
        $this->setForm($form);
        
		$fieldset = $form->addFieldset('registry_form', array('legend'=>Mage::helper('pixquiz')->__('Quiz Information')));

		if (Mage::getSingleton('adminhtml/session')->getFormData())
		{
			$data = Mage::getSingleton('adminhtml/session')->getFormData();
			Mage::getSingleton('adminhtml/session')->setFormData(null);
		}
		elseif(Mage::registry('quizz'))
		{
			$data = Mage::registry('quizz')->getData();
		}

		$fieldset->addField('name', 'text', array(
            'label' => Mage::helper('pixquiz')->__('Quiz Name'),
            'name' => 'name'
        ));
		
		 $form->setValues($data);
		 return parent::_prepareForm();
	}
}



?>