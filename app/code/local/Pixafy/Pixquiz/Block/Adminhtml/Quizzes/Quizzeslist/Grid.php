<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Quizzes_Quizzeslist_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
        parent::__construct();
        $this->setId('quiz_id');
        $this->setDefaultSort('quiz_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
    	$collection = Mage::getModel('pixquiz/quiz')
    		->getCollection();
        $collection->getSelect()
        ->joinLeft(array('quiz_question' => 'pixquiz_quiz_question'),
            'main_table.quiz_id = quiz_question.quiz_id',
            array('COUNT(quiz_question.question_id) as qty'))
        ->group(array('main_table.quiz_id', 
                    'main_table.name', 
                    'main_table.created_at',
                    'main_table.updated_at'));
    	$this->setCollection($collection);
    	return $collection;
    }

	protected function _prepareColumns()
    {
    	$helper = Mage::helper('pixquiz');

    	$this->addColumn('quiz_id', array(
    			'header' => $helper->__('Quiz Id'),
    			'type' => 'number',
    			'index'  => 'quiz_id'
    		));
    	$this->addColumn('name', array(
    			'header' => $helper->__('Name'),
    			'index'  => 'name'
    		));
    	$this->addColumn('qty', array(
    			'header' => $helper->__('Number of Questions'),
    			'index'  => 'qty'
    		));
    	$this->addColumn('created_at', array(
    			'header' => $helper->__('Created At'),
    			'type'	 => 'date',
    			'index'  => 'created_at'
    		));
    	$this->addColumn('updated_at', array(
    			'header' => $helper->__('Updated At'),
    			'type'	 => 'date',
    			'index'  => 'updated_at'
    		));

    	return parent::_prepareColumns();
    }
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getQuizId()));
    }
}


?>