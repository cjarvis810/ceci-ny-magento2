<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Quizzes_Quizzeslist extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
    {
	    $this->_blockGroup = 'pixquiz';
	    $this->_controller = 'adminhtml_quizzes_quizzeslist';
        parent::__construct();
    }

    public function getHeaderText()
    {
        return Mage::helper('adminhtml')->__('Quiz Management');
    }
}


?>