<?php 

class Pixafy_Pixquiz_Block_Adminhtml_Quizzes_Quizzform_Tab_Questioninfo extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
        parent::__construct();
        $this->setId('quiz_id');
        $this->setDefaultSort('quiz_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('pixquiz/question_collection')
            ->addFieldToFilter('status', '1');
        $collection->getSelect()
        ->joinLeft(array('quiz_question' => 'pixquiz_quiz_question'),
            'main_table.question_id = quiz_question.question_id AND
            quiz_question.quiz_id = ' . $this->getRequest()->getParam('id', 0)
            ,
            array('position'));

         $this->setCollection($collection);
         return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_quizz', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name' 		=> 'in_quizz',
            'values'	=>  $this->_getSelectedQuizzes(),
            'align'     => 'center',
            'index'     => 'question_id'
        ));
        $this->addColumn('question_id', array(
            'header'    => Mage::helper('pixquiz')->__('Question Id'),
            'sortable'  => true,
            'width'     => '60',
            'index'     => 'question_id'
        ));
        $this->addColumn('question', array(
            'header'    => Mage::helper('pixquiz')->__('Question'),
            'index'     => 'question'
        ));
        $this->addColumn('template_var', array(
            'header'    => Mage::helper('pixquiz')->__('Template Varible Name'),
            'width'     => '80',
            'index'     => 'template_var'
        ));
        $this->addColumn('position', array(
            'header'    => Mage::helper('pixquiz')->__('Position'),
            'width'     => '1',
            'type'      => 'number',
            'index'     => 'position',
            'editable'  => true
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _getSelectedQuizzes()
    {
        //$question_values = $this->getRequest()->getPost('selected_quizzes');
        //if (is_null($products)) {
            $pixquizCollection = Mage::getModel('pixquiz/quiz')->getCollection()
                ->addFieldToFilter('main_table.quiz_id' ,$this->getRequest()->getParam('id', 0))
                ->joinQuestionIds();
           	$question_values = array();

            foreach($pixquizCollection as $quizquestion)
			{
			    $question_values = $quizquestion->getQuestionIds();
			}
            return $question_values;
       // }
        //return $question_values;
    }
}



?>