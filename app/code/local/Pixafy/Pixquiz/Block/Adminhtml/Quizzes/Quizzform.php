<?php 
class Pixafy_Pixquiz_Block_Adminhtml_Quizzes_Quizzform extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();
        $this->_objectId = 'quiz_id';
        $this->_blockGroup = 'pixquiz';
        $this->_controller = 'adminhtml_quizzes';
        $this->_mode = 'quizzform';
        $this->_updateButton('save', 'label', Mage::helper('pixquiz')->__('Save Quiz'));
    }

	public function getHeaderText()
    {
        if(Mage::registry('quizz') && Mage::registry('quizz')->getId())
        {
            return Mage::helper('pixquiz')->__('Edit Quiz');
        }
        return Mage::helper('pixquiz')->__('New Quiz');
    }
}


?>