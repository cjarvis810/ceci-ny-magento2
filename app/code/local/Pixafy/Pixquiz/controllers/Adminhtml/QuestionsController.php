<?php 

class Pixafy_Pixquiz_Adminhtml_QuestionsController extends Mage_Adminhtml_Controller_Action {

	public function indexAction() {
		$this->loadLayout()
			 ->_addContent($this->getLayout()->createBlock('pixquiz/adminhtml_questions_questionlist')); 
		$this->renderLayout();
	}

	public function editAction()
	{
		$id = $this->getRequest()->getParam('id',null);
		if($id)
		{
			$question = Mage::getModel('pixquiz/question')->load((int)$id);
			if($question->getId())
			{
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if($data)
				{
					$question->setData($data)->setId($id);
				}
			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixquiz')->__('The question does not exist.'));
				$this->_redirect('*/*/');
			}
		}
		Mage::register('question',$question);

		$this->loadLayout()
			 ->_addContent($this->getLayout()->createBlock('pixquiz/adminhtml_questions_questionform'))
			 ->_addLeft($this->getLayout()->createBlock('pixquiz/adminhtml_questions_questionform_tabs')); 
		$this->renderLayout();
		return $this;
	}

	public function newAction()
	{
		$this->loadLayout()
			 ->_addContent($this->getLayout()->createBlock('pixquiz/adminhtml_questions_questionform'))
			 ->_addLeft($this->getLayout()->createBlock('pixquiz/adminhtml_questions_questionform_tabs')); 
		$this->renderLayout();
		return $this;
	}

	public function deleteQuestionOptions($question_id)
    {
    	$options = Mage::getModel('pixquiz/question_option')
    				->getCollection()
    				->addFieldToFilter('question_id', $question_id);
    	foreach($options as $opt)
    	{
    		$opt->delete();
    	}
    }

	public function saveAction()
	{
		$data = $this->getRequest()->getPost();
		if($data)
		{
			try
			{
				$id = $this->getRequest()->getParam('id',null);
				$question = Mage::getModel('pixquiz/question')->load($id);
				$question->addData($data);
				$question->save();
				$this->deleteQuestionOptions($question->getId());
				for($key = 0; $key < count($data["option_value"]); $key++)
				{
					$options = Mage::getModel('pixquiz/question_option');
					$options->load()
							->setQuestionId($question->getId())
							->setPosition($data["position"][$key])
							->setValue($data["option_value"][$key])
							->save();
				}
				$this->_redirect('*/*/');
			} 
			catch (Exception $e)
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixquiz')->__('There was a problem saving your question.'));
				$this->_redirect('*/edit/');
			}


		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixquiz')->__('There was a problem saving your question.'));
			$this->_redirect('*/edit/');
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id',null);
		if($id)
		{
			try
			{
				$question = Mage::getModel('pixquiz/question')->load($id);
				$question->delete();
				$this->_redirect('*/*/');
			}
			catch (Exception $e)
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixquiz')->__('There was a problem saving your question.'));
				$this->_redirect('*/*/');
			}
		}
	}
}

?>