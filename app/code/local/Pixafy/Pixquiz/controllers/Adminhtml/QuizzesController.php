<?php 

class Pixafy_Pixquiz_Adminhtml_QuizzesController extends Mage_Adminhtml_Controller_Action {

	public function indexAction() 
	{
		$this->loadLayout()
			 ->_addContent($this->getLayout()->createBlock('pixquiz/adminhtml_quizzes_quizzeslist')); 
		$this->renderLayout();
	}

	public function editAction()
	{

		$id = $this->getRequest()->getParam('id',null);
		if($id)
		{
			$quizz = Mage::getModel('pixquiz/quiz')->load((int)$id);
			if($quizz->getId())
			{
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if($data)
				{
					$quizz->setData($data)->setId($id);
				}
			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixquiz')->__('The question does not exist.'));
				$this->_redirect('*/*/');
			}
		}
		Mage::register('quizz',$quizz);
		$this->loadLayout()
			 ->_addContent($this->getLayout()->createBlock('pixquiz/adminhtml_quizzes_quizzform'))
			 ->_addLeft($this->getLayout()->createBlock('pixquiz/adminhtml_quizzes_quizzform_tabs')); 
		$this->renderLayout();
		return $this;
	}

	public function gridAction()
	{
	    $this->loadLayout();
	    $this->getResponse()->setBody(
	           $this->getLayout()->createBlock('pixquiz/adminhtml_quizzes_quizzform_tab_questioninfo')->toHtml()
	    ); 
	}

	private function getQuestionsIds($is_in_quiz)
	{
		$quiz_detail = (array)json_decode($is_in_quiz);
		$quiz_detail = array_flip($quiz_detail);
		return($quiz_detail);
	}

	public function saveAction()
	{

		if ($this->getRequest()->getPost())
		{
			try {
				$data = $this->getRequest()->getPost();
				$id = $this->getRequest()->getParam('id');

				if($data && $id)
				{
					$pixquiz = Mage::getModel('pixquiz/quiz')->load($id);
					$pixquiz->addData($data);
					if(!empty($data["is_in_quiz"]))
					{
						$questionIds = $this->getQuestionsIds($data["is_in_quiz"]);
						$pixquiz->setQuestionIds($questionIds);
					}
					$pixquiz->save();
					$this->_redirect('*/*/index');
				}
				elseif($data)
				{
					$pixquiz = Mage::getModel('pixquiz/quiz')->load();
					$pixquiz->addData($data);
					if(!empty($data["is_in_quiz"]))
					{
						$questionIds = $this->getQuestionsIds($data["is_in_quiz"]);
						$pixquiz->setQuestionIds($questionIds);
					}
					$pixquiz->save();
					$this->_redirect('*/*/index');
				}
			} catch (Exception $e) {
				$this->_getSession()->addError(Mage::helper('pixquiz')->__('An error occurred while saving the data. Please try again.'));
				Mage::log($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return $this;
			}
		}
		else
		{
			$this->_getSession()->addError(Mage::helper('pixquiz')->__('An error occurred while saving the data. Please try again.'));
			$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			return $this;
		}
	}

	public function newAction()
	{
		$this->loadLayout()
			 ->_addContent($this->getLayout()->createBlock('pixquiz/adminhtml_quizzes_quizzform'))
			 ->_addLeft($this->getLayout()->createBlock('pixquiz/adminhtml_quizzes_quizzform_tabs')); 
		$this->renderLayout();
		return $this;
	}
}

?>