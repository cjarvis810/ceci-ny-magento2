<?php

class Pixafy_Pixquiz_Model_Question extends Mage_Core_Model_Abstract {
    
    const STATUS_DISABLED = false;
    const STATUS_ENABLED = true;
    
    const TYPE_TEXT = 0;
    const TYPE_SELECT = 1;
    
    static protected $_types;
    static protected $_optionTypes;
    
    protected function _construct() {
        $this->_init('pixquiz/question');
    }
    
    public static function getTypes() {
        if (is_null(self::$_types)) {
            self::_loadTypes();
        }
        return self::$_types;
    }
    
    protected static function _loadTypes() {
        self::$_types = array(
            self::TYPE_TEXT => 'text',
            self::TYPE_SELECT => 'select'
        );
    }
    
    public static function getOptionTypes() {
        if (is_null(self::$_optionTypes)) {
            self::_loadOptionTypes();
        }
        return self::$_optionTypes;
    }
    
    protected static function _loadOptionTypes() {
        self::_loadTypes();
        self::$_optionTypes = array();
        foreach (self::$_types as $key => $value) {
            if (strpos($value, 'select') !== false) {
                self::$_optionTypes[$key] = $value;
            }
        }
    }
    
    public function getOptionCollection() {
        return Mage::getResourceModel('pixquiz/question_option_collection')
            ->addFieldToFilter('question_id', $this->getId());
    }
    
}

