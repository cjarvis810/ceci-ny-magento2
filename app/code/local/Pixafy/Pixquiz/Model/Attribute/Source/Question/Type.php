<?php

class Pixafy_Pixquiz_Model_Attribute_Source_Question_Type extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions() {
        $types = Pixafy_Pixquiz_Model_Question::getTypes();
        $options = array();
        foreach ($types as $value => $label) {
            $options[] = array(
                'label' => $label,
                'value' => $type
            );
        }
        return $options;
    }
    
}

