<?php

class Pixafy_Pixquiz_Model_Attribute_Source_Quiz extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions() {
        $options = array(array(
            'label' => ''
        ));
        $quizzes = Mage::getResourceModel('pixquiz/quiz_collection')
            ->setOrder('name', 'ASC');
        foreach ($quizzes as $quiz) {
            $options[] = array(
                'label' => $quiz->getName(),
                'value' => $quiz->getId()
            );
        }
        return $options;
    }
    
}

