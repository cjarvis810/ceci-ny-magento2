<?php

class Pixafy_Pixquiz_Model_Resource_Quiz extends Mage_Core_Model_Resource_Db_Abstract {
    
    protected function _construct() {
        $this->_init('pixquiz/quiz', 'quiz_id');
    }
    
    public function getQuestionIds(Pixafy_Pixquiz_Model_Quiz $quiz) {
        if (!$quiz->getId()) {
            return array();
        }
        
        $table = $this->getTable('pixquiz/quiz_question');
        
        $result = $this->_getReadAdapter()->select()
            ->from($table, 'question_id')
            ->where('quiz_id = ?', $quiz->getId())
            ->order('position ASC')
            ->query(PDO::FETCH_ASSOC);
        
        $questionIds = array();
        
        foreach ($result as $row) {
            $questionIds[] = $row['question_id'];
        }
        
        return $questionIds;
    }
    
    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
        $now = Mage::getSingleton('core/date')->timestamp(time());
        if (!$object->getId()) {
            $object->setCreatedAt($now);
        }
        $object->setUpdatedAt($now);
        return parent::_beforeSave($object);
    }
    
    protected function _afterSave(Mage_Core_Model_Abstract $object) {
        $this->_saveQuizQuestions($object);
        return parent::_afterSave($object);
    }
    
    protected function _saveQuizQuestions(Mage_Core_Model_Abstract $object) {
        $questionIds = $object->getData('question_ids');
        if (!is_null($questionIds)) {
            $table = $this->getTable('pixquiz/quiz_question');
        
            // delete old associations
            $this->_getWriteAdapter()->delete(
                $table,
                array('quiz_id = ?' => $object->getId())
            );

            // save new associations
            foreach ($questionIds as $position => $questionId) {
                $this->_getWriteAdapter()->insert(
                    $table,
                    array(
                        'quiz_id' => $object->getId(),
                        'question_id' => $questionId,
                        'position' => $position
                    )
                );
            }
        }
    }
    
}

