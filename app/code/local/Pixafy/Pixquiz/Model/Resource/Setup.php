<?php

class Pixafy_Pixquiz_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup {
    
    public function createSampleQuiz() {
        $questionIds = array();
        
        $question = Mage::getModel('pixquiz/question')
            ->setData(array(
                'status' => Pixafy_Pixquiz_Model_Question::STATUS_ENABLED,
                'question' => 'What is the bride\'s full name?',
                'template_var' => 'bride',
            ))->save();
        
        $questionIds[] = $question->getId();

        $question = Mage::getModel('pixquiz/question')
            ->setData(array(
                'status' => Pixafy_Pixquiz_Model_Question::STATUS_ENABLED,
                'question' => 'What is the groom\'s full name?',
                'template_var' => 'groom',
            ))->save();
        
        $questionIds[] = $question->getId();
        
        $quiz = Mage::getModel('pixquiz/quiz')
            ->setData(array(
                'name' => 'Wedding Invitation',
                'question_ids' => $questionIds
            ))->save();
        
        $category = Mage::getModel('catalog/category')->load(6)
            ->setQuizId($quiz->getId())
            ->save();
    }
    
    public function createSampleQuestionOptions() {
        
        $quiz = Mage::getModel('pixquiz/quiz')->load(1);
    
        $question = Mage::getModel('pixquiz/question')
            ->setData(array(
                'quiz_id' => $quiz->getId(),
                'question' => 'Who will be hosting the wedding?',
                'template_var' => 'host',
                'type' => Pixafy_Pixquiz_Model_Question::TYPE_SELECT
            ))->save();

        Mage::getModel('pixquiz/question_option')
            ->setData(array(
                'question_id' => $question->getId(),
                'value' => 'Bride\'s Family',
                'position' => 1
            ))->save();

        Mage::getModel('pixquiz/question_option')
            ->setData(array(
                'question_id' => $question->getId(),
                'value' => 'Groom\'s Family',
                'position' => 2
            ))->save();

        Mage::getModel('pixquiz/question_option')
            ->setData(array(
                'question_id' => $question->getId(),
                'value' => 'Bride',
                'position' => 3
            ))->save();

        Mage::getModel('pixquiz/question_option')
            ->setData(array(
                'question_id' => $question->getId(),
                'value' => 'Groom',
                'position' => 4
            ))->save();

        $questionIds = $quiz->getQuestionIds();
        $questionIds[] = $question->getId();
        $quiz->setQuestionIds($questionIds)->save();
    }
    
}

