<?php

class Pixafy_Pixquiz_Model_Resource_Question extends Mage_Core_Model_Resource_Db_Abstract {
    
    protected function _construct() {
        $this->_init('pixquiz/question', 'question_id');
    }
    
    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
        $now = Mage::getSingleton('core/date')->timestamp(time());
        if (!$object->getId()) {
            $object->setCreatedAt($now);
        }
        $object->setUpdatedAt($now);
        return parent::_beforeSave($object);
    }
}

