<?php

class Pixafy_Pixquiz_Model_Resource_Quiz_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    
    private $_joinedQuestionIds;

    protected function _construct() {
        $this->_init('pixquiz/quiz');
        $this->_joinedQuestionIds = false;
    }
    
    public function joinQuestionIds() {
        if (!$this->_joinedQuestionIds) {
            $this->getSelect()
                ->joinLeft(
                    $this->getTable('pixquiz/quiz_question') . ' AS qq',
                    'main_table.quiz_id = qq.quiz_id',
                    'GROUP_CONCAT(qq.question_id) question_ids'
                )->group('main_table.quiz_id');
            $this->_joinedQuestionIds = true;
        }
        return $this;
    }
    
    protected function _afterLoadData() {
        if ($this->_joinedQuestionIds) {
            foreach ($this as $item) {
                $item->setQuestionIds(explode(',', $item->getQuestionIds()));
            }
        }
        return parent::_afterLoadData();
    }
    
}

