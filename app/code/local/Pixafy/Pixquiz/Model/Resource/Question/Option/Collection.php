<?php

class Pixafy_Pixquiz_Model_Resource_Question_Option_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    
    protected function _construct() {
        $this->_init('pixquiz/question_option');
    }
}

