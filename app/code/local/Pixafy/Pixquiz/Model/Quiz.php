<?php

class Pixafy_Pixquiz_Model_Quiz extends Mage_Core_Model_Abstract {
    
    protected function _construct() {
        $this->_init('pixquiz/quiz');
    }
    
    public function getQuestionCollection() {
        return Mage::getResourceModel('pixquiz/question_collection')
            ->addFieldToFilter('question_id', array('in' => $this->getQuestionIds()));
    }
    
    public function getQuestionIds() {
        if (is_null($this->getData('question_ids'))) {
            $this->setQuestionIds($this->_getResource()->getQuestionIds($this));
        }
        return $this->getData('question_ids');
    }
    
}

