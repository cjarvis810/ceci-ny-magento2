<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Customer Data Helper
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author     Magento Core Team <core@magentocommerce.com>
 */

class Pixafy_Pixcustomer_Helper_Data extends Mage_Customer_Helper_Data
{
	const REGISTRY_KEY_FIRST_VISIT = 'first_visit';
	const COOKIE_ID_IS_SUSBCRIBED = 'is_subscribed';
	const COOKIE_ID_EMAIL_CAPTURE_SHOWN = 'emailCapture_shown';
    const LOGIN_CLASS_NAME = 'Pixafy_Pixcustomer_AccountController';
    const LOGIN_FUNCTION_NAME = 'loginPostAction';

    /**
     * Retrieve customer login url
     *
     * @return string
     */
    public function getLoginUrl()
    {
        return "javascript:showModal('login');";
    }

    /**
     * Retrieve customer register form url
     *
     * @return string
     */
    public function getRegisterUrl()
    {
        return "javascript:showModal('register');";
    }

	/**
     * Get customer session instance
     *
     * @return Mage_Customer_Model_Session
     */
    public function getCustomerSession()
    {
        return Mage::getSingleton("customer/session");
    }
	
		
	/**
     * Get cookie model instance
     *
     * @return Mage_Core_Model_Cookie
     */
    public function getCookie()
    {
        return Mage::getSingleton('core/cookie');
    }
	
	/**
     * Get configured cookie domain
     *
     * @return string
     */
    public function getCookieDomain()
    {
        $domain = $this->getCookie()->getDomain();
        if (!empty($domain[0]) && ($domain[0] !== '.')) {
            $domain = '.'.$domain;
        }
        return $domain;
    }
	
	/**
     * Get configured cookie path
     *
     * @return string
     */
	public function getCookiePath()
	{
		 return $this->getCookie()->getPath();
	}
	
	/**
     * Get configured email capture cookie expiration date
     *
     * @return int
     */
	public function getEmailCaptureCookieTime()
	{
		return 86400;
	}
	
	/**
     * checks if the current customer is subscribed to the newsletter
     *
     * @return string
     */
	public function isSubscribed()
	{
		if(Mage::helper("customer")->isLoggedIn())
		{
			$subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail(Mage::helper("customer")->getCustomer()->getEmail());
			return $subscriber->isSubscribed();
		}
		else
		{
			return false;
		}
	}

    public function getLoginClassName() {
        return self::LOGIN_CLASS_NAME;
    }

    public function getLoginMethodName() {
        return self::LOGIN_CLASS_NAME . '::' . self::LOGIN_FUNCTION_NAME;
    }
}
