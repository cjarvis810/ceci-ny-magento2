<?php
	class Pixafy_Pixcustomer_Model_Observer
	{ 
		/**
		 *@setRedirect
		 * 
		 * Sets redirect if necessary
		 * @param Varien_Object $observer 
		 */
		 public function setRedirect()
		 {
			$url = Mage::helper('core/http')->getHttpReferer();
			if($url)
			{
				$customerSession = Mage::getSingleton("customer/session");
				$customerSession->setBeforeAuthUrl($url);
				$customerSession->setAfterAuthUrl($url);
			}
		 }
		 
		 /**
		 *@checkVisitStatus
		 * 
		 * Checks to see if this is the customers first visit
		 * @param Varien_Object $observer 
		 */
		 public function checkVisitStatus($observer)
		 {
//			 $customerSession = $observer->getCustomerSession();
//			 if(!$customerSession->getVisited())
//			 {
//				Mage::register(Pixafy_Pixcustomer_Helper_Data::REGISTRY_KEY_FIRST_VISIT, true);
//				$customerSession->setVisited(true);
//				$customerSession->setIsFirstVisit(true);
//			 }
		 }
		 
		 /**
		 *@setSubscriptionStatus
		 * 
		 * Sets customer subscription status
		 * @param Varien_Object $observer 
		 */
		 public function setSubscriptionStatus($observer)
		 {
			$isSubscribed = $observer->getSubscriber() ? (int)$observer->getSubscriber()->isSubscribed() : (int)Mage::helper("pixcustomer")->isSubscribed();
			$this->_setSubscriptionStatus($isSubscribed);
		 }
		 
		 private function _setSubscriptionStatus($status)
		 {
			$cookieModel = Mage::getModel('core/cookie');
			$expirationTime = Mage::helper("pixcustomer")->getEmailCaptureCookieTime();
			$path = Mage::helper("pixcustomer")->getCookiePath();
			$domain = Mage::helper("pixcustomer")->getCookieDomain();
			$cookieModel->set(Pixafy_Pixcustomer_Helper_Data::COOKIE_ID_IS_SUSBCRIBED, $status, $expirationTime, $path, $domain);
		 }
	}
?>
