<?php 

class Pixafy_Pixcustomer_Block_Form_Login extends Mage_Customer_Block_Form_Login
{
	public function getForgotPasswordMessage()
	{
		$message = Mage::getSingleton('customer/session')->getForgotMessage();
		Mage::getSingleton('customer/session')->setForgotMessage('');
		return $message;
	}

    public function getPayPalMessage() {
        $message = Mage::getSingleton('customer/session')->getPayPalMessage();
        Mage::getSingleton('customer/session')->setPayPalMessage('');
        return $message;
    }


}


?>