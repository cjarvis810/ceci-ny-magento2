<?php

class Pixafy_Pixeventcalendar_Adminhtml_EventtypeController extends Mage_Adminhtml_Controller_Action {

    /**
     * Our default view
     */
    public function indexAction() {
        $this->loadLayout()
                ->_setActiveMenu('pixafy/pixeventcalendar')
                ->_addContent($this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventtype'))
                ->renderLayout();
    }

    /**
     * Our edit view
     */
    public function editAction() {
        //load our model
        $model = $this->loadModelFromRequest();
        if (!$model) {
            $this->_getSession()->addError('This moodel does not exist');
            return $this->_redirect('*/*/');
        }
        //check for form data        
        $data = $this->_getSession()->getFormData(true);
        if ($data) {
            $model->setData($data);
        }
        Mage::register('event_type_model', $model);
        $block = $this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventtype_edit');
        $this->loadLayout()->_addContent($block);
        $this->renderLayout();
        return $this;
    }

    /**
     * Our new view
     */
    public function newAction() {
        $block = $this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventtype_edit');
        $this->loadLayout()->_addContent($block);
        $this->renderLayout();
        return $this;
    }

    /**
     *  Try to save the model
     *  If editing, we will have an id and try to load it i
     * @return type
     */
    public function saveAction() {
        $params = $this->getRequest()->getParams();
        $eventType = $this->loadModelFromRequest();
        if (!$eventType) {
            if (empty($params['event_name'])) {
                $this->_getSession()->addError('Event name not given');
                return $this->_redirect('*/*/');
            }
            $eventType = Mage::getModel('pixeventcalendar/event_type');
        }
        $eventType->setName($params['event_name']);
        $eventType->setCalendarId($params['calendar_id']);
        return $this->tryToSaveModel($eventType);
    }

    /**
     * Delete the event type
     * @return type
     */
    public function deleteAction() {
        $eventType = $this->loadModelFromRequest();
        if (!$eventType) {
            $this->_getSession()->addError('This moodel does not exist');
            return $this->_redirect('*/*/');
        }
        $eventType->delete();
        $this->_getSession()->addSuccess('Event Type deleted succesfully');
        return $this->_redirect('*/*/');
    }

    /**
     * Try to save the model, if it does not work redirect
     * @param type $eventType
     * @return type
     */
    protected function tryToSaveModel($eventType) {
        try {
            $eventType->save();
        } catch (Exception $e) {
            $this->_getSession()->addError('Event Type could not save, possibly duplicate.');
            return $this->_redirect('*/*/');
        }
        $this->_getSession()->addSuccess('Event Type saved succesfully');
        return $this->_redirect('*/*/');
    }

    /**
     * Attempt to load the model from request
     * @return boolean
     */
    protected function loadModelFromRequest() {
        $id = $this->getRequest()->getParam('id', false);
        if (!$id) {
            return false;
        }
        $eventType = Mage::getModel('pixeventcalendar/event_type')->load($id);
        if (!$eventType->getId()) {
            return false;
        }
        return $eventType;
    }

}
