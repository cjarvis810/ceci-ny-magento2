<?php

class Pixafy_Pixeventcalendar_Adminhtml_EventcalendarController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        parent::_construct();
    }

    public function indexAction() {
        $this->loadLayout()
                ->_setActiveMenu('pixafy/pixeventcalendar')
                ->_addContent($this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventcalendar'));
        $this->renderLayout();
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id', null);
        $caledar = Mage::getModel('pixeventcalendar/calendar');
        if ($id) {
            $caledar->load((int) $id);
            if ($caledar->getCalendarId()) {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if ($data) {
                    $caledar->setData($data)->setCalendarId($id);
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixeventcalendar')->__('Does not exist'));
                $this->_redirect('*/*/');
            }
        }
        Mage::register('registry_data', $caledar);
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventcalendar_edit'))->_addLeft($this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventcalendar_edit_tabs'));
        $this->renderLayout();
        return $this;
    }

    public function editeventAction() {
        $id = $this->getRequest()->getParam('id', null);
        $event = Mage::getModel('pixeventcalendar/event');
        if ($id) {
            $event->load((int) $id);
            if ($event->getCalendarId()) {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if ($data) {
                    $event->setData($data)->setCalendarId($id);
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixeventcalendar')->__('Does not exist'));
                $this->_redirect('*/*/');
            }
        }
        Mage::register('event_data', $event);
        $this->loadLayout();
        $this->loadWidgetScripts();
        $this->_addContent($this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventcalendar_editevent'));
        $this->renderLayout();
        return $this;
    }

    protected function loadWidgetScripts() {
        $head = $this->getLayout()->getBlock('head');
        $head->setCanLoadExtJs(true);
        $head->addItem("js", "mage/adminhtml/variables.js");
        $head->addItem("js", "mage/adminhtml/wysiwyg/widget.js");
        $head->addItem("js", "lib/flex.js");
        $head->addItem("js", "lib/FABridge.js");
        $head->addItem("js", "mage/adminhtml/flexuploader.js");
        $head->addItem("js", "mage/adminhtml/browser.js");
        $head->addItem("js", "prototype/window.js");
        $head->addItem("js_css", "prototype/windows/themes/default.css");
        $head->addCss('lib/prototype/windows/themes/magento.css');
    }

    public function saveAction() {
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();
            if ($data) {
                try {
                    $id = $this->getRequest()->getParam('id', NULL);
                    if (!$id) {
                        $calendar->createdAt = now();
                    } else {
                        $calendar->updatedAt = now();
                    }
                    $caledar = Mage::getModel('pixeventcalendar/calendar')->load($id);
                    $caledar->addData($data);
                    $caledar->save();
                    if ($this->getRequest()->getParam('caledar_id')) {
                        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('calendar_id')));
                    } else {
                        $this->_redirect('*/*/');
                    }
                } catch (Exception $e) {
                    $this->_getSession()->addError(Mage::helper('pixeventcalendar')->__('An error occurred while saving the registry data. Please review the log and try again.'));
                    Mage::logException($e);
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return $this;
                }
            }
        }
    }

    public function saveeventAction() {
        if ($this->getRequest()->getPost()) {
            try {
                $data = $this->getRequest()->getPost();
                $id = $this->getRequest()->getParam('id');
                if ($data) {
                    $imgUpload = $this->handleImage();
                    if ($imgUpload || !empty($data['image']['delete'])) { //if file is being uploaded or delete checked
                        $data['image'] = $imgUpload;
                    } else {
                        unset($data['image']);//make sure we don't mess with what is there
                    }
                    $caledar = Mage::getModel('pixeventcalendar/event')->load($id);
                    $caledar->addData($data);
                    $caledar->save();
                    $this->_getSession()->addSuccess('Event saved succesfully');
                    $this->_redirect('*/*/editevent', array('id' => $caledar->getId()));
                }
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('pixeventcalendar')->__('An error occurred while saving the registry data. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/editevent', $id);
                return $this;
            }
        }
    }

    /**
     * 
     * @return file path or null
     */
    protected function handleImage() {
        if (!(isset($_FILES['image']) && file_exists($_FILES['image']['tmp_name']))) {
            return null;
        }
        $uploader = new Varien_File_Uploader('image');
        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png')); // or pdf or anything
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);
        $path = Mage::getBaseDir('media') . '/wysiwyg/eventimages';
        //if this fails, we'll jsut add the message to session, but we'll allow the saving to continue
        try {
            $uploader->save($path, $_FILES['image']['name']);
            return "wysiwyg/eventimages/{$_FILES['image']['name']}";
        } catch (Exception $e) {
            $this->_getSession()->addError("There was an errror saving the image: " . $e->getMessage());
            return null;
        }
    }

    public function newAction() {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventcalendar_edit'))->_addLeft($this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventcalendar_edit_tabs'));
        $this->renderLayout();
    }

    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('pixeventcalendar/calendar');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pixeventcalendar')->__('The calendar has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find the calendar to delete.'));
        $this->_redirect('*/*/');
    }

    public function deleteeventAction() {
        if ($id = $this->getRequest()->getParam('calendar_id')) {
            try {
                $model = Mage::getModel('pixeventcalendar/event');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pixeventcalendar')->__('The event has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/editevent', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find the event to delete.'));
        $this->_redirect('*/*/');
    }

    public function massDeleteAction() {
        $this->loadLayout();
        $this->renderLayout();
        return $this;
    }

}
