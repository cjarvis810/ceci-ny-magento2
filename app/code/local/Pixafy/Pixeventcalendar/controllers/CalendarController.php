<?php

class Pixafy_Pixeventcalendar_CalendarController extends Mage_Core_Controller_Front_Action {

    public function getEventsJsonAction() {
        $identifier = $this->getRequest()->getParam('identifier');
        $startDate = $this->getRequest()->getParam('start_date');
        $endDate = $this->getRequest()->getParam('end_date');
        $data = array(
            'calendar'=>array(),
            'events' => array()
        );
        $calendar = Mage::getModel('pixeventcalendar/calendar')->load($identifier, 'identifier');
        $data['calendar']['name']=$calendar->calendar_name;
        if ($calendar->getId()) {
            $events = $calendar->getEventCollection()
                ->addFieldToFilter('status', Pixafy_Pixeventcalendar_Model_Event::STATUS_ENABLED)
                ->addFieldToFilter('start_date', array('gteq' => $startDate, 'lteq' => $endDate))
                ->setOrder('start_date', 'ASC');
            foreach ($events as $i => $event) {
                $startDate = strtotime($event->getStartDate());
                $endDate = strtotime($event->getEndDate());
                $datediff = floor(($endDate - $startDate)/(60 * 60 * 24));
                for ($i = 0; $i <= $datediff; $i++) {
                    $start=str_replace(' ', 'T', $event->getStartDate());
                    $end=str_replace(' ', 'T', $event->getEndDate());
                    if(0<$datediff){
                        $start= date('Y-m-d\TH:i:s',strtotime($start . '+'.$i.' day'));
                        if($i!=$datediff) {
                            $end = $start;
                        }
                    }
                    $data['events'][] = array(
                        'title' => $event->getEventName(),
                        'start' => $start,
                        'end' => $end,
                        'location' => $event->getVenue(),
                        'description' => $event->getDescription(),
                        'url' => $event->getUrl() ? $event->getUrl() : '',
                        'appointments' => $event->getAppointments() ? $event->getAppointments() : '',
                        'register' => $event->getRegister() ? $event->getRegister() : ''
                    );
                }
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}

