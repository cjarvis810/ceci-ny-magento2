<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Gridevents extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        $this->setId('eventGrid');
        $this->setDefaultSort('event_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->_addButtonLabel = Mage::helper('pixeventcalendar')->__('Add Calendar');
        parent::__construct();
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('pixeventcalendar/event_collection')
                ->joinEventType()
                ->filterByCalendarId($this->getRequest()->getParam('id'));      
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('event_id', array(
            'header' => 'Event Id',
            'align' => 'right',
            'width' => '50px',
            'index' => 'event_id',
        ));
        $this->addColumn('event_name', array(
            'header' => 'Name',
            'align' => 'left',
            'index' => 'event_name',
        ));
        $this->addColumn('description', array(
            'header' => 'Description',
            'align' => 'left',
            'index' => 'description',
        ));
        $this->addColumn('start_date', array(
            'header' => 'Start Date',
            'align' => 'left',
            'index' => 'start_date',
        ));
        $this->addColumn('end_date', array(
            'header' => 'End Date',
            'align' => 'left',
            'index' => 'end_date',
        ));
        $this->addColumn('created_at', array(
            'header' => 'Created at',
            'align' => 'left',
            'index' => 'created_at',
        ));
        $this->addColumn('updated_at', array(
            'header' => 'Updated at',
            'align' => 'left',
            'index' => 'updated_at',
        ));
        $this->addColumn('status', array(
            'header' => 'Status',
            'align' => 'left',
            'index' => 'status',
        ));
        $this->addColumn('event_type', array(
            'header' => 'Event Type',
            'align' => 'left',
            'index' => 'name',
        ));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/editevent', array('id' => $row->getEventId()));
    }

}
