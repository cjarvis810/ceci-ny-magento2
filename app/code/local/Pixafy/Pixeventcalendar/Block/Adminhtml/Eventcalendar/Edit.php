<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	public function __construct() {
		parent::__construct();
		$this->_objectId = 'calendar_id';
		$this->_blockGroup = 'pixeventcalendar';
		$this->_controller = 'adminhtml_eventcalendar';
		$this->setTitle(Mage::helper('pixeventcalendar')->__('Calendar Information'));
		$this->_mode = 'edit';
		$this->_addButton('add', array('label' => 'New Event', 'onclick' => 'addEventEdit()', 'class' => 'add'), 0);
		$this->_updateButton('save', 'label', Mage::helper('pixeventcalendar')->__('Save Calendar'));
		$this->_updateButton('delete', 'label', Mage::helper('pixeventcalendar')->__('Delete Calendar'));
//		$this->_addButton('saveandcontinue', array(
//				'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
//				'onclick' => 'saveAndContinueEdit()',
//				'class' => 'save',
//				), -200);

		$this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('form_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'edit_form');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
                }
            }
 
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
			
			function addEventEdit(){
				window.location.href = '" . Mage::helper("adminhtml")->getUrl('*/*/editevent/', array('calendar_id' => $this->getRequest()->getParam('id'))) . "';
            }
        ";
	}

	/*
	 * This function is responsible for Including TincyMCE in Head.
	 */
	protected function _prepareLayout() {
		parent::_prepareLayout();
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		}
	}

	public function getHeaderText() {
		if (Mage::registry('registry_data') && Mage::registry('registry_data')->getId())
			return Mage::helper('pixeventcalendar')->__("Edit Calendar '%s'", $this->htmlEscape(Mage::registry('registry_data')->getCalendarName()));
		return Mage::helper('pixeventcalendar')->__('Add Calendar');
	}

}
