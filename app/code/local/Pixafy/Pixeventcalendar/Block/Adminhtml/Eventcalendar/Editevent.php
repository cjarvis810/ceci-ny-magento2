<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Editevent extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_objectId = 'event_id';
        $this->_blockGroup = 'pixeventcalendar';
        $this->_controller = 'adminhtml_eventcalendar';
        $this->setTitle(Mage::helper('pixeventcalendar')->__('Event Information'));
        $this->_mode = 'edit';
//		$this->_addButton('saveandcontinue', array(
//				'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
//				'onclick' => 'saveAndContinueEdit()',
//				'class' => 'save',
//				), -200);
        $this->_updateButton('save', 'label', Mage::helper('pixeventcalendar')->__('Save Event'));
        $this->_updateButton('delete', 'label', Mage::helper('pixeventcalendar')->__('Delete Event'));

        if ($this->getRequest()->getParam('id')) {
            $this->_addButton('delete', array(
                'label'     => Mage::helper('adminhtml')->__('Delete Event'),
                'class'     => 'delete',
                'onclick'   => 'deleteConfirm(\''. Mage::helper('adminhtml')->__('Are you sure you want to do this?')
                    .'\', \'' . Mage::helper("adminhtml")->getUrl('*/*/deleteevent/', array('calendar_id' => $this->getRequest()->getParam('id'))) . '\')',
            ));
        }

        $this->_formScripts[] = "
			function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/editevent/');
            }
            function toggleEditor() {
                if (tinyMCE.getInstanceById('form_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'edit_form');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
                }
            }
        ";
        parent::__construct();
    }

    public function getFormHtml() {
        $this->getChild('formevent')->setData('action', $this->getSaveUrl());
        return $this->getChildHtml('formevent');
    }

    protected function _prepareLayout() {
        if ($this->_blockGroup && $this->_controller && $this->_mode) {
            $this->setChild('formevent', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_' . $this->_mode . '_formevent'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
    }

    public function getHeaderText() {
        if (Mage::registry('event_data') && Mage::registry('event_data')->getId())
            return Mage::helper('pixeventcalendar')->__("Edit Event '%s'", $this->htmlEscape(Mage::registry('event_data')->getEventName()));
        return Mage::helper('pixeventcalendar')->__('Add Event');
    }

}
