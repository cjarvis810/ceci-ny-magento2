<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Edit_Formevent extends Mage_Adminhtml_Block_Widget_Form {

    public function __construct(array $args = array()) {
        parent::__construct($args);
    }

    protected function _prepareForm() {
        if (Mage::registry('event_data')) {
            $data = Mage::registry('event_data')->getData();
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/saveevent', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
                )
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getFormData()) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData();
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        } elseif (Mage::registry('event_data')) {
            $data = Mage::registry('event_data')->getData();
        }
        if ($this->getRequest()->getParam('calendar_id')) {
            $data['calendar_id'] = $this->getRequest()->getParam('calendar_id');
        }
        $fieldset = $form->addFieldset('event_form', array(
            'legend' => Mage::helper('pixeventcalendar')->__('Event information')));
        $fieldset->addField('calendar_id', 'hidden', array(
            'name' => 'calendar_id',
        ));
        $fieldset->addField('event_name', 'text', array(
            'label' => Mage::helper('pixeventcalendar')->__('Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'event_name',
        ));
        
        $options = ['0'=>'-- No Type --'];
                 
        foreach( Mage::getResourceModel("pixeventcalendar/event_type_collection") as $eventType ){
            $options[$eventType->getId()] = $eventType->getName(); 
        }
        
        
        $fieldset->addField('event_type_id', 'select', array(
            'label' => Mage::helper('pixeventcalendar')->__('Status'),
            'class' => 'required-entry',
            'required' => false,
            'name' => 'event_type_id',
            'values' => $options,
        ));


        /*
         * Editing the form field in wysiwyg editor.
         */
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $wysiwygConfig->addData(array(
            'add_variables' => false,
            'add_widgets' => true,
            'add_images' => true,
            'directives_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
            'directives_url_quoted' => preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive')),
            'widget_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/widget/index'),
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
            'files_browser_window_width' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_width'),
            'files_browser_window_height' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_height')
        ));


        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => Mage::helper('pixeventcalendar')->__('Description'),
            'title' => Mage::helper('pixeventcalendar')->__('Description'),
            'style' => 'width:800px; height:500px;',
            'config' => $wysiwygConfig,
            'class' => 'required-entry',
            'required' => true,
            'wysiwyg' => true
        ));

        $fieldset->addField('venue', 'text', array(
            'label' => Mage::helper('pixeventcalendar')->__('Event venue'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'venue',
        ));
        $fieldset->addField('start_date', 'date', array(
            'label' => Mage::helper('pixeventcalendar')->__('Start Date'),
            'class' => 'required-entry',
            'tabindex' => 1,
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'required' => true,
            'name' => 'start_date',
        ));
        $fieldset->addField('end_date', 'date', array(
            'label' => Mage::helper('pixeventcalendar')->__('End Date'),
            'tabindex' => 1,
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'required' => false,
            'name' => 'end_date',
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('pixeventcalendar')->__('Status'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'status',
            'value'=>1,
            'values' => array(array('value' => '1','label'=>'Enabled'),array('value' => '0','label'=>'Disabled')),
        ));

        $fieldset->addField('url', 'text', array(
            'label' => Mage::helper('pixeventcalendar')->__('URL'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'url',
        ));

        $fieldset->addField('image', 'image', array(
            'label' => 'Event Image',
            'required' => false,
            'name' => 'image'            
        ));
        
        $fieldset->addField('extra_content', 'editor', array(
            'label' => 'Extra Content',
            'required' => false,
            'name' => 'extra_content',
            'config' => $wysiwygConfig,
            'required' => false,
            'wysiwyg' => true
        ));

        $fieldset->addField('appointments', 'text', array(
            'label' => Mage::helper('pixeventcalendar')->__('Appointments'),
            'class' => 'required-entry',
            'required' => false,
            'name' => 'appointments',
        ));
        $fieldset->addField('register', 'editor', array(
            'name' => 'register',
            'label' => Mage::helper('pixeventcalendar')->__('Register'),
            'title' => Mage::helper('pixeventcalendar')->__('Register'),
            'style' => 'width:800px; height:50px;',
            'config' => $wysiwygConfig,
            'class' => 'required-entry',
            'required' => false,
            'wysiwyg' => true
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }

}
