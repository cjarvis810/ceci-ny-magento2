<?php

//class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Edit_Tab_Content extends Mage_Adminhtml_Block_Template implements Mage_Adminhtml_Block_Widget_Tab_Interface {
class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Edit_Tab_Content extends Mage_Adminhtml_Block_Widget_Form {
 
    protected function _prepareForm() {
 
        if (Mage::registry('registry_data')) {
            $data = Mage::registry('registry_data')->getData();
        } else {
            $data = array();
        }
 
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('registry_form', array('legend' => Mage::helper('pixeventcalendar')->__('More information')));
 
        /*
         * Editing the form field in wysiwyg editor.
         */
 
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $wysiwygConfig->addData(array('add_variables' => false,
            'add_widgets' => true,
            'add_images' => true,
            'directives_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
            'directives_url_quoted' => preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive')),
            'widget_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/widget/index'),
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
            'files_browser_window_width' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_width'),
            'files_browser_window_height' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_height')
        ));
 
 
        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => Mage::helper('pixeventcalendar')->__('Description'),
            'title' => Mage::helper('pixeventcalendar')->__('Description'),
            'style' => 'width:800px; height:500px;',
            'config' => $wysiwygConfig,
            'required' => false,
            'wysiwyg' => true
        ));
 
        $form->setValues($data);
    }
}