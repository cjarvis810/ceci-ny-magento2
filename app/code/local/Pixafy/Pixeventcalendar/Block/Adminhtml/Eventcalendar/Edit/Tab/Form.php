<?php

//class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
		if (Mage::registry('registry_data')) {
			$data = Mage::registry('registry_data')->getData();
		} else {
			$data = array();
		}

		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('registry_form', array('legend' => Mage::helper('pixeventcalendar')->__('Calendar Information')));

//		$fieldset->addField('calendar_id', 'hidden', array(
//				'name' => 'calendar_id',
//		));

		$fieldset->addField('calendar_name', 'text', array(
				'label' => Mage::helper('pixeventcalendar')->__('Name'),
				'class' => 'required-entry',
				'required' => true,
				'name' => 'calendar_name',
		));
        
        $fieldset->addField('identifier', 'text', array(
				'label' => Mage::helper('pixeventcalendar')->__('Identifier'),
				'class' => 'required-entry',
				'required' => true,
				'name' => 'identifier',
		));
        
		$form->setValues($data);
		return parent::_prepareForm();
	}

}
