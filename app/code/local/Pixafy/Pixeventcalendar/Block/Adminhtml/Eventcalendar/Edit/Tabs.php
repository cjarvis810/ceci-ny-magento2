<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

	public function __construct() {
		parent::__construct();
		$this->setId('eventcalendar_tabs');
		$this->setDestElementId('edit_form'); // this should be same as the form id define above
		$this->setTitle(Mage::helper('pixeventcalendar')->__('Calendar Information'));
	}

	protected function _beforeToHtml() {
		$this->addTab('form_section', array(
				'label' => Mage::helper('pixeventcalendar')->__('Calendar Information'),
				'title' => Mage::helper('pixeventcalendar')->__('Calendar Information'),
				'content' => $this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventcalendar_edit_tab_form')->toHtml(),
		));

        if ($this->getRequest()->getActionName() == 'edit') {
            $this->addTab('form_section1', array(
                    'label' => Mage::helper('pixeventcalendar')->__('Events'),
                    'title' => Mage::helper('pixeventcalendar')->__('Events'),
                    'content' => $this->getLayout()->createBlock('pixeventcalendar/adminhtml_eventcalendar_gridevents')->toHtml(),
            ));
        }

		return parent::_beforeToHtml();
	}

}
