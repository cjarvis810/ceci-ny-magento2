<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();
		$this->setId('eventcalendarGrid');
		$this->setDefaultSort('calendar_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$collection = Mage::getModel('pixeventcalendar/calendar')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('calendar_id', array(
				'header' => 'ID',
				'align' => 'right',
				'width' => '50px',
				'index' => 'calendar_id',
		));
		$this->addColumn('identifier', array(
				'header' => 'Identifier',
				'align' => 'left',
				'index' => 'identifier',
		));
		$this->addColumn('calendar_name', array(
				'header' => 'Name',
				'align' => 'left',
				'index' => 'calendar_name',
		));
		$this->addColumn('created_at', array(
				'header' => 'Created at',
				'align' => 'left',
				'index' => 'created_at',
		));
		$this->addColumn('updated_at', array(
				'header' => 'Updated at',
				'align' => 'left',
				'index' => 'updated_at',
		));
		return parent::_prepareColumns();
	}

	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit', array('id' => $row->getCalendarId()));
	}

//	protected function _prepareMassaction() {
//		$this->setMassactionIdField('calendar_id');
//		$this->getMassactionBlock()->setFormFieldName('registries');
//		$this->getMassactionBlock()->addItem('delete', array(
//				'label' => Mage::helper('pixeventcalendar')->__('Delete'),
//				'url' => $this->getUrl('*/*/massDelete'),
//				'confirm' => Mage::helper('pixeventcalendar')->__('Are you sure?')
//		));
//		return $this;
//	}

}
