<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventcalendar extends Mage_Adminhtml_Block_Widget_Grid_Container {

	public function __construct() {
		$this->_controller = 'adminhtml_eventcalendar';		
		$this->_blockGroup = 'pixeventcalendar';		
		$this->_headerText = Mage::helper('pixeventcalendar')->__('Calendar');
		$this->_addButtonLabel = Mage::helper('pixeventcalendar')->__('Add Calendar');
		parent::__construct();
	}


}
