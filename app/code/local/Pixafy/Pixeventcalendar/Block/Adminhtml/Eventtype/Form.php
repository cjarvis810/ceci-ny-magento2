<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventtype_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
                )
        );
        $fieldset = $form->addFieldset('event_type_form', array('legend' => 'ref information'));        
        $fieldset->addField('name', 'text', array(
            'label' => 'Event Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'event_name',
        ));        
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
