<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventtype_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('eventType');
        $this->setDefaultSort('event_type_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $this->setCollection(Mage::getResourceModel('pixeventcalendar/event_type_collection')->joinCalendar());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('event_type_id', array(
            'header' => 'Event Id',
            'align' => 'right',
            'width' => '50px',
            'index' => 'event_type_id',
        ));
        $this->addColumn('name', array(
            'header' => 'Event Name',
            'align' => 'left',
            'index' => 'name',
        ));
        $this->addColumn('calendar_name', array(
            'header' => 'Calendar Name',
            'align' => 'left',
            'index' => 'calendar_name',
        ));
        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row){        
        return $this->getUrl('*/*/edit', array('id'=>$row->getId()));
    }
}