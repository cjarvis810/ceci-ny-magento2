<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventtype_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();
        $editMode = Mage::registry('event_type_model');
        $this->_objectId = 'event_type_id';
        $this->_blockGroup = 'pixeventcalendar';
        $this->_controller = 'adminhtml_eventtype';
        $this->_mode = 'edit';
        $this->_headerText = $editMode ? 'Edit Event Type' : "New Event Type";
        $this->_updateButton('save', 'label', 'Save Event Type');
        $this->_updateButton('delete', 'label', 'Delete Event Type');
    }

}
