<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventtype_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * If we have a model here, it means we are editing / if not it is 'new'
     * @var type 
     */
    protected $model = null;

    public function __construct(array $args = array()) {
        if ($model = Mage::registry('event_type_model')) {
            $this->model = $model->getData();
        }
        parent::__construct($args);
    }

    protected function _prepareForm() {

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
                )
        );

        $legend = $this->model ? "Edit information for Event Type: " . $this->model["name"] : "Enter information for new Event Type";

        $fieldset = $form->addFieldset('event_type_form', array('legend' => $legend));
        $fieldset->addField('name', 'text', array(
            'label' => 'Event Type Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'event_name',
        ));
        
        $options = array();
        
        foreach (Mage::getResourceModel("pixeventcalendar/calendar_collection") as $c) {
            $options[$c->getId()] = $c->getCalendarName();
        }
        
        $fieldset->addField('calendar_id', 'select', array(
            'label' => Mage::helper('pixeventcalendar')->__('Calendar'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'calendar_id',
            'values' => $options,
        ));


        $form->setUseContainer(true);
        $this->setForm($form);
        if ($this->model) {
            $form->setValues($this->model);
        }

        return parent::_prepareForm();
    }

}
