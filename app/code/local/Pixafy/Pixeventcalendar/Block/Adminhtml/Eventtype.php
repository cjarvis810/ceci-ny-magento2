<?php

class Pixafy_Pixeventcalendar_Block_Adminhtml_Eventtype extends Mage_Adminhtml_Block_Widget_Grid_Container {
    public function _construct() {
        parent::_construct();
        $this->_controller = 'adminhtml_eventtype';
        $this->_blockGroup = 'pixeventcalendar';
        $this->_headerText = 'Event Types';
        $this->_addButtonLabel = 'Add Event Type';
    }
}