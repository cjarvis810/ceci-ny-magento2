<?php

class Pixafy_Pixeventcalendar_Model_Event extends Mage_Core_Model_Abstract {
    
    const STATUS_DISABLED = false;
    const STATUS_ENABLED = true;
    
    protected function _construct() {
        $this->_init('pixeventcalendar/event');
    }
    
}

