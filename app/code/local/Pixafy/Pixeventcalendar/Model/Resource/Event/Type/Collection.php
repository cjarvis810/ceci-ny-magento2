<?php

class Pixafy_Pixeventcalendar_Model_Resource_Event_Type_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    //whether a calendar is joined
    protected $calendarJoined = false;
    
    /**
     * nothing special here
     */
    protected function _construct() {
        $this->_init('pixeventcalendar/event_type');
    }

    /**
     * Filter by calendar 
     * @param type $id
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Collection
     */
    public function filterByCalendarId($id) {
        $this->addFieldToFilter('calendar_id', array('eq' => $id));
        return $this;
    }

    /**
     * Join calendar if not yet joined
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Type_Collection
     */
    public function joinCalendar() {
        if (!$this->calendarJoined) {
            $this->getSelect()
                    ->join(array("calendar" => "pixeventcalendar_calendar"), "main_table.calendar_id = calendar.calendar_id", "calendar.*");
            $this->calendarJoined = true;
        }
        return $this;
    }
    
    /**
     * Get Calendar
     * @param type $calendarName
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Type_Collection
     */    
    public function filterByCalendarName($calendarName) {
        $this->joinCalendar()->addFieldToFilter("calendar.identifier", $calendarName);        
        return $this;
    }
    
    /**
     * Add display order 
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Type_Collection
     */
    public function orderForDisplay(){
        $this->addOrder("main_table.display_order", "ASC");
        return $this;
    }

}
