<?php

class Pixafy_Pixeventcalendar_Model_Resource_Event_Type extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct() {
        $this->_init('pixeventcalendar/event_type', 'event_type_id');
    }
}