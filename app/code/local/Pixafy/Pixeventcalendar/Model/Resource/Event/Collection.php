<?php

class Pixafy_Pixeventcalendar_Model_Resource_Event_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    
    /**
     * If the event type is already joined
     * @var type 
     */
    protected $eventTypeJoined = false;
    
    /**
     * Whether the calendar is joined
     * @var bool 
     */
    protected $calendarJoined = false;
    
    
    protected function _construct() {
        $this->_init('pixeventcalendar/event');
    }
    
    /**
     * Join the even type so we can get the name
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Collection
     */
    public function joinEventType() {
        if( !$this->eventTypeJoined ){
            $this->getSelect()
                ->joinLeft(array("event_type" => "pixeventcalendar_event_type"), "main_table.event_type_id = event_type.event_type_id", "event_type.*");
            $this->eventTypeJoined = true;
        }
        return $this;
    }
    
    /**   
     * Join calendar if not yet joined
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Collection
     */
    public function joinCalendar() {
        if (!$this->calendarJoined) {
            $this->getSelect()
                    ->join(array("calendar" => "pixeventcalendar_calendar"), "main_table.calendar_id = calendar.calendar_id", "calendar.*");
            $this->calendarJoined = true;
        }
        return $this;
    }
    
    /**
     * Filter by calendar 
     * @param type $id
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Collection
     */
    public function filterByCalendarId( $id ){
        $this->addFieldToFilter('main_table.calendar_id', array('eq' => $id));
        return $this;
    }
    
    /**
     * Filter by calendar name
     * @param type $calendarName
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Collection
     */ 
    public function filterByCalendarName($calendarName) {
        $this->joinCalendar()->addFieldToFilter("calendar.identifier", $calendarName);        
        return $this;
    }
    
    
    /**
     * Add order by date
     * @return \Pixafy_Pixeventcalendar_Model_Resource_Event_Collection
     */
    public function orderByDate(){
        $this->addOrder("main_table.start_date", "ASC");
        return $this;
    }


}
