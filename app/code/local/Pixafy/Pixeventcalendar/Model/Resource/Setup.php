<?php

class Pixafy_Pixeventcalendar_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup {
    public function createSampleCalendar() {
        $calendar = Mage::getModel('pixeventcalendar/calendar')
            ->setData(array(
                'identifier' => 'appearances',
                'name' => 'Ceci\'s Appearances & Talks'
            ))->save();

        $event = Mage::getModel('pixeventcalendar/event')
            ->setData(array(
                'calendar_id' => $calendar->getId(),
                'status' => Pixafy_Pixeventcalendar_Model_Event::STATUS_ENABLED,
                'calendar_name' => 'Morgan Stanley Executive Women\'s Breakfast',
                'venue' => '1585 Broadway, 27th Floor, New York, New York',
                'description' => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                'start_date' => mktime(10, 30, 0, 10, 12, 2014),
                'end_date' => mktime(12, 30, 0, 10, 12, 2014),
            ))->save();

        $event = Mage::getModel('pixeventcalendar/event')
            ->setData(array(
                'calendar_id' => $calendar->getId(),
                'status' => Pixafy_Pixeventcalendar_Model_Event::STATUS_ENABLED,
                'event_name' => 'Ceci Speaks at the Knot Conference',
                'venue' => '130 West 23rd Street',
                'description' => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                'start_date' => mktime(10, 30, 0, 10, 25, 2014),
                'end_date' => mktime(12, 30, 0, 10, 25, 2014),
            ))->save();
    }
}

