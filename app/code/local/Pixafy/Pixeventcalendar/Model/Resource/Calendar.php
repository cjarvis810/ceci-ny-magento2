<?php

class Pixafy_Pixeventcalendar_Model_Resource_Calendar extends Mage_Core_Model_Resource_Db_Abstract {
    
    protected function _construct() {
        $this->_init('pixeventcalendar/calendar', 'calendar_id');
    }
    
    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
        $now = Mage::getSingleton('core/date')->timestamp(time());
        if (!$object->getId()) {
            $object->setCreatedAt($now);
        }
        $object->setUpdatedAt($now);
        return $this;
    }
    
}

