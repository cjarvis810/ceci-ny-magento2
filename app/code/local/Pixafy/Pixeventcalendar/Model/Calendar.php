<?php

class Pixafy_Pixeventcalendar_Model_Calendar extends Mage_Core_Model_Abstract {
    
    protected function _construct() {
        $this->_init('pixeventcalendar/calendar');
    }   
    
    public function getEventCollection() {
        return Mage::getResourceModel('pixeventcalendar/event_collection')
            ->addFieldToFilter('calendar_id', $this->getId());
    }
    
}

