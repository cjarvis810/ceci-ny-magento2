<?php

try {

    $installer = $this;
    $installer->startSetup();

    // edit event table
    $table = $installer->getTable('pixeventcalendar/event');
    // add appointments column
    $installer->getConnection()
        ->addColumn($table, 'appointments', array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 255,
            'comment' => 'Appointments'
        ));
    // add register column
    $installer->getConnection()
        ->addColumn($table, 'register', array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'Register'
        ));

    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}

