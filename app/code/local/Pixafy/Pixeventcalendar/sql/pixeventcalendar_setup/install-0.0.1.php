<?php

try {
    
    $installer = $this;
    $installer->startSetup();

    // create calendar table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixeventcalendar/calendar'))
        ->addColumn('calendar_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
            ), 'Calendar Id')
        ->addColumn('identifier', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Identfier')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Name')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Creation Time')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Update Time')
        ->addIndex($installer->getIdxName('pixeventcalendar/calendar', array('identifier'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('identifier'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->setComment('Calendars');
    $installer->getConnection()->createTable($table);

    // create event table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixeventcalendar/event'))
        ->addColumn('event_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
            ), 'Event Id')
        ->addColumn('calendar_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            ), 'Calendar Id')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default' => '0'
            ), 'Status')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Identfier')
        ->addColumn('venue', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Venue')
        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
            ), 'Name')
        ->addColumn('start_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Start Date')
        ->addColumn('end_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'End Date')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Creation Time')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Update Time')
        ->addIndex($installer->getIdxName('pixeventcalendar/event', array('start_date')),
            array('start_date'))
        ->addIndex($installer->getIdxName('pixeventcalendar/event', array('end_date')),
            array('end_date'))
        ->addForeignKey($installer->getFkName('pixeventcalendar/event', 'calendar_id', 'pixeventcalendar/calendar', 'calendar_id'),
            'calendar_id', $installer->getTable('pixeventcalendar/calendar'), 'calendar_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Calendar Events');
    $installer->getConnection()->createTable($table);

    $installer->endSetup();
    
    $installer->createSampleCalendar();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}

