<?php

try {
    
    $installer = $this;
    $installer->startSetup();

	$installer->run("ALTER TABLE {$this->getTable('pixeventcalendar_calendar')} CHANGE `name` `calendar_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Calendar name';");
	$installer->run("ALTER TABLE {$this->getTable('pixeventcalendar_event')} CHANGE `name` `event_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Event name';");
	$installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}

