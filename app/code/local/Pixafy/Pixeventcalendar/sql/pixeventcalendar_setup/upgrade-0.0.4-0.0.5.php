<?php

$installer = $this;
$installer->startSetup();

// edit event table
$table = $installer->run(
      "CREATE TABLE `pixeventcalendar_event_type` (
      `event_type_id` smallint(6) NOT NULL AUTO_INCREMENT,
      `name` varchar(255) NOT NULL,
      PRIMARY KEY (`event_type_id`),
      UNIQUE KEY `name` (`name`)
    );
 
    ALTER TABLE `pixeventcalendar_event`
    ADD COLUMN `event_type_id`  smallint NULL,
    ADD COLUMN `image`  varchar(255) NULL;
 "
);


$installer->endSetup();



