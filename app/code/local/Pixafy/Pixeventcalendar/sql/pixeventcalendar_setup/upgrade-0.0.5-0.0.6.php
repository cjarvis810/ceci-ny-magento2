<?php

$installer = $this;
$installer->startSetup();

// edit event table
$table = $installer->run("    
    ALTER TABLE `pixeventcalendar_event_type`
    ADD COLUMN `calendar_id` smallint NOT NULL;
    
    ALTER TABLE `pixeventcalendar_event_type`
    DROP INDEX `name`,
    ADD UNIQUE INDEX `name_calendar` (`name`, `calendar_id`) ;
    
    ALTER TABLE `pixeventcalendar_event`    
    ADD COLUMN `extra_content` varchar(255) NULL;
    
    ALTER TABLE `pixeventcalendar_event_type`
    ADD COLUMN `display_order`  tinyint UNSIGNED NOT NULL DEFAULT 1 COMMENT 'display order, intentionally not unique to make management easier' AFTER `calendar_id`;

 "
);


$installer->endSetup();



