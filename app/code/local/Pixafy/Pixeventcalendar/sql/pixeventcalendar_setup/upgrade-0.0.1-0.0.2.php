<?php

try {
    
    $installer = $this;
    $installer->startSetup();

    // edit event table
    $table = $installer->getTable('pixeventcalendar/event');
    $installer->getConnection()
        ->addColumn($table, 'url', array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 255,
            'comment' => 'URL'
        ));

    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}

