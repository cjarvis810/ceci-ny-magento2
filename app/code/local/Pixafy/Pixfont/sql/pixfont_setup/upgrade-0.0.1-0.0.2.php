<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 8/12/2015
 * Time: 1:25 PM
 */

$installer = $this;
$installer->startSetup();


// id, option_id(style), font_id
$installer->run("
  CREATE TABLE `pixfont_style` (
    `style_id`  INT NOT NULL AUTO_INCREMENT ,
    `option_id`  INT(11) NOT NULL ,
    `font_id`  INT(11) NOT NULL ,
    PRIMARY KEY (`style_id`)
    );

");


$installer->endSetup();