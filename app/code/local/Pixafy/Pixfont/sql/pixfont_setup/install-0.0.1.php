<?php

try {
    
    $installer = $this;
    $installer->startSetup();

    // create question table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixfont/font'))
        ->addColumn('font_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Font Id')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Name')
        ->addColumn('path', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            ), 'Path')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Creation Time')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Update Time')
        ->setComment('Fonts');
    $installer->getConnection()->createTable($table);

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}