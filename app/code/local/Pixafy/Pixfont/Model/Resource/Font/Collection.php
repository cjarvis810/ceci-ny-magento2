<?php

class Pixafy_Pixfont_Model_Resource_Font_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    
    protected function _construct() {
        $this->_init('pixfont/font');
    }
    
}

