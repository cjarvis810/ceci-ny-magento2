<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 8/12/2015
 * Time: 2:30 PM
 */

class Pixafy_Pixfont_Model_Resource_Style extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct()
    {
        $this->_init("pixfont/style", "style_id");
    }

}