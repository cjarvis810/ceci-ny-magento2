<?php

class Pixafy_Pixfont_Model_Font extends Mage_Core_Model_Abstract {

	protected static $_fontDir;
	protected static $_fontImageDir;

	protected function _construct() {
		$this->_init('pixfont/font');
		self::$_fontDir = Mage::getBaseDir('media') . DS . 'pixafy' . DS . 'fonts';
		self::$_fontImageDir = Mage::getBaseDir('media') . DS . 'pixafy' . DS . 'fonts' . DS . 'images';
	}

	public function getDimensions(string $text, $fontfamily, float $size, $angle = 0.00) {
		if (!$fontfamily) {
			return;
		}
//		$x = array();
//		$y = array();
		//list($x[0], $y[0], $x[1], $y[1], $x[2], $y[2], $x[3], $y[3]) = imagettfbbox($size, $angle, $fontfamily, $text);
		$dimension = imagettfbbox($size, $angle, $fontfamily, $text);
//		$minX = min($x);
//		$maxX = max($x);
//		$minY = min($y);
//		$maxY = max($y);
		$width = ($dimension[2] - $dimension[0]);
		$height = ($dimension[1] - $dimension[7]);
		return array($width, $height);
	}

	public function createImage(string $text, string $fontfamily, float $size, $angle = 0.0, $bgcolor = '#ffffff') {
		header('Content-Type: image/png');
		$fontfamily = self::$_fontDir . DS . $fontfamily;
		$dimension = $this->getDimensions($text, $fontfamily, $size, $angle);
		// Create the image
		$im = imagecreatetruecolor($dimension[0], $dimension[1]);
		// Create some colors
		$white = imagecolorallocate($im, 255, 255, 255);
		$grey = imagecolorallocate($im, 128, 128, 128);
		$black = imagecolorallocate($im, 0, 0, 0);
		imagefilledrectangle($im, 0, 0, 399, 29, $white);
		
		// Add some shadow to the text
		//imagettftext($im, 20, 0, 11, 21, $grey, $fontfamily, $text);

		// Add the text
		imagettftext($im, 20, 0, 10, 20, $black, $fontfamily, $text);
		imagepng($im);
		$save = self::$_fontImageDir . DS . str_replace(" ", "", strtolower($text)) . ".png";
		// Using imagepng() results in clearer text compared with imagejpeg()
		imagepng($im, $save);
		
		return true;
	}

}
