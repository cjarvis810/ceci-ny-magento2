<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 8/12/2015
 * Time: 2:28 PM
 */

class Pixafy_Pixfont_Model_Style extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('pixfont/style');
    }
}