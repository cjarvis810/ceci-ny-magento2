<?php

class Pixafy_Pixfont_Model_Attribute_Source_Font extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {
    public function getAllOptions() {
        $options = array(array(
            'label' => ''
        ));
        $fonts = Mage::getResourceModel('pixfont/font_collection')
            ->setOrder('name', 'ASC');
        foreach ($fonts as $font) {
            $options[] = array(
                'label' => $font->getName(),
                'value' => $font->getId()
            );
        }
        return $options;
    }
}

