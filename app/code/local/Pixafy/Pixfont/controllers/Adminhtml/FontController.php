<?php

class Pixafy_Pixfont_Adminhtml_FontController extends Mage_Adminhtml_Controller_Action {

	public function indexAction() {		
		$this->loadLayout()
				->_setActiveMenu('pixafy/pixfont')
				->_addContent($this->getLayout()->createBlock('pixfont/adminhtml_font'))
				->renderLayout();
	}

	public function editAction() {
		$id = $this->getRequest()->getParam('id', null);
		$font = Mage::getModel('pixfont/font');

		if ($id) {
			$font->load((int) $id);
			if ($font->getFontId()) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if ($data) {
					$font->setData($data)->setFontId($id);
				}
			} else {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixfont')->__('Does not exist'));
				$this->_redirect('*/*/');
			}
		}
		Mage::register('font_data', $font);
		$this->loadLayout()
				->_setActiveMenu('pixafy/pixfont')
				->_addContent($this->getLayout()->createBlock('pixfont/adminhtml_font_edit'))
				->renderLayout();
		return $this;
	}

	public function saveAction() {
		if ($this->getRequest()->getPost()) {
			$data = $this->getRequest()->getPost();

			if ($data) {

                // Save font //
				if (isset($_FILES['path']['name']) and ( file_exists($_FILES['path']['tmp_name']))) {

					try {
						$uploader = new Varien_File_Uploader('path');
						$uploader->setAllowedExtensions(array('ttf', 'otf', 'woff', 'eot'));
						$uploader->setAllowRenameFiles(false);
						// setAllowRenameFiles(true) -> move your file in a folder the magento way
						// setAllowRenameFiles(true) -> move your file directly in the $path folder
						$uploader->setFilesDispersion(false);

						$path = Mage::getBaseDir('media') . DS . 'fonts';

                        $_product = Mage::getModel('catalog/product');
                        $attr = $_product->getResource()->getAttribute("style");

                        // If fonts are specified save to respective fonts folder
                        if(!empty($data['styles'])) {
                            foreach($data['styles'] as $styleId) {

                                if ($attr->usesSource()) {

                                    $styleLabel = str_replace(" ", "_", $attr->getSource()->getOptionText($styleId));
                                    if(!is_dir($path.DS.$styleLabel)) {
                                        mkdir($path.DS.$styleLabel);
                                    }

                                    copy($_FILES['path']['tmp_name'], $path.DS.$styleLabel.DS.$_FILES['path']['name']);


                                }

                            }

                            // If no style is specified save to media/fonts folder
                        } else {
                            $uploader->save($path, $_FILES['path']['name']);
                        }

                        // Clean up your mess!
                        unlink($_FILES['path']['tmp_name']);

						$data['path'] = $_FILES['path']['name'];
					} catch (Exception $e) {
						
					}

				} else {
                    // Delete font //
					if (isset($data['path']['delete']) && $data['path']['delete'] == 1)
						$data['path'] = '';
					else
						unset($data['path']);
				}

                // Save to table //
				try {

                    // Save //
					$id = $this->getRequest()->getParam('id', NULL);

                    if($id) {
                        $font = Mage::getModel('pixfont/font')->load($id);
                        $font->addData($data);
                        $font->save();

                    } else {

                        $font = Mage::getModel('pixfont/font')
                                    ->getCollection()
                                    ->addFieldToFilter('name', $data['name'])
                                    ->addFieldToFilter('path',  $_FILES['path']['name'])
                                    ->getFirstItem();

                        // If font alias doesn't exist create one
                        if(!$font->getId()) {
                            $font->setData($data);
                            $font->save();
                        }

                        // If font alias does exist check pivot table for font alias and save
                        foreach($data['styles'] as $styleId) {

                            $styleCollection = Mage::getModel('pixfont/style')
                                ->getCollection()
                                ->addFieldToFilter('option_id', $styleId)
                                ->addFieldToFilter('font_id', $font->getId())
                                ->getFirstItem();

                            if(!$styleCollection->getId()) {
                                $styleCollection->setData(array(
                                    'option_id' => $styleId,
                                    'font_id'   => $font->getId()
                                ))->save();
                            }

                        }

                    }

                    // Redirect //
					if ($this->getRequest()->getParam('id')) {
						$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					} else {
						$this->_redirect('*/*/');
					}
				} catch (Exception $e) {
					$this->_getSession()->addError(Mage::helper('pixfont')->__('An error occurred while saving the registry data. Please review the log and try again.'));
					Mage::logException($e);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return $this;
				}
			}
		}

        return true;
      //  die("Save");

    }

	public function deleteAction() {
		if ($id = $this->getRequest()->getParam('id')) {
			try {
				$model = Mage::getModel('pixfont/font');
				$model->setId($id);

                $styleCollection = Mage::getModel('pixfont/style')->getCollection()
                    ->addFieldToFilter('font_id', $id)
                    ->getAllIds();

                foreach($styleCollection as $styleId) {

                    Mage::getModel('pixfont/style')->load($styleId)->delete();

                }

                $model->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pixfont')->__('The font has been deleted.'));
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find the font to delete.'));
		$this->_redirect('*/*/');
	}

	public function newAction() {
		$this->loadLayout()
				->_addContent($this->getLayout()->createBlock('pixfont/adminhtml_font_edit'))
				->renderLayout();
	}

	public function massDeleteAction() {
		$fontIds = $this->getRequest()->getParam('font_id');
		if (!is_array($fontIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixfont')->__('Please select font(s).'));
		} else {
			try {
				$fontModel = Mage::getModel('pixfont/font');
				foreach ($fontIds as $fontId) {
					$fontModel->load($fontId)->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pixfont')->__('Total of %d record(s) were deleted.', count($fontIds)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}

		$this->_redirect('*/*/index');
	}

}
