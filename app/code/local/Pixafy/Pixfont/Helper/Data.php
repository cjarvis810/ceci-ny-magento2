<?php

class Pixafy_Pixfont_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getFontsPath() {
        return Mage::getBaseDir('media') . 'fonts';
    }
    public function getFontsUrl() {
        return Mage::getBaseUrl('media') . 'fonts';
    }
}

