<?php

class Pixafy_Pixfont_Block_Adminhtml_Font_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();
		$this->setId('fontGrid');
		$this->setDefaultSort('font_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$collection = Mage::getModel('pixfont/font')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('font_id', array(
				'header' => 'ID',
				'align' => 'right',
				'width' => '50px',
				'index' => 'font_id',
		));
		$this->addColumn('name', array(
				'header' => 'Name',
				'align' => 'left',
				'index' => 'name',
				//'renderer' => 'pixfont/adminhtml_font_renderer_texttoimage'
		));
		$this->addColumn('path', array(
				'header' => 'File name',
				'align' => 'left',
				'index' => 'path',
		));
		$this->addColumn('created_at', array(
				'header' => 'Created at',
				'align' => 'left',
				'index' => 'created_at',
		));
		$this->addColumn('updated_at', array(
				'header' => 'Updated at',
				'align' => 'left',
				'index' => 'updated_at',
		));
		return parent::_prepareColumns();
	}

	protected function _prepareMassaction() {
		$this->setMassactionIdField('font_id');
		$this->getMassactionBlock()->setFormFieldName('font_id');

		$this->getMassactionBlock()->addItem('delete', array(
				'label' => Mage::helper('pixfont')->__('Delete'),
				'url' => $this->getUrl('*/*/massDelete'),
				'confirm' => Mage::helper('pixfont')->__('Are you sure?')
		));
		return $this;
	}

	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit', array('id' => $row->getFontId()));
	}

}
