<?php

class Pixafy_Pixfont_Block_Adminhtml_Font_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
		if (Mage::registry('font_data')) {
			$data = Mage::registry('font_data')->getData();
		} else {
			$data = array();
		}
		$form = new Varien_Data_Form(array(
				'id' => 'edit_form',
				'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
				'method' => 'post',
				'enctype' => 'multipart/form-data'
				)
		);

		$form->setUseContainer(true);
		$this->setForm($form);

		if (Mage::getSingleton('adminhtml/session')->getFormData()) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData();
			Mage::getSingleton('adminhtml/session')->setFormData(null);
		} elseif (Mage::registry('font_data')) {
			$data = Mage::registry('font_data')->getData();
		}
		if ($this->getRequest()->getParam('id')) {
			$data['id'] = $this->getRequest()->getParam('id');
		}
		$fieldset = $form->addFieldset('font_form', array(
				'legend' => Mage::helper('pixfont')->__('Font information')));

		$fieldset->addField('name', 'text', array(
				'label' => Mage::helper('pixfont')->__('Name / Alias'),
				'class' => 'required-entry',
				'required' => true,
				'name' => 'name',
		));

        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'style');

        $styles = array();
        foreach ($attribute->getSource()->getAllOptions(true, true) as $instance) {
            if(!empty($instance['value'])) {
                array_push($styles, array('value'=>$instance['value'],'label'=>$instance['label']));
            }
        }


        $fieldset->addField('styles', 'multiselect', array(
            'label'     => Mage::helper('pixfont')->__('Styles'),
            'name'      => 'styles[]',
            'value'     => array(80, 77),
            'values'    => $styles
        ));

		$fieldset->addField('path', 'file', array(
				'label' => Mage::helper('pixfont')->__('Font name'),
				'required' => false,
				'name' => 'path',
		));

		$form->setValues($data);
		return parent::_prepareForm();
	}

}
