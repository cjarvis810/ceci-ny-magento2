<?php

class Pixafy_Pixfont_Block_Adminhtml_Font_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	public function __construct() {
		$this->_objectId = 'font_id';
		$this->_blockGroup = 'pixfont';
		$this->_controller = 'adminhtml_font';
		$this->setTitle(Mage::helper('pixfont')->__('Font Information'));
		$this->_mode = 'edit';
		if ($this->getRequest()->getParam('id')) {
			$this->_addButton('delete', array(
					'label' => Mage::helper('adminhtml')->__('Delete Event'),
					'class' => 'delete',
					'onclick' => 'deleteConfirm(\'' . Mage::helper('adminhtml')->__('Are you sure you want to do this?')
					. '\', \'' . Mage::helper("adminhtml")->getUrl('*/*/delete/', array('id' => $this->getRequest()->getParam('id'))) . '\')',
			));
		}
		parent::__construct();
	}

	public function getHeaderText() {
		if (Mage::registry('font_data') && Mage::registry('font_data')->getId())
			return Mage::helper('pixfont')->__("Edit Font '%s'", $this->htmlEscape(Mage::registry('font_data')->getName()));
		return Mage::helper('pixfont')->__('Add Font');
	}

}
