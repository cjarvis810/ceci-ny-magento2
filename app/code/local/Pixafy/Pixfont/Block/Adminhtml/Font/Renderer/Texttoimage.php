<?php

class Pixafy_Pixfont_Block_Adminhtml_Font_Renderer_Texttoimage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

	public function render(Varien_Object $row) {
		$html = '<img ';
		$html .= 'id="' . $row['font_id'] . '" ';
		$html .= 'src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'pixafy/fonts/images/' . strtolower($row['name']) . '.png" ';
		$html .= 'class="grid-image ' . $this->getColumn()->getInlineCss() . '"/>';
		return $html;
	}

}
