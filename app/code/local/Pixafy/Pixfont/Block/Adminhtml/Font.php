<?php

class Pixafy_Pixfont_Block_Adminhtml_Font extends Mage_Adminhtml_Block_Widget_Grid_Container {

	public function __construct() {
		$this->_controller = 'adminhtml_font';
		$this->_blockGroup = 'pixfont';
		$this->_headerText = Mage::helper('pixfont')->__('Font');
		$this->_addButtonLabel = Mage::helper('pixfont')->__('+Add Font');
		parent::__construct();
	}

}
