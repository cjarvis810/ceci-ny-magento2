<?php
class Pixafy_Pixnewsletter_Block_Customer_Account_Lists extends Ebizmarts_MageMonkey_Block_Customer_Account_Lists
{
    public function renderGroup($group, $list, $checked = -1)
    {
        $fieldType = $group['form_field'];

        $email = $this->_getEmail();
        if ($email) {
            $memberInfo = $this->_memberInfo($list['id']);
        } else {
            $memberInfo['success'] = 0;
        }

        $myGroups = array();
        $listid=Mage::helper('monkey')->getDefaultList(Mage::app()->getStore()->getId());
        $groups=explode(',',Mage::getModel('monkey/api')->listMemberInfo($listid, Mage::getSingleton('customer/session')->getCustomer()->getEmail())['data'][0]['merges']['GROUPINGS'][0]['groups']);
        foreach ($group['groups'] as $g) {
            if(in_array($g['name'],$groups)) {
                $myGroups[$group['id']][] = $g['name'];
            }
        }

        switch ($fieldType) {
            case 'radio':
                $class = 'Varien_Data_Form_Element_Radios';
                break;
            case 'checkboxes':
                $class = 'Varien_Data_Form_Element_Checkboxes';
                break;
            case 'dropdown':
                $class = 'Varien_Data_Form_Element_Select';
                break;
            case 'hidden':
                $class = 'Varien_Data_Form_Element_Hidden';
                break;
            default:
                $class = 'Varien_Data_Form_Element_Text';
                break;
        }
        $html = $this->_generateHtml($myGroups, $group, $checked, $list, $class, $fieldType);
        return $html;

    }
}
?>