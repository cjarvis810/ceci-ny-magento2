<?php

// Include the Twitter OAuth lib
require_once 'Pixafy/Pixtwitter/etc/twitteroauth/twitteroauth.php';

class Pixafy_Pixtwitter_TwitterController extends Mage_Core_Controller_Front_Action {

    protected $consumerKey;
    protected $consumerSecret;
    protected $accessToken;
    protected $accessTokenSecret;
    protected $count;
    protected $screenName;
    protected $userId;

    public function _construct()
    {
        $this->apiKey = Mage::getStoreConfig('pixtwitter/general/key', Mage::app()->getStore());
        $this->apiSecret = Mage::getStoreConfig('pixtwitter/general/secret', Mage::app()->getStore());
        $this->accessToken = Mage::getStoreConfig('pixtwitter/general/access_token', Mage::app()->getStore());
        $this->accessTokenSecret = Mage::getStoreConfig('pixtwitter/general/access_token_secret', Mage::app()->getStore());
        $this->count = Mage::getStoreConfig('pixtwitter/general/count', Mage::app()->getStore());
        $this->screenName = Mage::getStoreConfig('pixtwitter/general/screen_name', Mage::app()->getStore());
        $this->userId = Mage::getStoreConfig('pixtwitter/general/user_id', Mage::app()->getStore());
        $this->status = Mage::getStoreConfig('pixtwitter/general/status', Mage::app()->getStore());
    }

    public function getTwitterJsonAction () {
        //if not enabled, return
        if (!$this->status) {
            echo 'this block was not enabled';
            return;
        }

        // The TwitterOAuth instance
        $connection = new TwitterOAuth(
            $this->apiKey,   		             // Consumer key
            $this->apiSecret,   	             // Consumer secret
            $this->accessToken,   		         // Access token
            $this->accessTokenSecret	         // Access token secret
        );

        $feedParams = array(
            'count' => $this->count,
            'exclude_replies' => true
        );
        if ($this->screenName) {
            $feedParams['screen_name'] = $this->screenName;
        } else {
            $feedParams['user_id'] = $this->userId;
        }

        $fetchedTweets = $connection->get(
            'statuses/user_timeline',
            $feedParams
        );
        // search query by hashtag
//        $feedParams = array(
//            'q' => '#haiku',
//            'result_type' => 'popular'
//        );
//
//        $fetchedTweets = $connection->get(
//            'search/tweets',
//            $feedParams
//        );

        // Did the fetch fail?
        if($connection->http_code != 200) {
            echo '{\'Message\' : \'There was an error connecting to the feed \''.$connection->http_code.'}';
            return;
        }
        header('Content-Type: application/json');
        echo json_encode($fetchedTweets);
    }
}