<?php

try {

    $installer = $this;
    $installer->startSetup();

    // create pixcatalog_category_block table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixcatalog/colorway'))
        ->addColumn('colorway_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary' => true,
            'identity' => true
            ), 'Colorway Id')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            ), 'Name')
        ->addColumn('colors', Varien_Db_Ddl_Table::TYPE_TEXT, '32k', array(
            ), 'Colors')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Creation Time')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            ), 'Update Time')
        ->setComment('Colorways');
    $installer->getConnection()->createTable($table);

    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixcatalog/category_colorway_grid'))
        ->addColumn('grid_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary' => true,
            'identity' => true
        ), 'Grid Id')
        ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Category Id')
        ->addColumn('colorway_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Colorway Id')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Position')
        ->addIndex($installer->getIdxName('pixcatalog/category_colorway_grid', array('category_id', 'position'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('category_id', 'position'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->addForeignKey($installer->getFkName('pixcatalog/category_colorway_grid', 'category_id', 'catalog/category', 'entity_id'),
            'category_id', $installer->getTable('catalog/category'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName('pixcatalog/category_colorway_grid', 'colorway_id', 'pixcatalog/colorway', 'colorway_id'),
            'colorway_id', $installer->getTable('pixcatalog/colorway'), 'colorway_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Category Colorway Grid');
    $installer->getConnection()->createTable($table);

    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixcatalog/product_colorway_grid'))
        ->addColumn('grid_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary' => true,
            'identity' => true
        ), 'Grid Id')
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Product Id')
        ->addColumn('colorway_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Colorway Id')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Position')
        ->addIndex($installer->getIdxName('pixcatalog/product_colorway_grid', array('product_id', 'position'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('product_id', 'position'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->addForeignKey($installer->getFkName('pixcatalog/product_colorway_grid', 'product_id', 'catalog/product', 'entity_id'),
            'product_id', $installer->getTable('catalog/product'), 'entity_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName('pixcatalog/product_colorway_grid', 'colorway_id', 'pixcatalog/colorway', 'colorway_id'),
            'colorway_id', $installer->getTable('pixcatalog/colorway'), 'colorway_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Product Colorway Grid');
    $installer->getConnection()->createTable($table);

    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}