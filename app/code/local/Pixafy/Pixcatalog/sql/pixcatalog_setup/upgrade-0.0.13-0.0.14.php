<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/9/2015
 * Time: 4:23 PM
 */


$installer = $this;
$installer->startSetup();

// id, product_id, customer_id, filename, image_type(motif | photo), (image hash) created timestamp
$installer->run("
    ALTER TABLE `pixcatalog_motif_photo_type` ADD `status` TINYINT NOT NULL;
");


$installer->endSetup();