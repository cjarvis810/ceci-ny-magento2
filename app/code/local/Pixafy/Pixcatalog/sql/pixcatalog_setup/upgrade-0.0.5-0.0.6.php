<?php

$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();


$installer->addAttribute('catalog_product', 'use_product_colorways', array(
    'label' => 'Use Product Specific Colorways',
    'input' => 'boolean',
    'type' => 'int',
    'source' => '',
    'required' => false,
    'position' => 110
));

$installer->endSetup();

$installer = $this;
$installer->startSetup();
$installer->run(
        'ALTER TABLE `pixcatalog_product_colorway_grid`
        ADD UNIQUE INDEX `p_id, c_id` (`product_id`, `colorway_id`) ;'
);

$installer->endSetup();
