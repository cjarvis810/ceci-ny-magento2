<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/8/2015
 * Time: 9:49 AM
 */

$installer = $this;
$installer->startSetup();


// id, product_id, customer_id, filename, image_type(motif | photo), (image hash) created timestamp
$installer->run("
CREATE TABLE `enhancement_prices` (
    `enhance_id`  int NOT NULL AUTO_INCREMENT ,
    `option_id`  INT(11) NOT NULL ,
    `price`  DECIMAL(12, 2) NOT NULL ,
    PRIMARY KEY (`enhance_id`)
    );

    CREATE TABLE `pixcatalog_product_motif_photo` (
    `mp_id`  INT(11) NOT NULL AUTO_INCREMENT ,
    `product_id`  INT(11) NOT NULL ,
    `filename`  VARCHAR (44) NOT NULL ,
    `file_hash` CHAR(40) NOT NULL,
    `created_at`  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`mp_id`)
    );

    CREATE TABLE `pixcatalog_motif_photo_type` (
    `mpit_id` INT NOT NULL AUTO_INCREMENT ,
    `mp_id` INT(11) NOT NULL ,
    `image_type` CHAR(5) NOT NULL,
    PRIMARY KEY (`mpit_id`)
    );
");


$installer->endSetup();