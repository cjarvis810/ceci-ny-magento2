<?php

$installer = $this;
$installer->startSetup();

//$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
//$installer->startSetup();

$entities = array(
    'catalog_category' => array(
        'is_coming_soon'   => array(
            'label' => 'Coming Soon?',
            'type' => 'int',
            'input' => 'select',
            'source' => 'eav/entity_attribute_source_boolean',
            'required' => false,
            'user_defined' => 1,
            'group' => 'General Information',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
        ),
    )
);

foreach ($entities as $entity => $attributes) {
    foreach ($attributes as $key => $value) {
        $installer->removeAttribute($entity, $key);
        if (!empty($value)) {
            $installer->addAttribute($entity, $key, $value);
        }
    }
}

$installer->endSetup();