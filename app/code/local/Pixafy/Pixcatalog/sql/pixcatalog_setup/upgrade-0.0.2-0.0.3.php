<?php

try {

    $installer = $this;
    $installer->startSetup();


    // create pixcatalog_category_block table
    $table = $installer->getConnection()
    ->newTable($installer->getTable('pixcatalog/category_block'))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'unsigned'  => true,
    'nullable'  => false,
    ), 'Category Id')
    ->addColumn('block_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'unsigned'  => true,
    'nullable'  => false,
    ), 'Block Id')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
    ), 'Position');
    $installer->getConnection()->createTable($table);

    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}