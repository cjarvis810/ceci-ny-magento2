<?php

try {

    $installer = $this;
    $installer->startSetup();

    $installer->run('ALTER TABLE ' . $installer->getTable('pixcatalog/category_colorway_grid') . ' DROP INDEX ' . $installer->getIdxName('pixcatalog/category_colorway_grid', array('category_id', 'position'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE));
    $installer->run('ALTER TABLE ' . $installer->getTable('pixcatalog/product_colorway_grid') . ' DROP INDEX ' . $installer->getIdxName('pixcatalog/product_colorway_grid', array('product_id', 'position'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE));

    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}