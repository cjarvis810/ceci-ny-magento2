<?php

$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();

$installer->addAttribute('catalog_category', 'css_class', array(
    'type'     => 'varchar',
    'label'    => 'Css Class',
    'input'    => 'text',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => 0,
    'group'             => 'Display Settings',
    'position'          => 100
));

$installer->endSetup();