<?php
$installer = $this;
$installer->startSetup();
$installer->run(
    'ALTER TABLE `pixcatalog_colorway` CHANGE `name` `colorway_name` VARCHAR(255) NULL DEFAULT NULL;'
);

$installer->endSetup();
