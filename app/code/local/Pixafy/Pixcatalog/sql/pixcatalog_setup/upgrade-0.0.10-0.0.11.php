<?php

$installer = $this;
$installer->startSetup();

//$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
//$installer->startSetup();

$entities = array(
    'catalog_product' => array(
        'original_fine_art'   => array(
            'label' => 'Original Fine Art',
            'type' => 'varchar',
            'input' => 'multiselect',
            'backend' => 'eav/entity_attribute_backend_array',
            'required' => false,
            'user_defined' => 1,
            'group' => 'Customization Options',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'option' => array(
                'value' => array(
                    'Watercolor' => array('Watercolor', 'Watercolor'),
                    'Calligraphy' => array('Calligraphy','Calligraphy'),
                    'Illustration' => array('Illustration', 'Illustration'),
                    'Hand Lettering' => array('Hand Lettering', 'Hand Lettering'),
                    'Patterns / Prints' => array('Patterns / Prints', 'Patterns / Prints'),
                )
            )
        ),
    )
);

foreach ($entities as $entity => $attributes) {
    foreach ($attributes as $key => $value) {
        $installer->removeAttribute($entity, $key);
        if (!empty($value)) {
            $installer->addAttribute($entity, $key, $value);
        }
    }
}

$installer->endSetup();