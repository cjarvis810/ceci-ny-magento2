<?php
$installer = $this;
$installer->startSetup();
$installer->run(
        'ALTER TABLE `pixwording_product_wording`
ADD UNIQUE INDEX `uniqru` (`product_id`, `wording_id`) ;
'
);

$installer->endSetup();
