<?php

try {

    $installer = $this;
    $installer->startSetup();

    // edit question table
    $table = $installer->getTable('pixcatalog/product_type_group');
    $installer->getConnection()
        ->addColumn($table, 'status', 'TINYINT(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT \'Status\'');

    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}