<?php
/*
$installer = $this;
$installer->startSetup();
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$entityTypeId = $setup->getEntityTypeId('catalog_category');
$setId = $setup->getDefaultAttributeSetId($entityTypeId);
$groupId = $setup->getAttributeGroupId($entityTypeId, $setId, 'General Information');
$setup->addAttribute('catalog_category', 'category_background', array(
    'label' => 'Occasion Color',
    'type' => 'varchar',
    'input' => 'text',
    'required' => false,
    'user_defined' => 1,
    'used_in_product_listing' => 0,
    'default_value' => 2,
    'visible_on_front' => 1,
    'group' => 'general',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
));
// update attributes group and sort
$attributes = array(
    'category_background'         => array(
        'sort'  => 80
    )
);
$installer->endSetup();
try {
    foreach ($attributes as $attributeCode => $attributeProp) {
        $setup->addAttributeToGroup(
            $entityTypeId,
            $setId,
            $groupId,
            $attributeCode,
            $attributeProp['sort']
        );
    }
    $installer->endSetup();
} catch (Exception $e) {
    Mage::logException($e);
    echo $e->getMessage();
}
*/