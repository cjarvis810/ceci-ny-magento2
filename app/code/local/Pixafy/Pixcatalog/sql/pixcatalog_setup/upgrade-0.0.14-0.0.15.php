<?php

try {

    $installer = $this;
    $installer->startSetup();
    // create product type group - product type table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixcatalog/product_type_group_product_type'))
        ->addColumn('group_value_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
            'identity' => true
        ), 'Grouping Prod Type Group Id to Prod Type Id')
        ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Group Id')
        ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'attribute value Id')
        ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Label')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'default' => '0'
        ), 'Position')
        ->addIndex($installer->getIdxName('pixcatalog/product_type_group_product_type', array('group_id', 'position'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('group_id', 'position'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
        ->addForeignKey($installer->getFkName('pixcatalog/product_type_group_product_type', 'group_id', 'pixcatalog/product_type_group', 'group_id'),
            'group_id', $installer->getTable('pixcatalog/product_type_group'), 'group_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Product Type Group to Product Type');
    $installer->getConnection()->createTable($table);

    $installer->endSetup();
} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}