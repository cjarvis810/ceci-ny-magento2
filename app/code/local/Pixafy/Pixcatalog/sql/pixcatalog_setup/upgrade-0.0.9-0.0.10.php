<?php

try {

    $installer = $this;
    $installer->startSetup();

    // create pixcatalog_category_block table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixcatalog/product_type_group'))
        ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary' => true,
            'identity' => true
        ), 'Group Id')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        ), 'Name')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Creation Time')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Update Time')
        ->setComment('Product Type Group');
    $installer->getConnection()->createTable($table);

    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixcatalog/product_type_group_grid'))
        ->addColumn('grid_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary' => true,
            'identity' => true
        ), 'Grid Id')
        ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Group Id')
        ->addColumn('attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Attribute Id')
        ->addColumn('value', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Value')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Position')
        ->addForeignKey($installer->getFkName('pixcatalog/product_type_group_grid', 'group_id', 'pixcatalog/product_type_group', 'group_id'),
            'group_id', $installer->getTable('pixcatalog/product_type_group'), 'group_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName('pixcatalog/product_type_group_grid', 'attribute_id', 'eav/attribute', 'attribute_id'),
            'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Product Type Group Grid');
    $installer->getConnection()->createTable($table);

    $installer->endSetup();

    $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
    $installer->startSetup();

    $entities = array(
        'catalog_product' => array(
            'length' => array(
                'label' => 'Length',
                'type' => 'decimal',
                'input' => 'text',
                'required' => false,
                'user_defined' => 1,
                'group' => 'General',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
            ),
            'width' => array(
                'label' => 'Width',
                'type' => 'decimal',
                'input' => 'text',
                'required' => false,
                'user_defined' => 1,
                'group' => 'General',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
            ),
            'printing_techniques' => array(
                'label' => 'Printing Techniques',
                'type' => 'varchar',
                'input' => 'multiselect',
                'backend' => 'eav/entity_attribute_backend_array',
                'required' => false,
                'user_defined' => 1,
                'group' => 'Customization Options',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'option' => array(
                    'value' => array(
                        'Lasercut',
                        'Engraving',
                        'Flat Printing',
                        'Letterpress',
                        'Foil Stamping',
                    )
                )
            ),
            'paper_stocks' => array(
                'label' => 'Paper Stocks',
                'type' => 'varchar',
                'input' => 'multiselect',
                'backend' => 'eav/entity_attribute_backend_array',
                'required' => false,
                'user_defined' => 1,
                'group' => 'Customization Options',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'option' => array(
                    'value' => array(
                        'Light',
                        'Medium',
                        'Heavy'
                    )
                )
            ),
            'foil_parts' => array(
                'label' => 'Foil Parts',
                'type' => 'varchar',
                'input' => 'multiselect',
                'backend' => 'eav/entity_attribute_backend_array',
                'required' => false,
                'user_defined' => 1,
                'group' => 'Customization Options',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'option' => array(
                    'value' => array(
                        'Text',
                        'Border',
                    )
                )
            ),
            'enhancements' => array(
                'label' => 'Enchancements',
                'type' => 'varchar',
                'input' => 'multiselect',
                'backend' => 'eav/entity_attribute_backend_array',
                'required' => false,
                'user_defined' => 1,
                'group' => 'Customization Options',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'option' => array(
                    'value' => array(
                        'Motif',
                        'Hebrew Salutation'
                    )
                )
            ),
        )
    );

    foreach ($entities as $entityName => $attributes) {
        foreach ($attributes as $attributeCode => $attributeData) {
            $installer->removeAttribute($entityName, $attributeCode);
            if (!is_null($attributeData)) {
                $installer->addAttribute($entityName, $attributeCode, $attributeData);
            }
        }
    }

    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}