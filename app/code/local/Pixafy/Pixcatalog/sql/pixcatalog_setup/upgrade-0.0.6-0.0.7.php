<?php

try {

    $installer = $this;
    $installer->startSetup();

    // create pixcatalog_category_block table
    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixcatalog/colorwheel_slice'))
        ->addColumn('slice_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary' => true,
            'identity' => true
        ), 'Colorway Id')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        ), 'Name')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Creation Time')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Update Time')
        ->setComment('Colorways');
    $installer->getConnection()->createTable($table);

    $table = $installer->getConnection()
        ->newTable($installer->getTable('pixcatalog/colorwheel_slice_colorway_grid'))
        ->addColumn('grid_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary' => true,
            'identity' => true
        ), 'Grid Id')
        ->addColumn('slice_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Slice Id')
        ->addColumn('colorway_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Colorway Id')
        ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned'  => true,
            'nullable'  => false,
        ), 'Position')
        ->addForeignKey($installer->getFkName('pixcatalog/colorwheel_slice_colorway_grid', 'slice_id', 'pixcatalog/colorwheel_slice', 'slice_id'),
            'slice_id', $installer->getTable('pixcatalog/colorwheel_slice'), 'slice_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey($installer->getFkName('pixcatalog/colorwheel_slice_colorway_grid', 'colorway_id', 'pixcatalog/colorway', 'colorway_id'),
            'colorway_id', $installer->getTable('pixcatalog/colorway'), 'colorway_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Colorwheel Slice Colorway Grid');
    $installer->getConnection()->createTable($table);

    $installer->endSetup();

} catch (Exception $e) {
    die($e->getMessage() . $e->getTraceAsString());
}