<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 1/12/2015
 * Time: 6:04 PM
 */

$installer = $this;
$installer->startSetup();

$entities = array(
    'catalog_product' => array(
        'product_type'   => array(
            'type'                       => 'int',
            'label'                      => 'Product Type',
            'input'                      => 'select',
            'source'                     => 'eav/entity_attribute_source_table',
            'position'                   => 100,
            'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'group'                      => 'General',
            'required'                   => false,
            'visible'                    => true,
            'user_defined'               => true,
            'used_in_product_listing'    => true,
            'option'                     => array(
                'values'                 => array(
                    'Invitations',
                    'Programs',
                    'Reply Cards',
                    'Escort Cards',
                    'Reception Cards',
                    'Place Cards',
                    'Save the Date Cards',
                    'Menus',
                    'Rehearsal Dinner Cards',
                    'Stamps',
                    'Brunch Cards',
                    'Rehearsal Dinner Cards',
                    'Cocktail Napkins'
                )
            )
        ),
    )
);

foreach ($entities as $entity => $attributes) {
    foreach ($attributes as $key => $value) {
        $installer->removeAttribute($entity, $key);
        if (!empty($value)) {
            $installer->addAttribute($entity, $key, $value);
        }
    }
}

$installer->endSetup();