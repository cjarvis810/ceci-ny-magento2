<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Product controller
 *
 * @category   Mage
 * @package    Mage_Catalog
 */
require_once "app" . DS . "code" . DS. "core" . DS . "Mage" . DS . "Catalog" . DS . "controllers" . DS . "ProductController.php";

class Pixafy_Pixcatalog_ProductController extends Mage_Catalog_ProductController
{
    public function quickviewAction()
    {
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
        Mage::register('product', $product);
       // echo $this->loadLayout()->getLayout()->getBlock('product.info')->toHtml();
        $this->_ajaxReturn(
            array(
                "qv" => $this->loadLayout()->getLayout()->getBlock('product.info')->toHtml(),
                "fb" => array("title" => $product->getName(),
                              "productUrl" => $product->getProductUrl(),
                              "imageUrl" => urlencode(trim(Mage::helper('catalog/image')->init($product, 'image')->resize(200, 200))),
                              "app_id" => Mage::getStoreConfig('pixfacebook/connection/id', Mage::app()->getStore())
                )
        ));
    }

    public function saveNextAction()
    {

        print "<pre>";
        $data = $this->getRequest()->getParams();
        $productOptions['customizable_options'] = array_filter($data['product']);

        if($productOptions['customizable_options']) {
            //print_r($data);
            print_r($productOptions);

            $quoteItemOptionModel = Mage::getModel('sales/quote_item_option')
                ->getCollection()
                ->addFieldToFilter('product_id', $data['product_id'])
                ->addFieldToFilter('item_id', $data['quote_item_id'])
                ->getFirstItem();

            //print_r($quoteItemOptionModel);
            $insertData = array(
                'product_id' => $data['product_id'],
                'item_id'    => $data['quote_item_id'],
                'code'       => 'customizable_options',
                'value'      => serialize($productOptions['customizable_options'])
            );

            //print_r($insertData);
            // Check if customizable option exist for this product/quote item
            if($optionsId = $quoteItemOptionModel->getId()) {

                // Match found update customizable option
                Mage::getModel('sales/quote_item_option')
                    ->load($optionsId)
                    ->addData($insertData)
                    ->save()->getId();

            } else {

                // No match insert new record
                Mage::getModel('sales/quote_item_option')
                    ->setData($insertData)
                    ->save()->getId();

            }

            // Update quote item price (sum of all enhancement options if any exist)
            if($productOptions['customizable_options']['enhancements']) {

                $totalEnhancement = 0;

                foreach($productOptions['customizable_options']['enhancements'] as $enhancementPrice) {

                    $totalEnhancement += $enhancementPrice;

                }

                // Needed to load quote for quote item model
                $quoteItem = Mage::getModel('sales/quote_item')
                    ->load($data['quote_item_id']);

                $quote = Mage::getModel('sales/quote')->load($quoteItem->getData('quote_id'));

                $quoteItemModel = Mage::getModel('sales/quote_item')
                    ->setQuote($quote)
                    ->load($data['quote_item_id']);


                $totalEnhancement += $quoteItemModel->getData('price');

                //print_r($quoteItemModel->getData());

                $quoteItemModel->setOriginalCustomPrice($totalEnhancement);
                $quoteItemModel->setCustomPrice($totalEnhancement);

                $quoteItemModel->getProduct()->setIsSuperMode(true);
                $quoteItemModel->save();


                //print $totalEnhancement;
                //print "\n";

            }



        }


        die("Successful!");
    }


    public function enhancementAction()
    {
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product_id'));
        Mage::register('product', $product);
        Mage::register('quoteItem', $product->createQuoteFromPersonalization());

        $this->loadLayout();
        $this->renderLayout();
    }

    /*
    public function saveAction()
    {

        $languageOptions    = $this->getRequest()->getPost('languageOptions');
        $printingCheck      = $this->getRequest()->getPost('printingCheck');
        $printQuantity      = $this->getRequest()->getPost('printQuantity');

        print_r($languageOptions);
        print "<br />";


        $block = $this->getLayout()
            ->createBlock('catalog/product_view')
            ->setTemplate('product/template/temp-ppp-enhancement.phtml');

        $this->getResponse()->setBody($block->toHtml());

        //  $this->_redirect('');
    }*/
    private function _ajaxReturn($data, $encode = true)
    {
        if($encode)
        {
            $data = Mage::helper('core')->jsonEncode($data);
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($data);
    }

//    public function ajaxviewAction() {
//        $this->loadLayout();
//        $quickViewBlock = $this->getLayout()->createBlock('')->setTemplate('pixcatalog/product/quickview.phtml');
//        $quickViewBlock->setChild('media', $this->getLayout()->createBlock('catalog/product_media')->setTemplate('pixcatalog/product/quickview/media.phtml'));
//        $data['html'] = $alsoViewedBlock->toHtml();
//        $this->_ajaxReturn($data);
//    }

}
