<?php
class Pixafy_Pixcatalog_AjaxfilterController extends Mage_Core_Controller_Front_Action
{
    public function viewAction()
    {
        if ($this->getRequest()->isAjax()) {
            $layer=null;
            if($this->getRequest()->getParam('q')){
                $layer = Mage::getSingleton('catalogsearch/layer');
            }else{
                $layer = Mage::getSingleton('catalog/layer');
            }
            if($layer==null){
                return;
            }
            if($this->getRequest()->getParam('id')) {
                $category = Mage::getModel('catalog/category')->load($this->getRequest()->getParam('id'));
                if ($category->getId()) {
                    $layer->setCurrentCategory($category);
                } else {
                    return;
                }
            }
            $json=array();
            $json['filterhtml'] = $this->getLayout()->createBlock('catalog/layer_view')->setLayer($layer)->setTemplate('catalog/layer/view.phtml')->toHtml();
            if($this->getRequest()->getParam('q')){
                $block = $this->getLayout()->createBlock('pixcatalog/product_list');
                $block->setAjaxCall(true);
                $block->setLayer($layer)->setTemplate('catalog/product/list.phtml');

                if(!array_key_exists("slice", $this->getRequest()->getParams())) {

                    $collection = Mage::getSingleton('catalogsearch/advanced')->addFilters(array('name' => $this->getRequest()->getParam('q')))->getProductCollection();
                    $collection->addAttributeToSelect('*');
                    foreach($this->getRequest()->getParams() as $name=>$value) {
                        if ($name!='q' && $name!='slice'){
                            $collection->addFieldToFilter($name,array('eq' => $value));
                        }
                    }

                } else {

                    $collection = null;
                    $collection = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('entity_id', array(
                            'in' => Mage::helper('pixcatalog')->getProductIdsMatchingColorway($this->getRequest()->getParam('slice'))
                        ));

                }

                $block->setCollection($collection);
                $block->setChild('swatches', $this->getLayout()->createBlock('core/template')->setLayer($layer)->setTemplate('pixafy/pixcatalog/product/swatches.phtml'));
                $json['producthtml'] = $block->toHtml();
            }else {
                $block = $this->getLayout()->createBlock('pixcatalog/product_list');
                $block->setAjaxCall(true);
                $block->setLayer($layer)->setTemplate('catalog/product/list.phtml');
                $block->setChild('swatches', $this->getLayout()->createBlock('core/template')->setLayer($layer)->setTemplate('pixafy/pixcatalog/product/swatches.phtml'));
                $json['producthtml'] = $block->toHtml();
            }
            $this->getResponse()->clearHeaders()->setHeader('content-type', 'application/json', true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
        }
    }
}