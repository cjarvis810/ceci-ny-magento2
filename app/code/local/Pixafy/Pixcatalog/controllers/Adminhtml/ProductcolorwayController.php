<?php

class Pixafy_Pixcatalog_Adminhtml_ProductcolorwayController extends Mage_Adminhtml_Controller_Action {

    public function indexAction(){
        $id = $this->getRequest()->getParam('id');
        $product = Mage::getModel('pixcatalog/catalog_product')->load($id);
        
        $block = $this->getLayout()->createBlock('pixcatalog/adminhtml_catalog_product_colorways_grid');
        $block->product = $product;
        $this->loadLayout();
        $this->getResponse()->setBody($block->toHtml());
    }
}