<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 4:04 PM
 */

class Pixafy_Pixcatalog_Adminhtml_ColorwayController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout()
            ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_colorway'))
            ->_setActiveMenu('pixafy/pixcatalog_colorway')
            ->renderLayout();
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id', null);
        $colorway = Mage::getModel('pixcatalog/colorway');

        if ($id) {
            $colorway->load((int) $id);
            if ($colorway->getColorwayId()) {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if ($data) {
                    $colorway->setData($data)->setColorwayId($id);
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('Does not exist'));
                $this->_redirect('*/*/');
            }
        }
        Mage::register('colorway_data', $colorway);
        $this->loadLayout()
            ->_setActiveMenu('pixafy/pixcatalog_colorway')
            ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_colorway_edit'))
            ->renderLayout();
        return $this;
    }

    public function saveAction() {
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();
            if ($data) {
                try {
                    $id = $this->getRequest()->getParam('id', NULL);
                    $colorway = Mage::getModel('pixcatalog/colorway')->load($id);
                    $colorway->addData($data);
                    $colorway->save();
                    $this->_getSession()->addSuccess(Mage::helper('pixcatalog')->__('The colorway ' . $colorway->getName() . ' has been saved.'));
                    $this->_redirect('*/*/');
                } catch (Exception $e) {
                    $this->_getSession()->addError(Mage::helper('pixcatalog')->__('An error occurred while saving the registry data. Please review the log and try again.'));
                    Mage::logException($e);
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return $this;
                }
            }
        }
    }

    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('pixcatalog/colorway');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pixcatalog')->__('The colorway has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('Unable to find the colorway to delete.'));
        $this->_redirect('*/*/');
    }

    public function newAction() {
        $this->loadLayout()
            ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_colorway_edit'))
            ->renderLayout();
    }

    public function massDeleteAction() {
        $colorwayIds = $this->getRequest()->getParam('colorway_id');
        if (!is_array($colorwayIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('Please select colorway(s).'));
        } else {
            try {
                $colorway = Mage::getModel('pixcatalog/colorway');
                foreach ($colorwayIds as $colorwayId) {
                    $colorway->load($colorwayId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pixcatalog')->__('Total of %d record(s) were deleted.', count($colorwayIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }
}