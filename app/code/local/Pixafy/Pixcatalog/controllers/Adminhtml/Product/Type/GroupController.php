<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 6/23/2015
 * Time: 5:15 PM
 */

class Pixafy_Pixcatalog_Adminhtml_Product_Type_GroupController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout()
            ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_product_type_group'))
            ->_setActiveMenu('pixafy/pixcatalog_product_type_group')
            ->renderLayout();
    }

    public function editAction()
    {

        $id = $this->getRequest()->getParam('id',null);
        if($id)
        {
            $group = Mage::getModel('pixcatalog/product_type_group')->load((int)$id);
            if($group->getGroupId())
            {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if($data)
                {
                    $group->setData($data)->setId($id);
                }
            }
            else
            {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('Does not exist.'));
                $this->_redirect('*/*/');
            }
        }
        Mage::register('product_type_group_data', $group);

        $this->loadLayout()
            ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_product_type_group_groupform'))
            ->_addLeft($this->getLayout()->createBlock('pixcatalog/adminhtml_product_type_group_groupform_tabs'));
        $this->renderLayout();
        return $this;
    }

    public function saveAction()
    {

        if ($this->getRequest()->getPost())
        {
            try {
                $data = $this->getRequest()->getPost();

                $id = $this->getRequest()->getParam('id');

                if($data && $id)
                {
                    $pixgroup = Mage::getModel('pixcatalog/product_type_group')->load($id);
                    $pixgroup->addData($data);
                    if(!empty($data["is_in_group"]))
                    {
                        $questionIds = $this->getQuestionsIds($data["is_in_group"]);
                        $pixgroup->setValueIds($questionIds);
                    }
                    $pixgroup->save();
                    $this->_redirect('*/*/index');
                }
                elseif($data)
                {
                    $pixgroup = Mage::getModel('pixcatalog/product_type_group')->load();
                    $pixgroup->addData($data);
                    if(!empty($data["is_in_group"]))
                    {
                        $questionIds = $this->getQuestionsIds($data["is_in_group"]);
                        $pixgroup->setValueIds($questionIds);
                    }
                    $pixgroup->save();
                    $this->_redirect('*/*/index');
                }
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('pixcatalog')->__('An error occurred while saving the data. Please try again.'));
                Mage::log($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return $this;
            }
        }
        else
        {
            $this->_getSession()->addError(Mage::helper('pixcatalog')->__('An error occurred while saving the data. Please try again.'));
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            return $this;
        }
    }

    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('pixcatalog/product_type_group');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pixcatalog')->__('The product type group has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('Unable to find the product type group to delete.'));
        $this->_redirect('*/*/');
    }

    public function newAction() {
        $this->loadLayout()
            ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_product_type_group_groupform'))
            ->_addLeft($this->getLayout()->createBlock('pixcatalog/adminhtml_product_type_group_groupform_tabs'))
            ->renderLayout();
    }

    public function massDeleteAction() {
        $groupIds = $this->getRequest()->getParam('group_id');
        if (!is_array($groupIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('Please select group(s).'));
        } else {
            try {
                $group = Mage::getModel('pixcatalog/product_type_group');
                foreach ($groupIds as $groupId) {
                    $group->load($groupId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pixcatalog')->__('Total of %d record(s) were deleted.', count($groupIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

    private function getQuestionsIds($is_in_group)
    {
        $group_detail = (array)json_decode($is_in_group);
        $group_detail = array_flip($group_detail);
        return($group_detail);
    }
}