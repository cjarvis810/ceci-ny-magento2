<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/7/2015
 * Time: 3:38 PM
 */

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') .DS. 'Catalog/ProductController.php';

class Pixafy_Pixcatalog_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{
    private $_salt = "Ie*FR8t23214ru@%#&ga223t";

    public function saveAction()
    {
        $data = $this->getRequest()->getParams();
        //print "<pre>";
        //print_r($data);

        if($data['tab'] == "product_info_tabs_motifs") {

            try {

                // Activate motif image
                if(isset($data['product']['motif_image']['activate'])) {

                    // @TODO Deactivate all other motif_image

                    $motifCollection = Mage::getModel('pixcatalog/product_motif_type')
                        ->getCollection()
                        ->addFieldToFilter('image_type', 'motif');


                    $activateId = $data['product']['motif_image']['activate'][0];
                    foreach($motifCollection as $motifImage) {

                        if($activateId == $motifImage->getData('mp_id')) {
                            Mage::getModel('pixcatalog/product_motif_type')->load($motifImage->getId())->addData(array(
                                'status' => 1
                            ))->save();

                        } else {

                            Mage::getModel('pixcatalog/product_motif_type')->load($motifImage->getId())->addData(array(
                                'status' => 0
                            ))->save();

                        }

                    }

                }

                // Activate photo image
                if(isset($data['product']['photo_image']['activate'])) {

                    $photoCollection = Mage::getModel('pixcatalog/product_motif_type')
                        ->getCollection()
                        ->addFieldToFilter('image_type', 'photo');


                    $activateId = $data['product']['photo_image']['activate'][0];
                    foreach($photoCollection as $photoImage) {

                        if($activateId == $photoImage->getData('mp_id')) {
                            Mage::getModel('pixcatalog/product_motif_type')->load($photoImage->getId())->addData(array(
                                'status' => 1
                            ))->save();

                        } else {

                            Mage::getModel('pixcatalog/product_motif_type')->load($photoImage->getId())->addData(array(
                                'status' => 0
                            ))->save();

                        }

                    }

                }



                // Remove Motif image
                if(isset($data['product']['motif_image']['remove'])) {
                    //print_r($data);

                    foreach($data['product']['motif_image']['remove'] as $motifImageId => $state) {

                        $collection = Mage::getModel('pixcatalog/product_motif_type')
                            ->getCollection()
                            ->addFieldToFilter('image_type', 'motif')
                            ->addFieldToFilter('mp_id', $motifImageId)
                            ->getFirstItem()->getId();

                        if($collection) {

                            // Remove from pivot table
                            Mage::getModel('pixcatalog/product_motif_type')->setId($collection)->delete();

                        }

                    }

                }

                // Remove Photo Image
                if(isset($data['product']['photo_image']['remove'])) {


                    foreach($data['product']['photo_image']['remove'] as $photoImageId => $state) {

                        $collection = Mage::getModel('pixcatalog/product_motif_type')
                            ->getCollection()
                            ->addFieldToFilter('image_type', 'photo')
                            ->addFieldToFilter('mp_id', $photoImageId)
                            ->getFirstItem()->getId();

                        if($collection) {

                            // Remove from pivot table
                            Mage::getModel('pixcatalog/product_motif_type')->setId($collection)->delete();

                        }


                    }

                }

                // Cleanup motif_photo table
                if(isset($data['product']['photo_image']['remove']) || isset($data['product']['motif_image']['remove'])) {
                    $collection = Mage::getModel('pixcatalog/product_motif_type')
                        ->getCollection()
                        ->getColumnValues('mp_id');


                    $motifPhotoCollectionIds = Mage::getModel('pixcatalog/product_motif_photo')
                    ->getCollection()
                    ->addFieldToFilter('mp_id', array(
                        'nin' => $collection
                    ))->getAllIds();

                    foreach($motifPhotoCollectionIds as $motifPhotoId) {
                        Mage::getModel('pixcatalog/product_motif_photo')->setId($motifPhotoId)->delete();
                        // @TODO Unlink files as well!
                    }


                }


                if($_FILES['product']['size']['motif_photo_image_file']) {

                        $motifPhotoSelection = $data['product']['motif_photo_selection'];

                        $uploader = new Varien_File_Uploader( array(
                            'name' => $_FILES['product']['name']['motif_photo_image_file'],
                            'type' => $_FILES['product']['type']['motif_photo_image_file'],
                            'tmp_name' => $_FILES['product']['tmp_name']['motif_photo_image_file'],
                            'error' => $_FILES['product']['error']['motif_photo_image_file'],
                            'size' => $_FILES['product']['size']['motif_photo_image_file']
                        ));

                        // Any extension would work
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);

                        $imageExtension = explode(".", $_FILES['product']['name']['motif_photo_image_file']);

                        // Sha` all good
                        $imageName = sha1(
                                sha1($_FILES['product']['name']['motif_photo_image_file']).
                                sha1(time()).
                                sha1($this->_salt)
                            ).".".$imageExtension[1];


                        $motifPhoto  = Mage::getBaseDir('media').DS.'product'.DS.'motif_photo'.DS;
                        $thumbnailDir = Mage::getBaseDir('media').DS.'product'.DS.'motif_photo'.DS.'thumbnail'.DS;

                        // Create directories //
                        if(!is_dir($motifPhoto)) {
                            mkdir($motifPhoto);
                        }

                        if(!is_dir($thumbnailDir)) {
                            mkdir($thumbnailDir);
                        }

                        $productId = $data['id'];
                        //$fileHash = sha1_file($_FILES['product']['tmp_name']['motif_photo_image_file']);

                        $motifPhotoModel = Mage::getModel('pixcatalog/product_motif_photo');


                        $insertData = array(
                            'product_id'    => $productId,
                            'filename'      => $imageName,
                           // 'file_hash'     => $fileHash
                        );


                        $motifPhotoModel->setData($insertData);

                        // Save record return id and save to lookup table
                        if($motifPhotoModelId = $motifPhotoModel->save()->getId()) {

                            // Loop through selection save to lookup table
                            foreach($motifPhotoSelection as $selection) {

                                $selection = strtolower($selection);

                                $insertData = array(
                                    'mp_id'     => $motifPhotoModelId,
                                    'image_type'=> $selection
                                );

                                Mage::getModel('pixcatalog/product_motif_type')
                                    ->setData($insertData)
                                    ->save();

                            }

                        }

                        if($uploader->save($motifPhoto, $imageName)) {
                            Mage::helper('pixcatalog')->resizeImageThumbnail($motifPhoto.$imageName, $thumbnailDir);
                        }

                }

            } catch (Exception $e) {

                Mage::log(print_r($_FILES, true));
                Mage::log($e->getMessage());

            }

            //print "<br><br>";
            //die("saveAction");
        }

        parent::saveAction();

    }


}