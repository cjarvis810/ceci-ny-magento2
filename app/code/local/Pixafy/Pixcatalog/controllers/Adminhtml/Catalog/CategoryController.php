<?php
/**
 * Catalog product controller
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once 'app' . DS . 'code' . DS . 'core' . DS . 'Mage' . DS . 'Adminhtml' . DS . 'controllers' . DS . 'Catalog' . DS . 'CategoryController.php';
class Pixafy_Pixcatalog_Adminhtml_Catalog_CategoryController extends Mage_Adminhtml_Catalog_CategoryController
{

    /**
     * Grid Action
     * Display list of products related to current category
     * if the param "tab" is set the cms blocks related to current category are returned
     *
     * @return void
     */
    public function gridAction()
    {

        if (!$category = $this->_initCategory(true)) {
            return;
        }
        $tab = $this->getRequest()->getParam('tab');
        if ($tab == 'promoblock'){
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('pixcatalog/adminhtml_category_tab_promoblock', 'category.promoblock.grid')
                    ->toHtml()
            );
        } else if ($tab == 'colorway') {
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('pixcatalog/adminhtml_category_tab_colorway', 'category.colorway.grid')
                    ->toHtml()
            );
        } else {
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('adminhtml/catalog_category_tab_product', 'category.product.grid')
                    ->toHtml()
            );
        }
    }
}