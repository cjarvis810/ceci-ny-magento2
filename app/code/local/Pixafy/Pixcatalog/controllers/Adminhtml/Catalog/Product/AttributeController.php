<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/6/2015
 * Time: 4:29 PM
 */

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') .DS. 'Catalog/Product/AttributeController.php';

class Pixafy_Pixcatalog_Adminhtml_Catalog_Product_AttributeController
    extends Mage_Adminhtml_Catalog_Product_AttributeController
{
    public function saveAction()
    {
        $data = $this->getRequest()->getParam('option');
        $priceOptions = (isset($data['price'])) ? $data['price'] : null;

        if($priceOptions) {

            // Check any of these option id's exist
            $enhancedPrices = Mage::getModel('pixcatalog/enhancement_price')
                ->getCollection()
                ->addFieldToFilter('option_id', array(
                    'in' => array_keys($data['price'])
                ));


            try {

                // If any exist
                if($enhancedPrices) {

                    // Update price
                    foreach($enhancedPrices as $enhancedPrice) {

                        $updateData = array(
                          'price' => $priceOptions[$enhancedPrice->getOptionId()]
                        );

                        $model = Mage::getModel('pixcatalog/enhancement_price')->load($enhancedPrice->getId())->addData($updateData);

                        $model->setId($enhancedPrice->getId())->save();

                        // Pop off array
                        unset($priceOptions[$enhancedPrice->getOptionId()]);

                    }

                }


                // If any are left INSERT into table
                if(!empty($priceOptions)) {

                    foreach($priceOptions as $optionId => $price) {

                        // If price is 0 do nothing otherwise save
                        if($price) {
                            $saveData = array(
                                'price' => $price,
                                'option_id' => $optionId
                            );

                            $model = Mage::getModel('pixcatalog/enhancement_price')->setData($saveData);
                            $model->save();

                        }
                    }

                }

            } catch(Exception $e) {

                Mage::log($e->getMessage());

            }
        }


        parent::saveAction();
    }
}
