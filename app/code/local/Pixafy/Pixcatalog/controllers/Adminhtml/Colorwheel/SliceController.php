<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 4:04 PM
 */

class Pixafy_Pixcatalog_Adminhtml_Colorwheel_SliceController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout()
            ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_colorwheel_slice'));
        $this->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $slice = Mage::getModel('pixcatalog/colorwheel_slice')->load($id);
        if ($slice->getId()) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if ($data) {
                $slice->setData($data);
            }
            Mage::register('colorwheel_slice', $slice);
            $this->loadLayout()
                ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_colorwheel_slice_edit'))
                ->_addLeft($this->getLayout()->createBlock('pixcatalog/adminhtml_colorwheel_slice_edit_tabs'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('The slice does not exist.'));
            $this->_redirect('*/*/');
        }

        return $this;
    }

    public function newAction()
    {
        Mage::register('colorwheel_slice', Mage::getModel('pixcatalog/colorwheel_slice'));
        $this->loadLayout()
            ->_addContent($this->getLayout()->createBlock('pixcatalog/adminhtml_colorwheel_slice_edit'))
            ->_addLeft($this->getLayout()->createBlock('pixcatalog/adminhtml_colorwheel_slice_edit_tabs'));
        $this->renderLayout();
        return $this;
    }

    private function getColorwayIds($is_in_slice)
    {
        $slice_details = (array)json_decode($is_in_slice);
        arsort($slice_details);

        $colorwayPosition = array();

        $count = 0;
        foreach($slice_details as $colorwayId => $position) {

            $colorwayPosition[$count] = $colorwayId;

            $count++;
        }

        return($colorwayPosition);
    }

    public function saveAction()
    {
        $data = $this->getRequest()->getPost();

        if($data)
        {
            try
            {
                $id = $this->getRequest()->getParam('id',null);
                $slice = Mage::getModel('pixcatalog/colorwheel_slice')->load($id);

                $colorwayIds = $this->getColorwayIds($data["is_in_slice"]);

                $slice->setColorwayIds($colorwayIds);

                $slice->addData($data);
                $slice->save();
                $this->_redirect('*/*/');
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('There was a problem saving the slice.'));
                $this->_redirect('*/edit/');
            }


        }
        else
        {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('There was a problem saving the slice.'));
            $this->_redirect('*/edit/');
        }
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id',null);
        if($id)
        {
            try
            {
                $question = Mage::getModel('pixcatalog/colorwheel_slice')->load($id);
                $question->delete();
                $this->_redirect('*/*/');
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pixcatalog')->__('There was a problem saving the slice.'));
                $this->_redirect('*/*/');
            }
        }
    }
}