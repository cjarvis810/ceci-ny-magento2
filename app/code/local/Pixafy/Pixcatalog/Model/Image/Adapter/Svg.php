<?php
/**
 * @category    Pixafy
 * @package     Pixafy_Pixcatalog_Model_Image_Adapter_Svg
 * @copyright   Copyright (c) 2015 Pixafy Inc. (http://www.pixafy.com)
 */

/**
 * @file        Svg.php
 * @author      John Cymerman <jcymerman@pixafy.com>
 */

class Pixafy_Pixcatalog_Model_Image_Adapter_Svg extends Varien_Image_Adapter_Abstract
{

    public function open($fileName) {
        $this->_fileName = $fileName;
        $this->_getFileAttributes();
        Mage::log($fileName);
        $this->_imageHandler = simplexml_load_file($this->_fileName);
    }

    public function save($destination=null, $newName=null) {
        if (!is_object($this->_imageHandler)) {
            throw new Exception('No image specified.');
        }
        $fileName = ( !isset($destination) ) ? $this->_fileName : $destination;

        if( isset($destination) && isset($newName) ) {
            $fileName = $destination . "/" . $newName;
        } elseif( isset($destination) && !isset($newName) ) {
            $info = pathinfo($destination);
            $fileName = $destination;
            $destination = $info['dirname'];
        } elseif( !isset($destination) && isset($newName) ) {
            $fileName = $this->_fileSrcPath . "/" . $newName;
        } else {
            $fileName = $this->_fileSrcPath . $this->_fileSrcName;
        }

        $destinationDir = ( isset($destination) ) ? $destination : $this->_fileSrcPath;

        if( !is_writable($destinationDir) ) {
            try {
                $io = new Varien_Io_File();
                $io->mkdir($destination);
            } catch (Exception $e) {
                throw new Exception("Unable to write file into directory '{$destinationDir}'. Access forbidden.");
            }
        }

        $this->_imageHandler->asXML($fileName);
    }

    public function display() {
        header('Content-Type: image/svg+xml');
        echo $this->_imageHandler->asXML();
    }

    public function resize($width=null, $height=null) {
        if (!$width) {
            $width = $height;
        } else if (!$height) {
            $height = $width;
        }
        $this->_imageHandler['width'] = $width;
        $this->_imageHandler['height'] = $height;
    }

    public function rotate($angle) {
        return false;
    }

    public function crop($top=0, $left=0, $right=0, $bottom=0) {
        $width = $right - $left;
        $height = $bottom - $top;
        $this->_imageHandler->addAttribute('viewBox', "$top $left $right $bottom");
    }

    public function watermark($watermarkImage, $positionX=0, $positionY=0, $watermarkImageOpacity=30, $repeat=false) {
        $child = $this->_imageHandler->addChild('image');
        $child->addAttribute('x', $positionX);
        $child->addAttribute('y', $positionY);
        $child->addAttribute('href', $watermarkImage, Pixafy_Pixcatalog_Helper_Svg::NS_XLINK);
        $child->addAttribute('style', 'opacity:' . ($watermarkImageOpacity/100) . ';');
    }

    public function checkDependencies() {

    }

}