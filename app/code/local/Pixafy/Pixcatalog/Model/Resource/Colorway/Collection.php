<?php

/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:46 PM
 */
class Pixafy_Pixcatalog_Model_Resource_Colorway_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected $leftJoinedProducts;
    protected $innerJoinedProduct;
    protected $innerJoinedCategory;

    protected function _construct() {
        $this->_init('pixcatalog/colorway');
    }

    /**
     * @param type $productId
     * @return \Pixafy_Pixcatalog_Model_Resource_Colorway_Collection
     */
    public function innerJoinProduct($productId) {
        if ($this->innerJoinedProduct) {
            return $this;
        }
        $this->getSelect()->join(array('ccg' => 'pixcatalog_product_colorway_grid'), 'main_table.colorway_id = ccg.colorway_id AND ccg.product_id = ' . $productId, array('position'));
        $this->innerJoinedProduct = true;
        return $this;
    }



    /**
     * 
     * @param type $categoryId
     */
    public function innerJoinCategory($categoryId) {
        if ($this->innerJoinedCategory) {
            return $this;
        }
        $this->getSelect()->join(array('ccg' => 'pixcatalog_category_colorway_grid'), 'main_table.colorway_id = ccg.colorway_id AND ccg.category_id = ' . $categoryId, array('position'));

        $this->innerJoinedCategory = true;
        return $this;
    }

    /**
     * 
     * @return \Pixafy_Pixcatalog_Model_Resource_Colorway_Collection
     */
    public function leftJoinProductColorwayPosition($productId = null) {
        if (!$this->leftJoinedProducts) {
            $this->getSelect()
                    ->joinLeft(array("g" => "pixcatalog_product_colorway_grid"), "main_table.colorway_id = g.colorway_id AND g.product_id = " . $productId
                            , array("g.position", "g.grid_id", "g.colorway_id as grid_colorway_id"));
            $this->leftJoinedProducts = true;
        }
        return $this;
    }

    public function leftJoinSliceColorway($sliceId=null)
    {
        $this->getSelect()
                ->joinLeft(array('ppcg'=>'pixcatalog_product_colorway_grid'), "main_table.");
    }

}
