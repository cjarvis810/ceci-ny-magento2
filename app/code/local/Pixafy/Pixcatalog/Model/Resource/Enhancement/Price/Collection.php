<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/6/2015
 * Time: 4:57 PM
 */
class Pixafy_Pixcatalog_Model_Resource_Enhancement_Price_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct() {
        $this->_init('pixcatalog/enhancement_price');
    }
}
