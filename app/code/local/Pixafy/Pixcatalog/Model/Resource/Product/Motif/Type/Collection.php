<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/8/2015
 * Time: 1:34 PM
 */

class Pixafy_Pixcatalog_Model_Resource_Product_Motif_Type_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('pixcatalog/product_motif_type');
    }

}