<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/8/2015
 * Time: 10:32 AM
 */

class Pixafy_Pixcatalog_Model_Resource_Product_Motif_Photo extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('pixcatalog/product_motif_photo', 'mp_id');
    }

}