<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:44 PM
 */

class Pixafy_Pixcatalog_Model_Resource_Product_Type_Group_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected function _construct() {
        $this->_init('pixcatalog/product_type_group');
        $this->_joinedTypeIds = false;
    }

    public function joinTypeIds() {
        if (!$this->_joinedTypeIds) {
            $this->getSelect()
                ->joinLeft(
                    $this->getTable('pixcatalog/product_type_group_product_type') . ' AS qq',
                    'main_table.group_id = qq.group_id',
                    'GROUP_CONCAT(qq.value_id) value_ids'
                )->group('main_table.group_id');
            $this->_joinedTypeIds = true;
        }
        return $this;
    }

    protected function _afterLoadData() {
        if ($this->_joinedTypeIds) {
            foreach ($this as $item) {
                $item->setValueIds(explode(',', $item->getTypeIds()));
            }
        }
        return parent::_afterLoadData();
    }
}