<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:44 PM
 */

class Pixafy_Pixcatalog_Model_Resource_Product_Type_Group extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('pixcatalog/product_type_group', 'group_id');
    }

    public function getTypeIds(Pixafy_Pixcatalog_Model_Product_Type_Group $group) {
        if (!$group->getId()) {
            return array();
        }

        $table = $this->getTable('pixcatalog/product_type_group_product_type');

        $result = $this->_getReadAdapter()->select()
            ->from($table, 'value_id')
            ->where('group_id = ?', $group->getId())
            ->order('position ASC')
            ->query(PDO::FETCH_ASSOC);

        $product_typeIds = array();

        foreach ($result as $row) {
            $product_typeIds[] = $row['value_id'];
        }

        return $product_typeIds;
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
        $now = Mage::getSingleton('core/date')->timestamp(time());
        if (!$object->getId()) {
            $object->setCreatedAt($now);
        }
        $object->setUpdatedAt($now);
        return parent::_beforeSave($object);
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object) {
        $this->_saveGroupTypes($object);
        return parent::_afterSave($object);
    }

    protected function _saveGroupTypes(Mage_Core_Model_Abstract $object) {
        $product_typeIds = $object->getData('value_ids');
        if (!is_null($product_typeIds)) {
            $table = $this->getTable('pixcatalog/product_type_group_product_type');
            // delete old associations
            $this->_getWriteAdapter()->delete(
                $table,
                array('group_id = ?' => $object->getId())
            );

            // save new associations
            foreach ($product_typeIds as $position => $product_typeId) {
                $this->_getWriteAdapter()->insert(
                    $table,
                    array(
                        'group_id' => $object->getId(),
                        'value_id' => $product_typeId,
                        'position' => $position,
                    )
                );
            }
        }
    }


}