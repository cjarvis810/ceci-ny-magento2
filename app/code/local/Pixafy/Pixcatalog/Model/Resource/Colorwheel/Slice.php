<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:45 PM
 */

class Pixafy_Pixcatalog_Model_Resource_Colorwheel_Slice extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('pixcatalog/colorwheel_slice', 'slice_id');
    }

    /**
     * Catalog promoblocks table name
     *
     * @var string
     */
    protected $_colorwheelSliceColorwayGridTable;

    protected function _getColorwheelSliceColorwayGridTable() {
        if (is_null($this->_colorwheelSliceColorwayGridTable)) {
            $this->_colorwheelSliceColorwayGridTable = $this->getTable('pixcatalog/colorwheel_slice_colorway_grid');
        }
        return $this->_colorwheelSliceColorwayGridTable;
    }

    public function getColorways(Mage_Core_Model_Abstract $slice) {
        $select = $this->_getWriteAdapter()->select()
            ->from($this->_getColorwheelSliceColorwayGridTable(), array('colorway_id', 'position'))
            ->where('slice_id = :slice_id')
            ->order('position ASC');
        $bind = array('slice_id' => (int)$slice->getId());

        return $this->_getWriteAdapter()->fetchPairs($select, $bind);
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
        $now = Mage::getSingleton('core/date')->timestamp(time());
        if (!$object->getId()) {
            $object->setCreatedAt($now);
        }
        $object->setUpdatedAt($now);

        return parent::_beforeSave($object);
    }

    /**
     * Process category data after save category object
     * save related block ids
     *
     * @param Varien_Object $object
     * @return Mage_Catalog_Model_Resource_Category
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {

        $this->_saveSliceColorways($object);
        return parent::_afterSave($object);
    }

    /**
     * Save category products relation
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Model_Resource_Category
     */
    protected function _saveSliceColorways($slice)
    {
        $colorwayIds = $slice->getData('colorway_ids');


        // Delete existing slices //

    }

    public function deleteAllColorways($slice) {
        $id = $slice->getId();
        $adapter = $this->_getWriteAdapter();
        /**
         * Delete all promoblocks
         */
        $cond = array(
            'slice_id=?' => $id
        );
        $adapter->delete($this->_getColorwheelSliceColorwayGridTable(), $cond);
    }
}