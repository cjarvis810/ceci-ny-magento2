<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:46 PM
 */

class Pixafy_Pixcatalog_Model_Resource_Colorwheel_Slice_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    protected function _construct() {
        $this->_init('pixcatalog/colorwheel_slice');
    }
}