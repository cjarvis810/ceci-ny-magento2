<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:45 PM
 */

class Pixafy_Pixcatalog_Model_Resource_Colorway extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('pixcatalog/colorway', 'colorway_id');
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
        $now = Mage::getSingleton('core/date')->timestamp(time());
        if (!$object->getId()) {
            $object->setCreatedAt($now);
        }
        $object->setUpdatedAt($now);

        if (is_array($object->getColors())) {
            $object->setColors(implode(',', $object->getColors()));
        }

        return parent::_beforeSave($object);
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object) {
        if (is_string($object->getColors())) {
            $object->setColors(explode(',', $object->getColors()));
        }

        // @TODO Insert colorway_id into pixcatalog_colorwheel_slice_colorway_grid
        return parent::_afterSave($object);
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object) {
        if (is_string($object->getColors())) {
            $object->setColors(explode(',', $object->getColors()));
        }
        return parent::_afterLoad($object);
    }

    protected function _afterDelete(Mage_Core_Model_Abstract $object) {
        $this->_getWriteAdapter()->delete(
            $this->getTable('pixcatalog/category_colorway_grid'),
            'colorway_id = ' . $object->getId()
        );
        $this->_getWriteAdapter()->delete(
            $this->getTable('pixcatalog/product_colorway_grid'),
            'product_id = ' . $object->getId()
        );
        return parent::_afterDelete($object);
    }
}