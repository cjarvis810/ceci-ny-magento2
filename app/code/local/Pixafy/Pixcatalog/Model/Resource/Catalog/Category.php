<?php

class Pixafy_Pixcatalog_Model_Resource_Catalog_Category extends Mage_Catalog_Model_Resource_Category
{

    /**
     * Catalog promoblocks table name
     *
     * @var string
     */
    protected $_categoryPromoblockTable;
    protected $_categoryColorwayGridTable;

    protected function _getCategoryPromoblockTable() {
        if (is_null($this->_categoryPromoblockTable)) {
            $this->_categoryPromoblockTable = $this->getTable('pixcatalog/category_block');
        }
        return $this->_categoryPromoblockTable;
    }

    protected function _getCategoryColorwayGridTable() {
        if (is_null($this->_categoryColorwayGridTable)) {
            $this->_categoryColorwayGridTable = $this->getTable('pixcatalog/category_colorway_grid');
        }
        return $this->_categoryColorwayGridTable;
    }

    /**
     * Get positions of associated to category products
     *
     * @param Mage_Catalog_Model_Category $category
     * @return array
     */
    public function getPromoBlocks($category)
    {
        $select = $this->_getWriteAdapter()->select()
            ->from($this->_getCategoryPromoblockTable(), array('block_id', 'position'))
            ->where('category_id = :category_id')
            ->order('position ASC');
        $bind = array('category_id' => (int)$category->getId());

        return $this->_getWriteAdapter()->fetchPairs($select, $bind);
    }

    public function getColorways(Mage_Core_Model_Abstract $category) {
        $select = $this->_getWriteAdapter()->select()
            ->from($this->_getCategoryColorwayGridTable(), array('colorway_id', 'position'))
            ->where('category_id = :category_id')
            ->order('position ASC');
        $bind = array('category_id' => (int)$category->getId());

        return $this->_getWriteAdapter()->fetchPairs($select, $bind);
    }

    /**
     * Process category data after save category object
     * save related block ids
     *
     * @param Varien_Object $object
     * @return Mage_Catalog_Model_Resource_Category
     */
    protected function _afterSave(Varien_Object $object)
    {
        $this->_saveCategoryBlocks($object);
        $this->_saveCategoryColorways($object);
        return parent::_afterSave($object);
    }

    /**
     * Save category products relation
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Model_Resource_Category
     */
    protected function _saveCategoryBlocks($category)
    {
        $category->setIsChangedPromoBlockList(false);
        $id = $category->getId();
        /**
         * new category-product relationships
         */
        $blocks = $category->getPostedBlocks();
        /**
         * Example re-save category
         */
        if ($blocks === null) {
            return $this;
        }

        /**
         * old category-promobloks relationships
         */
        $oldBlocks = $category->getPromoBlocks();
        $insert = array_diff_key($blocks, $oldBlocks);
        $delete = array_diff_key($oldBlocks, $blocks);

        /**
         * Find product ids which are presented in both arrays
         * and saved before (check $oldBlocks array)
         */
        $update = array_intersect_key($blocks, $oldBlocks);
        $update = array_diff_assoc($update, $oldBlocks);

        $adapter = $this->_getWriteAdapter();
        /**
         * Delete products from category
         */
        if (!empty($delete)) {
            $cond = array(
                'block_id IN(?)' => array_keys($delete),
                'category_id=?' => $id
            );
            $adapter->delete($this->_getCategoryPromoblockTable(), $cond);
        }

        /**
         * Add promo blocks to category
         */
        if (!empty($insert)) {
            $data = array();
            foreach ($insert as $blockId => $position) {
                $data[] = array(
                    'category_id' => (int)$id,
                    'block_id'    => (int)$blockId,
                    'position'    => $position,
                );
            }

            $adapter->insertMultiple($this->_getCategoryPromoblockTable(), $data);
        }

        /**
         * Update promo blocks in category
         */
        if (!empty($update)) {
            foreach ($update as $blockId => $position) {
                $where = array(
                    'category_id = ?'=> (int)$id,
                    'block_id = ?' => (int)$blockId
                );
                $bind  = array('position' => $position);
                $adapter->update($this->_getCategoryPromoblockTable(), $bind, $where);
            }
        }
        if (!empty($insert) || !empty($delete)) {
            $blockIds = array_unique(array_merge(array_keys($insert), array_keys($delete)));
        }

        if (!empty($insert) || !empty($update) || !empty($delete)) {
            $category->setIsChangedPromoBlockList(true);

            /**
             * Setting affected blocks to category for third party engine index refresh
             */
            $blockIds = array_keys($insert + $delete + $update);
            $category->setAffectedPromoblockIds($blockIds);
        }
        return $this;
    }

    public function deleteAllPromoBlocks($category) {
        $id = $category->getId();
        $adapter = $this->_getWriteAdapter();
        /**
         * Delete all promoblocks
         */
        $cond = array(
            'category_id=?' => $id
        );
        $adapter->delete($this->_getCategoryPromoblockTable(), $cond);
    }

    /**
     * Save category products relation
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Model_Resource_Category
     */
    protected function _saveCategoryColorways($category)
    {
        $category->setIsChangedColorwayList(false);
        $id = $category->getId();
        /**
         * new category-product relationships
         */
        $colorways = $category->getPostedColorways();
        /**
         * Example re-save category
         */
        if ($colorways === null) {
            return $this;
        }

        /**
         * old category-promobloks relationships
         */
        $oldColorways = $category->getColorways();

        $insert = array_diff_key($colorways, $oldColorways);
        $delete = array_diff_key($oldColorways, $colorways);

        /**
         * Find product ids which are presented in both arrays
         * and saved before (check $oldBlocks array)
         */
        $update = array_intersect_key($colorways, $oldColorways);
        $update = array_diff_assoc($update, $oldColorways);

        $adapter = $this->_getWriteAdapter();
        /**
         * Delete products from category
         */
        if (!empty($delete)) {
            $cond = array(
                'colorway_id IN(?)' => array_keys($delete),
                'category_id=?' => $id
            );
            $adapter->delete($this->_getCategoryColorwayGridTable(), $cond);
        }

        /**
         * Add promo blocks to category
         */
        if (!empty($insert)) {
            $data = array();
            foreach ($insert as $colorwayId => $position) {
                $data[] = array(
                    'category_id' => (int)$id,
                    'colorway_id'    => (int)$colorwayId,
                    'position'    => $position,
                );
            }

            $adapter->insertMultiple($this->_getCategoryColorwayGridTable(), $data);
        }

        /**
         * Update promo blocks in category
         */
        if (!empty($update)) {
            foreach ($update as $colorwayId => $position) {
                $where = array(
                    'category_id = ?'=> (int)$id,
                    'colorway_id = ?' => (int)$colorwayId
                );
                $bind  = array('position' => $position);
                $adapter->update($this->_getCategoryColorwayGridTable(), $bind, $where);
            }
        }

        return $this;
    }

    public function deleteAllColorways($category) {
        $id = $category->getId();
        $adapter = $this->_getWriteAdapter();
        /**
         * Delete all promoblocks
         */
        $cond = array(
            'category_id=?' => $id
        );
        $adapter->delete($this->_getCategoryColorwayGridTable(), $cond);
    }
}