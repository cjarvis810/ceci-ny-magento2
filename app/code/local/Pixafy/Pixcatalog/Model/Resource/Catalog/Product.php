<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @product    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Product entity resource model
 *
 * @product    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Model_Resource_Catalog_Product extends Mage_Catalog_Model_Resource_Product
{

    protected $_productColorwayGridTable;
    protected $_personalizableAttributeSetId;

    public function getPersonalizableAttributeSetId() {
        if (is_null($this->_personalizableAttributeSetId)) {
            $attributeSet = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->addFieldToFilter('entity_type_id', Mage_Catalog_Model_Product::ENTITY)
                ->addFieldToFilter('attribute_set_name', 'personalizable')
                ->getFirstItem();
            $this->_personalizableAttributeSetId = $attributeSet->getId();
        }
        return $this->_personalizableAttributeSetId;
    }

    protected function _getProductColorwayGridTable() {
        if (is_null($this->_productColorwayGridTable)) {
            $this->_productColorwayGridTable = $this->getTable('pixcatalog/product_colorway_grid');
        }
        return $this->_productColorwayGridTable;
    }

    public function getColorways(Mage_Core_Model_Abstract $product) {
        $select = $this->_getWriteAdapter()->select()
            ->from($this->_getProductColorwayGridTable(), array('colorway_id', 'position'))
            ->where('product_id = :product_id')
            ->order('position ASC');
        $bind = array('product_id' => (int)$product->getId());

        return $this->_getWriteAdapter()->fetchPairs($select, $bind);
    }

    /**
     * Process product data after save product object
     * save related block ids
     *
     * @param Varien_Object $object
     * @return Mage_Catalog_Model_Resource_Product
     */
    protected function _afterSave(Varien_Object $object)
    {
        $this->_saveProductColorways($object);
        return parent::_afterSave($object);
    }

    /**
     * Save product products relation
     *
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Resource_Product
     */
    protected function _saveProductColorways($product)
    {
        $product->setIsChangedColorwayList(false);
        $id = $product->getId();
        /**
         * new product-product relationships
         */
        if ($product->getUseCategoryColorways()) {
            $colorways = array();
        } else {
            $colorways = $product->getPostedColorways();
        }

        /**
         * Example re-save product
         */
        if ($colorways === null) {
            return $this;
        }

        /**
         * old product-promobloks relationships
         */
        $oldColorways = $product->getColorways();
        $insert = array_diff_key($colorways, $oldColorways);
        $delete = array_diff_key($oldColorways, $colorways);

        /**
         * Find product ids which are presented in both arrays
         * and saved before (check $oldBlocks array)
         */
        $update = array_intersect_key($colorways, $oldColorways);
        $update = array_diff_assoc($update, $oldColorways);

        $adapter = $this->_getWriteAdapter();
        /**
         * Delete products from product
         */
        if (!empty($delete)) {
            $cond = array(
                'colorway_id IN(?)' => array_keys($delete),
                'product_id=?' => $id
            );
            $adapter->delete($this->_getProductColorwayGridTable(), $cond);
        }

        /**
         * Add promo blocks to product
         */
        if (!empty($insert)) {
            $data = array();
            foreach ($insert as $colorwayId => $position) {
                $data[] = array(
                    'product_id' => (int)$id,
                    'colorway_id'    => (int)$colorwayId,
                    'position'    => $position,
                );
            }

            $adapter->insertMultiple($this->_getProductColorwayGridTable(), $data);
        }

        /**
         * Update promo blocks in product
         */
        if (!empty($update)) {
            foreach ($update as $blockId => $position) {
                $where = array(
                    'product_id = ?'=> (int)$id,
                    'colorway_id = ?' => (int)$colorwayId
                );
                $bind  = array('position' => $position);
                $adapter->update($this->_getProductColorwayGridTable(), $bind, $where);
            }
        }

        return $this;
    }

    public function deleteAllColorways($product) {
        $id = $product->getId();
        $adapter = $this->_getWriteAdapter();
        /**
         * Delete all promoblocks
         */
        $cond = array(
            'product_id=?' => $id
        );
        $adapter->delete($this->_getProductColorwayGridTable(), $cond);
    }
}
