<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/8/2015
 * Time: 1:31 PM
 */

class Pixafy_Pixcatalog_Model_Product_Motif_Type extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('pixcatalog/product_motif_type');
    }

}