<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:44 PM
 */

class Pixafy_Pixcatalog_Model_Product_Type_Group extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('pixcatalog/product_type_group');
    }

//    public function getQuestionCollection() {
////        return Mage::getResourceModel('pixquiz/question_collection')
////            ->addFieldToFilter('group_id', array('in' => $this->getQuestionIds()));
//        $name='product_type';
//        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
//        $attributeId = $attributeInfo->getAttributeId();
//        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
//        $attributeOptions = $attribute ->getSource()->getAllOptions(false);
//        $collection = new Varien_Data_Collection();
//        foreach($attributeOptions as $attributeOption){
//            $rowObj = new Varien_Object();
//            $rowObj->setData($attributeOption);
//            $collection->addItem($rowObj);
//        }
//        return $collection;
//    }

    public function getTypeIds() {
        if (is_null($this->getData('value_ids'))) {
            $this->setValueIds($this->_getResource()->getTypeIds($this));
        }
        return $this->getData('value_ids');
    }

}