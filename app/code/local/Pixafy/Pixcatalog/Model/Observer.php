<?php

class Pixafy_Pixcatalog_Model_Observer {

    /**
     * @param $observer
     *
     * add promo block data to the category before saving
     */
    function catalogCategoryPrepareSave ($observer){
        $category = $observer->getEvent()->getCategory();
        $request = $observer->getEvent()->getRequest();
        $data = $request->getPost();
        if (isset($data['category_blocks'])) {
            $blocks = Mage::helper('core/string')->parseQueryStr($data['category_blocks']);
            $blocks = Mage::helper('pixcatalog')->sanitizePromoBlockInput($blocks);
            $category->setPostedBlocks($blocks);
        }
        if (isset($data['category_colorways'])) {
            $colorways = Mage::helper('core/string')->parseQueryStr($data['category_colorways']);
            $colorways = Mage::helper('pixcatalog')->sanitizeColorwayInput($colorways);
            $category->setPostedColorways($colorways);
        }

    }

    /**
     * used to delete promo blocks when a category is deleted
     * @param $observer
     *
     */
    function catalogControllerCategoryDelete($observer) {
        $category = $observer->getEvent()->getCategory();
        if ($category) {
            $category->getResource()->deleteAllPromoBlocks($category);
            $category->getResource()->deleteAllColorways($category);
        }
    }
    
    
    function saveProduct($observer) {               
        $productId = $observer->getEvent()->getProduct()->getId();     
        $request = $observer->getEvent()->getRequest();
        $data = $request->getPost();
        $db = Mage::getSingleton("core/resource")->getConnection("core_write");
        $this->saveProductColorways($productId, $data, $db);
        $this->saveProductWordings($productId, $data, $db);
    }
    
    function saveProductColorways($productId, $data, $db){
        unset($data['colorway_position'][0]);   
        $db->beginTransaction();                
        try{ 
            //delete the ones removed            
            $sql = 'DELETE pixcatalog_product_colorway_grid
                   FROM pixcatalog_product_colorway_grid
                   WHERE product_id = ?';        
            $params = array( $productId );

            if( !empty( $data['colorway_checked'] )){
                $sql .= ' AND colorway_id NOT IN ( ' . str_pad("?", (count($data['colorway_checked']) * 2) - 1, ",?" ) . ')';
                $params = array_keys($data['colorway_checked']);
                array_unshift($params,$productId);
            }
            $db->query($sql, $params);
            
            //it doesn't really make sense to make this one query            
            if( !empty($data['colorway_checked']) ){
              foreach( $data['colorway_checked'] as $c => $p ){
                if( isset( $data['colorway_position'][$c]) ){  
                    if( empty($data['colorway_position'][$c])){
                        if(!isset($pos)){
                            $pos = $db->fetchOne("SELECT max(position) FROM pixcatalog_product_colorway_grid WHERE product_id = ?", 
                            array($productId));
                            $pos = $pos ? $pos + 1 : 1;
                        } else {
                            $pos++;
                        }
                        $data['colorway_position'][$c] = $pos;
                    }
                    
                    $db->query(
                        'INSERT INTO pixcatalog_product_colorway_grid ( `product_id`, `colorway_id`, `position` ) 
                         VALUES ( ? , ? , ? ) ON DUPLICATE KEY UPDATE position = VALUES( `position` );',
                         array( $productId, $c, $data['colorway_position'][$c])    
                    );
                }                
              }  
            }
            $db->commit();
        } catch( \Exception $e ){
            $db->rollback();
        }      
    }
    
    function saveProductWordings($productId, $data, $db){
     unset($data['wordings_checked'][0]);   
     $db->beginTransaction();                
        try{ 
            //delete the ones removed            
            $sql = 'DELETE pixwording_product_wording
                   FROM pixwording_product_wording
                   WHERE product_id = ?';        
            $params = array( $productId );

            if( !empty( $data['wordings_checked'] )){
                $sql .= ' AND wording_id NOT IN (' . str_pad("?", (count($data['wordings_checked']) * 2) - 1, ",?" ) . ')';
                $params = array_keys($data['wordings_checked']);
                array_unshift($params,$productId);
            }
            $db->query($sql, $params);
            
              //it doesn't really make sense to make this one query            
            if( !empty($data['wordings_checked']) ){
              foreach( $data['wordings_checked'] as $c => $p ){
                if( isset( $data['wordings_checked'][$c]) ){  
                    if( empty($data['wordings_position'][$c])){
                        if(!isset($pos)){
                            $pos = $db->fetchOne("SELECT max(position) FROM pixwording_product_wording WHERE product_id = ?", 
                            array($productId));
                            $pos = $pos ? $pos + 1 : 1;
                        } else {
                            $pos++;
                        }
                        $data['wordings_position'][$c] = $pos;
                    }
                    
                    $db->query(
                        'INSERT INTO pixwording_product_wording ( `product_id`, `wording_id`, `position` ) 
                         VALUES ( ? , ? , ? ) ON DUPLICATE KEY UPDATE position = VALUES( `position` );',
                         array( $productId, $c, $data['wordings_position'][$c])    
                    );
                }                
              }  
            }
       
            $db->commit();
        } catch( \Exception $e ){
            $db->rollback();
        }      
    }
    

    function deleteProduct($observer) {
        $productId = $observer->getEvent()->getProduct()->getId();                     
        $db = Mage::getSingleton("core/resource")->getConnection("core_write");
        $db->beginTransaction();                
        try{ 
            //delete the ones removed            
            $params = array( $productId );
            $sql = 'DELETE pixcatalog_product_colorway_grid
                   FROM pixcatalog_product_colorway_grid
                   WHERE product_id = ?';                    
            $db->query($sql, $params);
            $sql = 'DELETE pixwording_product_wording
                   FROM pixwording_product_wording
                   WHERE product_id = ?';        
            $db->query($sql, $params);
            $db->commit();
        } catch( \Exception $e ){
            $db->rollback();
        }      
    }

}

