<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Catalog product media gallery attribute backend model
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Model_Catalog_Product_Attribute_Backend_Media extends Mage_Catalog_Model_Product_Attribute_Backend_Media
{

    /**
     * Add image to media gallery and return new filename
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string                     $file              file path of image in file system
     * @param string|array               $mediaAttribute    code of attribute with type 'media_image',
     *                                                      leave blank if image should be only in gallery
     * @param boolean                    $move              if true, it will move source file
     * @param boolean                    $exclude           mark image as disabled in product page view
     * @return string
     */
    public function addImage(Mage_Catalog_Model_Product $product, $file,
        $mediaAttribute = null, $move = false, $exclude = true)
    {
        $file = realpath($file);

        if (!$file || !file_exists($file)) {
            Mage::throwException(Mage::helper('catalog')->__('Image does not exist.'));
        }

        Mage::dispatchEvent('catalog_product_media_add_image', array('product' => $product, 'image' => $file));

        $pathinfo = pathinfo($file);

        // add svg to list of acceptable extensions - jcymerman 3/27/15
        $imgExtensions = array('jpg', 'jpeg', 'gif', 'png', 'svg');
        if (!isset($pathinfo['extension']) || !in_array(strtolower($pathinfo['extension']), $imgExtensions)) {
            Mage::throwException(Mage::helper('catalog')->__('Invalid image file type.'));
        }

        // add special flag in_place, which means don't copy to normal product folders, use current folder instead - jcymerman 3/27/15
        if ($move === 'in_place') {
            $fileName = substr($file, strlen(Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product'));
        } else {
            $fileName       = Mage_Core_Model_File_Uploader::getCorrectFileName($pathinfo['basename']);
            $dispretionPath = Mage_Core_Model_File_Uploader::getDispretionPath($fileName);
            $fileName       = $dispretionPath . DS . $fileName;

            $fileName = $this->_getNotDuplicatedFilename($fileName, $dispretionPath);

            $ioAdapter = new Varien_Io_File();
            $ioAdapter->setAllowCreateFolders(true);
            $distanationDirectory = dirname($this->_getConfig()->getTmpMediaPath($fileName));

            try {
                $ioAdapter->open(array(
                    'path'=>$distanationDirectory
                ));

                /** @var $storageHelper Mage_Core_Helper_File_Storage_Database */
                $storageHelper = Mage::helper('core/file_storage_database');
                if ($move) {
                    $ioAdapter->mv($file, $this->_getConfig()->getTmpMediaPath($fileName));

                    //If this is used, filesystem should be configured properly
                    $storageHelper->saveFile($this->_getConfig()->getTmpMediaShortUrl($fileName));
                } else {
                    $ioAdapter->cp($file, $this->_getConfig()->getTmpMediaPath($fileName));

                    $storageHelper->saveFile($this->_getConfig()->getTmpMediaShortUrl($fileName));
                    $ioAdapter->chmod($this->_getConfig()->getTmpMediaPath($fileName), 0777);
                }
            }
            catch (Exception $e) {
                Mage::throwException(Mage::helper('catalog')->__('Failed to move file: %s', $e->getMessage()));
            }
        }

        $fileName = str_replace(DS, '/', $fileName);

        $attrCode = $this->getAttribute()->getAttributeCode();
        $mediaGalleryData = $product->getData($attrCode);
        $position = 0;
        if (!is_array($mediaGalleryData)) {
            $mediaGalleryData = array(
                'images' => array()
            );
        }

        foreach ($mediaGalleryData['images'] as &$image) {
            if (isset($image['position']) && $image['position'] > $position) {
                $position = $image['position'];
            }
        }

        $position++;
        $mediaGalleryData['images'][] = array(
            'file'     => $fileName,
            'position' => $position,
            'label'    => '',
            'disabled' => (int) $exclude
        );

        $product->setData($attrCode, $mediaGalleryData);

        if (!is_null($mediaAttribute)) {
            $this->setMediaAttribute($product, $mediaAttribute, $fileName);
        }

        return $fileName;
    }

    public function beforeSave($object)
    {
        $attrCode = $this->getAttribute()->getAttributeCode();
        $value = $object->getData($attrCode);
        if (!is_array($value) || !isset($value['images'])) {
            return;
        }

        if(!is_array($value['images']) && strlen($value['images']) > 0) {
            $value['images'] = Mage::helper('core')->jsonDecode($value['images']);
        }

        if (!is_array($value['images'])) {
            $value['images'] = array();
        }



        $clearImages = array();
        $newImages   = array();
        $existImages = array();
        if ($object->getIsDuplicate()!=true) {
            foreach ($value['images'] as &$image) {
                if(!empty($image['removed'])) {
                    // Remove layers and svgs from file system if removed from product - jcymerman 3/27/15
                    if (preg_match('/(?:^layer\d+\.png|\.svg)$/', basename($image['file']))) {
                        $fileName = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . str_replace('/', DS, $image['file']);
                        unlink($fileName);
                    }

                    $clearImages[] = $image['file'];
                } else if (!isset($image['value_id'])) {

                    // Don't use temp naming if layer or svg - jcymerman 3/27/15
                    if (preg_match('/(?:^layer\d+\.png|\.svg)$/', basename($image['file']))) {
                        $image['new_file'] = $image['file'];
                    } else {
                        $newFile = $this->_moveImageFromTmp($image['file']);
                        $image['new_file'] = $newFile;
                        $this->_renamedImages[$image['file']] = $newFile;
                        $image['file'] = $newFile;
                    }
                    $newImages[$image['file']] = $image;
                } else {
                    $existImages[$image['file']] = $image;
                }
            }
        } else {
            // For duplicating we need copy original images.
            $duplicate = array();
            foreach ($value['images'] as &$image) {
                if (!isset($image['value_id'])) {
                    continue;
                }
                $newFile = $this->_copyImage($image['file']);
                $newImages[$image['file']] = array(
                    'new_file' => $newFile,
                    'label' => $image['label']
                );
                $duplicate[$image['value_id']] = $newFile;
            }

            $value['duplicate'] = $duplicate;
        }

        foreach ($object->getMediaAttributes() as $mediaAttribute) {
            $mediaAttrCode = $mediaAttribute->getAttributeCode();
            $attrData = $object->getData($mediaAttrCode);

            if (in_array($attrData, $clearImages)) {
                $object->setData($mediaAttrCode, 'no_selection');
            }

            if (in_array($attrData, array_keys($newImages))) {
                $object->setData($mediaAttrCode, $newImages[$attrData]['new_file']);
                $object->setData($mediaAttrCode.'_label', $newImages[$attrData]['label']);
            }

            if (in_array($attrData, array_keys($existImages))) {
                $object->setData($mediaAttrCode.'_label', $existImages[$attrData]['label']);
            }
        }

        Mage::dispatchEvent('catalog_product_media_save_before', array('product' => $object, 'images' => $value));

        $object->setData($attrCode, $value);

        return $this;
    }

} // Class Mage_Catalog_Model_Product_Attribute_Backend_Media End
