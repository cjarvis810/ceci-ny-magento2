<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 8/13/2015
 * Time: 4:15 PM
 */

class Pixafy_Pixcatalog_Model_Catalog_Layer_Filter_Slice extends Mage_Catalog_Model_Layer_Filter_Abstract
{
    public function __construct()
    {
        parent::_construct();
        $this->_requestVar = 'slice';
    }

    /**
     * Apply slice filter to layer
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Mage_Core_Block_Abstract $filterBlock
     * @return  Mage_Catalog_Model_Layer_Filter_Slice
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = (int) $request->getParam($this->getRequestVar());
        if (!$filter || Mage::registry('pixafy_slice_filter')) {
            return $this;
        }

        $slice = Mage::getModel('pixcatalog/colorwheel_slice')->load($request->getParam('slice'));

        $state = $this->_createItem(
            $slice->getName(), $filter
        )->setVar($this->_requestVar);

        /* @var $state Mage_Catalog_Model_Layer_Filter_Item */
        $this->getLayer()->getState()->addFilter($state);
        Mage::register('pixafy_slice_filter', true);
        return $this;
    }

    public function getName()
    {
        //return 'Slice';
        return Mage::helper('pixcatalog')->__('Color');
    }

    protected function _getItemData()
    {
        $data = array();
        foreach(Mage::getModel('pixcatalog/colorwheel_slice')->getCollection()->getData() as $slice) {
            $data[] = array(
                'label' => $slice['name'],
                'value' => $slice['slice_id']
            );
        }

        return $data;
    }

    public function getItems()
    {
        return $this->_getItemData();
    }
    public function getAttributeModel()
    {
        return false;
    }
}