<?php

class Pixafy_Pixcatalog_Model_Catalog_Category extends Mage_Catalog_Model_Category
{
    /**
     * Retrieve array of block id's for category
     *
     * array($blockId => $position)
     *
     * @return array
     */
    public function getPromoBlocks()
    {
        if (!$this->getId()) {
            return array();
        }
        $blocks = $this->getData('promo_blocks');
        if (is_null($blocks)) {
            $blocks = $this->getResource()->getPromoBlocks($this);
            $this->setData('promo_blocks', $blocks);
        }
        return $blocks;
    }

    public function getColorways() {
        if (is_null($colorways = $this->getData('colorways'))) {
            $colorways = $this->getResource()->getColorways($this);
            $this->setColorways($colorways);
        }
        return $colorways;
    }
    
}