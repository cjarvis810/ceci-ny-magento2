<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Catalog view layer model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Model_Catalog_Layer extends Mage_Catalog_Model_Layer
{
    public function getCurrentColorwheelSlice() {
        if (is_null($slice = $this->getData('current_colorwheel_slice'))) {
            $slice = Mage::registry('current_colorwheel_slice');
            if (!$slice) {
                $slice = Mage::getModel('pixcatalog/colorwheel_slice');
            }
            $this->setCurrentColorwheelSlice($slice);
        }
        return $slice;
    }

    /**
     * Get layer state key
     *
     * @return string
     */
    public function getStateKey()
    {
        if ($this->_stateKey === null) {
            $this->_stateKey =
                'STORE_'.Mage::app()->getStore()->getId()
                . '_CAT_' . $this->getCurrentCategory()->getId()
                . '_CUSTGROUP_' . Mage::getSingleton('customer/session')->getCustomerGroupId()
                . '_COLORWHEELSLICE_' . Mage::getSingleton('customer/session')->getColorwheelSliceId();
        }

        return $this->_stateKey;
    }

    /**
     * Get default tags for current layer state
     *
     * @param   array $additionalTags
     * @return  array
     */
    public function getStateTags(array $additionalTags = array())
    {
        $additionalTags = array_merge($additionalTags, array(
            Mage_Catalog_Model_Category::CACHE_TAG.$this->getCurrentCategory()->getId().
            Pixafy_Pixcatalog_Model_Colorwheel_Slice::CACHE_TAG.$this->getCurrentColorwheelSlice()->getId()
        ));

        return $additionalTags;
    }
}
