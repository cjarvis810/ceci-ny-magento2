<?php

/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Catalog product model
 *
 * @method Mage_Catalog_Model_Resource_Product getResource()
 * @method Mage_Catalog_Model_Product setHasError(bool $value)
 * @method null|bool getHasError()
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Model_Catalog_Product extends Mage_Catalog_Model_Product {

    protected $colorwayCollection = false;

    public function getIsPersonalizable() {
        return false;
        return $this->getAttributeSetId() != $this->getResource()->getPersonalizableAttributeSetId();
    }

    public function getUseCategoryColorways() {
        /* NOT YET IMPLEMENTED - VINCENT TO DO
          if (is_null($useCategoryColorways = $this->getData('use_category_colorways')) && !is_null($productId = $this->getId())) {
          $useCategoryColorways = $this->getResource()->getAttributeRawValue($productId, 'use_category_colorways', Mage::app()->getStore()->getId());
          $this->setUseCategoryColorways($useCategoryColorways);
          }
          return $useCategoryColorways;
         */
        return true;
    }

    public function getFirstDeepestCategory() {
        if (is_null($deepest = $this->getData('first_deepest_category'))) {
            $deepest = $this->getCategoryCollection()
                    ->setOrder('level', 'desc')
                    ->setOrder('position', 'asc')
                    ->getFirstItem();
            $this->setFirstDeepestCategory($deepest);
        }
        return $deepest;
    }

    /**
     * Fetches the appropriate coloreways for this product
     */
    private function fetchColorwayCollection($limit = null) {
        //use_product_colorways
            $this->colorwayCollection = Mage::getResourceModel('pixcatalog/colorway_collection');
            if($this->getUseProductColorways()){
                $this->colorwayCollection->innerJoinProduct($this->getId());
            } else {
                $category = $this->getFirstDeepestCategory();
                $this->colorwayCollection->innerJoinCategory($category->getId());
            }
            if( $limit ){
                $this->colorwayCollection->getSelect()->limit(4);
            }
            $this->colorwayCollection->getSelect()->order(array('position ASC'));
    }

   public function getColorwayCollection()
   {

       if($this->isConfigurable()) {

           // Get configurable options
           $attributeValueOptions = array();

           $attribute = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'color');

           $collection =Mage::getResourceModel('eav/entity_attribute_option_collection')
               ->setAttributeFilter($attribute->getId())
               ->setPositionOrder('asc')
               ->setStoreFilter(0)
               ->load();

           foreach($collection as $colors) {

               $attr = $this->getResource()->getAttribute("color");
               if ($attr->usesSource()) {

                   $colorLabel = $attr->getSource()->getOptionText($colors->getOptionId());
                   $attributeValueOptions['Colors'][$colorLabel] =$colors->getValue();

               }

           }

           return $attributeValueOptions;
       }


       /*

       if($this->productSupportColorways()) {

           if ($this->colorwayCollection === FALSE) {
               $this->fetchColorwayCollection($limit);
           }
           return $this->colorwayCollection;

       } else {
           return false;
       }*/
   }


    private function productSupportColorways()
    {

        $product = Mage::getModel('catalog/product')
            ->load($this->getId());

        if(stripos($product->getData('image'), ".svg") !== false) {

            // .svg extension found that means it must have colorways
            return true;

        } else {
            // No .svg extension found assume no color ways
            return false;

        }

    }


    public function getColorways() {
        if (is_null($colorways = $this->getData('colorways'))) {
            if ($this->getUseCategoryColorways()) {
                $category = $this->getCategory();
                if (!$category || !$category->getId()) {
                    $category = $this->getFirstDeepestCategory();
                }
                Mage::log('COLORWAY CATEGORY: ' . $category->getId(), null, 'test.log');
                $colorways = $category->getColorways();
            } else {
                $colorways = $this->getResource()->getColorways($this);
            }
            $this->setColorways($colorways);
        }
        return $colorways;
    }

    /*
     * @Description     A list of personalized options used in product view page in "What can I personalize"
     */
    public function getPersonalizedOptions()
    {
        $personalizedOptions = array(
            'ps1' => 'Personalized option #1',
            'ps2' => 'Personalized option #2',
            'ps3' => 'Personalized option #3',
        );

        return $personalizedOptions;
    }

    /*
     * @Description     A list of languages supported for the product.
     */
    public function getLanguageOptions()
    {
        $languageOptions = array(
           'en'     => 'English',
           'spn'    => 'Spanish',
           'arb'    => 'Arabic'
        );

        return $languageOptions;
    }


    /*
     * @Description     Print types for product page in Printing and pricing
     */
    public function getPrintingOptions()
    {
        $printingOptions = array(
            'lc'    => 'Lasercut',
            'eg'    => 'Engraving',
            'fp'    => 'Flat Printing',
            'lp'    => 'Letterpress',
            'fs'    => 'Foil Stamping',
        );

        return $printingOptions;
    }

    public function getSkuFromColorWay($attrId)
    {

        // Get simple products id
        $simpleProductIds = Mage::getModel('catalog/product_type_configurable')
            ->setProduct($this)
            ->getUsedProductCollection()
            ->getAllIds();

        $model = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('color')
            ->addFieldToFilter('color', $attrId)
            ->addAttributeToFilter('entity_id',$simpleProductIds)
            ->getFirstItem();

        return ($model) ? $model->getSku() : Mage::throwException("Error finding color for simple product");

    }


    public function getAttributeIdFromColorName($colorName)
    {
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'color');

        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        foreach($options as $option) {
            if($colorName == $option['label']) {
                return $option['value'];
            }
        }

    }


    public function createQuoteFromPersonalization()
    {
        $customer_id = Mage::getSingleton('customer/session')->getId();

        if($customer_id) {

            //print "<pre>";
            $product_id = $this->getId();
            $store_id = Mage::app()->getStore()->getId();
            $customerObj = Mage::getModel('customer/customer')->load($customer_id);
            $quoteObj = Mage::getSingleton('checkout/session')->getQuote();

            //print_r($quoteObj->getId());


            if( !$quoteObj->getId() ) {

                $quoteObj = Mage::getModel('sales/quote')->assignCustomer($customerObj);
                //print_r($quoteObj->getData());
            }

           // die();


            // @TODO Update qty if product already exist
            //if (! $quoteObj->hasProductId($product_id) ) {
                // check customizable_options


                $storeObj = $quoteObj->getStore()->load($store_id);
                $quoteObj->setStore($storeObj);
                $productModel = Mage::getModel('catalog/product');
                $productObj = $productModel->load($product_id);
                $quoteItem = Mage::getModel('sales/quote_item')->setProduct($productObj);
                $quoteItem->setQuote($quoteObj);
                $quoteItem->setQty('1');
                $quoteItem->setStoreId($store_id);
                $quoteObj->addItem($quoteItem);
                $quoteObj->setStoreId($store_id);
                $quoteObj->collectTotals();
                $quoteObj->save();
            //}


            //die();
            return $quoteItem;
            //die("createQuoteFromPersonalization");

            // @TODO Create design project

        }

    }


    public function getFontStyling()
    {
        $fontPath = Mage::getBaseDir('media').DS."fonts";
        if ($handle = opendir($fontPath)) {

            $fonts = array();
            while (false !== ($entry = readdir($handle))) {

                if($entry != ".." && $entry != ".") {
                    //echo "$entry\n";
                    $fonts[] = $entry;
                }

            }
        }

        return $fonts;
    }


    public function hasProofs()
    {
        return true;
    }

    public function getMotifImages()
    {
        $model = Mage::getModel('pixcatalog/product_motif_photo')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getId())
            ->addFieldToFilter('pmpt.image_type', 'motif');

        return $model->join(array('pmpt' => 'pixcatalog/product_motif_type'),
            'main_table.mp_id = pmpt.mp_id');

    }

    public function getPhotoImages()
    {
        $model = Mage::getModel('pixcatalog/product_motif_photo')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getId())
            ->addFieldToFilter('pmpt.image_type', 'photo');

        return $model->join(array('pmpt' => 'pixcatalog/product_motif_type'),
            'main_table.mp_id = pmpt.mp_id');

    }

    public function getMotifPhotos()
    {
        $model = Mage::getModel('pixcatalog/product_motif_photo')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getId());

        return $model;
    }


    public function enhancementChecked($enhancementId, $quoteItemId)
    {

        $quoteItemOptionModel = Mage::getModel('sales/quote_item_option')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getId())
            ->addFieldToFilter('item_id', $quoteItemId)
            ->addFieldToFilter('code', 'customizable_options')
            ->getFirstItem();

        if($quoteItemOptionModel) {
            $optionsArray = unserialize($quoteItemOptionModel->getData('value'));

            if($optionsArray['enhancements'][$enhancementId]) {

                return true;

            } else {

                return false;

            }

        } else {

            return false;

        }

    }

}
