<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:44 PM
 */

class Pixafy_Pixcatalog_Model_Colorway extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('pixcatalog/colorway');
    }
}