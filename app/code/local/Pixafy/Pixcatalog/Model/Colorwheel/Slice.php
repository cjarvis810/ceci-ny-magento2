<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 2:44 PM
 */

class Pixafy_Pixcatalog_Model_Colorwheel_Slice extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('pixcatalog/colorwheel_slice');
    }

    public function getColorways() {
        if (is_null($colorways = $this->getData('colorways'))) {
            $colorways = $this->getResource()->getColorways($this);
            $this->setColorways($colorways);
        }
        return $colorways;
    }

    public function setColorwayIds($colorwayIds)
    {
        $binds = array(
            'slice_id'    => $this->getId(),
        );

        $read = Mage::getSingleton('core/resource')->getConnection('core_read');

        $query = "DELETE FROM pixcatalog_colorwheel_slice_colorway_grid WHERE slice_id = :slice_id";

        $read->query($query, $binds);

        foreach($colorwayIds as $position => $colorwayId) {

            $binds['colorway_id'] = $colorwayId;
            $binds['position'] = $position;
            //if(!$results->fetch()) {
                $write = Mage::getSingleton("core/resource")->getConnection("core_write");

                // Concatenated with . for readability
                $query = "insert into pixcatalog_colorwheel_slice_colorway_grid "
                    . "(slice_id, colorway_id, position) values "
                    . "(:slice_id, :colorway_id, :position)";

                $write->query($query, $binds);
           // }

        }
    }
}