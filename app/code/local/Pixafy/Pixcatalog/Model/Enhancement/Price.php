<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/6/2015
 * Time: 4:55 PM
 */

class Pixafy_Pixcatalog_Model_Enhancement_Price extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('pixcatalog/enhancement_price');
    }
}