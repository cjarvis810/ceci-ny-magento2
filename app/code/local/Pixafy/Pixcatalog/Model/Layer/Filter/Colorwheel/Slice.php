<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Layer category filter
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Model_Layer_Filter_Colorwheel_Slice extends Mage_Catalog_Model_Layer_Filter_Abstract
{
    /**
     * ColorwayId
     *
     * @var int
     */
    protected $_sliceId;

    /**
     * Applied Category
     *
     * @var Mage_Catalog_Model_Category
     */
    protected $_appliedSlice = null;

    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_requestVar = 'slice';
    }

    /**
     * Apply category filter to layer
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Mage_Core_Block_Abstract $filterBlock
     * @return  Mage_Catalog_Model_Layer_Filter_Category
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = (int) $request->getParam($this->getRequestVar());
        if (!$filter) {
            return $this;
        }
        $this->_sliceId = $filter;

        Mage::register('current_colorwheel_slice_filter', $this->getSlice(), true);

        $this->_appliedSlice = Mage::getModel('pixcatalog/colorwheel_slice')->load($filter);

        if ($this->_isValidSlice($this->_appliedSlice)) {
            $this->getLayer()->getProductCollection()
                ->addColorwheelSliceFilter($this->_appliedSlice);

            $this->getLayer()->getState()->addFilter(
                $this->_createItem($this->_appliedSlice->getName(), $filter)
            );
        }

        return $this;
    }

    /**
     * Validate category for be using as filter
     *
     * @param   Mage_Catalog_Model_Category $category
     * @return unknown
     */
    protected function _isValidSlice($slice)
    {
        return $slice->getId();
    }

    /**
     * Get filter name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('catalog')->__('Colorwheel Slice');
    }

    /**
     * Get selected category object
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getSlice()
    {
        if (!is_null($this->_sliceId)) {
            $slice = Mage::getModel('pixcatalog/colorwheel_slice')
                ->load($this->_sliceId);
            if ($slice->getId()) {
                return $slice;
            }
        }
        return $this->getLayer()->getCurrentColorwheelSlice();
    }

    /**
     * Get data array for building category filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
        $key = $this->getLayer()->getStateKey().'_'.$this->_requestVar;
        $data = $this->getLayer()->getAggregator()->getCacheData($key);

        if ($data === null) {
            $options = Mage::getResourceSingleton('pixcatalog/product_attribute_backend_colorwheel_slice')->getAllOptions();
            $optionsCount = $this->_getResource()->getCount($this);

            foreach ($options as $option) {
                if (is_array($option['value'])) {
                    continue;
                }
                if (Mage::helper('core/string')->strlen($option['value'])) {
                    $data[] = array(
                        'label' => $option['label'],
                        'value' => $option['value'],
                        'count' => isset($optionsCount[$option['value']]) ? $optionsCount[$option['value']] : 0,
                    );
                }
            }

            $tags = array(
                Pixafy_Pixcatalog_Model_Colorwheel_Slice::CACHE_TAG.':'. $slice->getId()
            );

            $tags = $this->getLayer()->getStateTags($tags);
            $this->getLayer()->getAggregator()->saveCacheData($data, $key, $tags);
        }
        return $data;
    }
}
