<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Product list toolbar
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Block_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{

    // custom sort function for ordering static blocks
    public static function cmpBlocks($a, $b) {
        return $a['position'] - $b['position'];
    }

    /**
     * Set collection to pager
     *
     * @param Varien_Data_Collection $collection
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setCollection($collection)
    {
        if ($collection->count() > 0 && Mage::helper('pixcatalog')->getUseInlinePromoBlocks() && $category = Mage::registry('current_category')) {
            $blocks = $category->getPromoBlocks();
            if (!empty($blocks)) {
                $p = $this->getCurrentPage();
                $sz = (int)$this->getLimit();
                $totalProducts = $collection->getSize();
                if (!$sz) {
                    $sz = $totalProducts;
                }
                $sortedBlocks = array(
                    'absolute' => array(),
                    'absolute_repeat' => array(),
                    'percentage' => array(),
                    'percentage_repeat' => array()
                );
                $totalBlocks = 0;
                foreach ($blocks as $blockId => $position) {
                    $posType = 'absolute';
                    if (substr($position, -1) == '%') {
                        $posType = 'percentage';
                        $position = floatval(substr($position, 0, -1)) / 100;
                        ++$totalBlocks;
                    } else if (substr($position, -2) == '%R') {
                        $posType = 'percentage_repeat';
                        $position = floatval(substr($position, 0, -2)) / 100;
                        $totalBlocks += intval(1 / $position);
                    } else if (substr($position, -1) == 'R') {
                        $posType = 'absolute_repeat';
                        $position = intval(substr($position, 0, -1));
                    } else {
                        $position = intval($position);
                        ++$totalBlocks;
                    }
                    $sortedBlocks[$posType][] = array(
                        'block_id' => $blockId,
                        'orig_position' => $position,
                        'position' => $position
                    );
                }
                $totalItems = $totalProducts + $totalBlocks;
                foreach ($sortedBlocks as &$blockGroup) {
                    usort($blockGroup, 'Pixafy_Pixcatalog_Block_Product_List_Toolbar::cmpBlocks');
                }
                while (!empty($sortedBlocks['absolute_repeat']) && $sortedBlocks['absolute_repeat'][0]['position'] <= $totalItems) {
                    ++$totalBlocks;
                    ++$totalItems;
                    $sortedBlocks['absolute_repeat'][0]['position'] += $sortedBlocks['absolute_repeat'][0]['orig_position'];
                    usort($sortedBlocks['absolute_repeat'], 'Pixafy_Pixcatalog_Block_Product_List_Toolbar::cmpBlocks');
                }
                foreach ($sortedBlocks['absolute_repeat'] as &$block) {
                    $block['position'] = $block['orig_position'];
                }
                usort($sortedBlocks['absolute_repeat'], 'Pixafy_Pixcatalog_Block_Product_List_Toolbar::cmpBlocks');
                $items = array();
                $start = ($p - 1) * $sz;
                $end = min($start + $sz, $totalItems);
                $firstProduct = null;
                $sizes = array(
                    'absolute' => count($sortedBlocks['absolute']),
                    'percentage' => count($sortedBlocks['percentage']),
                );
                $indexes = array(
                    'product' => 0,
                    'absolute' => 0,
                    'percentage' => 0,
                );
                for ($i = 1; $i <= $end; ++$i) {
                    if ($indexes['absolute'] < $sizes['absolute'] && ($sortedBlocks['absolute'][$indexes['absolute']]['position'] <= $i || $indexes['product'] >= $totalProducts)) {
                        $block = &$sortedBlocks['absolute'][$indexes['absolute']++];
                        $items[] = array(
                            'type' => 'absolute',
                            'block_id' => $block['block_id']
                        );
                    } else if (!empty($sortedBlocks['absolute_repeat']) && $sortedBlocks['absolute_repeat'][0]['position'] <= $i) {
                        $block = &$sortedBlocks['absolute_repeat'][0];
                        $items[] = array(
                            'type' => 'absolute_repeat',
                            'block_id' => $block['block_id']
                        );
                        $block['position'] += $block['orig_position'];
                        usort($sortedBlocks['absolute_repeat'], 'Pixafy_Pixcatalog_Block_Product_List_Toolbar::cmpBlocks');
                    } else if ($indexes['percentage'] < $sizes['percentage'] && $sortedBlocks['percentage'][$indexes['percentage']]['position'] <= $indexes['product'] / $totalProducts) {
                        $block = &$sortedBlocks['percentage'][$indexes['percentage']++];
                        $items[] = array(
                            'type' => 'percentage',
                            'block_id' => $block['block_id']
                        );
                    } else if (!empty($sortedBlocks['percentage_repeat']) && $sortedBlocks['percentage_repeat'][0]['position'] <= $indexes['product'] / $totalProducts) {
                        $block = &$sortedBlocks['percentage_repeat'][0];
                        $items[] = array(
                            'type' => 'percentage_repeat',
                            'block_id' => $block['block_id']
                        );
                        $block['position'] += $block['orig_position'];
                        usort($sortedBlocks['percentage_repeat'], 'Pixafy_Pixcatalog_Block_Product_List_Toolbar::cmpBlocks');
                    } else {
                        $items[] = array(
                            'type' => 'product'
                        );
                        if ($i >= $start && is_null($firstProduct)) {
                            $firstProduct = $indexes['product'];
                        }
                        ++$indexes['product'];
                    }
                }
                $products = array();
                if ($this->getCurrentOrder()) {
                    $collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
                }
                $collection->getSelect()->limit($indexes['product'] - $firstProduct, $firstProduct);
                foreach ($collection as $product) {
                    $products[] = $product;
                }
                $newCollection = new Varien_Data_Collection();
                $j = 0;
                for ($i = $start; $i < $end; ++$i) {
                    $item = &$items[$i];
                    if ($item['type'] == 'product') {
                        if(!is_null($products[$j])) {
                            $newCollection->addItem($products[$j++]);
                        }
                    } else {
                        $dummyProduct = Mage::getModel('catalog/product')->setId(-$i)->setBlockId($item['block_id']);
                        $newCollection->addItem($dummyProduct);
                    }
                }
                $this->_collection = $newCollection;
                $this->setFirstNum($start + 1)
                    ->setLastNum($end)
                    ->setTotalNum($totalItems)
                    ->setIsFirstPage($start == 0)
                    ->setLastPageNum(ceil($totalItems / $sz))
                    ->setIsLastPage($end >= $totalItems);
                return $this;
            }
        }
        parent::setCollection($collection);
        $this->_collection->load();
        $this->setFirstNum($collection->getPageSize()*($collection->getCurPage()-1)+1)
            ->setLastNum($collection->getPageSize()*($collection->getCurPage()-1)+$collection->count())
            ->setTotalNum($collection->getSize())
            ->setIsFirstPage($collection->getCurPage() == 1)
            ->setLastPageNum($collection->getLastPageNumber())
            ->setIsLastPage($collection->getCurPage() == $collection->getLastPageNumber());
        return $this;
    }

    public function getFirstNum()
    {
        if (is_null($this->getData('first_num'))) {
            $this->setData('first_num', parent::getFirstNum());
        }
        return $this->getData('first_num');
    }

    public function getLastNum()
    {
        if (is_null($this->getData('last_num'))) {
            $this->setData('last_num', parent::getLastNum());
        }
        return $this->getData('last_num');
    }

    public function getTotalNum()
    {
        if (is_null($this->getData('total_num'))) {
            $this->setData('total_num', parent::getTotalNum());
        }
        return $this->getData('total_num');
    }

    public function isFirstPage()
    {
        return $this->getIsFirstPage();
    }

    public function getIsFirstPage() {
        if (is_null($this->getData('is_first_page'))) {
            $this->setData('is_first_page', parent::isFirstPage());
        }
        return $this->getData('is_first_page');
    }

    public function getLastPageNum()
    {
        if (is_null($this->getData('last_page_num'))) {
            $this->setData('last_page_num', parent::getLastPageNum());
        }
        return $this->getData('last_page_num');
    }

    public function isLastPage() {
        return $this->getIsLastPage();
    }

    public function getIsLastPage() {
        return $this->getCurrentPage() == $this->getLastPageNum();
    }

    /**
     * Render pagination HTML
     *
     * @return string
     */
    public function getPagerHtml()
    {
        $pagerBlock = $this->getChild('product_list_toolbar_pager');

        if ($pagerBlock instanceof Varien_Object) {

            /* @var $pagerBlock Mage_Page_Block_Html_Pager */
            $pagerBlock->setAvailableLimit($this->getAvailableLimit());

            $pagerBlock->setUseContainer(false)
                ->setShowPerPage(false)
                ->setShowAmounts(false)
                ->setLimitVarName($this->getLimitVarName())
                ->setPageVarName($this->getPageVarName())
                ->setLimit($this->getLimit())
                ->setFrameLength(Mage::getStoreConfig('design/pagination/pagination_frame'))
                ->setJump(Mage::getStoreConfig('design/pagination/pagination_frame_skip'))
                ->setCollection($this->getCollection())
                ->setCurrentPage($this->getCurrentPage())
                ->setFirstNum($this->getFirstNum())
                ->setLastNum($this->getLastNum())
                ->setTotalNum($this->getTotalNum())
                ->setIsFirstPage($this->getIsFirstPage())
                ->setLastPageNum($this->getLastPageNum())
                ->setIsLastPage($this->getIsLastPage());

            return $pagerBlock->toHtml();
        }

        return '';
    }

}