<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Catalog layer category filter
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Block_Layer_Filter_Colorwheel_Slice extends Mage_Catalog_Block_Layer_Filter_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->_requestVar = 'slice';
        $this->_filterModelName = 'pixcatalog/layer_filter_colorwheel_slice';
    }


    /*
     * Apply colorwheel slice filter
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = (int)$request->getParam($this->getRequestVar());

        if (!$filter || Mage::registry('colorwheel_slice_filter')) {
            return $this;
        }

        // Select query //
        $select = $this->getLayer()->getProductCollection()->getSelect();
        /* @var $select Zend_Db_Select */

        //$select->where('aaaaaa');

        // @TODO Set labels to color selected
        $state = $this->_createItem(
            "Color slice", $filter
        )->setVar($this->_requestVar);

        /*
        if ($filter == self::FILTER_ON_SALE) {
            $select->where('price_index.final_price < price_index.price');
            $stateLabel = Mage::helper('inchoo_sale')->__('On Sale');
        } else {
            $select->where('price_index.final_price >= price_index.price');
            $stateLabel = Mage::helper('inchoo_sale')->__('Not On Sale');
        }

        $state = $this->_createItem(
            $stateLabel, $filter
        )->setVar($this->_requestVar);*/
        /* @var $state Mage_Catalog_Model_Layer_Filter_Item */

        $this->getLayer()->getState()->addFilter($state);

        Mage::register('colorwheel_slice_filter', true);

        return $this;
    }

    /**
     * Get filter name
     *
     * @return string
     */
    public function getName()
    {
        return 'Slice';
    }


}