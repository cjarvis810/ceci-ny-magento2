<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/7/2015
 * Time: 2:48 PM
 */
class Pixafy_Pixcatalog_Block_Adminhtml_Tabs_Motif extends Mage_Adminhtml_Block_Widget
{
    public function __construct()
    {
        parent::_construct();
        $this->setTemplate('pixafy/pixcatalog/tabs/motif-photo.phtml');
    }
}