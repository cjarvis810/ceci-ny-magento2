<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Colorways_Rendercheckbox extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row) {
      return "<input type='checkbox' name='colorway_checked[{$row->getColorwayId()}]' ". ($row->getGridColorwayId() ? 'checked': '')  ." />";
      
    }

}
