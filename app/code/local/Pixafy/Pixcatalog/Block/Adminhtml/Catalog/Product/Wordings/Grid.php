<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Wordings_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public $product;
    protected $colorways = false;

    public function __construct() {
        parent::__construct();
        $this->setId('colorway_id');
        $this->setUseAjax(true);
        $this->setDefaultSort('position');
        $this->setDefaultDir('asc');
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('pixwording/wording_collection');
        if ($this->product) {
            $collection->leftJoinProductWordingPositions($this->product->getId());                       
        }                
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('product_wording_ids[]', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'product_colorway',
            'field_name' => 'product_wording_ids[]',
            'width'=>'50px',
            'index' => 'wording_ids',
            'align' => 'center',            
            'editable' => true,
            "renderer" => "Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Wordings_Rendercheckbox"
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('pixcatalog')->__('Name'),
            'index' => 'wording_name'
        ));

        $this->addColumn('template', array(
            'header' => Mage::helper('pixcatalog')->__('Colors'),
            'align' => 'left',
            'index' => 'template'
        ));

        $this->addColumn('wording_position[]', array(
            'header' => Mage::helper('catalog')->__('Position'),
            'width' => '150',
            'index' => 'position',
            'name' => 'wording_position[]',            
            'field_name' => 'wording_position[]',
            'editable' => true,
            'renderer' => 'Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Wordings_Renderposition'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true, 'tab' => 'wordings'));
    }


}
