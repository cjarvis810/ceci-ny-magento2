<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Colorways_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public $product;
    protected $colorways = false;

    public function __construct() {
        parent::__construct();
        $this->setId('colorway_id');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('pixcatalog/colorway_collection');
        if ($this->product) {
            $collection->leftJoinProductColorwayPosition($this->product->getId());                       
        }                
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('product_colorway', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'product_colorway',
            'field_name' => 'color_way_ids[]',
            
            'index' => 'colorway_id',
            'align' => 'center',            
            'editable' => true,
            "renderer" => "Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Colorways_Rendercheckbox"
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('pixcatalog')->__('Name'),
            'index' => 'name'
        ));

        $this->addColumn('colors', array(
            'header' => Mage::helper('pixcatalog')->__('Colors'),
            'align' => 'left',
            'index' => 'colors'
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('pixcatalog')->__('Created At'),
            'index' => 'created_at',
            'type' => 'datetime',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('pixcatalog')->__('Updated at'),
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('colorway_position[]', array(
            'header' => Mage::helper('catalog')->__('Position'),
            'width' => '150',
            'index' => 'position',
            'name' => 'colorway_position[]',            
            'field_name' => 'colorway_position[]',
            'editable' => true,
            'renderer' => 'Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Colorways_Renderposition'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true, 'tab' => 'colorway'));
    }

    protected function getColorways() {
        $this->colorways = $this->product->getColorwayCollection();
    }

    protected function _getSelectedColorwayIds() {
        if ($this->colorways === false) {
            $this->getColorways();
        }
        $c = array();        
        foreach ($this->colorways as $cw) {
            $c[] = $cw->getColorwayId();
        }
        return $c;
    }

    protected function getPositions() {
        if ($this->colorways === false) {
            $this->getColorways();
        }
        $c = array();
        foreach ($this->colorways as $cw) {
            $c[] = $cw->getPosition();
        }
        return $c;
    }

}
