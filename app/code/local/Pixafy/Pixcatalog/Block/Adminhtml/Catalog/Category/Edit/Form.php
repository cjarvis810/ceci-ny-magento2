<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Category_Edit_Form extends Mage_Adminhtml_Block_Catalog_Category_Edit_Form
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('pixafy/pixcatalog/catalog/category/edit/form.phtml');
    }


    public function getBlocksJson()
    {
        $blocks = $this->getCategory()->getPromoBlocks();
        if (!empty($blocks)) {
            return Mage::helper('core')->jsonEncode($blocks);
        }
        return '{}';
    }

    public function getColorwaysJson()
    {
        $colorways = $this->getCategory()->getColorways();
        if (!empty($colorways)) {
            return Mage::helper('core')->jsonEncode($colorways);
        }
        return '{}';
    }
}