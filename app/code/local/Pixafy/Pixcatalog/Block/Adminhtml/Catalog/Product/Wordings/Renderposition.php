<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Wordings_Renderposition extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        return  "<input type='text' value='{$row->getPosition()}' name='wordings_position[{$row->getWordingId()}]' />";
    }

}
