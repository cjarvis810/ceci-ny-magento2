<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Edit_Tabs extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs {

    protected function _prepareLayout() {
        parent::_prepareLayout();
        if ($this->getProduct()->getId() && $this->getProduct()->getData('use_product_wording')) {
            $this->addTab('product_wordings', array(
                'label' => Mage::helper('catalog')->__('Product Wordings'),
                'url' => $this->getUrl('*/productwording/index/id/' . $this->getProduct()->getId(), array('_current' => true)),
                'class' => 'ajax',
            ));
        }
        if ($this->getProduct()->getId() && $this->getProduct()->getData('use_product_colorways')) {
            $this->addTab('colorway', array(
                'label' => Mage::helper('pixcatalog')->__('Colorways'),
                'url' => $this->getUrl('*/productcolorway/index/id/' . $this->getProduct()->getId(), array('_current' => true)),
                'class' => 'ajax',
            ));
        }

        $this->addTab("motifs", array(
            'label'     => Mage::helper('catalog')->__('Motifs / Photos'),
            'content'   => $this->getLayout()
            ->createBlock('pixcatalog/adminhtml_tabs_motif')->toHtml(),
        ));

    }

}
