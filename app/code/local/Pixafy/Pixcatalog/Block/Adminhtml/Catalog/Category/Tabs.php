<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Category_Tabs extends Mage_Adminhtml_Block_Catalog_Category_Tabs {

    protected function _prepareLayout() {
        parent::_prepareLayout();
        $this->addTab('promo', array(
            'label' => Mage::helper('catalog')->__('Promo Blocks'),
            'content' => $this->getLayout()->createBlock(
                    'pixcatalog/adminhtml_category_tab_promoblock', 'category.promoblock.grid'
            )->toHtml(),
        ));
        $this->addTab('colorway', array(
            'label' => Mage::helper('pixcatalog')->__('Colorways'),
            'content' => $this->getLayout()->createBlock(
                    'pixcatalog/adminhtml_category_tab_colorway', 'category.colorway.grid'
            )->toHtml(),
        ));
        $this->addTab('category_wordings', array(
            'label' => Mage::helper('catalog')->__('Category Wordings'),
            'content' => $this->getLayout()->createBlock(
                    'pixwording/adminhtml_categorywordings'
            )->toHtml(),
        ));
    }

}
