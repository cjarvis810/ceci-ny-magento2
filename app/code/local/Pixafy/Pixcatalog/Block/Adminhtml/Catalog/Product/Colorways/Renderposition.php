<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Colorways_Renderposition extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        return  "<input type='text' value='{$row->getPosition()}' name='colorway_position[{$row->getColorwayId()}]' />";
    }

}
