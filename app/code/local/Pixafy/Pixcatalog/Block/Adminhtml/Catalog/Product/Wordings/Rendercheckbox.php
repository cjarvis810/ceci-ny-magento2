<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Wordings_Rendercheckbox extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row) {
      return "<input type='checkbox' name='wordings_checked[{$row->getWordingId()}]' ". ($row->getProductWordingId() ? 'checked': '')  ." />";
      
    }

}
