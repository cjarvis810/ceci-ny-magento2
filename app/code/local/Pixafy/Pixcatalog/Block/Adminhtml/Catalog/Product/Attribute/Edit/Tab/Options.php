<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 7/7/2015
 * Time: 11:21 AM
 */

class Pixafy_Pixcatalog_Block_Adminhtml_Catalog_Product_Attribute_Edit_Tab_Options extends
    Mage_Adminhtml_Block_Catalog_Product_Attribute_Edit_Tab_Options
{

    public function __construct()
    {
        $this->setTemplate('pixafy/pixcatalog/product/attribute/options.phtml');
    }
}