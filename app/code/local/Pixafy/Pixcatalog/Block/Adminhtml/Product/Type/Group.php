<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 6:13 PM
 */

class Pixafy_Pixcatalog_Block_Adminhtml_Product_Type_Group extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_product_type_group';
        $this->_blockGroup = 'pixcatalog';
        $this->_headerText = Mage::helper('pixcatalog')->__('Product Type Group');
        $this->_addButtonLabel = Mage::helper('pixcatalog')->__('+Add Group');
        parent::__construct();
    }

}