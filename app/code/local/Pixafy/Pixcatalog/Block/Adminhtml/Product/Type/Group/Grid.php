<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Product_Type_Group_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();
		$this->setId('productTypeGroupGrid');
		$this->setDefaultSort('group_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$collection = Mage::getResourceModel('pixcatalog/product_type_group_collection');
           // ->joinNumTypes();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('group_id', array(
            'header' => 'ID',
            'align' => 'right',
            'width' => '50px',
            'index' => 'group_id',
		));
		$this->addColumn('name', array(
            'header' => 'Name',
            'align' => 'left',
            'index' => 'name',
		));
        $this->addColumn('num_types', array(
            'header' => 'Colors',
            'align' => 'left',
            'index' => 'num_types',
        ));
		$this->addColumn('created_at', array(
				'header' => 'Created at',
				'align' => 'left',
				'index' => 'created_at',
		));
		$this->addColumn('updated_at', array(
				'header' => 'Updated at',
				'align' => 'left',
				'index' => 'updated_at',
		));
		return parent::_prepareColumns();
	}

	protected function _prepareMassaction() {
		$this->setMassactionIdField('group_id');
		$this->getMassactionBlock()->setFormFieldName('group_id');

		$this->getMassactionBlock()->addItem('delete', array(
				'label' => Mage::helper('pixcatalog')->__('Delete'),
				'url' => $this->getUrl('*/*/massDelete'),
				'confirm' => Mage::helper('pixcatalog')->__('Are you sure?')
		));
		return $this;
	}

	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit', array('id' => $row->getGroupId()));
	}

}
