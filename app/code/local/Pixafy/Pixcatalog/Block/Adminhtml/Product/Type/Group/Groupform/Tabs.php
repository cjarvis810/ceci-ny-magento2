<?php 
class Pixafy_Pixcatalog_Block_Adminhtml_Product_Type_Group_Groupform_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
 
  public function __construct()
  {
      parent::__construct();
      $this->setId('form_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('pixcatalog')->__('Product Group Information'));
  }
 
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('pixcatalog')->__('Group Information'),
          'title'     => Mage::helper('pixcatalog')->__('Group Information'),
          'content'   => $this->getLayout()->createBlock('pixcatalog/adminhtml_product_type_group_groupform_tab_groupinfo')->toHtml()
      ));

      $this->addTab('form_section2', array(
          'label'     => Mage::helper('pixcatalog')->__('Types Information'),
          'title'     => Mage::helper('pixcatalog')->__('Types Information'),
          'content'   => $this->getLayout()->createBlock('pixcatalog/adminhtml_product_type_group_groupform_tab_typeinfo')->toHtml()
      ));
      
      return parent::_beforeToHtml();
  }
}


?>