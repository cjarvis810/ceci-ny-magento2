<?php 

class Pixafy_Pixcatalog_Block_Adminhtml_Product_Type_Group_Groupform_Tab_Typeinfo extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
        parent::__construct();
        $this->setId('group_id');
        $this->setDefaultSort('group_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
       //get current values
        $set = $this->_getSelectedProductTypesPositions();

        //get all values and mark current ones as selected
        $name='product_type';
        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
        $attributeId = $attributeInfo->getAttributeId();
        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
        $attributeOptions = $attribute ->getSource()->getAllOptions(false);
        $collection = new Varien_Data_Collection();
        foreach($attributeOptions as $attributeOption){
            $attributeOption['position'] = $set[(int)$attributeOption['value']] ? $set[(int)$attributeOption['value']] : '';
            $rowObj = new Varien_Object();
            $rowObj->setData($attributeOption);
            $collection->addItem($rowObj);
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();


    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_group', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name' 		=> 'in_group',
            'values'	=>  $this->_getSelectedProductTypes(),
            'align'     => 'center',
            'index'     => 'value'
        ));
        $this->addColumn('value_id', array(
            'header'    => Mage::helper('pixcatalog')->__('Value Id'),
            'sortable'  => true,
            'width'     => '60',
            'index'     => 'value'
        ));
        $this->addColumn('label', array(
            'header'    => Mage::helper('pixcatalog')->__('Label'),
            'index'     => 'label'
        ));
        $this->addColumn('position', array(
            'header'    => Mage::helper('pixcatalog')->__('Position'),
            'width'     => '1',
            'type'      => 'number',
            'index'     => 'position',
            'editable'  => true
        ));



        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _getSelectedProductTypes()
    {
            $collection = Mage::getModel('pixcatalog/product_type_group')->getCollection()
                ->addFieldToFilter('main_table.group_id' ,$this->getRequest()->getParam('id', 0))
                ->joinTypeIds();
           	$type_values = array();

            foreach($collection as $grouptypes)
			{
			    $type_values = $grouptypes->getTypeIds();
			}
            return $type_values;
    }

    protected function _getSelectedProductTypesPositions()
    {
        $current_values = array();
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = 'SELECT  pp.value_id, pp.position
        FROM pixcatalog_product_type_group AS main_table
        LEFT JOIN pixcatalog_product_type_group_product_type AS pp ON main_table.group_id = pp.group_id
        WHERE (main_table.group_id = '.$this->getRequest()->getParam('id', 0).')';
        $result = $read->fetchAll($sql);
        foreach($result as $grouptype)
        {
            $current_values[$grouptype['value_id']] = $grouptype['position'];
        }
        return $current_values;
    }
}
?>