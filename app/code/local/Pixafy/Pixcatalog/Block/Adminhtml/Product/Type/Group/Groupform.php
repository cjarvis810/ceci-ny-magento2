<?php
class Pixafy_Pixcatalog_Block_Adminhtml_Product_Type_Group_Groupform extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct() {
        $this->_objectId = 'group_id';
        $this->_blockGroup = 'pixcatalog';
        $this->_controller = 'adminhtml_product_type_group';
        $this->setTitle(Mage::helper('pixcatalog')->__('Product Type Group Information'));
        $this->_mode = 'groupform';
        if ($this->getRequest()->getParam('id')) {
            $this->_addButton('delete', array(
                'label' => Mage::helper('pixcatalog')->__('Delete Group'),
                'class' => 'delete',
                'onclick' => 'deleteConfirm(\'' . Mage::helper('adminhtml')->__('Are you sure you want to do this?')
                    . '\', \'' . Mage::helper("adminhtml")->getUrl('*/*/delete/', array('id' => $this->getRequest()->getParam('id'))) . '\')',
            ));
        }
        parent::__construct();
    }

    public function getHeaderText() {
        if (Mage::registry('product_type_group_data') && Mage::registry('product_type_group_data')->getId())
            return Mage::helper('pixcatalog')->__("Editing Group '%s'", $this->htmlEscape(Mage::registry('product_type_group_data')->getName()));
        return Mage::helper('pixcatalog')->__('Add Group');
    }

}


?>