<?php 
class Pixafy_Pixcatalog_Block_Adminhtml_Product_Type_Group_Groupform_Tab_Groupinfo extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
        $this->setForm($form);
        
		$fieldset = $form->addFieldset('registry_form', array('legend'=>Mage::helper('pixcatalog')->__('Product Type Group Information')));

		if (Mage::getSingleton('adminhtml/session')->getFormData())
		{
			$data = Mage::getSingleton('adminhtml/session')->getFormData();
			Mage::getSingleton('adminhtml/session')->setFormData(null);
		}
		elseif(Mage::registry('product_type_group_data'))
		{
			$data = Mage::registry('product_type_group_data')->getData();
		}

        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('pixcatalog')->__('Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',
        ));
        $fieldset->addField(
            'status',
            'select',
            array(
                'label' => Mage::helper('pixquiz')->__('Active'),
                'name'	=> 'status',
                'values' => array('1' => 'Yes', '0' => 'No'),
                'required' => TRUE
            )
        );
		
		 $form->setValues($data);
		 return parent::_prepareForm();
	}
}



?>