<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Category_Tab_Promoblock extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('catalog_category_promoblock');
        $this->setDefaultSort('block_id');
        $this->setUseAjax(true);
    }

    public function getCategory()
    {
        return Mage::registry('category');
    }

    protected function _addColumnFilterToCollection($column)
{
    // Set custom filter for in category flag
    if ($column->getId() == 'in_category') {
        $blockIds = $this->_getSelectedBlocks();
        if (empty($blockIds)) {
            $blockIds = 0;
        }
        if ($column->getFilter()->getValue()) {
            $this->getCollection()->addFieldToFilter('main_table.block_id', array('in'=>$blockIds));
        }
        elseif(!empty($blockIds)) {
            $this->getCollection()->addFieldToFilter('main_table.block_id', array('nin'=>$blockIds));
        }
    }
    else {
        parent::_addColumnFilterToCollection($column);
    }
    return $this;
}

    protected function _prepareCollection()
{
    if ($this->getCategory()->getId()) {
        $this->setDefaultFilter(array('in_category'=>1));
    }

    $collection = Mage::getModel('cms/block')
        ->getCollection()
        ->addFieldToFilter('is_active', '1');
    ;
    $collection->getSelect()
        ->joinLeft(array('ccb' => 'pixcatalog_category_block'),
            'main_table.block_id = ccb.block_id AND
        ccb.category_id = ' . $this->getRequest()->getParam('id', 0)
            ,
            array('position'));
    $this->setCollection($collection);

    return parent::_prepareCollection();
}

    protected function _prepareColumns()
{
    $this->addColumn('in_category', array(
        'header_css_class' => 'a-center',
        'type'      => 'checkbox',
        'name'      => 'in_category',
        'values'    => $this->_getSelectedBlocks(),
        'align'     => 'center',
        'index'     => 'block_id'
    ));
    $this->addColumn('title', array(
        'header'    => Mage::helper('catalog')->__('Title'),
        'index'     => 'title'
    ));
    $this->addColumn('identifier', array(
        'header'    => Mage::helper('cms')->__('Identifier'),
        'align'     => 'left',
        'index'     => 'identifier'
    ));

    $this->addColumn('is_active', array(
        'header'    => Mage::helper('cms')->__('Status'),
        'index'     => 'is_active',
        'type'      => 'options',
        'options'   => array(
            0 => Mage::helper('cms')->__('Disabled'),
            1 => Mage::helper('cms')->__('Enabled')
        ),
    ));

    $this->addColumn('creation_time', array(
        'header'    => Mage::helper('cms')->__('Date Created'),
        'index'     => 'creation_time',
        'type'      => 'datetime',
    ));

    $this->addColumn('update_time', array(
        'header' => Mage::helper('catalog')->__('Updated at'),
        'index' => 'update_time',
        'type' => 'datetime',
    ));
    $this->addColumn('position', array(
        'header'    => Mage::helper('catalog')->__('Position'),
        'width'     => '150',
        'index'     => 'position',
        'editable'  => true
    ));

    return parent::_prepareColumns();
}

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true, 'tab' => 'promoblock'));
    }

    protected function _getSelectedBlocks()
    {
        $blocks = $this->getRequest()->getPost('selected_blocks');
        if (is_null($blocks)) {
            $blocks = $this->getCategory()->getPromoBlocks();
            return $blocks ? array_keys($blocks) : array();
        }
        return $blocks;
    }

}