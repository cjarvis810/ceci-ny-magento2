<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Category_Tab_Colorway extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('catalog_category_colorway');
        $this->setDefaultSort('colorway_id');
        $this->setUseAjax(true);
    }

    public function getCategory()
    {
        return Mage::registry('category');
    }

    protected function _addColumnFilterToCollection($column)
{
    // Set custom filter for in category flag
    if ($column->getId() == 'in_category') {
        $colorwayIds = $this->_getSelectedColorwayIds();
        if (empty($colorwayIds)) {
            $colorwayIds = 0;
        }
        if ($column->getFilter()->getValue()) {
            $this->getCollection()->addFieldToFilter('main_table.colorway_id', array('in'=>$colorwayIds));
        } elseif (!empty($colorwayIds)) {
            $this->getCollection()->addFieldToFilter('main_table.colorway_id', array('nin'=>$colorwayIds));
        }
    }
    else {
        parent::_addColumnFilterToCollection($column);
    }
    return $this;
}

    protected function _prepareCollection()
{
    if ($this->getCategory()->getId()) {
        $this->setDefaultFilter(array('in_category' => 1));
    }

    $collection = Mage::getResourceModel('pixcatalog/colorway_collection');
    ;
    $collection->getSelect()
        ->joinLeft(
            array('ccg' => 'pixcatalog_category_colorway_grid'),
            'main_table.colorway_id = ccg.colorway_id AND ccg.category_id = ' . $this->getRequest()->getParam('id', 0),
            array('position')
        );

    $this->setCollection($collection);

    return parent::_prepareCollection();
}

    protected function _prepareColumns() {
        $this->addColumn('in_category_colorway', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name'      => 'in_category_colorway',
            'values'    => $this->_getSelectedColorwayIds(),
            'align'     => 'center',
            'index'     => 'colorway_id'
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('pixcatalog')->__('Name'),
            'index'     => 'name'
        ));

        $this->addColumn('colors', array(
            'header'    => Mage::helper('pixcatalog')->__('Colors'),
            'align'     => 'left',
            'index'     => 'colors'
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('pixcatalog')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'datetime',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('pixcatalog')->__('Updated at'),
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('position', array(
            'header'    => Mage::helper('catalog')->__('Position'),
            'width'     => '150',
            'index'     => 'position',
            'editable'  => true
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true, 'tab' => 'colorway'));
    }

    protected function _getSelectedColorwayIds()
    {
        $colorways = $this->getRequest()->getPost('selected_colorways');
        if (is_null($colorways)) {
            $colorways = $this->getCategory()->getColorways();
            return $colorways ? array_keys($colorways) : array();
        }
        return $colorways;
    }

}