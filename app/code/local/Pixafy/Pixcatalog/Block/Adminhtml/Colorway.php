<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 4/1/2015
 * Time: 6:13 PM
 */

class Pixafy_Pixcatalog_Block_Adminhtml_Colorway extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_colorway';
        $this->_blockGroup = 'pixcatalog';
        $this->_headerText = Mage::helper('pixcatalog')->__('Colorway');
        $this->_addButtonLabel = Mage::helper('pixcatalog')->__('+Add Colorway');
        parent::__construct();
    }

}