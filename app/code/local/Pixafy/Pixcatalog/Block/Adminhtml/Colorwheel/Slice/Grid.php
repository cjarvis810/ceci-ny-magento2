<?php 
class Pixafy_Pixcatalog_Block_Adminhtml_Colorwheel_Slice_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('slice_id');
        $this->setDefaultSort('slice_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('pixcatalog/colorwheel_slice_collection');
        $this->setCollection($collection);
        return $collection;
    }
    protected function _prepareColumns()
    {
        $helper = Mage::helper('pixcatalog');

        $this->addColumn('slice_id', array(
            'header' => $helper->__('Slice Id'),
            'type' => 'number',
            'index'  => 'slice_id'
        ));
        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index'  => 'name'
        ));
        $this->addColumn('created_at', array(
            'header' => $helper->__('Created At'),
            'type'	 => 'date',
            'index'  => 'created_at'
        ));
        $this->addColumn('updated_at', array(
            'header' => $helper->__('Updated At'),
            'type'	 => 'date',
            'index'  => 'updated_at'
        ));
        $this->addColumn('position', array(
            'header' => $helper->__('Position'),
            'index'  => 'position'
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('slice_id');
        $this->getMassactionBlock()->setFormFieldName('slice_id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('pixcatalog')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('pixcatalog')->__('Are you sure?')
        ));
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getSliceId()));
    }
}


?>