<?php 
class Pixafy_Pixcatalog_Block_Adminhtml_Colorwheel_Slice_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'slice_id';
        $this->_blockGroup = 'pixcatalog';
        $this->_controller = 'adminhtml_colorwheel_slice';
        $this->_mode = 'edit';
        $this->_updateButton('save', 'label', Mage::helper('pixcatalog')->__('Save Slice'));
    }

    public function getHeaderText()
    {
        if(Mage::registry('colorwheel_slice') && Mage::registry('colorwheel_slice')->getId())
        {
            return Mage::helper('pixcatalog')->__('Edit Slice');
        }
        return Mage::helper('pixcatalog')->__('New Slice');
    }
}


?>