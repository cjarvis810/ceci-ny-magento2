<?php 
class Pixafy_Pixcatalog_Block_Adminhtml_Colorwheel_Slice_Edit_Tab_Colorway extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
        parent::__construct();
        $this->setId('colorway_id');
        $this->setDefaultSort('colorway_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    protected function _prepareCollection()
    {
        $sliceId = Mage::registry('colorwheel_slice')->getId();
        if($sliceId) {
            $collection = Mage::getResourceModel('pixcatalog/colorway_collection');

            /*
             * SELECT * FROM `pixcatalog_colorwheel_slice` as pcs
LEFT JOIN pixcatalog_colorwheel_slice_colorway_grid as pccscg ON pcs.slice_id = pccscg.slice_id
LEFT JOIN pixcatalog_colorway as pcc ON pccscg.colorway_id = pcc.colorway_id
WHERE pcs.slice_id = 3;

             */

            /*
            $collection->getSelect()
                ->joinLeft(
                    array('scg' => 'pixcatalog_colorwheel_slice_colorway_grid'),
                    'main_table.slice_id = scg.slice_id')
                ->joinLeft(
                    array('pcc' => 'pixcatalog_colorway')
                )
            */

            $collection->getSelect()
                ->joinLeft(
                    array('scg' => 'pixcatalog_colorwheel_slice_colorway_grid'),
                    'main_table.colorway_id = scg.colorway_id AND scg.slice_id = ' . $sliceId,
                    array('position')
                );

            $this->setCollection($collection);
            return $collection;

        }
    }

	protected function _prepareColumns()
    {
    	$helper = Mage::helper('pixcatalog');

        $this->addColumn('in_slice', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name' 		=> 'in_slice',
            'values'	=>  $this->_getSelectedColorwaysIds(),
            'align'     => 'center',
            'index'     => 'colorway_id'
        ));


    	$this->addColumn('colorway_name', array(
            'header' => $helper->__('Colorway Name'),
            'index'  => 'colorway_name'
        ));

    	$this->addColumn('colors', array(
            'header' => $helper->__('Colors'),
            'index'  => 'colors'
        ));

        /*
    	$this->addColumn('created_at', array(
            'header' => $helper->__('Created At'),
            'type'	 => 'date',
            'index'  => 'created_at'
        ));

    	$this->addColumn('updated_at', array(
            'header' => $helper->__('Updated At'),
            'type'	 => 'date',
            'index'  => 'updated_at'
        ));
        */

        $this->addColumn('position', array(
            'header'    => Mage::helper('pixquiz')->__('Position'),
            'width'     => '1',
            'type'      => 'number',
            'index'     => 'position',
            'editable'  => true
        ));

    	return parent::_prepareColumns();
    }

    protected function _getSelectedColorwaysIds()
    {

        $colorways = Mage::registry('colorwheel_slice')->getColorways();

        $count=0;
        $tempArray = array();
        foreach($colorways as $colorwayId => $position) {
            $tempArray[$count] = $colorwayId;
            $count++;
        }

        return $tempArray;
    }
}


?>