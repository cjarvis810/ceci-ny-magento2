<?php 
class Pixafy_Pixcatalog_Block_Adminhtml_Colorwheel_Slice_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
 
  public function __construct()
  {
      parent::__construct();
      $this->setId('form_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('pixcatalog')->__('Edit Colorwheel Slice'));
  }
 
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('pixcatalog')->__('Slice Information'),
          'title'     => Mage::helper('pixcatalog')->__('Slice Information'),
          'content'   => $this->getLayout()->createBlock('pixcatalog/adminhtml_colorwheel_slice_edit_tab_information')->toHtml()
      ));

      $this->addTab('form_section1', array(
          'label'     => Mage::helper('pixquiz')->__('Colorways'),
          'title'     => Mage::helper('pixquiz')->__('Colorways'),
          'content'   => $this->getLayout()->createBlock('pixcatalog/adminhtml_colorwheel_slice_edit_tab_colorway')->toHtml()
      ));
      
      return parent::_beforeToHtml();
  }
}


?>