<?php 

class Pixafy_Pixcatalog_Block_Adminhtml_Colorwheel_Slice_Edit_Tab_Information extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('registry_form', array('legend' => Mage::helper('pixcatalog')->__('Colorwheel Slice Information')));

        if (Mage::getSingleton('adminhtml/session')->getFormData())
        {
            $data = Mage::getSingleton('adminhtml/session')->getFormData();
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        }
        elseif(Mage::registry('colorwheel_slice'))
        {
            $data = Mage::registry('colorwheel_slice')->getData();
        }

        $fieldset->addField(
            'name', 'text', array(
                'label' => Mage::helper('pixquiz')->__('Name'),
                'name'	=> 'name',
                'required' => TRUE
            )
        );

        $form->setValues($data);

        return parent::_prepareForm();
	}
}



?>