<?php 
class Pixafy_Pixcatalog_Block_Adminhtml_Colorwheel_Slice extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
    {
	    $this->_blockGroup = 'pixcatalog';
	    $this->_controller = 'adminhtml_colorwheel_slice';
        $this->_headerText = Mage::helper('pixcatalog')->__('Colowheel Slices');
        $this->_addButtonLabel = Mage::helper('pixcatalog')->__('+Add Slice');
        parent::__construct();
    }

    public function getHeaderText()
    {
        return Mage::helper('pixcatalog')->__('Colorwheel Management');
    }
}


?>