<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Colorway_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
		if (Mage::registry('colorway_data')) {
			$data = Mage::registry('colorway_data')->getData();
		} else {
			$data = array();
		}
		$form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
            )
		);

		$form->setUseContainer(true);
		$this->setForm($form);

		if (Mage::getSingleton('adminhtml/session')->getFormData()) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData();
			Mage::getSingleton('adminhtml/session')->setFormData(null);
		} elseif (Mage::registry('colorway_data')) {
			$data = Mage::registry('colorway_data')->getData();
		}
		if ($this->getRequest()->getParam('id')) {
			$data['id'] = $this->getRequest()->getParam('id');
		}
        if (isset($data['colors']) && is_array($data['colors'])) {
            $data['colors'] = implode(',', $data['colors']);
        }
		$fieldset = $form->addFieldset('colorway_form', array(
				'legend' => Mage::helper('pixcatalog')->__('Colorway Information')));

		$fieldset->addField('name', 'text', array(
				'label' => Mage::helper('pixcatalog')->__('Name'),
				'class' => 'required-entry',
				'required' => true,
				'name' => 'name',
		));
        $fieldset->addField('colors', 'text', array(
            'label' => Mage::helper('pixcatalog')->__('Colors'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'colors',
        ));

		$form->setValues($data);
		return parent::_prepareForm();
	}

}
