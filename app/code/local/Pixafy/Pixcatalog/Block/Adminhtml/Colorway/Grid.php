<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Colorway_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();
		$this->setId('colorwayGrid');
		$this->setDefaultSort('colorway_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$collection = Mage::getResourceModel('pixcatalog/colorway_collection');
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('colorway_id', array(
				'header' => 'ID',
				'align' => 'right',
				'width' => '50px',
				'index' => 'colorway_id',
		));
		$this->addColumn('name', array(
				'header' => 'Name',
				'align' => 'left',
				'index' => 'name',
		));
        $this->addColumn('colors', array(
            'header' => 'Colors',
            'align' => 'left',
            'index' => 'colors',
        ));
		$this->addColumn('created_at', array(
				'header' => 'Created at',
				'align' => 'left',
				'index' => 'created_at',
		));
		$this->addColumn('updated_at', array(
				'header' => 'Updated at',
				'align' => 'left',
				'index' => 'updated_at',
		));
		return parent::_prepareColumns();
	}

	protected function _prepareMassaction() {
		$this->setMassactionIdField('colorway_id');
		$this->getMassactionBlock()->setFormFieldName('colorway_id');

		$this->getMassactionBlock()->addItem('delete', array(
				'label' => Mage::helper('pixcatalog')->__('Delete'),
				'url' => $this->getUrl('*/*/massDelete'),
				'confirm' => Mage::helper('pixcatalog')->__('Are you sure?')
		));
		return $this;
	}

	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit', array('id' => $row->getColorwayId()));
	}

}
