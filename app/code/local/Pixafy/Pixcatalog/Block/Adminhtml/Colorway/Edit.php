<?php

class Pixafy_Pixcatalog_Block_Adminhtml_Colorway_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	public function __construct() {
		$this->_objectId = 'colorway_id';
		$this->_blockGroup = 'pixcatalog';
		$this->_controller = 'adminhtml_colorway';
		$this->setTitle(Mage::helper('pixcatalog')->__('Colorway Information'));
		$this->_mode = 'edit';
		if ($this->getRequest()->getParam('id')) {
			$this->_addButton('delete', array(
					'label' => Mage::helper('pixcatalog')->__('Delete Colorway'),
					'class' => 'delete',
					'onclick' => 'deleteConfirm(\'' . Mage::helper('adminhtml')->__('Are you sure you want to do this?')
					. '\', \'' . Mage::helper("adminhtml")->getUrl('*/*/delete/', array('id' => $this->getRequest()->getParam('id'))) . '\')',
			));
		}
		parent::__construct();
	}

	public function getHeaderText() {
		if (Mage::registry('colorway_data') && Mage::registry('colorway_data')->getId())
			return Mage::helper('pixcatalog')->__("Edit Colorway '%s'", $this->htmlEscape(Mage::registry('colorway_data')->getName()));
		return Mage::helper('pixcatalog')->__('Add Colorway');
	}

}
