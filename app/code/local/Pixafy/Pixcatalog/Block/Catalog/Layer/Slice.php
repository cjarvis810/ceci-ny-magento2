<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 8/13/2015
 * Time: 4:12 PM
 */

class Pixafy_Pixcatalog_Block_Catalog_Layer_Slice extends Mage_Catalog_Block_Layer_View
{
    protected function _construct()
    {
        Mage::register('current_layer', $this->getLayer(), true);
    }

    public function getLayer()
    {
        return Mage::getSingleton('pixafy_pixcatalog/catalog_layer');
    }

}