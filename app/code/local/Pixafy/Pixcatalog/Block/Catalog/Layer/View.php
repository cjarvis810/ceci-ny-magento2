<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Catalog layered navigation view block
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Block_Catalog_Layer_View extends Mage_Catalog_Block_Layer_View
{
    const SLICE_FILTER_POSITION = 2;
    protected $_colorwheelSliceBlockName;

    /**
     * Initialize blocks names
     */
    protected function _initBlocks()
    {
        parent::_initBlocks();
        $this->_colorwheelSliceBlockName = 'pixcatalog/catalog_layer_filter_slice';
    }

    /**
     * Prepare child blocks
     *
     * @return Mage_Catalog_Block_Layer_View
     */
    protected function _prepareLayout()
    {
        $colorwheelSliceBlock = $this->getLayout()->createBlock($this->_colorwheelSliceBlockName)
            ->setLayer($this->getLayer())
            ->init();
        $this->setChild('slice_filter', $colorwheelSliceBlock);
        return parent::_prepareLayout();
    }

    /**
     * Get all layer filters
     *
     * @return array
     */
    public function getFilters()
    {
        $filters = parent::getFilters();

        if(($sliceFilter = $this->_getColorwheelSliceFilter())) {
            // Insert sale filter to the self::SLICE_FILTER_POSITION position
            $filters[] = $sliceFilter;
            //$filters = array_merge($filters, array($sliceFilter));

            /*
            $filters = array_merge(
                array_slice(
                    $filters,
                    0,
                    self::SLICE_FILTER_POSITION - 1
                ),
                array($sliceFilter),
                array_slice(
                    $filters,
                    self::SLICE_FILTER_POSITION - 1,
                    count($filters) - 1
                )
            );*/
        }

        return $filters;


        /*
        $filters = array();
        if ($categoryFilter = $this->_getCategoryFilter()) {
            $filters[] = $categoryFilter;
        }

        $filterableAttributes = $this->_getFilterableAttributes();
        foreach ($filterableAttributes as $attribute) {
            $filters[] = $this->getChild($attribute->getAttributeCode() . '_filter');
        }

        if ($sliceFilter = $this->_getColorwheelSliceFilter()) {
            $filters[] = $sliceFilter;
        }
        return $filters;*/
    }

    /**
     * Get category filter block
     *
     * @return Mage_Catalog_Block_Layer_Filter_Category
     */
    protected function _getColorwheelSliceFilter()
    {
        return $this->getChild('slice_filter');
    }

    /**
     * Retrieve active filters
     *
     * @return array
     */
    public function getActiveFilters()
    {
        if (is_null($filters = $this->getData('active_filters'))) {
            $filters = $this->getLayer()->getState()->getFilters();
            if (!is_array($filters)) {
                $filters = array();
            }
            $this->setActiveFilters($filters);
        }
        return $filters;
    }

    /**
     * Get current filter state array
     *
     * @return array
     */
    public function getFilterState() {
        if (is_null($filterState = $this->getData('filter_state'))) {
            $filterState = array();
            foreach ($this->getActiveFilters() as $item) {
                $filterState[$item->getFilter()->getRequestVar()] = $item->getFilter()->getCleanValue();
            }
            $this->setFilterState($filterState);
        }
        return $filterState;
    }

    /**
     * Retrieve Clear Filters URL
     *
     * @return string
     */
    public function getClearUrl()
    {
        $filterState = $this->getFilterState();
        $params = array(
            '_current' => true,
            '_use_rewrite' => true,
            '_query' => $filterState,
            '_escape' => true
        );
        return Mage::getUrl('*/*/*', $params);
    }
}
