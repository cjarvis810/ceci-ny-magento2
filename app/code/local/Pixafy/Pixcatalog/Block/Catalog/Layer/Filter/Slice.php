<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 8/13/2015
 * Time: 4:07 PM
 */

class Pixafy_Pixcatalog_Block_Catalog_Layer_Filter_Slice extends
    Mage_Catalog_Block_Layer_Filter_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->_filterModelName = 'pixcatalog/catalog_layer_filter_slice';
    }
}