<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Html page block
 *
 * @category   Mage
 * @package    Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 *
 * @todo        separate order, mode and pager
 */
class Pixafy_Pixcatalog_Block_Catalog_Product_List_Toolbar_Pager extends Mage_Page_Block_Html_Pager
{
    /**
     * Return current page
     *
     * @return int
     */
    public function getCurrentPage()
    {
        if (is_null($this->getData('current_page'))) {
            $this->setData('current_page', parent::getCurrentPage());
        }
        return $this->getData('current_page');
    }

    /**
     * Set collection for pagination
     *
     * @param  Varien_Data_Collection $collection
     * @return Mage_Page_Block_Html_Pager
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_setFrameInitialized(false);

        return $this;
    }

    public function getFirstNum()
    {
        if (is_null($this->getData('first_num'))) {
            $this->setData('first_num', parent::getFirstNum());
        }
        return $this->getData('first_num');
    }

    public function getLastNum()
    {
        if (is_null($this->getData('last_num'))) {
            $this->setData('last_num', parent::getLastNum());
        }
        return $this->getData('last_num');
    }

    public function getTotalNum()
    {
        if (is_null($this->getData('total_num'))) {
            $this->setData('total_num', parent::getTotalNum());
        }
        return $this->getData('total_num');
    }

    public function isFirstPage()
    {
        return $this->getIsFirstPage();
    }

    public function getIsFirstPage()
    {
        if (is_null($this->getData('is_first_page'))) {
            $this->setData('is_first_page', parent::isFirstPage());
        }
        return $this->getData('is_first_page');
    }

    public function getLastPageNum()
    {
        if (is_null($this->getData('last_page_num'))) {
            $this->setData('last_page_num', parent::getLastPageNum());
        }
        return $this->getData('last_page_num');
    }

    public function isLastPage()
    {
        return $this->getIsLastPage();
    }

    public function getIsLastPage() {
        if (is_null($this->getData('is_last_page'))) {
            $this->setData('is_last_page', parent::isLastPage());
        }
        return $this->getData('is_last_page');
    }

    public function getPages()
    {
        $pages = array();
        if ($this->getLastPageNum() <= $this->_displayPages) {
            $pages = range(1, $this->getLastPageNum());
        }
        else {
            $half = ceil($this->_displayPages / 2);
            if ($this->getCurrentPage() >= $half
                && $this->getCurrentPage() <= $this->getLastPageNum() - $half
            ) {
                $start  = ($this->getCurrentPage() - $half) + 1;
                $finish = ($start + $this->_displayPages) - 1;
            }
            elseif ($this->getCurrentPage() < $half) {
                $start  = 1;
                $finish = $this->_displayPages;
            }
            elseif ($this->getCurrentPage() > ($this->getLastPageNum() - $half)) {
                $finish = $this->getLastPageNum();
                $start  = $finish - $this->_displayPages + 1;
            }

            $pages = range($start, $finish);
        }
        return $pages;
    }

    public function getFirstPageUrl()
    {
        return $this->getPageUrl(1);
    }

    public function getPreviousPageUrl()
    {
        return $this->getPageUrl($this->getCurrentPage() - 1);
    }

    public function getNextPageUrl()
    {
        return $this->getPageUrl($this->getCurrentPage() + 1);
    }

    public function getLastPageUrl()
    {
        return $this->getPageUrl($this->getLastPageNum());
    }

    /**
     * Initialize frame data, such as frame start, frame start etc.
     *
     * @return Mage_Page_Block_Html_Pager
     */
    protected function _initFrame()
    {
        if (!$this->isFrameInitialized()) {
            $start = 0;
            $end = 0;

            if ($this->getLastPageNum() <= $this->getFrameLength()) {
                $start = 1;
                $end = $this->getLastPageNum();
            }
            else {
                $half = ceil($this->getFrameLength() / 2);
                if ($this->getCurrentPage() >= $half
                    && $this->getCurrentPage() <= $this->getLastPageNum() - $half
                ) {
                    $start  = ($this->getCurrentPage() - $half) + 1;
                    $end = ($start + $this->getFrameLength()) - 1;
                }
                elseif ($this->getCurrentPage() < $half) {
                    $start  = 1;
                    $end = $this->getFrameLength();
                }
                elseif ($this->getCurrentPage() > ($this->getLastPageNum() - $half)) {
                    $end = $this->getLastPageNum();
                    $start  = $end - $this->getFrameLength() + 1;
                }
            }
            $this->_frameStart = $start;
            $this->_frameEnd = $end;

            $this->_setFrameInitialized(true);
        }

        return $this;
    }
}
