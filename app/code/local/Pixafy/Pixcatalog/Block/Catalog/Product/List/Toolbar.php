<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Product list toolbar
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Pixafy_Pixcatalog_Block_Catalog_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{

    // custom sort function for ordering static blocks
    public static function cmpBlocks($a, $b) {
        if (is_numeric($a) && !is_numeric($b)) {
            return -1;
        }
        if (!is_numeric($a) && is_numeric($b)) {
            return 1;
        }
        if (is_numeric($a) && is_numeric($b)) {
            return $a - $b;
        }
        return floatval(substr($a, 0, -1) - floatval(substr($b, 0, -1)));
    }

    /**
     * Set collection to pager
     *
     * @param Varien_Data_Collection $collection
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setCollection($collection)
    {
        if (Mage::helper('pixcatalog')->getUseInlinePromoBlocks() && $category = Mage::registry('current_category')) {
            $blocks = $category->getPromoBlocks();
            uasort($blocks, 'Pixafy_Pixcatalog_Block_Product_List_Toolbar::cmpBlocks');

            if (!empty($blocks)) {
                $p = $this->getCurrentPage();
                $sz = (int)$this->getLimit();
                $totalProducts = $collection->getSize();
                if (!$sz) {
                    $sz = $totalProducts;
                }
                
                $totalItems = $totalProducts + count($blocks);
                $sortedBlocks = array(
                    'absolute' => array(),
                    'percentage' => array()
                );
                foreach ($blocks as $blockId => $position) {
                    $posType = 'absolute';
                    if (substr($position, -1) == '%') {
                        $posType = 'percentage';
                        $position = floatval(substr($position, 0, -1)) / 100;
                    } else {
                        $position = intval($position);
                    }
                    $sortedBlocks[$posType][] = array(
                        'block_id' => $blockId,
                        'position' => $position
                    );
                }
                $numAbsoluteBlocks = count($sortedBlocks['absolute']);
                $numPercentageBlocks = count($sortedBlocks['percentage']);
                $items = array();
                $start = ($p - 1) * $sz;
                $end = min($start + $sz, $totalItems);
                $firstProduct = null;
                for ($i = 1, $j = 0, $k = 0, $x = 0; $i <= $end; ++$i) {
                    if ($j < $numAbsoluteBlocks && ($sortedBlocks['absolute'][$j]['position'] <= $i || $x >= $totalProducts)) {
                        $items[] = array(
                            'type' => 'absolute',
                            'index' => $j++
                        );
                    } else if ($k < $numPercentageBlocks && $sortedBlocks['percentage'][$k]['position'] <= $x / $totalProducts) {
                        $items[] = array(
                            'type' => 'percentage',
                            'index' => $k++
                        );
                    } else {
                        $items[] = array(
                            'type' => 'product'
                        );
                        if ($i >= $start && is_null($firstProduct)) {
                            $firstProduct = $x;
                        }
                        ++$x;
                    }
                }
                $products = array();
                if ($this->getCurrentOrder()) {
                    $collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
                }
                $collection->getSelect()->limit($x - $firstProduct, $firstProduct);
                foreach ($collection as $product) {
                    $products[] = $product;
                }
                $newCollection = new Varien_Data_Collection();
                $j = 0;
                for ($i = $start; $i < $end; ++$i) {
                    $item = &$items[$i];
                    if ($item['type'] == 'product') {
                        $newCollection->addItem($products[$j++]);
                    } else {
                        $dummyProduct = Mage::getModel('catalog/product')->setId(-$i)->setBlockId($sortedBlocks[$item['type']][$item['index']]['block_id']);
                        $newCollection->addItem($dummyProduct);
                    }
                }
                $this->_collection = $newCollection;
                $this->setFirstNum($start + 1)
                    ->setLastNum($end)
                    ->setTotalNum($totalItems)
                    ->setIsFirstPage($start == 0)
                    ->setLastPageNum(ceil($totalItems / $sz))
                    ->setIsLastPage($end >= $totalItems);
                return $this;
            }
        }
        parent::setCollection($collection);
        $this->_collection->load();
        $this->setFirstNum($collection->getPageSize()*($collection->getCurPage()-1)+1)
            ->setLastNum($collection->getPageSize()*($collection->getCurPage()-1)+$collection->count())
            ->setTotalNum($collection->getSize())
            ->setIsFirstPage($collection->getCurPage() == 1)
            ->setLastPageNum($collection->getLastPageNumber())
            ->setIsLastPage($collection->getCurPage() == $collection->getLastPageNumber());
        return $this;
    }

    public function getFirstNum()
    {
        if (is_null($this->getData('first_num'))) {
            $this->setData('first_num', parent::getFirstNum());
        }
        return $this->getData('first_num');
    }

    public function getLastNum()
    {
        if (is_null($this->getData('last_num'))) {
            $this->setData('last_num', parent::getLastNum());
        }
        return $this->getData('last_num');
    }

    public function getTotalNum()
    {
        if (is_null($this->getData('total_num'))) {
            $this->setData('total_num', parent::getTotalNum());
        }
        return $this->getData('total_num');
    }

    public function isFirstPage()
    {
        return $this->getIsFirstPage();
    }

    public function getIsFirstPage() {
        if (is_null($this->getData('is_first_page'))) {
            $this->setData('is_first_page', parent::isFirstPage());
        }
        return $this->getData('is_first_page');
    }

    public function getLastPageNum()
    {
        if (is_null($this->getData('last_page_num'))) {
            $this->setData('last_page_num', parent::getLastPageNum());
        }
        return $this->getData('last_page_num');
    }

    public function isLastPage() {
        return $this->getIsLastPage();
    }

    public function getIsLastPage() {
        return $this->getCurrentPage() == $this->getLastPageNum();
    }

    /**
     * Render pagination HTML
     *
     * @return string
     */
    public function getPagerHtml()
    {
        $pagerBlock = $this->getChild('product_list_toolbar_pager');

        if ($pagerBlock instanceof Varien_Object) {

            /* @var $pagerBlock Mage_Page_Block_Html_Pager */
            $pagerBlock->setAvailableLimit($this->getAvailableLimit());

            $pagerBlock->setUseContainer(false)
                ->setShowPerPage(false)
                ->setShowAmounts(false)
                ->setLimitVarName($this->getLimitVarName())
                ->setPageVarName($this->getPageVarName())
                ->setLimit($this->getLimit())
                ->setFrameLength(Mage::getStoreConfig('design/pagination/pagination_frame'))
                ->setJump(Mage::getStoreConfig('design/pagination/pagination_frame_skip'))
                ->setCollection($this->getCollection())
                ->setCurrentPage($this->getCurrentPage())
                ->setFirstNum($this->getFirstNum())
                ->setLastNum($this->getLastNum())
                ->setTotalNum($this->getTotalNum())
                ->setIsFirstPage($this->getIsFirstPage())
                ->setLastPageNum($this->getLastPageNum())
                ->setIsLastPage($this->getIsLastPage());

            return $pagerBlock->toHtml();
        }

        return '';
    }

}