<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 2/10/2015
 * Time: 5:55 PM
 */

class Pixafy_Pixcatalog_Helper_Data extends Mage_Core_Helper_Abstract {
    const XML_PATH_USE_INLINE_PROMO_BLOCKS = 'catalog/frontend/use_inline_promo_blocks';

    public function getUseInlinePromoBlocks() {
        return true;
        //return Mage::getStoreConfig(self::XML_PATH_USE_INLINE_PROMO_BLOCKS);
    }

    public function sanitizePromoBlockInput($blocks) {
        foreach($blocks as $block_id => $position) {
            $blocks[$block_id] = $this->cleanPosition($position);
        }
        return $blocks;
    }


    public function cleanPosition($position) {
        $position = strtoupper($position);
        $suffix = '';
        $isPercent = false;
        $endPos = null;
        if (substr($position, -1) == '%') {
            $isPercent = true;
            $endPos = -1;
        } else if (substr($position, -2) == '%R') {
            $isPercent = true;
            $endPos = -2;
        } else if (substr($position, -1) == 'R') {
            $endPos = -1;
        } else {
            $position = intval($position);
        }
        if (!is_null($endPos)) {
            $suffix = substr($position, $endPos);
            $position = substr($position, 0, $endPos);
        }
        if ($isPercent) {
            $position = min(max(floatval($position), 1), 100);
        } else {
            $position = intval($position);
            if ($suffix == 'R' && $position == 1) {
                ++$position;
            }
        }
        return $position . $suffix;
    }


    public function sanitizeColorwayInput($colorways) {
        foreach($colorways as $colorwayId => $position) {
            $colorways[$colorwayId] = $position? $position : 9999;
        }
        return $colorways;
    }

    public function getTopColorways($colorwayIds) {
        asort($colorwayIds);
        $topColorwayIds = array_slice($colorwayIds, 0, 4, true);
        $colorways = array();
        foreach ($topColorwayIds as $colorwayId => $position) {
            $colorways[] = Mage::getModel('pixcatalog/colorway')->load($colorwayId);
        }
        return $colorways;
    }
    public function getProductCollection($product){
        if(!$product->getId()){
            return null;
        }
        if($product->getData('collection')){
            return $product->getData('collection');
        }else{
            $category=$product->getCategoryIds();
            if(isset($category) && count($category)>0){
                foreach($category as $catid) {
                    if($catid==3 || $catid==2){
                        continue;
                    }
                    $grandfathercat = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($catid);
                    if($grandfathercat->getId() && $grandfathercat->getParentCategory()->getId()==3) {
                        $product->setData('collection', $grandfathercat->getName());
                        return $grandfathercat->getName();
                    }
                }
            }
        }
        return null;
    }
    public function getProductOccasion($product){
        if(!$product->getId()){
          return null;
        }
        if($product->getData('product_occasion')){
            return $product->getData('product_occasion');
        }else{
            foreach($product->getCategoryIds() as $catid){
                if (!is_null($catid) && $catid != 3 && $catid != 2) {
                    $category = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($catid);
                    if($category->getParentCategory()->getId() && $category->getParentCategory()->getId()!=3 && $category->getParentCategory()->getId()!=2) {
                        if ($category) {
                            $product->setData('product_occasion', $category->getName());
                            return $category->getName();
                        }
                    }
                }
            }
        }
        return null;
    }
    public function getProductStyle($product){
        if($product->getId()){
            if($product->getAttributeText('style') && is_array($product->getAttributeText('style'))){
                return $product->getAttributeText('style')[0];
            }else if($product->getAttributeText('style')) {
                return $product->getAttributeText('style');
            }
        }
        return null;
    }
    public function sortQuoteBlocks($array, $columnsize)
    {
        $productsarray = array();
        $quoteblocks = array();
        $imageblocks=array();
        $connection = Mage::getModel('core/resource')->getConnection('core_read');
        foreach ($array as $product) {
            if ($product->getBlockId()) {
                $sql='SELECT `position` FROM `pixcatalog_category_block` WHERE `block_id`="'.$product->getBlockId().'" AND `category_id`="'.Mage::registry('current_category')->getId().'" LIMIT 1';
                $position = $connection->fetchAll($sql);
                if(!empty($position) && $this->cleanPosition($position[0]['position'])>1){
                    return $array;
                }
                $block = Mage::getModel('cms/block')->load($product->getBlockId());
                if (strpos($block->getData('identifier'), 'category_texttual_inline_block') !== false) {
                    array_push($quoteblocks, array($product));
                } else {
                    array_push($imageblocks, $product);
                }
            } else if (!$product->getBlockId()) {
                array_push($productsarray, $product);
            }
        }
        for($i=0; $i<count($quoteblocks); $i++){
            if(count($imageblocks)>0) {
                array_unshift($quoteblocks[$i], array_pop($imageblocks));
            }
            if(count($imageblocks)>0) {
                array_push($quoteblocks[$i], array_pop($imageblocks));
            }
        }
        $lastpostion = 0;
        if(Mage::app()->getRequest()->isAjax() && Mage::app()->getRequest()->getParam('p')) {
            $lastpostion = Mage::getSingleton('core/session')->getAjaxOffset()-2;
        }else{
            Mage::getSingleton('core/session')->unsAjaxOffset();
        }
        for($i=0; $i<count($quoteblocks); $i++){
            if($i==0 && $lastpostion==0) {
               $lastpostion=(((int)($columnsize / 2)) + (($columnsize) * ($i + 1)));
                if(count($quoteblocks[$i])>1) {
                    $lastpostion=$lastpostion-1;
                }
               array_splice($productsarray, $lastpostion, 0, $quoteblocks[$i]);
           }else{
               if(($lastpostion+($columnsize-1))>=count($productsarray)) {
                   break;
               }else{
                   $lastpostion=($lastpostion+($columnsize));
                   array_splice($productsarray, $lastpostion, 0, $quoteblocks[$i]);
               }
           }
        }
        if((count($productsarray)%5)>0){
            Mage::getSingleton('core/session')->setAjaxOffset((count($productsarray)%5));
        }
        return $productsarray;
    }

    public function getCategoryClass()
    {
        $product = Mage::registry('current_product');
        $categoryIds = $product->getCategoryIds();

        if(count($categoryIds) ){
            $firstCategoryId = end($categoryIds);
            $_category = Mage::getModel('catalog/category')->load($firstCategoryId);
            $_parentCategory = Mage::getModel('catalog/category')->load($_category->getData('parent_id'));
            
            return "category-".strtolower($_parentCategory->getUrlKey());
        }

    }

    /*
     * @Description     Retrieves the prices if any that are set for this particular option id
     */
    public function enhancedOptionPrice($optionId)
    {
        $model = Mage::getModel("pixcatalog/enhancement_price")
            ->getCollection()
            ->addFieldToFilter('option_id', $optionId)
            ->getFirstItem();


        return ($model) ? $model->getData('price') :  null;
    }

    public function resizeImageThumbnail($imageUrl=null, $saveDirectory=null)
    {

        if($imageUrl && is_dir($saveDirectory)) {

            print $imageUrl;
            // resize image only if the image file exists and the resized image file doesn't exist
            // the image is resized proportionally with the width/height 135px
            $imageObj = new Varien_Image($imageUrl);
            $imageObj->constrainOnly(false);
            $imageObj->keepAspectRatio(TRUE);
            $imageObj->keepFrame(FALSE);
            $imageObj->resize(135, 135);

            $urlParts = explode(DS, $imageUrl);

            return $imageObj->save($saveDirectory. end($urlParts));


        } else {

            return false;

        }

    }

    /*
     * @Description     Get all ids of products matching the primary color of this colorway
     */
    public function getProductIdsMatchingColorway($sliceId)
    {
        // Get primary color
        $colorwaysIds = array_flip(Mage::getModel('pixcatalog/colorwheel_slice')->load($sliceId)->getColorways());

        $colorsArray = array();
        $count = 0;
        foreach($colorwaysIds as $colorwayId) {

            $colorway = Mage::getModel('pixcatalog/colorway')->load($colorwayId);
            //print_r($colorway->getColors());
            foreach($colorway->getColors() as $color) {
                $colorsArray[$count++] = $color;
            }

        }

        $colorsArray = array_unique($colorsArray);

        /**
         * Get the resource model
         */
        $resource = Mage::getSingleton('core/resource');

        /**
         * Retrieve the read connection
         */
        $readConnection = $resource->getConnection('core_read');

        $query = 'SELECT * FROM pixcatalog_product_colorway_grid';

        /**
         * Execute the query and store the results in $results
         */
        $results = $readConnection->fetchAll($query);

        /**
         * Print out the results
         */
        //var_dump($results);

        $productIds = array();
        foreach($results as $result) {
            $colorway = Mage::getModel('pixcatalog/colorway')->load($result['colorway_id']);

            if(array_intersect($colorway->getColors(), $colorsArray)) {
                $productIds[] = $result['product_id'];
            }
        }

        return array_unique($productIds);

    }

}