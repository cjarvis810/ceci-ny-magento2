<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 4/30/2015
 * Time: 2:23 PM
 */

class Pixafy_Pixcatalog_Helper_Orders extends Mage_Core_Helper_Abstract
{
    protected $customerId;

    public function __construct()
    {
        $this->customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
    }

    public function getCcType($ccType=null)
    {
        if(!is_null($ccType)) {
            switch(strtolower($ccType)) {
                case 'vi':
                    $cardType = "Visa";
                    break;

                case 'mc':
                    $cardType = "Mastercard";
                    break;

                case 'ae':
                    $cardType = "American Express";
                    break;

                case 'di':
                    $cardType = "Discovery";
                    break;

            }

            return $cardType;

        } else {
            //Mage::exception('')
            return false;
        }
    }


    public function isPaymentMethodCC($paymentMethod)
    {
        if(stripos($paymentMethod, "credit") !== false) {
            return true;
        } else {
            return false;
        }
    }



    public function getOrderProgression()
    {
        return array(
            'pending' => 2,
            'pending_payment' => 2,
            'payment_review' => 2,
            'fraud' => 2,
            'pending_paypal' => 2,
            'processing' => 3,
            'holded' => 3,
            'complete' => 4,
            'closed' => 5
        );
    }

}