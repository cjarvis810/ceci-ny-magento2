<?php
/**
 * @category    Pixafy
 * @package     Pixafy_Pixcatalog
 * @copyright   Copyright (c) 2015 Pixafy Inc. (http://www.pixafy.com)
 */

/**
 * Catalog image helper
 *
 * @author      John Cymerman <jcymerman@pixafy.com>
 */
class Pixafy_Pixcatalog_Helper_Catalog_Image extends Mage_Catalog_Helper_Image
{   
    //whether this is an svg or not    
    public $svg = false;
    
    /**
     * Initialize Helper to work with Image
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $attributeName
     * @param mixed $imageFile
     * @return Pixafy_Pixcatalog_Helper_Image
     */
    public function init(Mage_Catalog_Model_Product $product, $attributeName, $imageFile=null)
    {
        parent::init($product, $attributeName, $imageFile);
        $model = $this->_getModel();
        $baseFile = isset($imageFile)? $imageFile : $model->getBaseFile();
        $ext = pathinfo($baseFile, PATHINFO_EXTENSION);
        if ($ext == 'svg') {
            if (strpos($baseFile, Mage::getBaseDir('media')) === false) {
                $baseFile = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . str_replace('/', DS, $baseFile);
            }
            $imageProcessor = Mage::getModel('pixcatalog/image_adapter_svg');
            $imageProcessor->open($baseFile);
            $model->setImageProcessor($imageProcessor);
            $this->svg = true;
        }
        return $this;
    }

    /**
     * Check - is this file an image
     *
     * @param string $filePath
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function validateUploadFile($filePath) {
        return true;
    }

}