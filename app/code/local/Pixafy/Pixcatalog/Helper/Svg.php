<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 3/23/2015
 * Time: 9:16 AM
 */

class Pixafy_Pixcatalog_Helper_Svg extends Mage_Core_Helper_Abstract {

    protected $sku;
    protected $newMainG;
    protected $xmlObjectArray = array(); // @TODO remove


    const NS_XML = 'http://www.w3.org/2000/svg';
    const NS_XLINK = 'http://www.w3.org/1999/xlink';

    public function getFtpDir($childDir = null) {
        $dir = Mage::getBaseDir('var') . DS . 'ftp' . DS;
        if (is_string($childDir)) {
            $dir .= $childDir . DS;
        }
        return $dir;
    }

    public function getFtpFontsDir() {
        return $this->getFtpDir('fonts');
    }

    public function getFtpProductsDir($sku = null) {
        $dir = $this->getFtpDir('products');
        if (isset($sku)) {
            $dir .= $sku . DS;
        }

        return $dir;
    }

    public function getStagingDir($childDir = null) {
        return $this->getPublicDir($childDir? ("staging" .DS. $childDir) : 'staging');
    }

    public function getStagingUrl($childDir = null, $addBase = true) {
        return $this->getPublicUrl($childDir? "staging/$childDir" : 'staging', $addBase);
    }

    public function getStagingFontsDir() {
        return $this->getStagingDir('fonts');
    }

    public function getPublicDir($childDir = null) {
        $dir = Mage::getBaseDir('media') . DS;
        if (is_string($childDir)) {
            $dir .= $childDir . DS;
        }
        return $dir;
    }

    public function getStagingProductsDir() {
        return Mage::getBaseDir('media') . DS . 'staging' . DS . 'products' . DS;
    }


    public function getPublicUrl($childDir = null, $addBase = true) {
        $dir = $addBase? Mage::getBaseUrl('media') : '';
        if (is_string($childDir)) {
            $dir .= "$childDir/";
        }
        return $dir;
    }

    public function getPublicFontsUrl($addBase = true) {
        return $this->getPublicUrl('fonts');
    }

    public function getPublicFontsDir() {
        return $this->getPublicDir('fonts');
    }

    public function getPublicProductsDir($sku = null) {
        $childDir = 'catalog' . DS . 'product' . DS . 'public';
        if (isset($sku)) {
            $childDir .= DS . $sku;
        }
        return $this->getPublicDir($childDir);
    }

    public function getPublicProductsUrl($sku = null, $addBase = true) {
        $childDir = $addBase? 'catalog/product/public' : 'public';
        if (isset($sku)) {
            $childDir .= "/$sku";
        }
        return $this->getPublicUrl($childDir, $addBase);
    }

    public function getPrivateProductsDir($sku = null) {
        $childDir = 'catalog' . DS . 'product' . DS . 'private';
        if (isset($sku)) {
            $childDir .= DS . $sku;
        }
        return $this->getPublicDir($childDir);
    }

    public function getPrivateProductsUrl($sku = null, $addBase = true) {
        $childDir = $addBase? 'catalog/product/private' : 'private';
        if (isset($sku)) {
            $childDir .= "/$sku";
        }
        return $this->getPublicUrl($childDir, $addBase);
    }

    public function getFontsCssPath() {
        return Mage::getBaseDir('media') . DS . 'css' . DS . 'fonts.css';
    }

    public function getFontsAllowedExtensions() {
        return array('ttf', 'otf');
    }

    public function getLayersAllowedExtensions() {
        return array('png');
    }

    public function getDesignsAllowedExtensions() {
        return array('svg');
    }

    public function getOptionalImageTypes() {
        return array('Motif', 'Hebrew_Salutation', 'Stamp_1', 'Stamp_2', 'Stamp_3', 'Monogram', 'Photo');
    }

    public function getLocales() {
        return array('en', 'es', 'ar');
    }

    public function createWatermarkLayer($width, $height, $destination) {
        $mainLayer = Iw_Workshop::initVirginLayer($width, $height, 'ffffff');
        /* NO WATERMARK FOR NOW
        $watermarkLayer = Iw_Workshop::initFromPath(Mage::getBaseDir() . DS . 'skin' . DS . 'frontend' . DS . 'enterprise' . DS . 'ceciny' . DS . 'images' . DS . 'icon-ceci.png');
        $k = 0;
        for ($x = 0; $x < $width; $x += $watermarkLayer->getWidth()) {
            for ($y = $k * $watermarkLayer->getHeight(); $y < $height; $y += $watermarkLayer->getHeight() << 1) {
                $mainLayer->addLayerOnTop($watermarkLayer, $x, $y);
            }
            $k = ($k + 1) % 2;
        }
        $watermarkLayer->delete();
        */
        $image = $mainLayer->getResult();
        imagepng($image, $destination);
        $mainLayer->delete();
        return true;
    }

    public function saveFontCss() {
        $css = "";
        $publicFontsPath = $this->getPublicFontsDir();
        $publicFontsDir = opendir($publicFontsPath);
        while ($fontPath = readdir($publicFontsDir)) {
            $pathinfo = pathinfo($fontPath);
            if (in_array($pathinfo['extension'], $this->getFontsAllowedExtensions())) {
                $source = $publicFontsPath . DS . $fontPath;
                $url = $this->getPublicFontsUrl() . $fontPath;
                $css .= <<<CSS
@font-face {
    font-family: '$pathinfo[filename]';
    src: url('$url');
}

CSS;
            }
        }
        closedir($publicFontsDir);
        $file = fopen($this->getFontsCssPath(), 'w');
        fwrite($file, $css);
        fclose($file);
    }

    public function importSvg($sku, $source, $destination) {
        if (!file_exists($source)) {
            throw new Mage_Core_Exception("File $source does not exist.");
        }
        $this->sku = $sku;
        $old = simplexml_load_file($source);
        $cssUrl = Mage::getBaseUrl('media') . 'css/fonts.css';
        $xmlnsUrl = self::NS_XML;
        $xlinkUrl = self::NS_XLINK;
        $header = <<<XML
<?xml version="1.0" ?>
<?xml-stylesheet type="text/css" href="$cssUrl"?>
 <svg xmlns="$xmlnsUrl" xmlns:xlink="$xlinkUrl" />
XML;
        // Header //
        $new = new SimpleXMLElement($header);

        foreach ($old->attributes() as $key => $value) {
            $new->addAttribute($key, $value);
        }
        foreach ($old->attributes(self::NS_XML) as $key => $value) {
            $new->addAttribute($key, $value, self::NS_XML);
        }
        foreach ($old->attributes(self::NS_XLINK) as $key => $value) {
            $new->addAttribute($key, $value, self::NS_XLINK);
        }
        $new->addAttribute('id', 'svg-' . $sku);
        $viewBox = explode(' ', $old['viewBox']);

        // <svg height width> //
        $new->addAttribute('width', $viewBox[2] . 'px');
        $new->addAttribute('x', '0px');

        $new->addAttribute('height', $viewBox[3] . 'px');
        $new->addAttribute('y', '0px');

        // <g class="mainContainer> //
        $this->newMainG = $new->addChild('g');
        $this->newMainG->addAttribute('class', 'mainContainer');

        /// Bleedline clip path //
        $bleedlines = $old->xpath("//*[@id='bleedline']");

        if(!empty($bleedlines)) {

            // <defs> //
            $defs = $this->newMainG->addChild('defs');

            // <shape> //
            $defShape = $defs->addChild($bleedlines[0]->getName());

            foreach($bleedlines[0]->attributes() as $key => $value) {
                if($key != 'id') {
                    $defShape->addAttribute($key, $value);
                } else {
                    $defShape->addAttribute('id', 'bleedline_'.$sku);
                }
            }

            // <clipPath> //
            $clipPath = $this->newMainG->addChild('clipPath');
            $clipPathId = 'bleedline_1_'.$sku; // Change to bleedline
            $clipPath->addAttribute('id', $clipPathId);
            $clipPath->addAttribute('style', 'display:none');

            // <use> //
            $use = $clipPath->addChild('use');
            $use->addAttribute('overflow', 'visible');
            $use->addAttribute('xlink:href', '#bleedline_'.$sku, self::NS_XLINK);

        }

        $pathinfo = pathinfo($destination);

        // Watermark //
        // @TODO
        //$this->createWatermarkLayer($viewBox[2], $viewBox[3], $pathinfo['dirname'] . '/layer0.png');

        // Image //

        /*
        $newImage = $this->newMainG->addChild('image');
        $newImage->addAttribute('width', $viewBox[2]);
        $newImage->addAttribute('height', $viewBox[3]);
        $newImage->addAttribute("xlink:href", $this->getStagingUrl("products/$sku") . 'layer0.png', self::NS_XLINK);
        */


        // Recursively follow down the rabbit hole, and set all nested paths within this mainContainer group
        $this->getRabbitPath($old);


        $numTextGroups = $numFonts = 0;
        $fonts = array();
        $textTags = array();
        $imageCount = 0;

        // Master group //
        foreach ($old->g as $oldG1) {

            foreach($oldG1->path as $innerPath) {
                $path = $this->newMainG->addChild('path');
                foreach ($innerPath->attributes() as $key => $value) {
                    if($key == "clip-path") {
                        $value = "url(#$clipPathId)";
                    }
                    $path->addAttribute($key, $value);
                }

            }

            // <filter> //
            $colorId = intval(preg_replace('/[^0-9]/', '', $oldG1['id']));
            $filter = $this->newMainG->addChild('filter');
            $filter->addAttribute('id', 'svg-filter-' . $sku . '-' . $colorId);
            $feColorMatrix = $filter->addChild('feColorMatrix');
            $feColorMatrix->addAttribute('class', 'color-' . $colorId);
            $feColorMatrix->addAttribute('type', 'matrix');
            $feColorMatrix->addAttribute('values', '1 0 0 0 0   0 1 0 0 0   0 0 1 0 0   0 0 0 1 0');

            // Loop through images //
            foreach ($oldG1->image as $oldImage) {
                $newImage = $this->newMainG->addChild('image');

                // Extract image and decode it //
                $data = $oldImage->attributes(self::NS_XLINK);
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                // Save to staging product location //
                file_put_contents($this->getStagingDir("products".DS."$sku").'layout'.$imageCount.'.png', $data);

                // Image attributes //
                foreach ($oldImage->attributes() as $key => $value) {
                    if($key == 'clip-path') {
                        $newImage->addAttribute('clip-path', "url(#$clipPathId)");
                    } elseif($key != "href") {
                        $newImage->addAttribute($key, $value);
                    }
                }

                if(!$newImage->attributes()->{'clip-path'}) {
                    $newImage->addAttribute('clip-path', "url(#$clipPathId)");
                }

                $newImage->addAttribute('filter', 'url(#svg-filter-' . $sku . '-' . $colorId.')');
                $newImage->addAttribute('xlink:href', $this->getStagingUrl("products/$sku").'layout'.$imageCount.'.png', self::NS_XLINK);
                $imageCount++;
            }

            if (isset($oldG1->text)) {
                ++$numTextGroups;
                $textAnchor = null;
                $rectX = null;
                $textLength = null;

                $textCount=1;
                foreach($this->sortTextFields($oldG1->text) as $oldText) {
                    $textId = intval(preg_replace('/[^0-9]/', '', $oldText['id']));
                    $newClasses = array('group-' . $numTextGroups, 'text-' . $textCount++);

                    // Adding foil attribute to text tag
                    if(stripos($oldText['id'], "foil") !== false) {
                        $newClasses[] = 'foil';
                    }

                    // <text><path> //
                    if (isset($oldText->textPath)) {
                        $newText = $this->newMainG->addChild('text');

                        // For colorways //
                        $newText->addAttribute('filter', 'url(#svg-filter-' . $sku . '-' . $colorId . ')');

                        $newTextPath = $newText->addChild('textPath');
                        foreach ($oldText->textPath->attributes(self::NS_XLINK) as $key => $value) {
                            $newTextPath->addAttribute('xlink:' . $key, $value, self::NS_XLINK);
                        }
                        $newTSpan = $newTextPath->addChild('tspan', htmlspecialchars((string)$oldText->textPath->tspan));
                        foreach ($oldText->textPath->tspan->attributes() as $key => $value) {
                            if ($key == 'font-family') {
                                $value = substr($value, 1, -1);
                                $fontIndex = array_search($value, $fonts);
                                if ($fontIndex === false) {
                                    $fonts[++$numFonts] = $value;
                                    $fontIndex = $numFonts;
                                }
                                $newClasses[] = 'font-' . $fontIndex;
                            }
                            $newTSpan->addAttribute($key, $value);
                        }
                        $newTSpan->addAttribute('class', implode(' ', $newClasses));

                    } else {
                        $newG = $this->newMainG->addChild('g');
                        $newG->addAttribute('class', 'group-' . $numTextGroups);

                        // Adding foil attribute to text tag && create the background image //
                        if(stripos($oldText['id'], "foil") !== false) {

                            // <defs>
                            $patternDefs = $newG->addChild("defs");

                            // <pattern>
                            $pattern = $patternDefs->addChild("pattern");
                            $pattern->addAttribute('id', 'p1-'.$sku);
                            $pattern->addAttribute('width', $oldG1->rect->attributes()->width);
                            $pattern->addAttribute('height', $oldG1->rect->attributes()->height);

                            $foilImage = $pattern->addChild("image");

                            // Define dimensions //
                            //$transformedWidth = $oldG1->rect->attributes()->width / 2;
                            //$foilImage->addAttribute('x', '-'.$transformedWidth);
                            //$foilImage->addAttribute('y', '-'.$oldG1->rect->attributes()->height);
                            $foilImage->addAttribute('width', $oldG1->rect->attributes()->width);
                            $foilImage->addAttribute('height', $oldG1->rect->attributes()->height);

                            // Strip script name since this will be crontab'ed
                            if(stripos(Mage::getBaseUrl(), "comb_ftp.php/") !== false) {
                                $baseUrl = str_replace("comb_ftp.php/", "", Mage::getBaseUrl());
                            }

                            if(stripos(Mage::getBaseUrl(), "comb_ftp_staging.php/") !== false) {
                                $baseUrl = str_replace("comb_ftp_staging.php/", "", Mage::getBaseUrl());
                            }

                            // Get the foil image //
                            $foilImageUrl = $baseUrl.'media/images/foil.png';
                            $foilImage->addAttribute('xlink:href', $foilImageUrl, self::NS_XLINK);

                            $oldText->addAttribute('fill', "url(#p1-$sku)");

                        }

                        if (isset($oldText->tspan)) {
                            $newText = $newG->addChild('text');
                            $newText->addChild('tspan', htmlspecialchars((string)$oldText->tspan));
                        } else {
                            $newText = $newG->addChild('text', htmlspecialchars((string)$oldText));
                        }

                        foreach ($oldText->attributes() as $key => $value) {
                            if (in_array($key, array('id', 'transform'))) {
                                continue;
                            } else if ($key == 'font-family') {
                                $value = substr($value, 1, -1);
                                $fontIndex = array_search($value, $fonts);
                                if ($fontIndex === false) {
                                    $fonts[++$numFonts] = $value;
                                    $fontIndex = $numFonts;
                                }
                                $newClasses[] = 'font-' . $fontIndex;
                            }
                            $newText->addAttribute($key, $value);
                        }

                        if (!empty($newClasses)) {
                            $newText->addAttribute('class', implode(' ', $newClasses));
                        }

                        $newText->addAttribute('filter', 'url(#svg-filter-' . $sku . '-' . $colorId . ')');
                        if ($textLength !== null) {
                            $newText->addAttribute('lengthAdjust', 'spacing');
                            $newText->addAttribute('textLength', $textLength);
                        }

                        $transform = isset($oldText['transform'])? $oldText['transform'] : null;

                        if ($textAnchor && $transform && preg_match('/^(matrix\\(\\S+\\s+\\S+\\s+\\S+\\s+\\S+)\\s+(\\S+)\\s+(\\S+\\))$/', $transform, $matches)) {
                            $transform = "$matches[1] $rectX $matches[3]";
                            $newText->addAttribute('text-anchor', $textAnchor);
                        }

                        if ($transform) {
                            $newG->addAttribute('transform', $transform);
                            //$newText->addAttribute('transform', $transform);
                        }

                    }
                }

            }

            // <g> //
            if (isset($oldG1->g)) {
                foreach ($oldG1->g as $oldG2) {
                    ++$numTextGroups;
                    $textAnchor = null;
                    $rectX = null;
                    $textLength = null;

                    // <rect> //
                    if (isset($oldG2->rect)) {
                        $oldRect = $oldG2->rect;
                        $textAnchor = 'middle';
                        $rectX = floatval($oldRect['x']) + floatval($oldRect['width']) / 2;
                        $rectWidth = $oldRect['width'];
                        if (isset($oldRect['id'])) {
                            if (preg_match('/share/i', $oldRect['id'])) {
                                $new->addAttribute('data-group-' . $numTextGroups . '-share-size', 'yes');
                            }
                            if (preg_match('/left/i', $oldRect['id'])) {
                                $textAnchor = 'start';
                                $rectX = floatval($oldRect['x']);
                            } else if (preg_match('/right/i', $oldRect['id'])) {
                                $textAnchor = 'end';
                                $rectX = floatval($oldRect['x']) + floatval($oldRect['width']);
                            } else if (preg_match('/justify/i', $oldRect['id'])) {
                                $textLength = intval($rectWidth) + 1;
                            }
                        }
                        if (!isset($textLength)) {
                            $new->addAttribute('data-group-' . $numTextGroups . '-max-width', $rectWidth);
                        }
                    }

                    $oldTexts = [];
                    if (isset($oldG2->text)) {
                        foreach ($oldG2->text as $text) {
                            if (isset($text['id'])) {
                                $oldTexts[(string)$text['id']] = $text;
                            } else {
                                $oldTexts[] = $text;
                            }
                        }
                    }
                    if (isset($oldG2->g)) {
                        foreach ($oldG2->g as $oldG3) {
                            if (isset($oldG3->text)) {
                                foreach ($oldG3->text as $text) {
                                    if (isset($oldG3['id'])) {
                                        $oldTexts[(string)$oldG3['id']] = $text;
                                    } else {
                                        $oldTexts[] = $text;
                                    }
                                }
                            }
                        }
                    }

                    // <text> //
                    foreach ($oldTexts as $textId => $oldText) {
                        $newClasses = array('group-' . $numTextGroups, 'text-' . $textCount++);
                        if (is_string($textId)) {
                            $textId = intval(preg_replace('/[^0-9]/', '', $textId));

                            // Adding foil attribute to text tag
                            if (stripos($textId, "foil") !== false) {
                                $newClasses[] = 'foil';
                            }

                            if (stripos($textId, "shrink") !== false || stripos($textId, "share") !== false) {
                                $newClasses[] = 'shrink-fit';
                            }
                        }

                        // <text><path> //
                        if (isset($oldText->textPath)) {
                            $newText = $this->newMainG->addChild('text');

                            // For colorways //
                            $newText->addAttribute('filter', 'url(#svg-filter-' . $sku . '-' . $colorId . ')');

                            $newTextPath = $newText->addChild('textPath');
                            foreach ($oldText->textPath->attributes(self::NS_XLINK) as $key => $value) {
                                $newTextPath->addAttribute('xlink:' . $key, $value, self::NS_XLINK);
                            }
                            $newTSpan = $newTextPath->addChild('tspan', htmlspecialchars((string)$oldText->textPath->tspan));
                            foreach ($oldText->textPath->tspan->attributes() as $key => $value) {
                                if ($key == 'font-family') {
                                    $value = substr($value, 1, -1);
                                    $fontIndex = array_search($value, $fonts);
                                    if ($fontIndex === false) {
                                        $fonts[++$numFonts] = $value;
                                        $fontIndex = $numFonts;
                                    }
                                    $newClasses[] = 'font-' . $fontIndex;
                                }
                                $newTSpan->addAttribute($key, $value);
                            }
                            $newTSpan->addAttribute('class', implode(' ', $newClasses));

                        } else {
                            $newG = $this->newMainG->addChild('g');
                            $newG->addAttribute('class', 'group-' . $numTextGroups);

                            // Adding foil attribute to text tag && create the background image //
                            if (in_array("foil", $newClasses)) {

                                // <defs>
                                $patternDefs = $newG->addChild("defs");

                                // <pattern>
                                $pattern = $patternDefs->addChild("pattern");
                                $pattern->addAttribute('id', 'p1-'.$sku);
                                $pattern->addAttribute('width', $oldG2->rect->attributes()->width);
                                $pattern->addAttribute('height', $oldG2->rect->attributes()->height);


                                $foilImage = $pattern->addChild("image");

                                // Define dimensions //
                                //$transformedWidth = $oldG2->rect->attributes()->width / 2;
                                //$foilImage->addAttribute('x', '-'.$transformedWidth);
                                //$foilImage->addAttribute('y', '-'.$oldG2->rect->attributes()->height);
                                $foilImage->addAttribute('width', $oldG2->rect->attributes()->width);
                                $foilImage->addAttribute('height', $oldG2->rect->attributes()->height);

                                // Strip script name since this will be crontab'ed
                                if(stripos(Mage::getBaseUrl(), "comb_ftp.php/") !== false) {
                                    $baseUrl = str_replace("comb_ftp.php/", "", Mage::getBaseUrl());
                                }

                                if(stripos(Mage::getBaseUrl(), "comb_ftp_staging.php/") !== false) {
                                    $baseUrl = str_replace("comb_ftp_staging.php/", "", Mage::getBaseUrl());
                                }

                                // Get the foil image //
                                $foilImageUrl = $baseUrl.'media/images/foil.png';
                                $foilImage->addAttribute('xlink:href', $foilImageUrl, self::NS_XLINK);

                                $oldText->addAttribute('fill', "url(#p1-$sku)");

                            }

                            if (isset($oldText->tspan)) {
                                $newText = $newG->addChild('text');
                                $newText->addChild('tspan', htmlspecialchars((string)$oldText->tspan));
                            } else {
                                $newText = $newG->addChild('text', htmlspecialchars((string)$oldText));
                            }

                            foreach ($oldText->attributes() as $key => $value) {
                                if (in_array($key, array('id', 'transform'))) {
                                    continue;
                                } else if ($key == 'font-family') {
                                    $value = substr($value, 1, -1);
                                    $fontIndex = array_search($value, $fonts);
                                    if ($fontIndex === false) {
                                        $fonts[++$numFonts] = $value;
                                        $fontIndex = $numFonts;
                                    }
                                    $newClasses[] = 'font-' . $fontIndex;
                                }
                                $newText->addAttribute($key, $value);
                            }
                            if (!empty($newClasses)) {
                                $newText->addAttribute('class', implode(' ', $newClasses));
                            }
                            $newText->addAttribute('filter', 'url(#svg-filter-' . $sku . '-' . $colorId . ')');
                            if ($textLength !== null) {
                                $newText->addAttribute('lengthAdjust', 'spacing');
                                $newText->addAttribute('textLength', $textLength);
                            }
                            $transform = isset($oldText['transform'])? $oldText['transform'] : null;

                            if ($textAnchor && $transform && preg_match('/^(matrix\\(\\S+\\s+\\S+\\s+\\S+\\s+\\S+)\\s+(\\S+)\\s+(\\S+\\))$/', $transform, $matches)) {
                                $transform = "$matches[1] $rectX $matches[3]";
                                $newText->addAttribute('text-anchor', $textAnchor);
                            }
                            if ($transform) {
                                $newG->addAttribute('transform', $transform);
                                //$newText->addAttribute('transform', $transform);
                            }
                        }
                    }
                }
            }

        }


        $new->asXML($destination);
        return true;
    }

    public function sortTextFields(SimpleXMLElement $element)
    {

        foreach($element as $textField) {
            $textTags[] = $textField;
        }

        uasort($textTags, 'Pixafy_Pixcatalog_Helper_Svg::compareTextFieldPositions');

        return $textTags;
    }

    public static function compareTextFieldPositions($a, $b)
    {

        preg_match_all("/(\d+\.\d+)/",$a->attributes()->{'transform'}, $aMatches);

        preg_match_all("/(\d+\.\d+)/",$b->attributes()->{'transform'}, $bMatches);

        $aXcord = $aMatches[0][0];
        $aYcord = $aMatches[0][1];

        $bXcord = $bMatches[0][0];
        $bYcord = $bMatches[0][1];

        if($aYcord == $bYcord) {
            return (($aXcord - $bXcord) < 0) ? -1 : 1;
        }

        return ($aYcord < $bYcord) ? -1 : 1;

    }


    protected function getRabbitPath(SimpleXMLElement $xmlHoles)
    {

        foreach($xmlHoles->children() as $child) {
            print $child->g['id'];

            if(isset($child->g)) {

                $this->getRabbitPath($child);

            } else {

                if(strtolower($child->attributes()['id']) != "bleedlayer" ) {
                    if(isset($child->path)) {

                        $flattenPath = $this->newMainG->addChild('path');

                        foreach($child->path->attributes() as $key => $value) {
                            $flattenPath->addAttribute($key, $value);

                        }

                    }

                }


            }

        }

    }


    public function validateSvg($sku, $source, $filename) {
        if (!file_exists($source)) {
            throw new Mage_Core_Exception("File $source for $sku does not exist.");
        }
        $svg = simplexml_load_file($source);
        if (!isset($svg->g)) {
            throw new Mage_Core_Exception("The SVG $filename for $sku contains no groups.");
        }
        $publicFontsDir = $this->getPublicFontsDir();
        foreach ($svg->g as $i => $g1) {
            if (isset($g1->rect)) {
                $rects = $g1->rect;
                if (!is_array($rects)) {
                    $rects = array($rects);
                }
                foreach ($rects as $rect) {
                    if (!in_array((string)$rect['id'], $this->getOptionalImageTypes())) {
                        throw new Mage_Core_Exception("Tier 1 Group " . ($i + 1) . " of $filename for $sku has a rectangle with the unrecognized name '" . $rect['id'] . "'.");
                    }
                }
            }
            if (!isset($g1['id']) || !preg_match('/^color_?\\d+/i', $g1['id'])) {
                throw new Mage_Core_Exception("Tier 1 Group " . ($i + 1) . " of $filename for $sku does not have a proper name (name is '" . $g1['id'] . "' but expected form is: 'Color \$n').");
            }
            if (!isset($g1->image) && !isset($g1->g)) {
                throw new Mage_Core_Exception("Tier 1 Group '" . $g1['id'] . "' of $filename for $sku does not have any images or text groups.");
            }
            if (isset($g1->g)) {
                foreach ($g1->g as $g2) {
                    if (!isset($g2->g) && !isset($g2->text)) {
                        throw new Mage_Core_Exception("Tier 2 Group '" . $g2['id'] . "' of $filename for $sku does not have any text.");
                    }
                    foreach ($g2->text as $text) {
                        /* if (isset($text['id']) && preg_match('/^line_?\\d+/i', $text['id'])) {
                            throw new Mage_Core_Exception("Tier 2 Group " . $g2['id'] . "' of $filename for $sku contains text that does not have a proper name (name is '" . $text['id'] . "' but expected form is: 'Line \$n'.");
                        } */
                        if (isset($text['font-family'])) {
                            $fontFileFound = false;
                            foreach ($this->getFontsAllowedExtensions() as $ext) {
                                $fontFile = $publicFontsDir . DS . $text['font-family'] . ".$ext";
                                if (file_exists($fontFile)) {
                                    $fontFileFound = true;
                                    break;
                                }
                            }
                            if (!$fontFileFound) {
                                throw new Mage_Core_Exception("Tier 2 Group " . $g2['id'] . "' of $filename for $sku contains text that does not have a recognized font-family (font-family is '" . $text['font-family'] . "' but no font file with a name like that was found in $publicFontsDir.");
                            }
                        }
                    }
                    foreach ($g2->g as $g3) {
                        foreach ($g3->text as $text) {
                            if (isset($text['font-family'])) {
                                $fontFileFound = false;
                                foreach ($this->getFontsAllowedExtensions() as $ext) {
                                    $fontFile = $publicFontsDir . DS . $text['font-family'] . ".$ext";
                                    if (file_exists($fontFile)) {
                                        $fontFileFound = true;
                                        break;
                                    }
                                }
                                if (!$fontFileFound) {
                                    throw new Mage_Core_Exception("Tier 2 Group " . $g2['id'] . "' of $filename for $sku contains text that does not have a recognized font-family (font-family is '" . $text['font-family'] . "' but no font file with a name like that was found in $publicFontsDir.");
                                }
                            }
                        }
                    }
                    if (!isset($g2->rect)) {
                        throw new Mage_Core_Exception("Tier 2 Group '" . $g2['id'] . "' of $filename for $sku does not have any rectangles.");
                    }
                }
            }
        }
        return true;
    }

}