<?php
/**
 * Created by PhpStorm.
 * User: jcymerman
 * Date: 1/12/2015
 * Time: 4:30 PM
 */

class Pixafy_Pixtemplates_Helper_Menu extends Mage_Core_Helper_Abstract {

    const PRODUCT_TYPES_OFFSET = 0;
    const PRODUCT_TYPES_LENGTH = 12;

    protected $_productTypeGroups;

    public function getRootCategoryId() {
        return Mage::app()->getStore()->getRootCategoryId();
    }
    public function getMenuSubcategories($categoryId) {
        return Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToFilter('parent_id', $categoryId)
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('include_in_menu', 1)
            ->addAttributeToSelect(array('name', 'url_key', 'is_coming_soon'))
            ->joinUrlRewrite()
            ->setOrder('position', 'asc');
    }

    public function getProductTypes($type = 'product_type', $offset = self::PRODUCT_TYPES_OFFSET, $length = self::PRODUCT_TYPES_LENGTH) {
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $type);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }
        return array_slice($options, $offset, $length);
    }

    public function getProductTypeGroups() {
        if (is_null($this->_productTypeGroups)) {
            $this->_productTypeGroups = array(
                array(
                    'Invitation Suite' => array(
                        'Invitations',
                        'Mailer Envelopes',
                        'Mailer Envelope Liners',
                        'Reply Card Envelopes',
                        'Bellybands',
                        'Wraps',
                        'Sleeves',
                        'Reception Cards',
                        'Direction Cards',
                        'Itinerary Cards',
                        'Stamps'
                    ),
                ),
                array(
                    'Save the Date' => array(
                        'Save the Date Cards',
                        'Save the Date Envelopes',
                        'Stamps',
                    ),
                    'Rehearsal Dinner' => array(
                        'Rehearsal Dinner Cards',
                        'Rehearsal Dinner Envelopes',
                        'Stamps'
                    ),
                    'Brunch' => array(
                        'Brunch Cards',
                        'Brunch Envelopes',
                        'Stamps'
                    ),
                ),
                array(
                    'Wedding Accessories' => array(
                        'Programs',
                        'Escort Cards',
                        'Place Cards',
                        'Menus',
                        'Menu Bellybands',
                        'Table Signs',
                        'Thank You Cards',
                        'Thank You Envelopes'
                    )
                )
            );
        }
        return $this->_productTypeGroups;
    }
    public function isActiveMenuItem($name){
        $requesturi=strtok(Mage::app()->getRequest()->getOriginalPathInfo(),'?');
        $name=strtolower($name);
        if($name=='collections'){
            return (strpos($requesturi,'/collections') !== false);
        }else if($name=='the world of ceci'){
            return (strpos($requesturi,'/company') !== false) || (strpos($requesturi,'/world-of-ceci') !== false) || (strpos($requesturi,'/philanthropy') !== false) || (strpos($requesturi,'/careers') !== false) || (strpos($requesturi,'/artistry') !== false) || (strpos($requesturi,'/ceci-bio') !== false);
        }else if($name=='ceci style'){
            //return (strpos($requesturi,'/company') !== false);
        }
        return false;
    }

//    public function getPrintingTechniquesTypes($offset = self::PRODUCT_TYPES_OFFSET, $length = self::PRODUCT_TYPES_LENGTH) {
//        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'printing_techniques');
//        if ($attribute->usesSource()) {
//            $options = $attribute->getSource()->getAllOptions(false);
//        }
//        return array_slice($options, $offset, $length);
//    }
//
//    public function getOriginalFineArtTypes($offset = self::PRODUCT_TYPES_OFFSET, $length = self::PRODUCT_TYPES_LENGTH) {
//        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'original_fine_art');
//        if ($attribute->usesSource()) {
//            $options = $attribute->getSource()->getAllOptions(false);
//        }
//        return array_slice($options, $offset, $length);
//    }

        public function getProductGroupTypeProductTypes() {
            $groups = array();
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            $sql = 'SELECT  main_table.group_id, main_table.name, pp.value_id, pp.position
            FROM pixcatalog_product_type_group AS main_table
            LEFT JOIN pixcatalog_product_type_group_product_type AS pp ON main_table.group_id = pp.group_id
            WHERE main_table.status = 1';
            $result = $read->fetchAll($sql);
            foreach($result as $group)
            {
                $groups[$group['group_id']]['values'][$group['position']] = $group['value_id'];
                $groups[$group['group_id']]['name'] = $group['name'];
            }
            return $groups;
        }

        public function getProductTypeLabels() {
            $name='product_type';
            $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
            $attributeId = $attributeInfo->getAttributeId();
            $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
            $attributeOptions = $attribute ->getSource()->getAllOptions(false);
            $attributesByValueId = array();
            foreach($attributeOptions as $attributeOption){
                $attributesByValueId[$attributeOption['value']] = $attributeOption['label'];
            }
            return $attributesByValueId;
        }
}