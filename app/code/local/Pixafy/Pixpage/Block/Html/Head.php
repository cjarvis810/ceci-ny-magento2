<?php

class Pixafy_Pixpage_Block_Html_Head extends Mage_Page_Block_Html_Head {

    /**
     * Our url 
     * @var string 
     */
    protected $baserUrl;
    
    /**
     * Our root dir
     * @var string 
     */
    protected $root;

    /**
     * Our Config Values
     * @var array
     */
    protected $configs;   
    
    public function _construct() {
        $this->root = Mage::getBaseDir();
        $this->baserUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $this->configs = json_decode(file_get_contents("{$this->root}/pix_env_config.json"), true);
        parent::_construct();
    }

    /**
     * Check our environment configs and load what's necessary
     */
    public function getCssJsHtml() {
        $html = parent::getCssJsHtml();
        if ($this->configs['use-min']) {
            $html .= $this->displayMinified();
        } else {
            $html .= $this->displayOriginal();
        }
        return $html;
    }

    /**
     * Get the minified scripts
     * @return string html
     */
    protected function displayMinified() {
        $html = '';
        foreach (array("js", "css") as $type) {
            foreach ($this->configs[$type] as $scripts) {
                $html .= $this->scriptHtml($scripts['destination'], $type, $this->configs['last-updated']);
            }
        }
        return $html;
    }

    /**
     *  Get the original scripts
     * @return string Html
     */
    protected function displayOriginal() {
        $html = '';
        foreach (array("js", "css") as $type) {
            foreach ($this->configs[$type] as $scripts) {
                foreach ($scripts['folders'] as $folder) {
                    foreach (new DirectoryIterator("{$this->root}/$folder") as $fileInfo) {
                        if ($fileInfo->getExtension() === $type) {
                            $html.= $this->scriptHtml("$folder" . $fileInfo->getFilename(), $type, $fileInfo->getMTime());
                        }
                    }                    
                }
                foreach ($scripts['files'] as $file) {
                        $html.= $this->scriptHtml($file, $type, filemtime($this->root . '/' .$file ));
                    }
            }
        }
        return $html;
    }

    /**
     * Return html
     * @param type $script
     * @param type $type
     * @return type
     */
    private function scriptHtml($script, $type, $cacheBust) {
        switch ($type) {
            case 'js':
                return "\n" . '<script type="text/javascript" src="' . $this->baserUrl . $script . '?' . $cacheBust . '"></script>';
            case 'css':
                return "\n" .'<link rel="stylesheet" type="text/css" href="' . $this->baserUrl . $script . '?' . $cacheBust . '" media="all" />';
        }
    }

}
