<?php
class Pixafy_Pixcore_Helper_Data extends Mage_Core_Helper_Abstract {

    const COOKIE_ID_JUST_LOGGED_IN = 'just_logged_in';
    const COOKIE_ID_JUST_SUBSCRIBED = 'just_subscribed';

    public function resizeImagesByType($product, $galleryImages, $types) {
        $imagesByType = array();

        // Get simple product images
        $configurable= Mage::getModel('catalog/product_type_configurable')->setProduct($product);
        $simpleCollection = $configurable->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();

        foreach($simpleCollection as $simpleProduct) {

            foreach($types as $label => $specs) {

                $imagesByType[$label][$simpleProduct->getId()] = array(
                    "image" => (string) Mage::helper('catalog/image')->init($product, $specs['image_type'], $simpleProduct->getImage())->resize($specs['w'], $specs['h']),
                    "label" => $simpleProduct->getSku(),
                    "name"  => $simpleProduct->getImage(),
                    "attributeId" => $simpleProduct->getColor()
                );

            }

        }

        // Get gallery images
        foreach ($galleryImages as $galleryImage) {

            foreach ($types as $label => $specs) {

                if (!isset($imagesByType[$label])) {
                    $imagesByType[$label] = array();
                }

                $imagesByType[$label][$galleryImage->getValueId()] = array(
                    "image" => (string) Mage::helper('catalog/image')->init($product, $specs['image_type'], $galleryImage->getFile())->resize($specs['w'], $specs['h']),
                    "label" => $galleryImage->getLabel(),
                    "name" => $galleryImage->getFile()
                );
            }
        }


        return $imagesByType;
    }

}