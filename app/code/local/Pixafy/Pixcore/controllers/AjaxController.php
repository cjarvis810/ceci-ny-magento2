<?php

    class Pixafy_Pixcore_AjaxController extends Mage_Core_Controller_Front_Action
    {

        function linksAction()
        {
            $this->loadLayout();
            $this->_initLayoutMessages('customer/session');
            $data = array();

            $cookieModel = Mage::getModel('core/cookie');
            $pixcustomer = Mage::helper("pixcustomer");


            $expirationTime = $pixcustomer->getEmailCaptureCookieTime();
            $path = $pixcustomer->getCookiePath();
            $domain = $pixcustomer->getCookieDomain();
            $isSubscribed = (int)$pixcustomer->isSubscribed();

            $data['logged_in'] = (int)Mage::helper("customer")->isLoggedIn();
            if($cookieModel->get(Pixafy_Pixcustomer_Helper_Data::COOKIE_ID_IS_SUSBCRIBED) === false)
            {
                $cookieModel->set(Pixafy_Pixcustomer_Helper_Data::COOKIE_ID_IS_SUSBCRIBED, $isSubscribed, $expirationTime, $path, $domain);
            }
            if(!$cookieModel->get(Pixafy_Pixcustomer_Helper_Data::COOKIE_ID_EMAIL_CAPTURE_SHOWN))
            {
                if($isSubscribed)
                {
                    $data['new_user'] = false;
                }
                else
                {
                    $data['new_user'] = true;
                    $cookieModel->set(Pixafy_Pixcustomer_Helper_Data::COOKIE_ID_EMAIL_CAPTURE_SHOWN, "yes", $expirationTime, $path, $domain);
                }
            }
            else
            {
                $data['new_user'] = false;
            }

            if(!$data['logged_in'])
            {
                $data['login_form'] = $this->getLayout()->createBlock('customer/form_login')->setTemplate('persistent/customer/form/popup-login.phtml')->toHtml();
            }
            else
            {
                $this->getLayout()->getUpdate()->addHandle('customer_logged_in');

                $wishlist = Mage::helper('wishlist');
                $data['wish_num'] = $wishlist->getItemCount();

                $data['user'] = array(
                    'email' 			=> base64_encode(Mage::getSingleton('customer/session')->getCustomer()->getEmail()),
                    'is_subscriber' 	=> $isSubscribed,
                    'just_logged_in' 	=> false
                );

                if($cookieModel->get(Pixafy_Pixcore_Helper_Data::COOKIE_ID_JUST_LOGGED_IN)){

                    $data['user']['just_logged_in'] = true;

                    $cookieModel->delete(Pixafy_Pixcore_Helper_Data::COOKIE_ID_JUST_LOGGED_IN);
                }
            }

//            $linksBlock = $this->getLayout()->createBlock('page/html_header')->setTemplate('page/links.phtml');
//            $linksBlock->setChild('topSearch', $this->getLayout()->createBlock('core/template')->setTemplate('catalogsearch/form.mini.phtml'));
//            $linksBlock->setChild('pixwishlist_sidebar',$this->getLayout()->createBlock('pixwishlist/customer_sidebar')->setTemplate('wishlist/sidebar.phtml'));
//
//            $data['cart_num'] = Mage::helper('checkout/cart')->getSummaryCount();

//            if( $data['cart_num'] > 0){
//                //this is the cart contents that client wants on every page.
//                $items = Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
//                $data['cart_contents'] = Mage::helper('pixcart')->getCartContentsFromItems($items);
//
//            }

            //this is probably the heaviest part of this ajax call.
            //there should be some way to reduce this.
//            $cartBlock = $this->getLayout()->createBlock('checkout/cart_sidebar')->setTemplate('checkout/cart/cartheader.phtml');
//            $cartBlock->addItemRender('simple','checkout/cart_item_renderer','checkout/cart/sidebar/default.phtml');
//            $cartBlock->addItemRender('grouped','checkout/cart_item_renderer_grouped','checkout/cart/sidebar/default.phtml');
//            $cartBlock->addItemRender('configurable','checkout/cart_item_renderer_configurable','checkout/cart/sidebar/default.phtml');
//            $cartBlock->setChild('extra_actions', $this->getLayout()->createBlock('core/text_list')->setTemplate('checkout/cart/cartheader.phtml'));
//
//            $linksBlock->setChild('topCart', $cartBlock);

            $data['html'] = '';//$linksBlock->toHtml();

            //$data['account_links'] = $this->getLayout()->getBlock('account.links')->toHtml();
            //$data['mobile_menu'] = $mobileMenuBlock->toHtml();
            //$data['mobile_menu_links'] = $this->getLayout()->createBlock('core/template')->setTemplate('page/mobile-links.phtml')->toHtml();

            $this->_ajaxReturn($data);
        }


        public function setColorwheelSettingsAction()
        {
            $data = $this->getRequest()->getPost();

            // Store slice ID in session //
            Mage::getSingleton('core/session')->setColorWheelSetting($data);

            $this->_ajaxReturn($data);
        }

        protected function getColorwayIdFromName($colorwayName)
        {
            if(!empty($colorwayName)) {
                if(!is_array($colorwayName)) {
                    $colorwayName = explode("-", $colorwayName);
                }

                $color = end($colorwayName);

                $data = Mage::getModel('pixcatalog/colorway')->getCollection()
                    ->addFieldToFilter('colorway_name', array('like'=>$color."%"));

                //return str_replace(",", '","', $data->getData()[0]['colors']);
                return  $data;

            } else {
                return false;
            }

        }

        /*
         * @Return (Json Array)     This will return the hex colors based on the name
         */
        public function getHexColorAction()
        {
            // overlayName: "color-overlay-black"
            //$colorName['overlayName'] = 'color-overlay-black';

            if($colorName = Mage::getSingleton('core/session')->getColorWheelSetting()) {

                $results = $this->getColorwayIdFromName($colorName['overlayName']);

                $this->getResponse()->setBody($results->getData()[0]['colors']);

            }

        }

        /*
         * @Return  (Array)     This return an array of sku's supported by the current color wheel
         */
        public function getProductSkuSupportedByCurrentColorwheelAction()
        {

            $overlay = Mage::getSingleton('core/session')->getColorWheelSetting();
            // Get the currrent colorway_id
            if($colorway = $this->getColorwayIdFromName($overlay['overlayName'])) {

                // Get a list of product_id's matching that colorway_id
                $model = Mage::getModel('pixcatalog/colorway')->getCollection()
                    ->join(
                    array('ppcg' => 'pixcatalog/product_colorway_grid'),
                    'main_table.colorway_id=ppcg.colorway_id'
                )->addFieldToFilter('main_table.colorway_id', $colorway->getAllIds());

                if($model->getData()) {

                    // Get the sku name of the product id's
                    foreach($model->getData() as $colorway) {
                        $productIds[] = $colorway['product_id'];

                    }

                    $productsCollection = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->addAttributeToFilter('entity_id', array('in' => $productIds));

                    $productSku = array();
                    foreach($productsCollection as $product) {
                        $productSku[] = $product->getSku();
                    }

                    $this->getResponse()->setBody(json_encode($productSku));

                }

            }

        }

        public function getColorwheelSettingsAction()
        {

            $result = Mage::getSingleton('core/session')->getColorWheelSetting();

            $this->_ajaxReturn($result);
        }

        public function getSliceIdsAction()
        {

            // Get color wheel slices //
            $data = Mage::getModel('pixcatalog/colorwheel_slice')->getCollection()
                ->addFieldToSelect('slice_id');

            $this->_ajaxReturn($data->getData());
        }


        public function deleteCookieAction()
        {
            Mage::getModel('core/cookie')->delete("newsletterPopup");
            Mage::getModel('core/cookie')->delete("elapsedPopup");

           /* Mage::getSingleton('core/cookie')
                ->set('newsletterPopup', '{"popupStatus": "black", "popupSeconds": 1}', '/'); */
        }


        public function setWaitConditionAction()
        {
            Mage::getSingleton('core/cookie')
                ->set('newsletterPopup', '{"popupStatus": "wait", "popupSeconds": 0}', time()+86400);

            // 2 days //
            $expiration = 60*60*24*2;

            Mage::getSingleton('core/cookie')
                ->set('elapsedPopup', '1', $expiration);
        }

        public function getPopupCookieAction()
        {

            // If elasedPopup is gone assume time has been reset and set black cookie as status
            if($newsletterPopup = Mage::getModel('core/cookie')->get('newsletterPopup')) {
                $newsletterPopup = json_decode($newsletterPopup);

                if(!Mage::getModel('core/cookie')->get('elapsedPopup') && $newsletterPopup->popupStatus == 'wait') {
                    $this->setBlackCookieAction();

                }

            }

            $data = json_encode(Mage::getModel('core/cookie')->get('newsletterPopup'));

            $this->getResponse()->setHeader('Content-type', 'application/json');

            $this->getResponse()->setBody($data);

        }

        /*
         * newsletterPopup = {state: "|colored|black", seconds: "1-240"}
         */
        public function setPopupCookieAction()
        {
            $params = $this->getRequest()->getParams();

            // We use isset cause an empty state is a possible state //
            if(isset($params['popupStatus'], $params['popupSeconds'])) {

                Mage::getSingleton('core/cookie')
                    ->set('newsletterPopup', '{"popupStatus": "'.$params["popupStatus"].'", "popupSeconds": '.$params["popupSeconds"].'}', time()+86400);

            }

            $this->getResponse()->setBody("success");

        }

        public function setBlackCookieAction()
        {
            //Mage::getModel('core/cookie')->delete("newsletterPopup");
            Mage::getSingleton('core/cookie')
                ->set('newsletterPopup', '{"popupStatus": "black", "popupSeconds": 1}', time()+86400);
        }


        public function setColoredCookieAction()
        {
            $params = $this->getRequest()->getParams();
            
            // We use isset cause an empty state is a possible state //
            if(isset($params['popupSeconds']) && intval($params['popupSeconds'])) {

                Mage::getSingleton('core/cookie')
                    ->set('newsletterPopup', '{"popupStatus": "colored", "popupSeconds": '.$params["popupSeconds"].'}', time()+86400);

                /*Mage::getSingleton('core/cookie')
                    ->set('newsletterPopup', '{"popupStatus": "", "popupSeconds": "1"}');*/
            }

            $this->getResponse()->setBody("success");

        }


        public function popupNewsletterAction()
        {
            // Ensure user is logged in
            if(Mage::helper("customer")->isLoggedIn()) {
                $params = $this->getRequest()->getParams();
                Mage::getSingleton('core/cookie')
                      ->set('newsletterPopup', 'colored');

                Mage::getSingleton('core/session')->setNewsletterPopup("colored");

            }

            $this->getResponse()->setBody("popupNewsletterAction");
        }

        public function getGiftCardAmountAction()
        {

            $data = $this->getRequest()->getParams();

            //print_r($data);

            // Get sale orders
            $collection = Mage::getModel('enterprise_giftcardaccount/giftcardaccount')
                    ->getCollection()
                    ->addFieldToFilter('code', $data['giftcardCode']);
                    //->addFieldToFilter('gift_cards', array('neq'=>'a:0:{}'));


            $this->getResponse()->setBody($collection->getData()[0]['balance']);
        }


        public function getProductImageAction()
        {
            $params = $this->getRequest()->getParams();

            if(intval($params['image_id'], $params['product_id'])) {
                $_product = Mage::getModel('catalog/product')->load($params['product_id']);
                $imageUrl = $_product->getMediaGalleryImages()->getItemByColumnValue('value_id', $params['image_id'])->getUrl();
                $this->getResponse()->setBody($imageUrl);
            }

        }

        public function getProductImageFromSkuAction()
        {
            $params = $this->getRequest()->getParams();

            if($params['sku'] && $params['productsku']) {

                $url[$params['productsku']] = Mage::getModel('catalog/product')->loadByAttribute('sku',$params['sku'])->getImageUrl();

                $this->getResponse()->setBody(json_encode($url));
            }

        }



        private function _ajaxReturn($data, $encode = true)
        {
            if($encode)
            {
                $data = Mage::helper('core')->jsonEncode($data);
            }
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($data);
        }

//        public function navAction()
//        {
// //           $key = (int)Mage::app()->getStore()->isCurrentlySecure().'_'.Pixafy_Pixcore_Helper_Redis::NAV_KEY;
// //           $html = Mage::helper("pixcore/redis")->get($key);
//            if(!$html)
//            {
//                $navBlock = Mage::app()->getLayout()->createBlock('page/html_topmenu');
//                $navBlock->setTemplate('page/html/topmenu.phtml');
//                $navBlock->setShowNav(true);
//                $html = $navBlock->toHtml();
//    //          Mage::helper("pixcore/redis")->set($key,$html);
//            }
//            $data = array("nav" => $html);
//            $this->_ajaxReturn($data);
//        }
    }

?>