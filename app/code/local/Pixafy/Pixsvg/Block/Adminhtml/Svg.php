<?php
class Pixafy_Pixsvg_Block_Adminhtml_Svg extends Mage_Core_Block_Template{
    private $svgs=array();
    private $fonts=array();
    public function getSvgs(){
        $this->svgs=array();
        $productsStagingPath= Mage::helper('pixcatalog/svg')->getStagingProductsDir();
        if (is_dir($productsStagingPath)) {
            $productsStagingDir = opendir($productsStagingPath);
            while ($sku = readdir($productsStagingDir)) {
                if (in_array($sku, array('.', '..'))) continue;
                $skuStagingPath = $productsStagingPath . $sku;
                if (is_dir($skuStagingPath)) {
                    $skuStagingDir = opendir($skuStagingPath);
                    $doneflag=file_exists($skuStagingPath.DS.'file.done');
                    while ($filename = readdir($skuStagingDir)) {
                        if (in_array($filename, array('.', '..'))) continue;
                        $source = $skuStagingPath . DS . $filename;
                        $pathinfo = pathinfo($source);
                        if ($pathinfo['extension'] != 'svg') continue;
                        if($doneflag){
                            $this->svgs['done'][$sku] = Mage::helper('pixcatalog/svg')->getStagingUrl("products/$sku") . "$filename";
                        }else {
                            $this->svgs['staged'][$sku] = Mage::helper('pixcatalog/svg')->getStagingUrl("products/$sku") . "$filename";
                        }
                    }
                }
            }
        }
        return $this->svgs;
    }
    public function getFonts(){
        $this->fonts=array();
        if (is_dir(Mage::getBaseDir('base').DS.'media/fonts')) {
            $dir = opendir(Mage::getBaseDir('base').DS.'media/fonts');
            while ($filename = readdir($dir)) {
                if (in_array($filename, array('.', '..'))) continue;
                $this->fonts[] = pathinfo('media/fonts/' . $filename, PATHINFO_FILENAME);
            }
        }
        return $this->fonts;
    }


}
?>