<?php
class Pixafy_Pixsvg_Helper_Data extends Mage_Core_Helper_Abstract {
    public function processSVG($contents, $path){
        $xdoc = new DomDocument;
        $xdoc->loadXML($contents);
        foreach($xdoc->getElementsByTagName('image') as $array){
            $attribNode = $array->getAttributeNode('xlink:href');
            if(!file_exists($path.DS.'./foil.png') && strpos($attribNode->value,'foil.png') !== false) {
               file_put_contents($path.'foil.png', file_get_contents($attribNode->value));
               $attribNode->value = './foil.png';
            }
            $attribNode->value = $path.DS.$attribNode->value;
        }
        return $xdoc->saveXML();
    }
}
?>