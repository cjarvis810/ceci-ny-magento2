<?php

class Pixafy_Pixsvg_Adminhtml_SvgController extends Mage_Adminhtml_Controller_Action {
    protected $_publicActions = array('index','downloadeps','markasdone');
	public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
	}
    public function downloadepsAction(){
        $sku=$this->getRequest()->getParam('sku');
        $svg=Mage::helper('pixcatalog/svg')->getStagingProductsDir().$sku.DS.'main.svg';
        if(file_exists($svg)) {
            shell_exec("echo '".Mage::helper('pixsvg')->processSVG(file_get_contents($svg),Mage::helper('pixcatalog/svg')->getStagingProductsDir().$sku)."' | inkscape -z -f /dev/stdin -E ".escapeshellarg(Mage::helper('pixcatalog/svg')->getStagingProductsDir().$sku.DS.'main.eps'));
            header('Location: '.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA). "staging/products/$sku/main.eps");
        }else{
            header('Location: '.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK).'admin/svg/index/');
        }
    }
    public function markasdoneAction(){
        $sku=$this->getRequest()->get('sku');
        $productsStagingPath= Mage::helper('pixcatalog/svg')->getStagingProductsDir();
        if(rtrim($productsStagingPath,'/')!=rtrim($productsStagingPath.$sku,'/') && is_dir($productsStagingPath . $sku) && !file_exists($productsStagingPath.$sku.DS.'file.done')){
            touch($productsStagingPath.$sku.DS.'file.done');
        }
       header('Location: '.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK).'admin/svg/index/');
    }
}
