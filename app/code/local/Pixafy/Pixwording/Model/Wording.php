<?php
class Pixafy_Pixwording_Model_Wording extends Mage_Core_Model_Abstract {

    public function _construct() {
        $this->_init('pixwording/wording');
    }
    
    /**
     * Gets Template with variables interprete and replaced
     * @param array $variables
     * @return array
     */
    public function getResolvedTemplate(array $variables){
        $variables = array_merge( $variables, array( "\n" => '~newline~' , "\r" => '~newline~' ));
        str_replace(array_keys($variables), $variables, $this->getTemplate());
        return array_filter(explode("~newline~", str_replace(array_keys($variables), $variables, $this->getTemplate())));
    } 
    
}