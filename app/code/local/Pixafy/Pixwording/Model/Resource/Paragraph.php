<?php

class Pixafy_Pixwording_Model_Resource_Paragraph extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct() {
        $this->_init('pixwording/paragraph', 'paragraph_id');
    }
}