<?php

class Pixafy_Pixwording_Model_Resource_Category_Product_Type_Wording extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct() {
        $this->_init('pixwording/category_product_type_wording', 'category_product_type_wording_id');
    }
}