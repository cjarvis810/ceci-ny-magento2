<?php

class Pixafy_Pixwording_Model_Resource_Category_Product_Type_Wording_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected $wordingJoined;
    
    protected function _construct() {
        $this->_init('pixwording/category_product_type_wording');
    }
    
    /**   
     * Join Containt
     * @return \Pixafy_Pixwording_Model_Resource_Category_Product_Type_Wording_Collection
     */
    public function joinWording() {
        if (!$this->wordingJoined) {
            $this->getSelect()
                    ->join(array("wording" => "pixwording_wording"), "main_table.wording_id = wording.wording_id", "wording.*");
            $this->wordingJoined = true;
        }
        return $this;
    }
    
   public function filterByOptionId($id){
       $this->addFieldToFilter('main_table.product_type_id', array('eq' => $id));
       return $this;       
   }
   
   public function filterByCategoryId($id){
       $this->addFieldToFilter('main_table.category_id', array('eq' => $id));
       return $this;
   }
        
}
