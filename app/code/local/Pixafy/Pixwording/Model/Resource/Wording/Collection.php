<?php

class Pixafy_Pixwording_Model_Resource_Wording_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected $productWordingJoined;

    protected function _construct() {
        $this->_init('pixwording/wording');
    }

    public function leftJoinProductWordingPositions($productId ) {
        if (!$this->leftJoinedProducts) {
            $this->getSelect()
                    ->joinLeft(array("g" => "pixwording_product_wording"), "main_table.wording_id = g.wording_id AND g.product_id = " . $productId
                            , array("g.position", "g.product_wording_id" ));
            $this->leftJoinedProducts = true;
        }
        return $this;
    }

    /**
     * Filter by the paragraph
     * @param type $id
     */
    public function filterByParagraphId($id) {
        $this->addFieldToFilter('main_table.paragraph_id', array('eq' => $id));
        return $this;
    }

}
