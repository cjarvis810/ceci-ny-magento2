<?php

class Pixafy_Pixwording_Model_Resource_Wording extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct() {
        $this->_init('pixwording/wording', 'wording_id');
    }
}