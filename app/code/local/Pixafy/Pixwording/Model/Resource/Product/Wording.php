<?php

class Pixafy_Pixwording_Model_Resource_Product_Wording extends Mage_Core_Model_Resource_Db_Abstract {
    protected function _construct() {
        $this->_init('pixwording/product_wording', 'product_wording_id');
    }
}