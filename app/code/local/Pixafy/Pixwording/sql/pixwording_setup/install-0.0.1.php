<?php
$installer = $this;
$installer->startSetup();
$installer->run("

    CREATE TABLE `pixwording_category_product_type_wording` (
      `category_product_type_wording_id` int(11) NOT NULL AUTO_INCREMENT,
      `category_id` int(11) NOT NULL,
      `product_type_id` int(11) NOT NULL,
      `wording_id` int(11) NOT NULL,
      `position` int(11) NOT NULL DEFAULT '1',
      PRIMARY KEY (`category_product_type_wording_id`)
    );


    CREATE TABLE `pixwording_product_wording` (
      `product_wording_id` int(11) NOT NULL AUTO_INCREMENT,
      `product_id` int(11) DEFAULT NULL,
      `wording_id` int(11) DEFAULT NULL,
      `position` int(11) DEFAULT NULL,
      PRIMARY KEY (`product_wording_id`)
    );



    CREATE TABLE `pixwording_wording` (
      `wording_id` int(11) NOT NULL AUTO_INCREMENT,
      `template` varchar(255) NOT NULL,
      `wording_name` varchar(255) NOT NULL,
      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
      PRIMARY KEY (`wording_id`),
      UNIQUE KEY `name` (`wording_name`) USING BTREE
    );

");
$installer->endSetup();

$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();



$installer->addAttribute('catalog_product', 'use_product_wording', array(
    'label' => 'Use Product Specific Wordings',
    'input' => 'boolean',
    'type' => 'int',
    'source' => '',
    'required' => false,        
    'position' => 100
));
 
$installer->endSetup();