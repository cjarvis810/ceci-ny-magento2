<?php

class Pixafy_Pixwording_Adminhtml_CategorywordingController extends Mage_Adminhtml_Controller_Action {

    public function gridAction() {
        $params = $this->getRequest()->getParams();
        if (isset($params['action'])) {
            switch ($params['action']) {
                case 'addTemplate':
                    $this->addTemplate($params);
                    break;
                case 'massupdate':
                    $this->massUpdate($params);
                    break;
                case 'massdelete':
                    $this->massDelete($params);
                    break;
            }
        }
        $block = $this->getLayout()->createBlock('pixwording/adminhtml_category_product_type_wording_grid');
        $block->categoryId = isset($params['category_id']) ? $params['category_id'] : false;
        $block->optionId = isset($params['option_id']) ? $params['option_id'] : false;
        $this->loadLayout();
        $this->getResponse()->setBody($block->toHtml());
    }

    public function addTemplate(array $params) {
        $model = Mage::getModel('pixwording/category_product_type_wording');
        $model->setProductTypeId($params['option_id']);
        $model->setCategoryId($params['category_id']);
        $model->setWordingId($params['wording_id']);
        $db = Mage::getSingleton("core/resource")->getConnection("core_read");
        $pos = $db->fetchOne("SELECT max(position) FROM pixwording_category_product_type_wording WHERE category_id = ? AND product_type_id = ?", 
                array($params['category_id'], $params['option_id'] ));
        $pos = $pos ? $pos + 1 : 1;
        $model->setPosition($pos);
        $model->save();
    }

    public function wordingGridAction() {
        $this->loadLayout();
        $block = $this->getLayout()->createBlock('pixwording/adminhtml_wording_grid');
        $block->mode = 'choose';
        $block->setTemplate('pixafy/pixwording/categoryajaxgrid.phtml');
        $block->setRowClickCallback('');
        $this->getResponse()->setBody($block->toHtml());
    }
    
    public function massUpdate($params){
        $models = json_decode($params['items'], true);
        foreach( $models as $id => $value ){
           $model = Mage::getModel('pixwording/category_product_type_wording')->load($id);
           $model->setPosition($value);
           $model->save();
        }        
    }
    
    public function massDelete($params){
        $models = json_decode($params['items'], true);
        foreach( $models as $id => $value ){
           $model = Mage::getModel('pixwording/category_product_type_wording')->load($id);           
           $model->delete();
        }
       
    }

}
