<?php
require_once Mage::getBaseDir('code') . '/local/Pixafy/Pixhelper/Adminhtml/controllers/BasicCrudController.php';

class Pixafy_Pixwording_Adminhtml_WordingController extends Pixafy_Pixhelper_Adminhtml_BasicCrudController {
    
    public function modelName() {
        return 'wording';
    }

    public function moduleShortName() {
        return 'pixwording';
    }
   
    public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('pixwording/adminhtml_wording_grid')->toHtml()
        );
    }
   


   protected function setModelDataFromFormData($model, $data) {
       parent::setModelDataFromFormData($model, $data);
       $model->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());
       if( !$model->getId() ){
           $model->setCreatedAt(Mage::getSingleton('core/date')->gmtDate());
       }       
    }

    
}