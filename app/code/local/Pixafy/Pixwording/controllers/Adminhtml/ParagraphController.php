<?php
require_once Mage::getBaseDir('code') . '/local/Pixafy/Pixhelper/Adminhtml/controllers/BasicCrudController.php';

class Pixafy_Pixwording_Adminhtml_ParagraphController extends Pixafy_Pixhelper_Adminhtml_BasicCrudController {
    
    public function modelName() {
        return 'paragraph';
    }

    public function moduleShortName() {
        return 'pixwording';
    }
    

    
    /**
     * We'll be handling our template saving logic from here
     * Wordings are multiple to one with paragraphs
     * @return type
     */
    public function editAction() {        
        if( $this->getRequest()->getParam('template', false) !== false ){
            return $this->handleTemplates();
        }
        return parent::editAction();
    }
    
    /**
     * Sort of router withing controller here
     * @return boolean
     */
    public function handleTemplates(){
        $post = $this->getRequest()->getPost();
        $paragraph = $this->loadModelFromRequest();
        if(!$paragraph ){ //if we got here somehow just back out
            return false;
        }        
        switch ($post['action'] ){
            case 'create':
                $this->createWording($paragraph->getId(), $post['textValue']);
                break;
            case 'update':
                $this->updateWording($post['id'], $post['textValue']);
                break;
            case 'delete':
                $this->deleteWording($post['id']);
                break;
        }
        Mage::register($this->modelName . '_model', $paragraph);
        $this->loadLayout();
        $layout = $this->getLayout();
        $block = $layout->createBlock('pixwording/adminhtml_wording_templates');        
        $block->messages = $layout->getBlock('messages');
        echo $block->toHtml();
    }
    
    protected function createWording($paragraphId, $template){
        $wording = Mage::getModel('pixwording/wording' );
        $wording->setParagraphId( $paragraphId );
        $wording->setTemplate( $template );
        $wording->save();
        $this->_getSession()->addSuccess('Wording added succesfully.');
    }
    
    protected function updateWording($id, $template){
       $wording = Mage::getModel('pixwording/wording' )->load($id);
       if( !$wording->getId() ){
           $this->_getSession()->addError('Wording not found.');
           return;
       }       
       $wording->setTemplate($template);
       $wording->save();
       $this->_getSession()->addSuccess('Wording updated succesfully.');
    }
    
    protected function deleteWording($id){
       $wording = Mage::getModel('pixwording/wording' )->load($id);
       if( !$wording->getId() ){
           $this->_getSession()->addError('Wording not found.');
           return;
       }        
       $wording->delete();
       $this->_getSession()->addSuccess('Wording deleted succesfully.');
    }

}