<?php
        
class Pixafy_Pixwording_Block_Adminhtml_Paragraph extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_paragraph';
        $this->_blockGroup = 'pixwording';
        $this->_headerText = 'Paragraph';
        $this->_addButtonLabel = 'Add Paragraph';
        parent::__construct();        
    }
   
}
