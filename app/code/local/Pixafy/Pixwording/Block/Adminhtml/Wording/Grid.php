<?php

class Pixafy_Pixwording_Block_Adminhtml_Wording_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public $mode;

    public function __construct() {
        parent::__construct();
        $this->setId('wording_id');
        $this->setDefaultSort('wording_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $this->setCollection(Mage::getResourceModel('pixwording/wording_collection'));
        return parent::_prepareCollection();
    }
    
    protected function _prepareMassaction() {
        parent::_prepareMassaction();
    }
    
    protected function _prepareColumns() {

        $this->addColumn('wording_id', array(
            'header' => 'Wording Id',
            'align' => 'right',
            'width' => '50px',
            'index' => 'wording_id',
        ));

        $this->addColumn('wording_name', array(
            'header' => 'Wording Name',
            'align' => 'left',
            'index' => 'wording_name',
        ));

        $this->addColumn('template', array(
            'header' => 'Template',
            'align' => 'left',
            'index' => 'template',
        ));

        $this->addColumn('created', array(
            'header' => 'Created',
            'align' => 'left',
            "type" => 'datetime',
            'index' => 'created_at',
        ));

        $this->addColumn('updated', array(
            'header' => 'Updated',
            'align' => 'left',
            "type" => 'datetime',
            'index' => 'updated_at',
        ));


        //  'renderer' => 'Pixafy_Pixwording_Block_Adminhtml_Wording_Templaterenderer'


        return parent::_prepareColumns();
    }


    public function getRowUrl($row) {
        if ($this->mode === 'choose') {
            return '#'; //' . $row->getId();
        }
        return $this->getUrl('*/wording/edit', array('wording_id' => $row->getId()));
    }
    
   

    public function getGridUrl() {
        return $this->getUrl('*/wording/grid', array('_current' => true));
    }

}
