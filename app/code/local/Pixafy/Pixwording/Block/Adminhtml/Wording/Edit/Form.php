<?php

class Pixafy_Pixwording_Block_Adminhtml_Wording_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * If we have a model here, it means we are editing / if not it is 'new'
     * @var type 
     */
    protected $model = null;

    public function __construct(array $args = array()) {
        if ($model = Mage::registry('wording_model')) {
            $this->model = $model->getData();
        }
        parent::__construct($args);
        $this->setTemplate('pixafy/pixwording/form.phtml');
    }

    protected function _prepareForm() {

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('wording_id' => $this->getRequest()->getParam('wording_id'))),
            'method' => 'post',
                )
        );

        $legend = $this->model ? "Edit information for Wording: " . $this->model["wording_name"] : "Enter information for new Wording";

        $fieldset = $form->addFieldset('wording_form', array('legend' => $legend));
        $fieldset->addField('wording_name', 'text', array(
            'label' => 'Wording Name',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'wording_name',
        ));
        
        $fieldset->addField('template', 'textarea', array(
            'label' => 'Template',
            'class' => 'required-entry',
            'required' => true,            
            'name' => 'template',
        ));




        $form->setUseContainer(true);

        $this->setForm($form);
        if ($this->model) {
            $form->setValues($this->model);
        }

        return parent::_prepareForm();
    }

    public function getTemplates() {
        if ($this->model) {
            return Mage::getResourceModel("pixwording/wording_collection")->filterByParagraphId($this->model["wording_id"]);
        }
        
    }

}
