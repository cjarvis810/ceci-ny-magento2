<?php

class Pixafy_Pixwording_Block_Adminhtml_Wording_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function _construct() {
        parent::_construct();
        $editMode = Mage::registry('wording_model');
        $this->_objectId = 'wording_id';
        $this->_blockGroup = 'pixwording';
        $this->_controller = 'adminhtml_wording';
        $this->_mode = 'edit';
        $this->_headerText = $editMode ? 'Edit Wording' : "New Wording";
        $this->_updateButton('save', 'label', 'Save Wording');
        $this->_updateButton('delete', 'label', 'Delete Wording');
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();    }

}
