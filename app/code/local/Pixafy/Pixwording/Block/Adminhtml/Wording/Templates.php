<?php

class Pixafy_Pixwording_Block_Adminhtml_Wording_Templates extends Mage_Adminhtml_Block_Widget {

    protected $paragraphId;
    
    public $messages;

    public function __construct() {
        parent::__construct();
        $this->setTemplate('pixafy/pixwording/templates.phtml');
        if ($model = Mage::registry('paragraph_model')) {
            $this->paragraphId = $model->getParagraphId();
        }
    }

    protected function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getTemplates() {
        if ($this->paragraphId) {
            return Mage::getResourceModel("pixwording/wording_collection")->filterByParagraphId($this->paragraphId);
        }
        return array();
    }

}
