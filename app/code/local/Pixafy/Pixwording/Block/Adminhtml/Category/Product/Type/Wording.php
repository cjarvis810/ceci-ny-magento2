<?php
        
class Pixafy_Pixwording_Block_Adminhtml_Wording extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_categorywording';
        $this->_blockGroup = 'pixwording';
        $this->_headerText = 'Category Wording Templates';
        $this->_addButtonLabel = 'Add Template';
        parent::__construct();        
    }
   
}
