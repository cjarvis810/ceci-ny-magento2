<?php

class Pixafy_Pixwording_Block_Adminhtml_Category_Product_Type_Wording_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public $categoryId;
    public $optionId;

    public function __construct() {
        parent::__construct();
        $this->setId('wording_id');
        $this->setDefaultSort('wording_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setRowClickCallback('');
        $this->setMassactionBlockName('Pixafy_Pixwording_Block_Adminhtml_Category_Product_Type_Wording_Massaction');
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('pixwording/category_product_type_wording_collection')->joinWording();
        if ($this->categoryId) {
            $collection->filterByCategoryId($this->categoryId);
        }
        if ($this->optionId) {
            $collection->filterByOptionId($this->optionId);
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('wording_name', array(
            'header' => 'Wording Name',
            'align' => 'left',
            'index' => 'wording_name',
        ));

        $this->addColumn('template', array(
            'header' => 'Template',
            'align' => 'left',
            'index' => 'template',
        ));


        $this->addColumn('position', array(
            'header' => 'Position',
            'align' => 'left',
            'index' => 'position',
            "type" => 'input'
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->getMassactionBlock()->setUseAjax(true);
        $this->setMassactionIdField('mass_action');
        $this->getMassactionBlock()->setFormFieldName('mass_action');
        $this->getMassactionBlock()->addItem('update', array(
            'label' => 'Update',
            'url' => $this->getUrl('*/categorywording/massupdate', array('' => '')), // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
            'confirm' => 'Update positions?'
        ));
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => 'Delete',
            'url' => $this->getUrl('*/categorywording/massdelete', array('' => '')), // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
            'confirm' => 'Delete selected rows?'
        ));
        // options code here
        return $this;
    }

    public function getRowUrl($row) {
        return '#'; // $this->getUrl('*/wording/edit', array('wording_id' => $row->getId()));
    }

    public function getGridUrl() {
        return $this->getUrl('*/wording/grid', array('_current' => true));
    }

}
