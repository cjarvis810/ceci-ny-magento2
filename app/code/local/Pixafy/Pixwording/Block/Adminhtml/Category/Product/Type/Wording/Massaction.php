<?php

class Pixafy_Pixwording_Block_Adminhtml_Category_Product_Type_Wording_Massaction extends Mage_Adminhtml_Block_Widget_Grid_Massaction  {
    
    public function getJavascript(){       
        $x =  "{$this->getJsObjectName()} = new varienGridMassaction('{$this->getHtmlId()}', "                
                . "{$this->getGridJsObjectName()}, '{$this->getSelectedJson()}'"
                . ", '{$this->getFormFieldNameInternal()}', '{$this->getFormFieldName()}');"
                . "{$this->getJsObjectName()}.setItems({$this->getItemsJson()}); "
                . "{$this->getJsObjectName()}.setGridIds('{$this->getGridIdsJson()}');"
                . ($this->getUseAjax() ? "{$this->getJsObjectName()}.setUseAjax(true);" : '')
                . ($this->getUseSelectAll() ? "{$this->getJsObjectName()}.setUseSelectAll(true);" : '')
                . "{$this->getJsObjectName()}.errorText = '{$this->getErrorText()}';"                               
               
                . "wording_id_massactionJsObject.apply = function () {
                    window.customMassAction();
                 };"; 
                    
         return $x;
    }
}

