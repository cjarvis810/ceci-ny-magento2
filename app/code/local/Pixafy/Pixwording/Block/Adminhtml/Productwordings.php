<?php

class Pixafy_Pixwording_Block_Adminhtml_Productwordings extends Mage_Adminhtml_Block_Widget {
    
    public $product;
    
    public function __construct() {
        parent::__construct();
        $this->setTemplate('pixafy/pixwording/productwordings.phtml');
    }

    
    public function getProductWordingUrl() {
        return Mage::helper("adminhtml")->getUrl("adminhtml/productwording/grid");
    }

    public function getWordingUrl() {
        return Mage::helper("adminhtml")->getUrl("adminhtml/productwording/wordinggrid");
    }

    public function getProductTypes() {
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'product_type');
        $options = $attribute->getSource()->getAllOptions(false);
        return $options;
    }

    public function getWordings() {
        return Mage::getResourceModel('pixwording/Category_Product_Wording_Collection');
    }

    public function getGrid() {
        return $this->getLayout()->createBlock('pixwording/adminhtml_wording_grid')->toHtml();
    }

    public function getCategoryWordings() {
        return $this->getLayout()->createBlock('pixwording/adminhtml_category_product_type_wording_grid')->toHtml();
    }
    
    public function getProductWordingGrid(){
       $block = $this->getLayout()->createBlock('pixwording/adminhtml_product_wording_grid');
       $block->productId = $this->product->getId();
       return $block->toHtml(); 
    }

}
