<?php
	$installer = $this;
    $installer->startSetup();
    
    $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
    
    $entityTypeId     = $setup->getEntityTypeId('customer');
    $attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
    $attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
    
    $setup->addAttribute('customer','facebook_email',
		array(
			'type'          => 'varchar',
			'label'         => 'Facebook Email',
			'input'         => 'text',
			'required'      => false,
			'visible'       => true,
			'position'      => 0,
			'user_defined'  => 1,
		)
	);
    
    $setup->addAttributeToGroup(
    		$entityTypeId,
    		$attributeSetId,
    		$attributeGroupId,
    		'facebook_email',
    		'999'  //sort_order
    );
    
    
    $oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'facebook_email');
    $oAttribute->setData('used_in_forms', array('adminhtml_customer'));
    
    $oAttribute->save();
    $installer->endSetup();