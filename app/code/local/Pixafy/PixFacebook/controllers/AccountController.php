<?php
class Pixafy_PixFacebook_AccountController extends Mage_Core_Controller_Front_Action{
	
	protected function _getSession()
	{
		return Mage::getSingleton('customer/session');
	}
	
	public function createPostAction(){
		$session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $return = array("success" => "false", "message" => $this->__("You already logged in, please refresh the page."));
            echo json_encode($return);
            return;
        }
        $session->setEscapeMessages(true); // prevent XSS injection in user input
        $error_str = array();
        if ($this->getRequest()->isPost()) {
            $errors = array();

            if (!$customer = Mage::registry('current_customer')) {
                $customer = Mage::getModel('customer/customer')->setId(null);
            }

            /* @var $customerForm Mage_Customer_Model_Form */
            $customerForm = Mage::getModel('customer/form');
            $customerForm->setFormCode('customer_account_create')
                ->setEntity($customer);

            $customerData = $customerForm->extractData($this->getRequest());
            
            //if (intval($this->getRequest()->getParam('is_subscribed', false))) {
                $customer->setIsSubscribed(1);
            //}
			if($this->getRequest()->getParam("facebook_acc")){
				$customer->setData("facebook_acc", $this->getRequest()->getParam("facebook_acc"));
				$name = $this->getRequest()->getPost('name');
                $email = $this->getRequest()->getPost('email');
                if (!empty($email)) {
                    $customer->setData('facebook_email', $email);
                }
				if(!empty($name)){
					$fname = $customer->getData("firstname");
					$lname = $customer->getData("lastname");
					if(empty($fname)&&empty($lname)&&!empty($name)){
						$name_arr = explode(" ", $name);
						$customer->setData("firstname", isset($name_arr[0])?$name_arr[0]:"");
						$customer->setData("lastname", isset($name_arr[1])?$name_arr[1]:"");
					}
				}
			}
            /**
             * Initialize customer group id
             */
            $customer->getGroupId();

            try {
                $customerErrors = $customerForm->validateData($customerData);
                if ($customerErrors !== true) {
                    $errors = array_merge($customerErrors, $errors);
                } else {
                    $customerForm->compactData($customerData);
                    $customer->setPassword($this->getRequest()->getPost('password'));
                    $customer->setConfirmation($this->getRequest()->getPost('password'));
                    $customerErrors = $customer->validate();
                    if (is_array($customerErrors)) {
                        $errors = array_merge($customerErrors, $errors);
                    }
                }

                $validationResult = count($errors) == 0;
				
                if (true === $validationResult) {
                    $customer->save();
					Mage::dispatchEvent('customer_register_success',
                        array('account_controller' => $this, 'customer' => $customer)
                    );

                    if ($customer->isConfirmationRequired()) {
                        $customer->sendNewAccountEmail(
                            'confirmation',
                            $session->getBeforeAuthUrl(),
                            Mage::app()->getStore()->getId()
                        );
                        $return = array("success" => "true", "message" => $this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
                        echo json_encode($return);
                        //$this->_redirectSuccess(Mage::getUrl('*/*/index', array('_secure'=>true)));
                        return;
                    } else {
                    	
                        $session->setCustomerAsLoggedIn($customer);
                        $url = $this->_welcomeCustomer($customer);
                        $afterlogin_redirect = $session->getBeforeAuthUrl(true); //$this->getRequest()->getPost('afterlogin_redirect') != "false"?$this->getRequest()->getPost('afterlogin_redirect'):false;
                        $return = array("success" => "true", "message" => "", "redirect" => $afterlogin_redirect?$afterlogin_redirect:$this->_redirectSuccess($url));
                        echo json_encode($return);
                        return;
                    }
                } else {
                    $session->setCustomerFormData($this->getRequest()->getPost());
                    if (is_array($errors)) {
                        foreach ($errors as $errorMessage) {
                            $error_str[] = $errorMessage;
                        }
                    } else {
                    	$error_str[] = $session->addError();
                    }
                }
            } catch (Mage_Core_Exception $e) {
                $session->setCustomerFormData($this->getRequest()->getPost());
                if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                    $url = Mage::getUrl('customer/account/forgotpassword');
                    $error_str[] = $this->__('Please click the SIGN IN box above.', $url);
                    $switch_form = true;
                    $session->setEscapeMessages(false);
                } else {
                    $error_str[] = $e->getMessage();
                }
            } catch (Exception $e) {
            	$error_str[] = $this->__('Cannot save the customer.'); 
                
            }
        }
        $return = array("success" => "false", "message" => $error_str, "switch_form" => isset($switch_form)?"true":"false");
		echo json_encode($return);
	}
	
	
	protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
	{
		$this->_getSession()->addSuccess(
			$this->__('Thank you for registering with %s.', Mage::app()->getStore()->getFrontendName())
		);
		if ($this->_isVatValidationEnabled()) {
			// Show corresponding VAT message to customer
			$configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType();
			$userPrompt = '';
			switch ($configAddressType) {
				case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
					$userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation', Mage::getUrl('customer/address/edit'));
					break;
				default:
					$userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation', Mage::getUrl('customer/address/edit'));
			}
			$this->_getSession()->addSuccess($userPrompt);
		}
	
		$customer->sendNewAccountEmail(
				$isJustConfirmed ? 'confirmed' : 'registered',
				'',
				Mage::app()->getStore()->getId()
		);
	
		$successUrl = Mage::getUrl('customer/*/index', array('_secure'=>true));
		if ($this->_getSession()->getBeforeAuthUrl()) {
			$successUrl = $this->_getSession()->getBeforeAuthUrl(true);
		}
		return $successUrl;
	}
	
	protected function _redirectSuccess($defaultUrl)
	{
		$custom_referer = $this->getRequest()->getParam("custom_referer") == "0"?false:$this->getRequest()->getParam("custom_referer");
		if($custom_referer){
			return $custom_referer;
		}
		//else{
		//	return Mage::getBaseUrl();
		//}
		$successUrl = $this->getRequest()->getParam(self::PARAM_NAME_SUCCESS_URL);
		if (empty($successUrl)) {
			$successUrl = $defaultUrl;
		}
		if (!$this->_isUrlInternal($successUrl)) {
			$successUrl = Mage::app()->getStore()->getBaseUrl();
		}
		
		return $successUrl;
	}
	
	protected function _redirectSuccessStandard($defaultUrl)
	{
		$successUrl = $this->getRequest()->getParam(self::PARAM_NAME_SUCCESS_URL);
		if (empty($successUrl)) {
			$successUrl = $defaultUrl;
		}
		if (!$this->_isUrlInternal($successUrl)) {
			$successUrl = Mage::app()->getStore()->getBaseUrl();
		}
		$this->getResponse()->setRedirect($successUrl);
		return $this;
	}
	
	protected function _isVatValidationEnabled($store = null)
	{
		return Mage::helper('customer/address')->isVatValidationEnabled($store);
	}
	
	
	public function loginPostAction()
	{
		if ($this->_getSession()->isLoggedIn()) {
			$return = array("success" => "false", "message" => $this->__("You already logged in, please refreash page."));
            echo json_encode($return);
			return;
		}
		$session = $this->_getSession();
	
		if ($this->getRequest()->isPost()) {
			$login["username"] = $this->getRequest()->getPost('email');
			$login["password"] = $this->getRequest()->getPost('password');
			if (!empty($login['username']) && !empty($login['password'])) {
				try {
					$session->login($login['username'], $login['password']);
					if ($session->getCustomer()->getIsJustConfirmed()) {
						$this->_welcomeCustomer($session->getCustomer(), true);
					}
					$success = "true";
					$message = "";
				} catch (Mage_Core_Exception $e) {
					switch ($e->getCode()) {
						case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
							$value = Mage::helper('customer')->getEmailConfirmationUrl($login['username']);
							$message = Mage::helper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
							$success = "false";
							break;
						case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
							$success = "false";
							$message = $e->getMessage().$this->__(" Forgot password? <a href='%s'>Click here</a>", Mage::getUrl('customer/account/forgotpassword'));
							break;
						default:
							$success = "false";
							$message = $e->getMessage();
							break;
					}
					$session->setUsername($login['username']);
				} catch (Exception $e) {
					// Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
				}
			} else {
				$success = "false";
				$message = $this->__('Login and password are required.');
			}
		}
		if($success == "true"){
			$redirect = $this->_loginPostRedirect();
		}else{
			$redirect = "";
		}
		$return = array("success" => $success, "message" => $message, "redirect" => $redirect);
		
		echo json_encode($return);
	}
	
	protected function _loginPostRedirect()
	{
		
		$session = $this->_getSession();
		$custom_referer = $this->getRequest()->getParam("custom_referer") == "0"?false:$this->getRequest()->getParam("custom_referer");
		if($custom_referer){
			return $custom_referer;
		}
		if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
			// Set default URL to redirect customer to
			$session->setBeforeAuthUrl(Mage::helper('customer')->getAccountUrl());
			// Redirect customer to the last page visited after logging in
			if ($session->isLoggedIn()) {
				if (!Mage::getStoreConfigFlag(
						Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
				)) {
					$referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
					if ($referer) {
						// Rebuild referer URL to handle the case when SID was changed
						$referer = Mage::getModel('core/url')
						->getRebuiltUrl(Mage::helper('core')->urlDecode($referer));
						if ($this->_isUrlInternal($referer)) {
							$session->setBeforeAuthUrl($referer);
						}
					}
				} else if ($session->getAfterAuthUrl()) {
					$session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
				}
			} else {
				$session->setBeforeAuthUrl(Mage::helper('customer')->getLoginUrl());
			}
		} else if ($session->getBeforeAuthUrl() == Mage::helper('customer')->getLogoutUrl()) {
			$session->setBeforeAuthUrl(Mage::helper('customer')->getDashboardUrl());
		} else {
			if (!$session->getAfterAuthUrl()) {
				$session->setAfterAuthUrl($session->getBeforeAuthUrl());
			}
			if ($session->isLoggedIn()) {
				$session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
			}
		}
		return $session->getBeforeAuthUrl(true);
	}
	
	
	function socloginAction(){

		if ($this->_getSession()->isLoggedIn()) {
			$return = array("success" => "false", "message" => $this->__("You already logged in, please refresh the page."));
			echo json_encode($return);
			return;
		}

		$session = $this->_getSession();
		if($this->getRequest()->getPost('password')){
			$pass = $this->getRequest()->getPost('password');
		}else{
			$pass = substr(md5(mt_rand()), 0, 10);
			$this->getRequest()->setPost("password", $pass);
		}
		
		if ($this->getRequest()->isPost()) {
			$login = $this->getRequest()->getPost('email');
			if (!empty($login)) {
				try {
					$collection = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("entity_id")->addAttributeToFilter("email", array("eq" => $login));
					if(count($collection)){
						$customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getWebsite()->getId())->loadByEmail($login);
						//if($this->getRequest()->getPost('password')){
						//	$session->login($login, $pass);
						//}else{
							$session->setCustomerAsLoggedIn($customer);
						//}
						$redirect = $this->_loginPostRedirect();
						if($this->getRequest()->getPost('twitter_acc')){
							$customer->setData("twitter_acc", $this->getRequest()->getPost('twitter_acc'));
							$customer->save();
						}
						if($this->getRequest()->getPost('facebook_acc')){
							$name = $this->getRequest()->getPost('name');
							if(!empty($name)){
								$fname = $customer->getData("firstname");
								$lname = $customer->getData("lastname");
								if(empty($fname)&&empty($lname)&&!empty($name)){
									$name_arr = explode(" ", $name);
									$customer->setData("firstname", isset($name_arr[0])?$name_arr[0]:"");
									$customer->setData("lastname", isset($name_arr[1])?$name_arr[1]:"");
								}
							}
							//$filename = $this->uploadImg($this->getRequest()->getPost('image'));
// 							if($filename){
// 								$customer->setData("image", $filename);
// 							}
							$customer->setData("facebook_acc", $this->getRequest()->getPost('facebook_acc'));
							
							$customer->save();
						}
						$success = "true";
						$message = "";

						$afterlogin_redirect = ($this->getRequest()->getPost('facebook_acc')) ? $this->_getRefererUrl() : $session->getBeforeAuthUrl(true); //$this->getRequest()->getPost('afterlogin_redirect') != "false"?$this->getRequest()->getPost('afterlogin_redirect'):false;
						$return = array("success" => $success, "message" => $message, "redirect" => $afterlogin_redirect?$afterlogin_redirect:$redirect);
						echo json_encode($return);
						return;
						//$session->login($login['username'], $login['password']);
						//$return = array("success" => "true", "message" => "");
					}else{
						
						$this->createPostAction();
						return;
						///$return = array("success" => "false", "message" => $this->__('This email not registered in our database yet. Please click "Sign Up" to register this email.'));
					}
				} catch (Mage_Core_Exception $e) {
					switch ($e->getCode()) {
						case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
							$value = Mage::helper('customer')->getEmailConfirmationUrl($login['username']);
							$message = Mage::helper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
							$success = "false";
							break;
						case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
							$success = "false";
							$message = $e->getMessage();
							break;
						default:
							$success = "false";
							$message = $e->getMessage();
						break;
					}
					$return = array("success" => "false", "message" => $message);
				} catch (Exception $e) {
					// Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
					$return = array("success" => "false", "message" => $this->__("Unknown error. Please contact administration."));
				}
			}
			
		}
		if(!isset($return)){
			$return = array("success" => "false", "message" => $this->__("Unknown error. Please contact administration."));
		}
		echo json_encode($return);
	}
			
	public function uploadImg($fileName)
	{
		if(preg_match("/https\:\/\//", $fileName)){
			$original_file = preg_replace("/https\:\/\//", "http://", $fileName);
		}else{
			$original_file = $fileName;
		}
		ob_start();
		$channel = curl_init();
		curl_setopt($channel, CURLOPT_URL, $original_file);
		curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($channel, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($channel, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($channel, CURLOPT_HEADER, 0);
		$fileBytes = curl_exec($channel);
		curl_close($channel);
		$cont = ob_get_contents();
		ob_end_clean();
		//var_dump($cont);
		//var_dump(sizeof($fileBytes));
		if(sizeof($cont)){
			$filename_arr = explode("/", $fileName);
			$fileName = $filename_arr[sizeof($filename_arr) - 1];
			//echo($fileName);
			$folders[0] = $fileName{0};
			$folders[1] = $fileName{1};
			$path = Mage::getBaseDir("media")."/customer/".$folders[0]."/".$folders[1];
			//$path = Mage::getBaseDir("media")."/customer/";
			if(!is_dir($path)){
				mkdir($path, 0, true);
			}
			if(is_dir($path)){
				chmod(Mage::getBaseDir("media")."/customer/".$folders[0], 0775);
				chmod(Mage::getBaseDir("media")."/customer/".$folders[0]."/".$folders[1], 0775);
			
				$fileWritter = fopen($path."/".$fileName, 'w');
				fwrite($fileWritter, $cont);
				fclose($fileWritter);
			}
			return "/".$folders[0]."/".$folders[1]."/".$fileName;
			//$result = downloadImageFromUrl($original_file, $path."/".$fileName);
			//var_dump($result);
			//file_put_contents($path."/".$fileName, $file);
		}else{
			return false;
		}
		
	}
	
	public function editPostAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/edit');
        }

        if ($this->getRequest()->isPost()) {
            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getSession()->getCustomer();

            /** @var $customerForm Mage_Customer_Model_Form */
            $customerForm = Mage::getModel('customer/form');
            $customerForm->setFormCode('customer_account_edit')
                ->setEntity($customer);

            $customerData = $customerForm->extractData($this->getRequest());

            $errors = array();
            //$customerErrors = $customerForm->validateData($customerData);
            //if ($customerErrors !== true) {
                //$errors = array_merge($customerErrors, $errors);
            //} else {
                $customerForm->compactData($customerData);
                $errors = array();

                // If password change was requested then add it to common validation scheme
                

                // Validate account and compose list of errors if any
                if (is_array($customerErrors)) {
                    $errors = array_merge($errors, $customerErrors);
                }
            //}

            if (!empty($errors)) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
                foreach ($errors as $message) {
                    $this->_getSession()->addError($message);
                }
                $this->_redirect('customer/account/edit');
                return $this;
            }

            try {
                
                $customer->save();
                $this->_getSession()->setCustomer($customer);
                    //->addSuccess($this->__('Image has been saved.'));

                $this->_redirect('customer/account/edit');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
            }
        }

        $this->_redirect('customer/account/edit');
    }
    
    public function forgotPasswordPostAction()
    {
    	$email = (string) $this->getRequest()->getPost('email');
    	if ($email) {
    		if (!Zend_Validate::is($email, 'EmailAddress')) {
    			$this->_getSession()->setForgottenEmail($email);
    			//$this->_getSession()->addError($this->__('Invalid email address.'));
    			//$this->_redirect('*/*/forgotpassword');
    			$return = array("success" => "false", "message" => $this->__('Invalid email address.'));
    			echo json_encode($return);
    			return;
    		}
    
    		/** @var $customer Mage_Customer_Model_Customer */
    		$customer = Mage::getModel('customer/customer')
    		->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
    		->loadByEmail($email);
    
    		if ($customer->getId()) {
    			try {
    				$newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
    				$customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
    				$customer->sendPasswordResetConfirmationEmail();
    			} catch (Exception $exception) {
    				//$this->_getSession()->addError($exception->getMessage());
    				//$this->_redirect('*/*/forgotpassword');
    				$return = array("success" => "false", "message" => $exception->getMessage());
    				echo json_encode($return);
    				return;
    			}
    		}
    		//$this->_getSession()
    		//->addSuccess(Mage::helper('customer')->__('If there is an account associated with %s you will receive an email with a link to reset your password.', Mage::helper('customer')->htmlEscape($email)));
    		//$this->_redirect('*/*/');
    		$return = array("success" => "true", "message" => Mage::helper('customer')->__('If there is an account associated with %s you will receive an email with a link to reset your password.', Mage::helper('customer')->htmlEscape($email)));
    		echo json_encode($return);
    		return;
    	} else {
    		//$this->_getSession()->addError($this->__('Please enter your email.'));
    		//$this->_redirect('*/*/forgotpassword');
    		$return = array("success" => "false", "message" => $this->__('Please enter your email.'));
    		echo json_encode($return);
    		return;
    	}
    }
    

    
    public function createPostCustomAction()
    {
    	$session = $this->_getSession();
    	if ($session->isLoggedIn()) {
    		$this->_redirect('*/*/');
    		return;
    	}
    	$session->setEscapeMessages(true); // prevent XSS injection in user input
    	if ($this->getRequest()->isPost()) {
    		$errors = array();
    
    		if (!$customer = Mage::registry('current_customer')) {
    			$customer = Mage::getModel('customer/customer')->setId(null);
    		}
    
    		/* @var $customerForm Mage_Customer_Model_Form */
    		$customerForm = Mage::getModel('customer/form');
    		$customerForm->setFormCode('customer_account_create')
    		->setEntity($customer);
    
    		$customerData = $customerForm->extractData($this->getRequest());
    
    		if ($this->getRequest()->getParam('is_subscribed', false)) {
    			$customer->setIsSubscribed(1);
    		}
    
    		/**
    		 * Initialize customer group id
    		 */
    		$customer->getGroupId();
    
    		if ($this->getRequest()->getPost('create_address')) {
    			/* @var $address Mage_Customer_Model_Address */
    			$address = Mage::getModel('customer/address');
    			/* @var $addressForm Mage_Customer_Model_Form */
    			$addressForm = Mage::getModel('customer/form');
    			$addressForm->setFormCode('customer_register_address')
    			->setEntity($address);
    
    			$addressData    = $addressForm->extractData($this->getRequest(), 'address', false);
    			$addressErrors  = $addressForm->validateData($addressData);
    			if ($addressErrors === true) {
    				$address->setId(null)
    				->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
    				->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));
    				$addressForm->compactData($addressData);
    				$customer->addAddress($address);
    
    				$addressErrors = $address->validate();
    				if (is_array($addressErrors)) {
    					$errors = array_merge($errors, $addressErrors);
    				}
    			} else {
    				$errors = array_merge($errors, $addressErrors);
    			}
    		}
    
    		try {
    			$customerErrors = $customerForm->validateData($customerData);
    			if ($customerErrors !== true) {
    				$errors = array_merge($customerErrors, $errors);
    			} else {
    				$customerForm->compactData($customerData);
    				$customer->setPassword($this->getRequest()->getPost('password'));
    				$customer->setConfirmation($this->getRequest()->getPost('confirmation'));
    				$customerErrors = $customer->validate();
    				if (is_array($customerErrors)) {
    					$errors = array_merge($customerErrors, $errors);
    				}
    			}
    
    			$validationResult = count($errors) == 0;
    
    			if (true === $validationResult) {
    		//		Mage::getSingleton("customer/session")->setSource(Pixafy_Pixlistrak_Helper_Data::XML_FB_SOURCE_ID_PATH);
			//		Mage::getSingleton("customer/session")->setSourceAttribtueValue(Pixafy_Pixlistrak_Helper_Data::XML_FB_SOURCE_ATTRIBUTE_VALUE_ID_PATH);
					$customer->save();
					Mage::dispatchEvent('customer_register_success',
    				array('account_controller' => $this, 'customer' => $customer
                   // , 'source' => Pixafy_Pixlistrak_Helper_Data::SOURCE_TYPE_FACEBOOK
                    )
    				);
    
    				if ($customer->isConfirmationRequired()) {
    					$customer->sendNewAccountEmail(
    							'confirmation',
    							$session->getBeforeAuthUrl(),
    							Mage::app()->getStore()->getId()
    					);
    					$session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
    					$this->_redirectSuccess(Mage::getUrl('customer/*/index', array('_secure'=>true)));
    					return;
    				} else {
    					$session->setCustomerAsLoggedIn($customer);
    					$url = $this->_welcomeCustomer($customer);
    					$this->_redirectSuccessStandard($url);
    					return;
    				}
    			} else {
    				$session->setCustomerFormData($this->getRequest()->getPost());
    				if (is_array($errors)) {
    					foreach ($errors as $errorMessage) {
    						$session->addError($errorMessage);
    					}
    				} else {
    					$session->addError($this->__('Invalid customer data'));
    				}
    			}
    		} catch (Mage_Core_Exception $e) {
    			$session->setCustomerFormData($this->getRequest()->getPost());
    			if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
    				$url = Mage::getUrl('customer/account/forgotpassword');
    				$message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
    				$session->setEscapeMessages(false);
    			} else {
    				$message = $e->getMessage();
    			}
    			$session->addError($message);
    		} catch (Exception $e) {
    			$session->setCustomerFormData($this->getRequest()->getPost())
    			->addException($e, $this->__('Cannot save the customer.'));
    		}
    	}
    
    	$this->_redirectError(Mage::getUrl('customer/*/create', array('_secure' => true)));
    }
    
	
	public function loginRedirectAction()
	{
		echo $this->getLayout()->createBlock("core/template")->setTemplate("pixfacebook/customer/login_redirect.phtml")->setRedirectUrl(Mage::getUrl(Mage::getStoreConfig('pixfacebook/redirect/login'), array("_secure" => true)))->toHtml();
        return;
	}
    
}