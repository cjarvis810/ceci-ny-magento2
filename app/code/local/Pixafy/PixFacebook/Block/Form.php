<?php 
class Pixafy_PixFacebook_Block_Form extends Mage_Core_Block_Template{
	
	function getCheckoutUrl(){
		$url = Mage::getStoreConfig('pixfacebook/redirect/checkout', Mage::app()->getStore());
		return $url&&!empty($url)?$this->getUrl($url):$this->getUrl();
	}

	function getLoginUrl(){
		$url = Mage::getStoreConfig('pixfacebook/redirect/login', Mage::app()->getStore());
		return $url&&!empty($url)?$this->getUrl($url):$this->getUrl();
	}
	
	function getRegisterUrl(){
		$url = Mage::getStoreConfig('pixfacebook/redirect/register', Mage::app()->getStore());
		return $url&&!empty($url)?$this->getUrl($url):$this->getUrl();
	}

	function isAllowedOnRegister(){
		return Mage::getStoreConfig('pixfacebook/status/register', Mage::app()->getStore());
	}
	
	function isAllowedOnLogin(){
		return Mage::getStoreConfig('pixfacebook/status/login', Mage::app()->getStore());
	}
	
	function isAllowedOnCheckout(){
		return Mage::getStoreConfig('pixfacebook/status/checkout', Mage::app()->getStore());
	}
	
}
?>