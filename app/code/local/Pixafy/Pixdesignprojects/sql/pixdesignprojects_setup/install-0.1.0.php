<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 6/5/2015
 * Time: 2:24 PM
 */

$installer = $this;
$installer->startSetup();


$installer->run("
    CREATE TABLE `pixdesignprojects_pixdesigns` (
    `sales_grid_id`  int NOT NULL AUTO_INCREMENT ,
    `customer_id`   int(10) NOT NULL ,
    `quote_item_id`  int(10) NOT NULL ,
    `design_project_id`  int(10) NOT NULL ,
    `qty` int(11) ,
    `last_updated`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
    PRIMARY KEY (`sales_grid_id`)
    );

");


$installer->endSetup();