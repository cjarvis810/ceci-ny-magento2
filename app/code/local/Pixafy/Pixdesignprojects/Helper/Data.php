<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 5/13/2015
 * Time: 12:27 PM
 */

class Pixafy_Pixdesignprojects_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function removeAllRelatedDesignProjectQuoteItems($designProjectId)
    {
        // Query the pivot table and get all quote_item_ids
        $records = Mage::getModel("pixdesignprojects/pixdesigns")
                            ->getCollection()
                            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
                            ->addFieldToFilter('design_project_id', $designProjectId);



        foreach($records as $record) {

            // Delete all records for this customer for this design project on the pivot table if the following conditions are met
            $quoteItem = Mage::getModel('sales/quote_item')->load($record->getData('quote_item_id'));

            if($quoteItem) {
                // If the quantities equal each other then just remove the entire quote item
                if($record->getData('qty') == intval($quoteItem->getData('qty'))) {

                    $quoteItem->delete();

                } else {
                    // Otherwise decrease qty by QuoteItem - record->qty
                    $qty = $record->getData('qty') - intval($quoteItem->getData('qty'));

                    // Update quantity //
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->query("UPDATE sales_flat_quote_item SET qty=".$qty." WHERE quote_id=". Mage::getSingleton('checkout/session')->getQuoteId());

                }

            }

            $record->delete();
        }

    }


    public function activateDesignProject($designProjectId)
    {

        $this->deactivateAllOtherDesignProjects();

        if(intval($designProjectId)) {

            // Update quantity //
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $write->query("UPDATE sales_flat_quote SET design_project_status=1 WHERE entity_id=".intval($designProjectId)." AND customer_id=". Mage::getSingleton('customer/session')->getId());
        }


    }

    protected function deactivateAllOtherDesignProjects()
    {

        // Update quantity //
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $write->query("UPDATE sales_flat_quote SET design_project_status=0 WHERE customer_id=". Mage::getSingleton('customer/session')->getId());


    }

}