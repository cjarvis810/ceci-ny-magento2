<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 6/8/2015
 * Time: 9:21 AM
 */

class Pixafy_Pixdesignprojects_Model_Resource_Pixdesigns extends
    Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct()
    {
        $this->_init("pixdesignprojects/pixdesigns", "sales_grid_id");
    }
}