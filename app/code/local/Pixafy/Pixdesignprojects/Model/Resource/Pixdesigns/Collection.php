<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 6/8/2015
 * Time: 9:24 AM
 */

class Pixafy_Pixdesignprojects_Model_Resource_Pixdesigns_Collection extends
    Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init("pixdesignprojects/pixdesigns");
    }
}