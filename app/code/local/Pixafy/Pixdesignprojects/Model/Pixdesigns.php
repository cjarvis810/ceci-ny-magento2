<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 6/8/2015
 * Time: 9:16 AM
 */

class Pixafy_Pixdesignprojects_Model_Pixdesigns extends
    Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('pixdesignprojects/pixdesigns');
    }
}