<?php
/**
 * Created by PhpStorm.
 * User: jjones
 * Date: 5/13/2015
 * Time: 10:29 AM
 */

class Pixafy_Pixdesignprojects_ProjectsController extends Mage_Core_Controller_Front_Action
{

    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }


    public function indexAction()
    {
        $this->loadLayout();

        // Create form block

        $this->renderLayout();
    }

    public function setActiveDesignProjectAction()
    {
        $params = $this->getRequest()->getParams();

        if($params && intval(key($params))) {

            $designQuoteId = intval(key($params));

            // Activate this design project
            Mage::helper('Pixdesignprojects')->activateDesignProject($designQuoteId);

        }

        $this->_redirectReferer();
    }

    public function removeAction()
    {
        $params = $this->getRequest()->getParams();

        if(!empty($params) && intval(key($params))) {

            $designQuoteId = intval(key($params));

            // Remove all quote items attached to this design project
            Mage::helper('Pixdesignprojects')->removeAllRelatedDesignProjectQuoteItems($designQuoteId);


            $model = Mage::getModel('sales/quote')
                ->getCollection()
                ->addFieldtoFilter('entity_id', $designQuoteId)
                ->addFieldtoFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->addFieldtoFilter('design_project_name', array('notnull'=>true))
                ->getFirstItem();

            $model->delete();
        }

        $this->_redirectReferer();

    }

    public function saveAction()
    {
        $params = $this->getRequest()->getParams();

        $params['customer_id'] = Mage::getSingleton('customer/session')->getCustomer()->getId();

        // Deactivate all other quotes for this customer
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $write->query("UPDATE sales_flat_quote SET design_project_status=0 WHERE customer_id=". intval(Mage::getSingleton('customer/session')->getCustomer()->getId()));

        /*
        $model = Mage::getModel("sales/quote")
                    ->getCollection()
                    ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())*/

        // Create new quote
        $params['quote_design_id'] =  $_quote = Mage::getSingleton('checkout/session')->getQuote()->getId();
        $model = Mage::getModel('sales/quote')->setData($params);
        $model->save();

        $_quote = Mage::getSingleton('checkout/session')->getQuote();

        $this->_redirectReferer();
    }

    protected function createNewQuote()
    {
        //$this->setCurrentQuoteInactive();
        $customer_id = Mage::getSingleton('customer/session')->getId();
        $store_id = Mage::app()->getStore()->getId();

        // Get the customer object
        $customerObj = Mage::getModel('customer/customer')->load($customer_id);

        // Assign the new quote to the customer
        $quoteObj = Mage::getModel('sales/quote')->assignCustomer($customerObj);
        $storeObj = $quoteObj->getStore()->load($store_id);
        $quoteObj->setStore($storeObj);
        $quoteObj->setStoreId($store_id);
        $quoteObj->setIsActive(true);
        $quoteObj->save();

        return $quoteObj->getId();
    }

    protected function setCurrentQuoteInactive()
    {
        $quote = Mage::getSingleton('sales/quote');

        // Set inactive
        $quote->setIsActive(0);
        $quote->save();

    }
}