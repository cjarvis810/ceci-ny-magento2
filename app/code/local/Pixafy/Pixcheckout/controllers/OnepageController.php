<?php

require_once Mage::getBaseDir('code') . "/core/Mage/Checkout/controllers/OnepageController.php";
class Pixafy_Pixcheckout_OnepageController extends Mage_Checkout_OnepageController
{
    public function totalAction(){
        if ($this->_expireAjax()) {
            return null;
        }
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('checkout_onepage_progress_totals');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();
        $this->getResponse()->setBody($output);
        return $output;
    }
}