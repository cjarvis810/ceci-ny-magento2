<?php
require_once Mage::getBaseDir('code') . "/core/Mage/Checkout/controllers/CartController.php";
class Pixafy_Pixcheckout_CartController extends Mage_Checkout_CartController
{


    private function _createMinicart(){

        $this->loadLayout();

        $miniCart = $this->getLayout()->createBlock('checkout/cart_sidebar');
        $miniCart->setTemplate('checkout/cart/cartheader.phtml');
        $miniCart->addItemRender('simple', 'checkout/cart_item_renderer', 'checkout/cart/sidebar/default.phtml');
        $miniCart->addItemRender('grouped', 'checkout/cart_item_renderer_grouped', 'checkout/cart/sidebar/default.phtml');
        $miniCart->addItemRender('configurable', 'checkout/cart_item_renderer_configurable', 'checkout/cart/sidebar/default.phtml');

        return $miniCart->toHtml();

    }

    public function addAction()
    {
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        if($params['isAjax'] == 1){
            $response = array();
            try {
                if (isset($params['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }

                $product = $this->_initProduct();
                $related = $this->getRequest()->getParam('related_product');

                /**
                 * Check product availability
                 */
                if (!$product) {
                    $response['status'] = 'ERROR';
                    $response['message'] = $this->__('Unable to find Product ID');
                }

                $cart->addProduct($product, $params);
                if (!empty($related)) {
                    $cart->addProductsByIds(explode(',', $related));
                }

                $cart->save();

                $this->_getSession()->setCartWasUpdated(true);

                /**
                 * @todo remove wishlist observer processAddToCart
                 */
                Mage::dispatchEvent('checkout_cart_add_product_complete',
                    array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
                );

                if (!$this->_getSession()->getNoCartRedirect(true)) {
                    if (!$cart->getQuote()->getHasError()){
                        $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));

                        $response['status'] = 'SUCCESS';
                        $response['message'] = $message;
                        $this->loadLayout();
                        //$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
                        //$sidebar = $this->getLayout()->getBlock('cart_sidebar')->toHtml();
                        //$response['toplink'] = $toplink;
                        $response['minicart'] = $this->_createMinicart();
//                        $topCart = $this->getLayout()->getBlock('cart_sidebar')->toHtml();
//                        $response['topCartContent'] = $topCart;


                        $response['totalqty'] = Mage::helper('checkout/cart')->getSummaryCount();

                    }
                }
            } catch (Mage_Core_Exception $e) {
                $msg = "";
                if ($this->_getSession()->getUseNotice(true)) {
                    $msg = $e->getMessage();
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $msg .= $message.'<br/>';
                    }
                }

                $response['status'] = 'ERROR';
                $response['message'] = $msg;
            } catch (Exception $e) {
                $response['status'] = 'ERROR';
                $response['message'] = $this->__('Cannot add the item to shopping cart.');
                Mage::logException($e);
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;


        }else{
            return parent::addAction();
        }
    }
}