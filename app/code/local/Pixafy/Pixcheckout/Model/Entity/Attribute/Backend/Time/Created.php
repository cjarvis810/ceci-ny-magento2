<?php
class Pixafy_Pixcheckout_Model_Entity_Attribute_Backend_Time_Created extends Mage_Eav_Model_Entity_Attribute_Backend_Time_Created
{
    public function beforeSave($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();

        $date = $object->getData($attributeCode);

        if (is_null($date)) {
            if ($object->isObjectNew()) {

                $object->setData($attributeCode, Varien_Date::now());
            }
        } else {
            $zendDate = Mage::app()->getLocale()->utcDate(null, $date, true, $this->_getFormat($date));
            $object->setData($attributeCode, Mage::getModel('core/date')->gmtDate());
        }

        return $this;
    }
}
?>