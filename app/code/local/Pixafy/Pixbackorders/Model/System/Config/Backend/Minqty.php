<?php

class Pixafy_Pixbackorders_Model_System_Config_Backend_Minqty extends Mage_CatalogInventory_Model_System_Config_Backend_Minqty
{
    protected function _beforeSave()
    {
        $minQty = (int)$this->getValue();
        $this->setValue((string) $minQty);
        return $this;
    }
}
