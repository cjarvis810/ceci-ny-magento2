<?php
class Pixafy_Pixestimateddeliverydate_Helper_Data extends Wyomind_Estimateddeliverydate_Helper_Data {
    public function getFromDate($_product, $storeId, $additionalFrom = 0, $additionalTo = 0) {
        $inventory = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
        $manage_stock = $inventory->getManageStock() || ($inventory->getUseConfigManageStock() && Mage::getStoreConfig("cataloginventory/item_options/manage_stock", $storeId));
        $type = ((int) $inventory->getQty() <= 0 && $manage_stock) ? "backorders" : "orders";

        if ($_product->isAvailable()) {
            $dateFormat = Mage::getStoreConfig("estimateddeliverydate/display_settings/date_format", $storeId);
            $leadtime = $this->getLeadtime($_product, $storeId, $type, $additionalFrom, $additionalTo, false);
            return  $this->dateTranslate(Mage::getSingleton('core/date')->date($dateFormat, Mage::getSingleton('core/date')->gmtTimestamp() + 86400 * $leadtime["from"]));
        }
    }
}
?>