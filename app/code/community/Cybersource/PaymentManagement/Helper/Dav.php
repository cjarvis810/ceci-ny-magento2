<?php

/**
 *  CyberSource PaymentManagement Delivery Address Verification Helper
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Helper_Dav extends Mage_Core_Helper_Abstract
{

    const APPROVED = 'approved';
    const DISAPPROVED = 'disapproved';
    const NOMATCH = 'nomatch';



    /**
     * Checks if shipping address country has been disapproved
     *
     * @return bool
     */
    public function wasDeliveryAddressDisapproved() {
        $mark = Mage::registry('DeliveryAddressDisapproved');
        if ($mark) {
            return true;
        } else {
            return false;
        }

    }


    /**
     * Examines CyberSource reason codes to see if shipping address country is disapproved
     *
     * @return bool
     */
    public function isDeliveryAddressGettingDisapproved($davResponse) {
        $davReplyNode = $davResponse->getResponse()->davReply;
        if (isset($davReplyNode)) {
            $receivedReasonCode = $davReplyNode->reasonCode;
            if (isset($receivedReasonCode)) {
                $disapprovingReasonCodes = array(450, 451, 452, 453, 454, 455, 456, 457,
                        458, 459, 460, 461, 700, 701, 702, 703);
                if (in_array($receivedReasonCode, $disapprovingReasonCodes)) {
                    return true;
                } else {
                    return false;
                }
            }
        }

    }


    /**
     * Generate a hashed digest of address
     *
     * @param  $address
     * @return string
     */
    public function createHashedAddressDigest(Mage_Customer_Model_Address_Abstract $address)
    {
        $addressDigest = trim($address->getData('street')) .
        trim($address->getData('city')) .
        trim($address->getData('region')) .
        trim($address->getData('postcode')) .
        trim($address->getData('country_id'));
        $hashedAddressDigest = hash('md5', $addressDigest	, false);
        return $hashedAddressDigest;
    }


    /**
     * Mark delivery (shipping) address as disapproved (not verified)
     *
     * @see Cybersource_PaymentManagement_Model_Observer
     * @return void
     */
    public function markDeliveryAddressDisapproved() {
        Mage::register('DeliveryAddressDisapproved', true);
    }


    /**
     * Remove disapproved mark (if there is any) for delivery (shipping) address
     *
     * @return void
     */
    public function removeDeliveryAddressDisapprovedMark() {
        $mark = Mage::registry('DeliveryAddressDisapproved');
        if($mark) {
            Mage::unregister('DeliveryAddressDisapproved');
        }
    }


}