<?php
/**
 *  CyberSource Credit Card Processing (Payment) Helper
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */



class Cybersource_PaymentManagement_Helper_Payment extends Mage_Core_Helper_Abstract
{


	/*
	* Add payment update to order's comment history
	* @param $payment
	* @param $subscriptionId
	*/
	public function addComment(Varien_Object $payment, $response) {
	    $comment = '';
	    if (isset($response->getData('response')->decision)) {
	        $decision = $response->getData('response')->decision;
	        if($decision) {
	            $comment .= Mage::helper('cybersource')->__('CyberSource Payment Decision:');
	            $comment .=  $decision;
	            $comment .= ' ; ';
	        }
	    }
	    if (isset($response->getData('response')->reasonCode)) {
	        $reasonCode = $response->getData('response')->reasonCode;
	        if($reasonCode) {
	            $comment .= Mage::helper('cybersource')->__('Reason Code:');
	            $comment .= $reasonCode;
	            $comment .= ' ; ';
	        }
	    }
	    if (isset($response->getData('response')->ccAuthReply->reasonCode)) {
	        $authReasonCode = $response->getData('response')->ccAuthReply->reasonCode;
	        if($authReasonCode) {
	            $comment .= Mage::helper('cybersource')->__('Authorization Reason Code:');
	            $comment .= $authReasonCode;
	            $comment .= ' ; ';
	        }
	    }
	    if (isset($response->getData('response')->ccAuthReply->authorizationCode)) {
	        $authCode = $response->getData('response')->ccAuthReply->authorizationCode;
	        if($authCode) {
	            $comment .= Mage::helper('cybersource')->__('Authorization Code:');
	            $comment .= $authCode;
	            $comment .= ' ; ';
	        }
	    }
	    if (isset($response->getData('response')->ccAuthReply->avsCode)) {
	        $avsCode = $response->getData('response')->ccAuthReply->avsCode;
	        if($avsCode) {
	            if (Mage::getStoreConfig('payment/cybersource_card_processing/avs')) {
	                $comment .= Mage::helper('cybersource')->__('AVS Code:');
	                $comment .= $avsCode;
	                $comment .= ' ; ';
	            }
	        }
	    }
	    if (isset($response->getData('response')->ccAuthReply->cvCode)) {
	        $cvCode = $response->getData('response')->ccAuthReply->cvCode;
	        if($cvCode) {
	            if (Mage::getStoreConfig('payment/cybersource_card_processing/useccv')) {
	                $comment .= Mage::helper('cybersource')->__('CVN Code:');
	                $comment .= $cvCode;
	                $comment .= ' ; ';
	            }
            }
        }
        $comment .= Mage::helper('cybersource')->prepareSignature();
        $payment->getOrder()->addStatusHistoryComment($comment);
    }


    /*
     * Add payment update to order's comment history
    * @param $payment
    * @param $subscriptionId
    */
    public function addCommentForSoftReject(Varien_Object $payment, $response, $reasonCode) {
        $comment = '';
        if($reasonCode) {
            switch ($reasonCode) {
                case Cybersource_PaymentManagement_Model_Soap_Response::CODE_CVN_DECLINE:
                    $configuredOrderStatus = Mage::getStoreConfig('payment/cybersource_card_processing/cvn_order');
                    $declineReason = Mage::helper('cybersource')->__('CVN Decline');
                break;
                case Cybersource_PaymentManagement_Model_Soap_Response::CODE_AVS_DECLINE:
                    $configuredOrderStatus = Mage::getStoreConfig('payment/cybersource_card_processing/avs_order');
                    $declineReason = Mage::helper('cybersource')->__('AVS Decline');
                break;
                default:
                    $configuredOrderStatus = 'N/A';
                    $declineReason = Mage::helper('cybersource')->__('Soft Decline');
                break;
            }
            $comment .= Mage::helper('cybersource')->__('Order Status Update:') . $configuredOrderStatus . ' ; ';
            $comment .= Mage::helper('cybersource')->__('Status Update Reason:') . $declineReason. ' ; ';
            $requestId = $response->getRequestId();
            if($requestId) {
                $comment .= $this->__('Request ID:') . $requestId. ' ; ';
            }
        } else {
            $comment .= Mage::helper('cybersource')->__('Payment rejected. Please try again.');
        }
        $comment .= Mage::helper('cybersource')->prepareSignature();
        $payment->getOrder()->addStatusHistoryComment($comment);
    }

}