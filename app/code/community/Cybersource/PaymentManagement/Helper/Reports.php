<?php
/**
 *  CyberSource Credit Card Processing (Payment) Helper
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Helper_Reports extends Mage_Core_Helper_Abstract
{
    /**
     * XML configuration paths
     */
    const XML_PATH_ENABLED = 'payment/cybersource_card_processing/dm_reporting_query';
    /**
     * Check whether reward module is enabled in system config
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED);
    }

	/*
	* Add conversion update to order's comment history
	*
	* @param $payment
	* @param $subscriptionId
	*/
	public function addComment(Varien_Object $payment, $note = null,  $isThereConversion )
	{
	    $comment =  Mage::helper('cybersource')->__(' Decision Manager Update (conversion report): ') ;
	    if($note) {
	        $comment .= $note;
	    }
	    $comment .= Mage::helper('cybersource')->prepareSignature();
	    $payment->getOrder()->addStatusHistoryComment($comment);
	}

	/*
	 * Add notification for the admin
	 *
	* @param $payment
	* @param $subscriptionId
	*/
	public function addNotice($note = null, $isThereConversion = false )
	{
	   $message =  Mage::helper('cybersource')->__(' Decision Manager Update (conversion report): ') ;
	   if($note) {
	       $message .= $note ;
	   }
	    if($isThereConversion) {
	        $comment .= Mage::helper('cybersource')->__(' There is a conversion for this order. See comments history.');
	    }
	    $message .= Mage::helper('cybersource')->prepareSignature();
	    Mage::getModel('core/session/abstract')->addNotice($message);
	}


}