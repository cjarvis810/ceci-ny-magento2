<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Helper_Decision extends Mage_Core_Helper_Abstract
{
    const ACCEPT = 'ACCEPT';
    const REVIEW = 'REVIEW';
    const REJECT = 'REJECT';

    /**
     * Mark Decision Manager Result as REVIEW
     *
     * @see Cybersource_PaymentManagement_Model_Observer
     * @return void
     */
    public function markDecisionReview() {
        Mage::register('DecisionManagerResult', self::REVIEW);
    }


    /**
     * Was the Decision Manager Result REVIEW
     *
     * @return bool
     */
    public function wasDecisionReview() {
        $decision = Mage::registry('DecisionManagerResult');
        if ($decision == self::REVIEW) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Mark Decision Manager Result as REVIEW
     *
     * @see Cybersource_PaymentManagement_Model_Observer
     * @return void
     */
    public function markDecisionReject() {
        Mage::register('DecisionManagerResult', self::REJECT);
    }


    /**
     * Was the Decision Manager Result REVIEW
     *
     * @return bool
     */
    public function wasDecisionReject() {
        $decision = Mage::registry('DecisionManagerResult');
        if ($decision == self::REJECT) {
            return true;
        } else {
            return false;
        }

    }




}
