<?php

/**
 *  CyberSource PaymentManagement Data Helper
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Get value from Cybersource config
     *
     * @param string $field Field name to get
     * @return string|int Valid of field from config
     */
    public function getConfigValue($field)
    {
        return Mage::getStoreConfig('payment/cybersource_card_processing/' . $field);
    }


    /**
     * Add extension signature for comment history comments
     *
     * @return string Signature
     */
    public function prepareSignature()
    {
        $lang = Mage::helper('cybersource')->__(' [CyberSource Payment Management]');
    	return $lang;
    }



}
