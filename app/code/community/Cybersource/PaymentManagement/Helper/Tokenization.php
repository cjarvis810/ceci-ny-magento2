<?php

/**
 *  CyberSource PaymentManagement Tokenization Helper
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Helper_Tokenization extends Mage_Core_Helper_Abstract
{
	/*
	* Add tokenization update to order's comment history
	* @param $payment
	* @param $subscriptionId
	*
	* @return void
	*/

	public function addComment($payment, $subscriptionId)
	{
		if (Mage::getStoreConfig('payment/cybersource_payment_tokenization/enabled') == 1) {
			$comment = Mage::helper('cybersource')->__('Payment Tokenization Update: ')
			  . Mage::helper('cybersource')->__('Created customer profile and saved Subscription ID. ')
			  . Mage::helper('cybersource')->__('Subscription ID: '). $subscriptionId
			  . Mage::helper('cybersource')->prepareSignature();
			$payment->getOrder()->addStatusHistoryComment($comment);
		}
	}

}
