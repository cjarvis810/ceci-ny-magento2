<?php
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$definition = array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'comment'   => 'Cybersource Request Token'
);
$columnName = 'cybersource_request_token';
$connection->addColumn($installer->getTable('sales/order_payment'), $columnName, $definition);


// Add tables for logging and decision manager
$installer->run("
DROP TABLE IF EXISTS `{$this->getTable('cybersource_soap_debug')}`;
CREATE TABLE `{$this->getTable('cybersource_soap_debug')}` (
  `entity_id` int(10) unsigned NOT NULL auto_increment,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `request` text,
  `response` text,
  `tags` text,
  PRIMARY KEY  (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{$this->getTable('cybersource_dm_conversion')}`;
CREATE TABLE `{$this->getTable('cybersource_dm_conversion')}` (
  `entity_id` int(10) unsigned NOT NULL auto_increment,
  `order_id` int(24),
  `conversion_to` text,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
// Add attribute for payment tokenization
$setupEAV = new Mage_Eav_Model_Entity_Setup('core_setup');
$setupEAV->addAttribute('customer', 'cybersource_subscription_id', array(
		'type'          => 'text',
		'label'         => 'Subscription ID',
		'input'         => 'text',
		'visible'       => true,
		'required'      => true,
		'user_defined'  => false,
		'default'       => ""
));

$setupEAV = new Mage_Eav_Model_Entity_Setup('core_setup');
// Add attribute for delivery address verification
$setupEAV->addAttribute('customer', 'cybersource_verif_address', array(
    'type'          => 'text',
    'label'         => 'Verification address',
    'input'         => 'text',
    'visible'       => true,
    'required'      => true,
    'user_defined'  => false,
    'default'       => ""
));

$installer->endSetup();