<?php

/**
 * CyberSource PaymentManagement Tokenization Model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Tokenization extends Cybersource_PaymentManagement_Model_Cybersource
{


    /**
     * Checks if payment tokenization is enabled
     *
     * @return bool
     */
    protected function isPaymentTokenizationEnabled() {
       $tokenizationSetting = Mage::getStoreConfig('payment/cybersource_payment_tokenization/enabled');
        return (bool) $tokenizationSetting;
    }

    /**
     * Checks if payment toikenization is applicable
     *
     * @param  $response Cybersource_PaymentManagement_Model_Soap_Response
     * @return bool
     */
    protected function isPaymentTokenizationApplicable(
            Cybersource_PaymentManagement_Model_Soap_Response $response,
            Varien_Object $payment
            ) {
        $reasonCode = $response->getReasonCode();
        $applicableReasonCodes = array(Cybersource_PaymentManagement_Model_Soap_Response::CODE_SUCCESS, Cybersource_PaymentManagement_Model_Soap_Response::CODE_DECISION_REVIEW);
        $cybersourceRequestToken = $payment->getCybersourceRequestToken();
        $parentTransactionId = $payment->getParentTransactionId();

        if (in_array($reasonCode, $applicableReasonCodes)) {
            if($cybersourceRequestToken && $parentTransactionId) {
                return false;
            }
            else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Returns the customer's subscription IDs
     *
     * @param  $customerId
     *
     * @return mixed Customer SubscriptionId(s)
     * Example return:
     * order:100000786=token:9997000022098038,order:100000787=token:9997000022093583
     */

    public function getCustomerSubscriptionId($customerId) {
            $customer = Mage::getModel('customer/customer')->load($customerId);
            $customerSubscriptionIds = $customer->getcybersource_subscription_id();
            return $customerSubscriptionIds;
    }


	/**
	 * Create payment token (Subscription ID)
	 *
	 * @param Cybersource_PaymentManagement_Model_Soap_Response $request
	 * @param Cybersource_PaymentManagement_Model_Soap_Response $response
	 * @param Varien_Object $payment Payment data object
	 *
	 * @return
	 */
	protected function tokenizePayment(
			Cybersource_PaymentManagement_Model_Soap_Response $response,
			Cybersource_PaymentManagement_Model_Soap_Request $request,
			Varien_Object $payment
	) {
	    $isPaymentTokenizationEnabled = $this->isPaymentTokenizationEnabled();
	    $isPaymentTokenizationApplicable = $this->isPaymentTokenizationApplicable($response, $payment);
	    $checkoutMethod = Mage::getSingleton('checkout/cart')->getQuote()->getCheckoutMethod();

	    if ($isPaymentTokenizationEnabled && $isPaymentTokenizationApplicable) {
	        if ($checkoutMethod != Mage_Sales_Model_Quote::CHECKOUT_METHOD_GUEST) {
				$request->tokenizationRequest($response);
				$responseProfile = $this->getSoapClient()->getResponse($request);
				if (isset($responseProfile->getResponse()->paySubscriptionCreateReply->subscriptionID)) {
					$subscriptionId = $responseProfile->getResponse()->paySubscriptionCreateReply->subscriptionID;
					$this->saveSubscripitonId($payment, $responseProfile);
					Mage::helper('cybersource/tokenization')->addComment($payment, $subscriptionId);
				} else {
					$issue = Mage::helper('cybersource')->__('No subscription ID created (tokenization). Reason code = ') ;
					$issue .= $responseProfile->getReasonCode();
					//$payment->getOrder()->addStatusHistoryComment($issue);
				}
	        }
		}
	}




	/**
	 * Save payment token (Subscription ID ) at both customer-level and order-level
	 *
	 * @param $payment
	 * @param $responseProfile
	 *
	 * @return Cybersource_PaymentManagement_Model_Cybersource
	 */
	public function saveSubscripitonId($payment, $responseProfile)
	{
	    if (Mage::getStoreConfig('payment/cybersource_payment_tokenization/enabled') == 1) {
	        $newSubscriptionId = $responseProfile->getResponse()->paySubscriptionCreateReply->subscriptionID;
	        $orderNumber = $payment->getOrder()->getData('increment_id');
	        $customerId = $payment->getOrder()->getData('customer_id');
	        $customer = Mage::getModel('customer/customer')->load($customerId);
	        $customerSubscriptionId = $customer->getcybersource_subscription_id();
	        if(empty($customerSubscriptionId)) {
	            $customerSubscriptionId = $customerSubscriptionId . 'order:' . $orderNumber . '=token:' . $newSubscriptionId;
	        } else {
	            $customerSubscriptionId = $customerSubscriptionId . ',order:' . $orderNumber . '=token:' . $newSubscriptionId;
	        }

	        $customer->setcybersource_subscription_id($customerSubscriptionId)->save();
	    }
	    return $this;
	}


}