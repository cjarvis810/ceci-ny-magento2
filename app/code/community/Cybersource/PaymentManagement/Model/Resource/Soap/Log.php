<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Resource_Soap_Log
    extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Set table name and primary key
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('cybersource/soap_log', 'entity_id');
    }

}
