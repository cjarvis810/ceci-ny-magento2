<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Resource_Soap_Log_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Initialize collection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('cybersource/soap_log');
    }

}

