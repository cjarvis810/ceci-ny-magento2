<?php

/**
 * Decision manager conversion resource model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Resource_Conversion
    extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Sets the main database table for conversion resource model
     *
     * @return void
     */
    protected function _construct()
    {
        //@see table node (inside conversion) in config.xml for the actual db name
        $this->_init('cybersource/conversion', 'id');
    }

}
