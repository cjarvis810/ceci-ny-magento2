<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 *
 * Fallback Cybersource observer when ioncube is not installed. This stubs out
 * oberserver methods to do nothing. This also adds a message in the admin inbox
 * that ioncube must be installed.
 */
class Cybersource_PaymentManagement_Model_Observer
{

    public function prepareInvoiceAccordingly($observer)
    {
        return false;
    }

    public function checkStatus($observer)
    {
        return false;
    }

    public function checkIoncube($observer)
    {
        return false;
    }

}
