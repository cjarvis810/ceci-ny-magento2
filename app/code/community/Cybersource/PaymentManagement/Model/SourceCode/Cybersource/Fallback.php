<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 *
 * Fallback Cybersource model when ioncube is not installed. This returns that the
 * payment method is not available.
 */
class Cybersource_PaymentManagement_Model_Cybersource
    extends Mage_Payment_Model_Method_Abstract
{

    protected $_canUseInternal = false;
    protected $_canUseCheckout = false;
    protected $_canUseForMultishipping = false;
    protected $_canManageRecurringProfiles = false;
    protected $_code = 'cybersource_card_processing';

    public function isAvailable($quote = null)
    {
        return false;
    }

}
