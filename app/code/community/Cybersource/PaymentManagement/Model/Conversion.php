<?php

/**
 * Decision manager conversion model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Conversion extends Mage_Core_Model_Abstract
{

    protected $paymentAction;

    /**
     * Ties conversion model to its resource model and collection.
     *
     * @see Cybersource_PaymentManagement_Model_Resource_Conversion for Resource Model
     * @see Cybersource_PaymentManagement_Model_Resource_Conversion_Collection for Collection
     * @see resourceModel node in config.xml to find the directory for the resource model.
     *      (That directory will be located inside the Model directory fo the extension)
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('cybersource/conversion');
        $this->paymentAction = Mage::helper('cybersource')->getConfigValue('payment_action');
    }


    /**
     * Download conversion report for order and update order accordingly
     * @see reviewPaymentAction method of the core OrderController class
     *
     * @param $orderId
     *
     * @return void
     */
    public function getDecisionConversion($orderId) {
        $order = Mage::getModel('sales/order')->load($orderId);
        $status = $order->getStatus();
        if($status == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
            $incrementId = $order->getIncrementId();
            $payment = $order->getPayment();
            $transactionId = $payment->getLastTransId();
            $txn = Mage::getModel('sales/order_payment_transaction')->load($transactionId, 'txn_id');
            $conversionReport = Mage::getModel('cybersource/reports')->fetchOnDemandConversionReport();
            if($conversionReport) {
                if( isset($conversionReport->Conversion) ) {
                    foreach ($conversionReport->Conversion as $conversion) {
                        $tid = explode(" ", (string) $conversion['RequestID']);
                        $requestId = (string) $tid[0];
                        $newDecision = (string) $conversion->NewDecision;
                        if($requestId == $transactionId) {
                            $note = Mage::helper('cybersource')->__(' Found decision conversion for this order. ');
                            $note = Mage::helper('cybersource')->__(' See Comments History. ');
                            $IsAlreadyProcessed = Mage::registry($order->getId());
                            if(!$IsAlreadyProcessed) {
                                $this->reflectConversion($newDecision, $payment, $transactionId);
                            }
                        }
                    }
                    Mage::unregister($order->getId());
                } else {
                    $note = Mage::helper('cybersource')->__(' No conversions available in the downloaded report. ');
                    Mage::helper('cybersource/reports')->addNotice( $note , false);
                }
            }
        } else {
            $issue =  Mage::helper('cybersource')->__(' Invalid action. Order status is not ')
             . Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW;
           throw new Cybersource_PaymentManagement_Exception($issue);
        }
    }


    /**
     * Download conversion report for all payment_review orders and
     * update orders accordingly
     *
     * @see reviewPaymentAction method of the core OrderController class
     *
     *
     * @return void
     */
    public function getAllDecisionConversions() {
        $conversionReport = Mage::getModel('cybersource/reports')->fetchOnDemandConversionReport();
        if($conversionReport) {
            if(isset($conversionReport->Conversion) ) {
                try {
                    foreach ($conversionReport->Conversion as $conversion) {
                        $tid = explode(" ", (string) $conversion['RequestID']);
                        $transactionId = (string) $tid[0];
                        $newDecision = (string) $conversion->NewDecision;
                        $mRn = explode(" ", (string) $conversion['MerchantReferenceNumber']);
                        $incrementId = (string) $mRn[0];
                        $order= Mage::getModel('sales/order')->loadByIncrementId($incrementId);
                        $payment = $order->getPayment();
                        $paymentMethod = $payment->getMethod();
                        $status = $order->getStatus();
                        if($status == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
                            $IsAlreadyProcessed = Mage::registry($order->getId());
                            if(!$IsAlreadyProcessed) {
                                $this->reflectConversion($newDecision, $payment, $transactionId);
                            }
                            Mage::unregister($order->getId());
                        }
                    }
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
    }

    /**
     * Manually accept or reject Cybersource Payment
     *
     * @param $orderId
     * @param $decision
     *
     * @return void
     */
    public function forceCybersourcePayment($orderId, $decision) {
        $order = Mage::getModel('sales/order')->load($orderId);
        $status = $order->getStatus();
        $payment = $order->getPayment();
        $transactionId = $payment->getLastTransId();
        if($status == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
            switch ($decision) {
                case Cybersource_PaymentManagement_Helper_Decision::ACCEPT:
                    $this->reflectConversion(Cybersource_PaymentManagement_Helper_Decision::ACCEPT, $payment, $transactionId);;
                    break;
                case Cybersource_PaymentManagement_Helper_Decision::REJECT:
                    $this->reflectConversion(Cybersource_PaymentManagement_Helper_Decision::REJECT, $payment, $transactionId);;
                    break;
                default:
                    return ;
                    break;
            }
        }
    }

    /**
     * Process the conversion decision
     *
     * @param $newDecision
     * @param $payment
     * @param $transactionId
     *
     * @return void
     */
    protected function reflectConversion($newDecision, $payment, $transactionId)
    {
        if($newDecision == Cybersource_PaymentManagement_Helper_Decision::ACCEPT) {
              $this->acceptReviewedPayment($payment, $transactionId);
        } elseif($newDecision == Cybersource_PaymentManagement_Helper_Decision::REJECT) {
            $this->rejectReviewedPayment($payment, $transactionId);
        }
    }

    /**
     * Accept the payment  and update order details
     *
     * @param $payment
     * @param $transactionId
     *
     * @return void
     */
    protected function acceptReviewedPayment($payment, $transactionId) {
        $order = $payment->getOrder();
        $transaction = $payment->getTransaction($transactionId);
        if($this->paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE) {
            $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
            $order->save();
        } elseif($this->paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
            $this->captureAcceptedPayment($transactionId);
        } else {
            $issue = Mage::helper('cybersource')->__(' Unexpected payment action method. ') ;
            throw new Cybersource_PaymentManagement_Exception($issue);
        }
    }

    /**
     * Reject the payment and update order details
     *
     * @param $payment
     * @param $transactionId
     *
     * @return void
     */
    protected function rejectReviewedPayment($payment, $transactionId) {
        $order = $payment->getOrder();
        $transaction = $payment->getTransaction($transactionId);
        if($this->paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE) {
            $transaction->setIsClosed(true);
            $order->addRelatedObject($transaction);
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true);
            $order->save();
        } elseif($this->paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
            $transaction->setAdditionalInformation($this->_isTransactionFraud, true);
            $transaction->setIsClosed(true);
            $order->addRelatedObject($transaction);
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true);
            $order->save();
        } else {
            $issue = Mage::helper('cybersource')->__(' Unexpected payment action method. ') ;
            throw new Cybersource_PaymentManagement_Exception($issue);
        }
    }

    /**
     * Charge the credit card and update order detais
     *
     * @param $txn_id
     *
     * @return bool
     */
    protected function captureAcceptedPayment($txn_id)
    {
        $txn = Mage::getModel('sales/order_payment_transaction')->load($txn_id, 'txn_id');
        if (!$txn->hasData('order_id')) {
            return false;
        }
        $order = Mage::getModel('sales/order')->load($txn->getData('order_id'));
        $invoice = Mage::getResourceModel('sales/order_invoice_collection')
                       ->addFieldToFilter('transaction_id', array('eq' => (string)$txn_id))
                       ->getLastItem();
        if ($invoice->isEmpty()) {
            $invoice = $order->getInvoiceCollection()->getLastItem();
        }
        $invoice->pay();
        $invoice->save();
        $payment = $order->getPayment();
        $payment->setIsTransactionPending(false);
        $payment->setIsFraudDetected(false);
        if(!$invoice->getIsPaid()) {
            try {
                $payment ->capture($invoice);
                $message =  Mage::helper('cybersource')->__( 'The invoice has been captured. ');
                Mage::getModel('core/session/abstract')->addSuccess($message);
            } catch (Exception $e) {
                $issue =  Mage::helper('cybersource')->__(' Error capturing the invoice ');
                Mage::logException($e);
            }
        }
        $payment ->save();
        $comment = Mage::helper('cybersource')->__(' Conversion Report Update: ' ) ;
        if($invoice->getIsPaid()) {
            $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
            if (Mage::getStoreConfig('payment/cybersource_card_processing/dm_comments')) {
                $comment .= Mage::helper('cybersource')->__(' Payment is accepted and invoice is paid.') ;
            }
        } else {
            $comment = Mage::helper('cybersource')->__(' Invoice is still not paid. ') ;
        }
        $order->addStatusHistoryComment($comment . Mage::helper('cybersource')->prepareSignature())->save();
        $order->save();
    }
}
