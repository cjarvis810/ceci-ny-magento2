<?php
/**
 * CyberSource Model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----

 */
class Cybersource_PaymentManagement_Model_Cybersource extends Mage_Payment_Model_Method_Cc
{
    /**
     * Block class for form
     * @var string
     */
    protected $_formBlockType = 'cybersource/form';

    /**
     * Payment code
     * @var string
     */
    protected $_code = 'cybersource_card_processing';

    /**
     * Payment Method fields
     * @var bool
     */
    protected $_isGateway = true;

    /**
     * Magento flag for whether payment method can authorize a payment
     * @var bool
     */
    protected $_canAuthorize = true;

    /**
     * Magento flag for whether payment method can capture a payment
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * Magento flag for whether payment method can capture a partial payment
     * @var bool
     */
    protected $_canCapturePartial = true;

    /**
     * Magento flag for whether payment method can refund a payment
     * @var bool
     */
    protected $_canRefund = true;

    /**
     * Magento flag for whether payment method can partially refund a payment
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * Magento flag for whether payment method can void
     * @var bool
     */
    protected $_canVoid = true;

    /**
     * Magento flag for whether payment method can user internal
     * @var bool
     */
    protected $_canUseInternal = true;

    /**
     * Magento flag for whether payment method can use checkout
     * @var bool
     */
    protected $_canUseCheckout = true;

    /**
     * Magento flag for whether payment method can be used in a multiple shipment order
     * @var bool
     */
    protected $_canUseForMultishipping = false;

    /**
     * Magento flag for whether payment method can save a credit card
     * @var bool
     */
    protected $_canSaveCc = false;



    /**
     * Accpeted currency default value
     *
     * @deprecated
     * @see  getAcceptedCurrencyCodes()
     * @var array
     */
    protected $_allowCurrencyCode = array('USD');


    /**
     * Magento flag for whether payment method can be reviewed (accept or deny)
     * Disabled Magento's default Accept and Deny Payment (orange buttons) actions
     *
     * @var bool
     */

    protected $_canReviewPayment = false;

    /**
     * Magento flag for whether payment method can fetch transaction info
     *
     * disabled Magento's default Get Payment Update (orange button)
     *
     * @see canFetchTransactionInfo()
     * @var bool
     */
    protected $_canFetchTransactionInfo = false;



    /**
     * Check if payment method is available
     *
     * @param null $quote
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        return Mage::getStoreConfig('cybersource/account/merchant')
        && Mage::getStoreConfig('cybersource/account/key')
        && parent::isAvailable($quote);
    }

    /**
     * Returns true if the payment action is set to ACTION_AUTHORIZE
     *
     * @see Cybersource_PaymentManagement_Model_System_Config_Source_Action
     *
     * @return bool
     */
    public function isAuthorizeOnly() {
        $paymentAction = $this->paymentAction = Mage::helper('cybersource')->getConfigValue('payment_action');
        if($paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Returns true if the payment action is set to ACTION_AUTHORIZE_CAPTURE
     *
     * @see Cybersource_PaymentManagement_Model_System_Config_Source_Action
     *
     * @return bool
     */
    public function isAuthorizeAndCapture() {
        $paymentAction = $this->paymentAction = Mage::helper('cybersource')->getConfigValue('payment_action');
        if($paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Authorize payment
     *
     * @param Varien_Object $payment
     * @param float $amount
     * @return self
    */
    public function authorize(Varien_Object $payment, $amount)
    {
        parent::authorize($payment, $amount);
        try {
            $error = false;

            // Run Service Credit Card Processing
            $request = Mage::getModel('cybersource/payment')->assembleAuthorizeRequest($payment, $amount);
            $request->appendMerchantDefinedDataFields($payment);
            $response = $this->getSoapClient()->getResponse($request);
            Mage::getModel('cybersource/payment')->saveTransactionDetails($payment, $response);
            Mage::helper('cybersource/payment')->addComment($payment, $response);


            /**
             * Clarification:
             * response-> reasonCode is returned by CyberSource and used here
             * response -> ccAuthReply -> reasonCode is returned by the processor and NOT used here
             *
            * */
            $cybersourceReasonCode = $response->getReasonCode();
            if ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_SUCCESS) {

            } elseif($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_CVN_DECLINE) {
                Mage::helper('cybersource/payment')->addCommentForSoftReject($payment, $response, $cybersourceReasonCode);
                Mage::register('soft-reject', 'cvn-reject');

            } elseif ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_AVS_DECLINE) {
                Mage::helper('cybersource/payment')->addCommentForSoftReject($payment, $response, $cybersourceReasonCode);;
                Mage::register('soft-reject', 'avs-reject');

            } else {
                //Run Service: Decision Manager
                if ($this->getConfigData('dm_enabled')){
                    //Run Service: Decision Manager
                    $error = Mage::getModel('cybersource/decision')->manageDecision($response,$payment, 'authorize');
                } else {
                    $error = Mage::helper('cybersource')->__('Payment error. Please try again after reviewing
                            your address and credit card information.');
                }
            }

            // Service: Payment Tokenization
            Mage::getModel('cybersource/tokenization')->tokenizePayment($response, $request, $payment);

            // Service: Delivery Address Verification
            Mage::getModel('cybersource/dav')->verifyDeliveryAddress($payment);

        } catch (Exception $e) {
            $error = $this->__('Gateway request error: ' . $e->getMessage() . Mage::helper('cybersource')->prepareSignature()); //@internal TALK-MESSAGE
            //$error = $this->__('Payment gateway  error. Please try again.');
        }
        if ($error) {
            Mage::throwException($this->__('Payment error. Please try again.'). $error); //@internal TALK-MESSAGE

        }
        return $this;
    }

    /**
     * Capture payment
     *
     * @param Varien_Object $payment
     * @param $amount
     * @return self
     */
    public function capture(Varien_Object $payment, $amount)
    {
        parent::capture($payment, $amount);
        try {
            $error = false;

            $request = Mage::getModel('cybersource/payment')->assembleCaptureRequest($payment, $amount);
            $request->appendMerchantDefinedDataFields($payment);
            $response = $this->getSoapClient()->getResponse($request);
            Mage::getModel('cybersource/payment')->saveTransactionDetails($payment, $response);
            Mage::helper('cybersource/payment')->addComment($payment, $response);

            /**
             * Clarification:
             * response-> reasonCode is returned by CyberSource and used here
             * response -> ccAuthReply -> reasonCode is returned by the processor and NOT used here
             *
            * */
            $cybersourceReasonCode = $response->getReasonCode();
            if ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_SUCCESS) {

            } elseif($cybersourceReasonCode == 242) {
                $error = $response->getReasonMessage();
            } elseif($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_CVN_DECLINE) {
                $error = Mage::helper('cybersource')->__('Payment rejected. Please correct your Card Verification Number and try again.');
            } elseif ($cybersourceReasonCode == Cybersource_PaymentManagement_Model_Soap_Response::CODE_AVS_DECLINE) {
                $error = Mage::helper('cybersource')->__('Payment rejected. Please correct your billing address and try again.');
            } else {
                if ($this->getConfigData('dm_enabled')){
                    //Run Service: Decision Manager
                    $error = Mage::getModel('cybersource/decision')->manageDecision($response,$payment, 'capture');
                } else {
                    $error = Mage::helper('cybersource')->__('There is an error in processing the payment. Please try again.');
                }
            }

            // Service: Payment Tokenization
            Mage::getModel('cybersource/tokenization')->tokenizePayment($response, $request, $payment);

            // Service: Delivery Address Verification
            Mage::getModel('cybersource/dav')->verifyDeliveryAddress($payment);


        } catch (Exception $e) {
            $error = $this->__('Gateway request error: ' . $e->getMessage() . Mage::helper('cybersource')->prepareSignature()); //@todo TALK-MESSAGE
        }
        if ($error) {
            Mage::throwException($error); //@internal TALK-MESSAGE
        }
        return $this;
    }

    /**
     * Refund payment
     *
     * @param Varien_Object $payment
     * @param float $amount
     * @return Cybersource_PaymentManagement_Model_Cybersource|Mage_Payment_Model_Abstract
     */
    public function refund(Varien_Object $payment, $amount)
    {
        // Check if refund action is available
        parent::refund($payment, $amount);

        $error = false;

        if ($payment->getParentTransactionId() && $payment->getCybersourceRequestToken()) {
            $request = Mage::getModel('cybersource/soap_request_credit', array(
                    Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID => $payment->getParentTransactionId(),
                    Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN => $payment->getCybersourceRequestToken()
            ));
            $request->addAmountData($amount);
            $request->addCurrencyData($payment);
            $request->getRequest()->purchaseTotals->currency = $payment->getOrder()->getData('order_currency_code');
            //$request->addItemsData($payment->getData('creditmemo')->getAllitems());
        } else {
            // @todo implement if transaction ID or cybersource token is not set
        }

        try {
            $response = $this->getSoapClient()->getResponse($request);

            if ($response->isSuccess()) {
                $payment->setLastTransId($response->getRequestId());
                $payment->setLastCybersourceRequestToken($response->getRequestToken());
                $payment->setCcTransId($response->getRequestId());
                $payment->setTransactionId($response->getRequestId());
                $payment->setIsTransactionClosed(0);
                $payment->setCybersourceRequestToken($response->getRequestToken());
            } else {
                $error = $response->getMessage(); // @internal TALK-MESSAGE
            }
        } catch (Exception $e) {
            $error = $this->__('Gateway request error: ' . $e->getMessage()); // @internal TALK-MESSAGE
        }

        if ($error) {
            // @internal TALK-MESSAGE
            Mage::throwException($error);
        }

        return $this;
    }

    /**
     * Cancel payment
     *
     * @param Varien_Object $payment
     * @return Cybersource_PaymentManagement_Model_Cybersource
     */
    public function cancel(Varien_Object $payment)
    {
        // Check if cancel action is available
        parent::cancel($payment);

        $error = false;

        if ($payment->getLastTransId() && $payment->getLastCybersourceRequestToken()) {
            $request_id = $payment->getLastTransId();
            $request_token = $payment->getLastCybersourceRequestToken();
        } elseif ($payment->getParentTransactionId() && $payment->getCybersourceRequestToken()) {
            $request_id = $payment->getParentTransactionId();
            $request_token = $payment->getCybersourceRequestToken();
        } else {
            // @todo implement if transaction ID or cybersource token is not set
        }

        $request = Mage::getModel('cybersource/soap_request_reversal', array(
                Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID => $request_id,
                Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN => $request_token,
                'order_id' => $payment->getOrder()->getData('increment_id')
        ));

        $request->addAmountData($payment->getData('amount_authorized'));
        $request->addPayment($payment);

        try {
            $response = $this->getSoapClient()->getResponse($request);

            if ($response->isSuccess()) {
                $payment->setLastTransId($response->getRequestId());
                $payment->setLastCybersourceRequestToken($response->getRequestToken());
                $payment->setCcTransId($response->getRequestId());
                $payment->setTransactionId($response->getRequestId());
                $payment->setIsTransactionClosed(0);
                $payment->setCybersourceRequestToken($response->getRequestToken());
                $message = 'Full Authorization Reversal successful.'
                        . Mage::helper('cybersource')->prepareSignature();
                $payment->getOrder()->addStatusHistoryComment($message)->save(); //@internal TALK-COMMENT
            } else {
                // $error = $response->getMessage();
                $message = 'Full Authorization Reversal not supported for this transaction.'
                        . Mage::helper('cybersource')->prepareSignature();
                $payment->getOrder()
                ->addStatusHistoryComment($message)
                ->save(); //@internal TALK-COMMENT
            }
        } catch (Exception $e) {
            $message = 'Gateway request error: ' . $e->getMessage()
            . Mage::helper('cybersource')->prepareSignature();
            $error = $this->__($message); //@internal TALK-MESSAGE
        }

        if ($error) {
            //@internal TALK-MESSAGE
            Mage::throwException($error);
        }
        return $this;
    }

    /**
     * Void payment
     *
     * @param Varien_Object $payment
     * @return Cybersource_PaymentManagement_Model_Cybersource|Mage_Payment_Model_Abstract
     */
    public function void(Varien_Object $payment)
    {
        $error = false;

        if ($payment->getTransaction($payment->getParentTransactionId())->getData('txn_type') == 'authorization') {

            return $this->cancel($payment);
        } else {
            //check if this payment can refund
            parent::void($payment);


            if ($payment->getParentTransactionId() && $payment->getCybersourceRequestToken()) {
                $request = Mage::getModel('cybersource/soap_request_void', array(
                        Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID => $payment->getParentTransactionId(),
                        Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN => $payment->getCybersourceRequestToken()
                ));
                //$request->addAmountData($amount);
                //$request->addPayment($payment);
            }

            try {
                $response = $this->getSoapClient()->getResponse($request);

                if ($response->isSuccess()) {
                    $payment->setLastTransId($response->getRequestId());
                    $payment->setLastCybersourceRequestToken($response->getRequestToken());
                    $payment->setCcTransId($response->getRequestId());
                    $payment->setTransactionId($response->getRequestId());
                    $payment->setIsTransactionClosed(0);
                    $payment->setCybersourceRequestToken($response->getRequestToken());
                } else {
                    $error = $response->getMessage(); //@todo TALK-MESSAGE
                }
            } catch (Exception $e) {
                $message = Mage::helper('cybersource')->prepareSignature()
                . 'Gateway request error: ' . $e->getMessage();
                $error = $this->__($message); //@todo TALK-MESSAGE
            }

            if ($error) {
                //@internal TALK-MESSAGE
                Mage::throwException($error);
            }
            return $this;
        }
    }

    /**
     * Validate credit cards
     *
     * @return Cybersource_PaymentManagement_Model_Cybersource|Mage_Payment_Model_Abstract
     */
    public function validate()
    {
        Mage_Payment_Model_Method_Abstract::validate();

        $info = $this->getInfoInstance();
        $errorMsg = false;
        $availableTypes = explode(',', $this->getConfigData('cctypes'));

        $ccNumber = $info->getCcNumber();

        // remove credit card number delimiters such as "-" and space
        $ccNumber = preg_replace('/[\-\s]+/', '', $ccNumber);
        $info->setCcNumber($ccNumber);

        $ccType = '';

        if (in_array($info->getCcType(), $availableTypes)) {
            if ($this->validateCcNum($ccNumber) //  credit card type number validation
                    || ($this->OtherCcType($info->getCcType()) && $this->validateCcNumOther($ccNumber))
            ) {

                $ccType = 'OT';
                $ccTypeRegExpList = $this->getCcVerificationRegEx();

                foreach ($ccTypeRegExpList as $ccTypeMatch => $ccTypeRegExp) {
                    if (preg_match($ccTypeRegExp, $ccNumber)) {
                        $ccType = $ccTypeMatch;
                        break;
                    }
                }

                if (!$this->OtherCcType($info->getCcType()) && $ccType != $info->getCcType()) {
                    //@internal TALK-MESSAGE
                    $message = Mage::helper('cybersource')->prepareSignature()
                    . 'Credit card number mismatch with credit card type.';
                    $errorMsg = $this->__($message);
                }
            } else {
                //@internal TALK-MESSAGE
                $message = Mage::helper('cybersource')->prepareSignature()
                . 'Invalid Credit Card Number';
                $errorMsg = $this->__($message);
            }
        } else {
            //@internal TALK-MESSAGE
            $message = Mage::helper('cybersource')->prepareSignature()
            . 'Credit card type is not allowed for this payment method.';
            $errorMsg = $this->__($message);
        }

        //validate credit card verification number
        if ($errorMsg === false && $this->hasVerification()) {
            $verifcationRegEx = $this->getCvnVerificationRegEx();
            $regExp = isset($verifcationRegEx[$info->getCcType()]) ? $verifcationRegEx[$info->getCcType()] : '';
            if (!$info->getCcCid() || !$regExp || !preg_match($regExp, $info->getCcCid())) {
                //@todo TALK-MESSAGE
                $message = Mage::helper('cybersource')->prepareSignature()
                . 'Please enter a valid credit card verification number.';
                $errorMsg = $this->__($message);
            }
        }

        if ($ccType != 'SS' && !$this->_validateExpDate($info->getCcExpYear(), $info->getCcExpMonth())) {
            //@internal TALK-MESSAGE
            $message = Mage::helper('cybersource')->prepareSignature()
            . 'Incorrect credit card expiration date.';
            $errorMsg = $this->__($message);
        }

        if ($errorMsg) {
            Mage::throwException($errorMsg); //@internal TALK-MESSAGE
        }
        return $this;
    }


    /**
     * Get SOAP client
     *
     * @return Cybersource_PaymentManagement_Model_Soap_Client
     */
    protected function getSoapClient()
    {
        return Mage::getModel('cybersource/soap_client');
    }

    /**
     * Traslation method
     *
     * @param $message
     * @return string
     */
    protected function __($message)
    {
        return Mage::helper('cybersource')->__($message);
    }

    /**
     * CVN verification
     *
     * @return array
     */
    public function getCvnVerificationRegEx()
    {
        $verificationList = parent::getVerificationRegEx();
        $verificationList['EC'] = $verificationList['OT']; // Eurocard
        $verificationList['DC'] = $verificationList['OT']; // Diners Club
        $verificationList['CBLANCHE'] = $verificationList['OT']; // Carte Blanche
        $verificationList['ER'] = $verificationList['OT']; // EnRoute
        $verificationList['JAL'] = $verificationList['OT']; // JAL
        $verificationList['DE'] = $verificationList['OT']; // Delta
        $verificationList['VE'] = $verificationList['OT']; // Visa Electron
        $verificationList['DA'] = $verificationList['OT']; // Dankort
        $verificationList['LA'] = $verificationList['OT']; // Laser
        $verificationList['CBLEUE'] = $verificationList['OT']; // Carte Bleue
        $verificationList['CS'] = $verificationList['OT']; // Carta Si
        $verificationList['EAN'] = $verificationList['OT']; // Encoded Account Number
        $verificationList['UATP'] = $verificationList['OT']; // UATP
        $verificationList['MI'] = $verificationList['OT']; // Maestro International
        $verificationList['SC'] = $verificationList['OT']; // Santander Card
        return $verificationList;
    }

    /**
     * Credit Card Number verification
     *
     * @todo Change RegExp if necessary
     * @return array
     */
    public function getCcVerificationRegEx()
    {
        $ccTypeRegExpList = array('VI' => '/^4[0-9]{12}([0-9]{3})?$/', // Visa
                'MC' => '/^5[1-5][0-9]{14}$/', // MasterCard
                'AE' => '/^3[47][0-9]{13}$/', // American Express
                'DI' => '/^6(?:011|5[0-9]{2})[0-9]{12}$/', // Discover
                'DC' => '/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/', // Diners Club
                'CBLANCHE' => '/^30[0-5][0-9]{11}$/', // Carte Blanche
                'JCB' => '/^(3[0-9]{15}|(2131|1800)[0-9]{11})$/', // JCB
                'ER' => '/^(2014|2019)[0-9]{11}$/', // EnRoute
                'DE' => '/^(4137|4462|45|46|48|49)[0-9]{12,14}$/', // Delta
                'VE' => '/^(4026|417500|4508|4844|4913|4917)[0-9]{10, 12}$/', // Visa Electron
                'DA' => '/^4571[0-9]{12}$/', // Dankort
                'LA' => '/^((6304|6706|6771|6709)[0-9]{12}([0-9]{3}))$/', // Laser
                'CBLEUE' => '/^4[0-9]{12,15}$/', // Carte Bleue
                'CS' => '/^4[0-9]{15}$/', // Carta Si
                'MI' => '/^(5018|5020|5038|6304|6759|6761|6762|6763)[0-9]{8,15}$/', // Maestro International
        );
        return $ccTypeRegExpList;
    }

    /**
     * Check for CC verification
     * @return bool
     */
    public function hasVerification()
    {
        $cvn_exceptions = explode(',', $this->getConfigData('ccv_exception'));
        $cc_type = $this->getInfoInstance()->getCcType();
        return parent::hasVerification() && !in_array($cc_type, $cvn_exceptions);
    }


}