<?php
/**
 *
 * CyberSource Observer
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */

class Cybersource_PaymentManagement_Model_Observer
{

    /**
     * Capture if payment method is Auhtorize Only
     *
     * @param Varien_Event_Observer $observer Event object
     * @return void
     */
    public function prepareInvoiceAccordingly(Varien_Event_Observer $observer)
    {
        $_helper = Mage::helper('cybersource');

        if ($observer->getShipment()->getOrder()->getPayment()->getMethodInstance()->getCode() == 'cybersource_card_processing') {

            if (Mage::getModel('cybersource/payment')->isAuthorizeOnly() && $_helper->getConfigValue('capture') != 'never') {

                if ($_helper->getConfigValue('capture') == 'once') {
                    $invoices = $observer->getShipment()->getOrder()->getInvoiceCollection();
                    if ($invoices->count() == 0) {
                        $invoice = Mage::getModel('sales/service_order', $observer->getShipment()->getOrder());
                        $invoice->prepareInvoice()->register()->capture()->save();
                    }
                } elseif ($_helper->getConfigValue('capture') == 'partial') {

                    $products = array();
                    $products['all'] = $observer->getShipment()->getOrder()->getItemsCollection()->walk(function ($item) {
                                return $item->getQtyOrdered();
                            });
                    $products['shipped'] = array();
                    $products['invoiced'] = array();

                    foreach ($observer->getShipment()->getAllItems() as $item) {
                        $products['shipped'][$item->getOrderItemId()] = $item->getQty();
                    }

                    foreach ($products['all'] as $id => $item) {
                        if (array_key_exists($id, $products['shipped'])) {
                            $products['invoiced'][$id] = $products['shipped'][$id];
                        } else {
                            $products['invoiced'][$id] = 0;
                        }
                    }

                    $invoice = Mage::getModel('sales/service_order', $observer->getShipment()->getOrder());
                    $invoice->prepareInvoice($products['invoiced'])->register()->capture()->save();
                    //@todo add to session warning about capturing
                }
            }
        }
    }

    /**
     * Update order based on AVS, CVN or Decision Manager results
     *
     * @todo replace hard-coded values with constants
     *
     * @param Varien_Event_Observer $observer Event object
     * @return void
     */
    public function updateOrderAccordingly(Varien_Event_Observer $observer)
    {
        // Service: Billing Adress (AVS) and Card (CVN) Verification
        $softReject = Mage::registry('soft-reject');
        if($softReject == 'cvn-reject') {
            $configuredOrderStatus = Mage::getStoreConfig('payment/cybersource_card_processing/cvn_order');
            if ($configuredOrderStatus == 'canceled') {
                $observer->getOrder()->cancel()->save();
            } elseif ($configuredOrderStatus == 'holded') {
                $observer->getOrder()->hold()->save();
            }
            Mage::unregister('soft-reject');
        } elseif ($softReject == 'avs-reject') {
            $configuredOrderStatus = Mage::getStoreConfig('payment/cybersource_card_processing/avs_order');
            if ($configuredOrderStatus == 'canceled') {
                $observer->getOrder()->cancel()->save();
            } elseif ($configuredOrderStatus == 'holded') {
                $observer->getOrder()->hold()->save();
            }
            Mage::unregister('soft-reject');
        }
        // Service: Delivery Address  Verification
        $wasDeliveryAddressDisapproved = Mage::helper('cybersource/dav')->wasDeliveryAddressDisapproved();
        if ($wasDeliveryAddressDisapproved) {
            if (Mage::getStoreConfig('payment/cybersource_card_processing/dav_status') == 'holded') {
                // Decision manager takes precedence
               if( !Mage::helper('cybersource/decision')->wasDecisionReject() &&
                        !Mage::helper('cybersource/decision')->wasDecisionReview()) {
                    $observer->getOrder()->hold()->save();
                }
            }
        }
        // Service: Decision Manager
        $wasDecisionReject = Mage::helper('cybersource/decision')->wasDecisionReject();
        if ($wasDecisionReject) {
            $observer->getOrder()->cancel()->save();
            Mage::unregister('DecisionManagerResult');
        }

    }

    /**
     * @see Fallback file
     *
     * @param Varien_Event_Observer $observer Event object
     * @return bool
     */
    public function checkIoncube(Varien_Event_Observer $observer)
    {
        return false;
    }

    /**
     * Add device fingerprinting block to collect user agent data
     *
     * @param Varien_Event_Observer $observer Event object
     * @return void
     */
    public function addFingerprint(Varien_Event_Observer $observer)
    {
        if (Mage::getStoreConfig('payment/cybersource_card_processing/df_enabled')) {
            if (Mage::getSingleton('core/layout')->getBlock('before_body_end')) {
                $block = Mage::getSingleton('core/layout')->createBlock('cybersource/fingerprint', 'fingerprint');
                Mage::getSingleton('core/layout')->getBlock('before_body_end')->insert($block, '', true);
            }
        }
    }


    /**
     * Scheduled decision conversions update
     *
     * @return void
     */
    public function getAllDecisionConversions() {
        Mage::getModel('cybersource/conversion')->getAllDecisionConversions();
    }





}
