<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_AuthMethods
    extends Cybersource_PaymentManagement_Model_System_Config_Source_Abstract
{

    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'VI',
                'label' => $this->__('Verified by Visa')
            ),
            array(
                'value' => 'MC',
                'label' => $this->__('MasterCard SecureCode')
            ),
            array(
                'value' => 'MI',
                'label' => $this->__('Maestro SecureCode')
            ),
            array(
                'value' => 'JCB',
                'label' => $this->__('JCB J/Secure')
            ),
            array(
                'value' => 'AE',
                'label' => $this->__('American Express Safekey')
            ),
        );
    }

}
