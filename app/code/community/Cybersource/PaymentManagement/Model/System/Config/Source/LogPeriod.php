<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_LogPeriod
{

    /**
     * Option for config support section
     * @return array
     */
    public function toOptionArray()
    {
        return array(array('value' => '1', 'label' => '24 hours'),
            array('value' => '2', 'label' => '1 week'),
            array('value' => '3', 'label' => '1 month'),
            array('value' => '4', 'label' => '3 months'),
            array('value' => '5', 'label' => '6 months'),
            array('value' => '6', 'label' => '12 months')
        );
    }

}
