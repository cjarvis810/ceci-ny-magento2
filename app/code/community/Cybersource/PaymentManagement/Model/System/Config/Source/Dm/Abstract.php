<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Dm_Abstract
{

    // set null to enable all possible
    protected $_stateStatuses = array(
        Mage_Sales_Model_Order::STATE_NEW,
        Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
        Mage_Sales_Model_Order::STATE_PROCESSING,
        Mage_Sales_Model_Order::STATE_COMPLETE,
        Mage_Sales_Model_Order::STATE_CLOSED,
        Mage_Sales_Model_Order::STATE_CANCELED,
        Mage_Sales_Model_Order::STATE_HOLDED,
    );

    /**
     * New option values for select
     * 
     * @return array
     */
    public function toOptionArray()
    {
        $statuses = Mage::getSingleton('sales/order_config');
        $result = array();
        foreach ($this->_stateStatuses as $state) {
            $result[$state] = $statuses->getStateLabel($state);
        }
        return $result;
    }

}
