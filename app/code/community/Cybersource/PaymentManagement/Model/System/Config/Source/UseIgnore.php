<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_UseIgnore
    extends Cybersource_PaymentManagement_Model_System_Config_Source_Abstract
{

    /**
     * New adminhtml option values for select
     * 
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 0,
                'label' => $this->__('Ignore')
            ),
            array(
                'value' => 1,
                'label' => $this->__('Use')
            ),
        );
    }

}
