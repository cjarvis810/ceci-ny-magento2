<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Env
{

    public function toOptionArray()
    {
        return array(
            array('value' => 'production', 'label' => 'Production'),
            array('value' => 'test', 'label' => 'Test')
        );
    }

}
