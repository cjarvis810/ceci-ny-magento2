<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Cctype
    extends Mage_Payment_Model_Source_Cctype
{

    /**
     * @TODO put description here!!!!
     * @return array
     */
    public function getAllowedTypes()
    {
        $types = array_keys($this->getCardTypes());
        // @todo Do we need card type Other ?
        return $types;
    }

    /**
     * @TODO put description here!!!!
     * @return array
     */
    public function getCardTypes()
    {
        return array('VI' => '001', // Visa
            'MC' => '002', // MasterCard
            'EC' => '002', // Eurocard
            'AE' => '003', // American Express
            'DI' => '004', // Discover
            'DC' => '005', // Diners Club
            'CBLANCHE' => '006', // Carte Blanche
            'JCB' => '007', // JCB
            'ER' => '014', // EnRoute
            'JAL' => '021', // JAL
            'DE' => '031', // Delta
            'VE' => '033', // Visa Electron
            'DA' => '034', // Dankort
            'LA' => '035', // Laser
            'CBLEUE' => '036', // Carte Bleue
            'CS' => '037', // Carta Si
            'EAN' => '039', // Encoded Account Number
            'UATP' => '040', // UATP
            'MI' => '042', // Maestro International
            'SC' => '043', // Santander Card
        );
    }

    /**
     * @TODO put description here!!!!
     * @param $type
     * @return mixed
     */
    public function getCardCode($type)
    {
        $types = $this->getCardTypes();
        return $types[$type];
    }

    public function toOptionArray()
    {

        /**
         * making filter by allowed cards
         */
        $allowed = $this->getAllowedTypes();
        $options = array();


        $popular_codes = array('DI', 'AE', 'MC', 'VI');

        $all_types = Mage::getSingleton('payment/config')->getCcTypes();
        $popular = array_intersect_key($all_types, array(
            'DI' => $all_types['DI'],
            'AE' => $all_types['AE'],
            'MC' => $all_types['MC'],
            'VI' => $all_types['VI']
        ));
        arsort($popular);
        asort($all_types);

        foreach ($popular as $code => $name) {
            $options[] = array('value' => $code, 'label' => $name);
        }


        foreach ($all_types as $code => $name) {
            if (in_array($code, array_keys($popular))) {
                unset($all_types[$code]);
            } elseif (in_array($code, $allowed) || !count($allowed)) {
                $options[] = array('value' => $code, 'label' => $name);
            }
        }

        return $options;
    }

}
