<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Range
    extends Cybersource_PaymentManagement_Model_System_Config_Source_Abstract
{

    public function toOptionArray()
    {
        return array(
            array('value' => 'day', 'label' => 'day'),
            array('value' => 'week', 'label' => 'week'),
            array('value' => 'month', 'label' => 'month'),
        );
    }

}
