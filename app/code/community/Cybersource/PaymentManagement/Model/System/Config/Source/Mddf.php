<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Mddf
{
    /**
     * Get options for MDDF (Merchant Defined Data Fields)
     *
     * @return array Option array
     */
    public function toOptionArray()
    {

        if (Mage::registry('mddf_increment')) {
            $number = (int) Mage::registry('mddf_increment') + 1;
            Mage::unregister('mddf_increment');
            Mage::register('mddf_increment', $number);
        } else {
            Mage::register('mddf_increment', 1);
            $number = 1;
        }

        return array(
            array('value' => $number, 'label' => Mage::helper('adminhtml')->__('Send in MDD' . $number)),
            array('value' => 0, 'label' => Mage::helper('adminhtml')->__('Do not send')),
        );
    }

}
