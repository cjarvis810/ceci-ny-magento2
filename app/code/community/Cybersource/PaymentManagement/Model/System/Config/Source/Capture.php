<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Capture
{

    public function toOptionArray()
    {
        return array(
            array('value' => 'never', 'label' => 'Never with Shipment'),
            array('value' => 'once', 'label' => 'All with First Shipment'),
            array('value' => 'partial', 'label' => 'Partially with each Partial Shipment')
        );
    }

}
