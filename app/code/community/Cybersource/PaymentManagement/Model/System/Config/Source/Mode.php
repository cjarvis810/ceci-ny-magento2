<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Mode
{

    /**
     * New option values
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label' => 'Test'),
            array('value' => 0, 'label' => 'Live'),
        );
    }

}
