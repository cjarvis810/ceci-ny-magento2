<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Fingerprint_Dns
{

    public function toOptionArray()
    {
        return array(
            array('value' => 'remote', 'label' => 'h.online-metrix.net'),
            array('value' => 'local', 'label' => 'Local Redirect URL')
        );
    }

}
