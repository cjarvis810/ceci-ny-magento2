<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Dm_Intervals
{

    /**
     * New option values for select
     * 
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 1,
                'label' => Mage::helper('core')->__('Every 1 hour')
            ),
            array(
                'value' => 3,
                'label' => Mage::helper('core')->__('Every 3 hour')
            ),
            array(
                'value' => 6,
                'label' => Mage::helper('core')->__('Every 6 hour')
            ),
            array(
                'value' => 9,
                'label' => Mage::helper('core')->__('Every 9 hour')
            ),
            array(
                'value' => 12,
                'label' => Mage::helper('core')->__('Every 12 hour')
            )
        );
    }

}
