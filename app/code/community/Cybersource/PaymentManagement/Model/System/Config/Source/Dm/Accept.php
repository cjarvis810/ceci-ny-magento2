<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Dm_Accept
    extends Cybersource_PaymentManagement_Model_System_Config_Source_Dm_Abstract
{

    protected $_stateStatuses = array(
        Mage_Sales_Model_Order::STATE_PROCESSING,
    );

}
