<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
abstract class Cybersource_PaymentManagement_Model_System_Config_Source_Abstract
{

    protected function __($message)
    {
        return Mage::helper('cybersource')->__($message);
    }

}
