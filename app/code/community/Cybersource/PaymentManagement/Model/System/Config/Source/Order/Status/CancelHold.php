<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Order_Status_CancelHold
    extends Mage_Adminhtml_Model_System_Config_Source_Order_Status
{

    protected $_stateStatuses = array(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_HOLDED);

    public function toOptionArray()
    {
        if ($this->_stateStatuses) {
            $statuses = Mage::getSingleton('sales/order_config')->getStateStatuses($this->_stateStatuses);
        } else {
            $statuses = Mage::getSingleton('sales/order_config')->getStatuses();
        }
        $options = array();

        foreach ($statuses as $code => $label) {
            $options[] = array(
                'value' => $code,
                'label' => $label
            );
        }
        return $options;
    }

}
