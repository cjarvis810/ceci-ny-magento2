<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_Dm_Reject
    extends Cybersource_PaymentManagement_Model_System_Config_Source_Dm_Abstract
{

    protected $_stateStatuses = array(
        Mage_Sales_Model_Order::STATE_PROCESSING,
        Mage_Sales_Model_Order::STATE_CANCELED,
        Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW,
    );

}
