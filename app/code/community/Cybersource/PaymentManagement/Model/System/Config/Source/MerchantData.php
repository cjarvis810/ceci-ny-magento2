<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_System_Config_Source_MerchantData
    extends Cybersource_PaymentManagement_Model_System_Config_Source_Abstract
{

    public function toOptionArray()
    {
        return array(
            array(
                'value' => 0,
                'label' => $this->__('Disable')
            ),
            array(
                'value' => 1,
                'label' => $this->__('Field #1')
            ),
            array(
                'value' => 2,
                'label' => $this->__('Field #2')
            ),
            array(
                'value' => 3,
                'label' => $this->__('Field #3')
            ),
            array(
                'value' => 4,
                'label' => $this->__('Field #4')
            ),
        );
    }

}
