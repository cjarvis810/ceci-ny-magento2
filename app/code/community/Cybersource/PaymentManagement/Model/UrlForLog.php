<?php

/**
 * Download Log Model
 * 
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_UrlForLog
{

    /** 
     * Prepare log download link
     * 
     * @param $element
     * @param $currentValue
     * @return string
     */
    public function getCommentText($element, $currentValue)
    {
        $value = $element->asCanonicalArray();
        $key = Mage::getModel('adminhtml/url');
        $url = $key->getUrl('cybersource/log/log/', array('p' => $value['period']));
        return 'Select a time range then <a href=' . $url . '>click here to download logs</a>. Downloading very large files might impact site performance.';
    }

}
