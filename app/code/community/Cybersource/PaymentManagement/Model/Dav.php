<?php

/**
 * CyberSource PaymentManagement Delivery Address Verification Model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Dav extends Cybersource_PaymentManagement_Model_Cybersource
{


    /**
     * Checks if delivery address verification is enabled
     *
     * @return bool
     */
    protected function isDeliveryAddressVerificationEnabled() {
        return Mage::getStoreConfig('payment/cybersource_card_processing/dav_enable');
    }

    /**
     * Checks if shipping address country is enabled for delivery address verification
     *
     * @param  $shippingAddress
     * @return bool
     */
    protected function isDeliveryAddressVerificationApplicable($shippingAddress) {

        $enabledCountryIds = explode(',', Mage::getStoreConfig('payment/cybersource_card_processing/dav_country'));
        $shippingCountryId = $shippingAddress->getData('country_id') ;
        if (in_array($shippingCountryId, $enabledCountryIds)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verify delivery (shipping) address
     * If there is a locally stored verification result then uses it instead of calling CyberSource
     *
     * @param $payment
     * @return Cybersource_PaymentManagement_Model_Dav
     */
    protected function verifyDeliveryAddress($payment) {

        $isDeliveryAddressVerificationEnabled = $this->isDeliveryAddressVerificationEnabled();
        $shippingAddress = $payment->getOrder()->getShippingAddress();
        $isDeliveryAddressVerificationApplicable = $this->isDeliveryAddressVerificationApplicable($shippingAddress);
        if($isDeliveryAddressVerificationEnabled && $isDeliveryAddressVerificationApplicable) {
            try {
                $customerId = $payment->getOrder()->getData('customer_id');
                $customer = Mage::getModel('customer/customer')->load($customerId);
                $billingAddress = $payment->getOrder()->getBillingAddress();
                $stashedAddressStatus = $this->fetchStashedAddressStatus($customerId, $shippingAddress);
                if(isset($stashedAddressStatus)) {
                    switch ($stashedAddressStatus){
                        case Cybersource_PaymentManagement_Helper_Dav::APPROVED:
                            $comment =  $this->__('Shipping address had been verified.');
                            $comment .= Mage::helper('cybersource')->prepareSignature();
                            $payment->getOrder()->addStatusHistoryComment($comment)->save();
                            break;
                        case Cybersource_PaymentManagement_Helper_Dav::DISAPPROVED:
                            Mage::helper('cybersource/dav')->markDeliveryAddressDisapproved();
                            $comment = $this->__('Shipping address had NOT been verified.');
                            $comment .= Mage::helper('cybersource')->prepareSignature();
                            $payment->getOrder()->addStatusHistoryComment($comment)->save();
                            break;
                        default:
                            $this->callDeliveryAddressVerificationService($payment, $billingAddress, $shippingAddress);
                            break;
                    }

                }

            } catch (Exception $e) {
                Mage::logException($e);
                return false;
            }
        }
    }



    /**
     * Calls CyberSource Delivery Address Verification Service
     *
     * @param $payment
     * @param $billingAddress
     * @param $shippingAddress
     * @return void
     */
    protected function callDeliveryAddressVerificationService($payment, $billingAddress, $shippingAddress) {
        $davResponse = Mage::getModel('cybersource/soap_request_dav')->fetchAddressVerificationResponse($billingAddress, $shippingAddress);
        if($davResponse) {
            $isDeliveryAddressGettingDisapproved =  Mage::helper('cybersource/dav')->isDeliveryAddressGettingDisapproved($davResponse);
        }
        if ($isDeliveryAddressGettingDisapproved) {
            Mage::helper('cybersource/dav')->markDeliveryAddressDisapproved();
            $verificationResult = Cybersource_PaymentManagement_Helper_Dav::DISAPPROVED;
            $reasonCode = $davResponse->getResponse()->davReply->reasonCode;
            $comment = $this->__('Shipping address was NOT verified.');
            if($reasonCode)  {
                $comment .= 'Reason code: ' . $reasonCode;
            }
            $comment .= Mage::helper('cybersource')->prepareSignature();
        } else {
            Mage::helper('cybersource/dav')->removeDeliveryAddressDisapprovedMark();
            $verificationResult = Cybersource_PaymentManagement_Helper_Dav::APPROVED;
            $normalizedAddress = $this->fetchNormalizedAddress($payment, $davResponse);
            $comment = $this->__('Shipping address was verified.');
            $comment .= $normalizedAddress;
            $comment .= Mage::helper('cybersource')->prepareSignature();
        }
        $payment->getOrder()->addStatusHistoryComment($comment)->save(); //@internal TALK-COMMENT
        $this->stashAddressVerificationResult($payment, $verificationResult);
    }


    /**
     * Store the address digest and its vefification result locally
     *
     * @param $payment
     * @param $verificationResult ('approved' or 'disapproved')
     * @return void
     */
    protected function stashAddressVerificationResult($payment, $verificationResult) {
        $customerId = $payment->getOrder()->getData('customer_id');
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $shippingAddress = $payment->getOrder()->getShippingAddress();
        $hashedAddressDigest = Mage::helper('cybersource/dav')->createHashedAddressDigest($shippingAddress);
        $timeStamp =  Mage::getModel('core/date')->timestamp(time());
        $formattedTimeStamp = date('Y-m-d', $timeStamp);
        $newResult = $hashedAddressDigest . ',' . $formattedTimeStamp . ',' . $verificationResult;
        $allResults = $customer->getData('cybersource_verif_address');
        if (!is_null($allResults)) {
            $newResult = '|' . $newResult;
        }
        $customer->setData('cybersource_verif_address', $allResults . $newResult)->save();
    }


	/**
	 * Check if there is a locally stored verification result
	 *
	 * @param $customerId
	 * @param $shippingAddress
	 * @return string Verification result
	 */
    protected function fetchStashedAddressStatus($customerId, Mage_Customer_Model_Address_Abstract $shippingAddress) {

        $hashedShippingAddressDigest = Mage::helper('cybersource/dav')->createHashedAddressDigest($shippingAddress);
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $stashedVerificationResults = explode('|',  $customer->getData('cybersource_verif_address'));
        foreach ($stashedVerificationResults as $result) {
            if (!is_null($result)) {
                $data = explode(',', $result);
                if ($data[0] == $hashedShippingAddressDigest) {
                    $stashedAddressStatus = $data[2];
                    break;
                }
            }
        }

        if(isset($stashedAddressStatus)) {
            switch ($stashedAddressStatus){
                case Cybersource_PaymentManagement_Helper_Dav::APPROVED:
                    return Cybersource_PaymentManagement_Helper_Dav::APPROVED;
                    break;
                case Cybersource_PaymentManagement_Helper_Dav::DISAPPROVED:
                    return Cybersource_PaymentManagement_Helper_Dav::DISAPPROVED;
                    break;
                case Cybersource_PaymentManagement_Helper_Dav::NOMATCH:
                    return Cybersource_PaymentManagement_Helper_Dav::NOMATCH;
                    break;
                default:
                    return Cybersource_PaymentManagement_Helper_Dav::NOMATCH;
                    break;
            }
        } else {
            return Cybersource_PaymentManagement_Helper_Dav::NOMATCH;
        }

    }



	/**
	 * Returns the address fields normalized/standardized by CyberSource
	 *
	 * @param $payment
	 * @param $davResponse
	 * @return string
	 */
	protected function fetchNormalizedAddress($payment, $davResponse) {
	    $normalized = $this->__('Recommended shipping address standardization: ');
	    $addressFromMagento = $payment->getOrder()->getShippingAddress()->toArray();
	    $addressFromCybersource = get_object_vars($davResponse->getResponse()->davReply);
	    //@todo second address

	    if (isset($addressFromCybersource['standardizedAddress1'])) {
	        if ($addressFromMagento['street'] != $addressFromCybersource['standardizedAddress1']) {
	            $normalized .=  $this->__('Normalized street address: ');
	            $normalized .=  $addressFromCybersource['standardizedAddress1'];
	            $normalized .=  ' | ';
	        }
	    }
	    if (isset($addressFromCybersource['standardizedCity'])) {
	        if ($addressFromMagento['city'] != $addressFromCybersource['standardizedCity']) {
	            $normalized .=  $this->__('Normalized city: ');
	            $normalized .=  $addressFromCybersource['standardizedCity'];
	            $normalized .=  ' | ';
	        }
	    }
	    if (isset($addressFromCybersource['standardizedPostalCode'])) {
	        if ($addressFromMagento['postcode'] != $addressFromCybersource['standardizedPostalCode']) {
	            $normalized .=  $this->__('Normalized postal code: ');
	            $normalized .= $addressFromCybersource['standardizedPostalCode'];
	            $normalized .=  ' | ';
	        }
	    }
	    if (isset($addressFromCybersource['standardizedState'])) {
	        if ($addressFromMagento['region'] != $addressFromCybersource['standardizedState']) {
	            $normalized .=  $this->__('Normalized state: ');
	            $normalized .= $addressFromCybersource['standardizedState'];
	            $normalized .=  ' | ';
	        }
	    }
	    if (isset($addressFromCybersource['standardizedCountry'])) {
	        if ($addressFromMagento['country_id'] != $addressFromCybersource['standardizedCountry']) {
	            $normalized .=  $this->__('Normalized country: ');
	            $normalized .= $addressFromCybersource['standardizedCountry'];
	            $normalized .=  ' | ';
	        }
	    }
	    return $normalized;
	}



}