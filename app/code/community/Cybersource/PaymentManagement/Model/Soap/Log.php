<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Log extends Mage_Core_Model_Abstract
{

    /**
     * Customer entity ID
     * @var int
     */
    protected $customerId;

    /**
     * Set resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('cybersource/soap_log');
    }

    /**
     * Get if log is enabled in config
     *
     * @return boolean True if log is enabled
     */
    public function isLogEnabled()
    {
        return Mage::getStoreConfig('payment/support/log');
    }

    /**
     * Convert object to Xml
     *
     * @param  object       $object   Object to convert
     * @param  string|null  $rootName Root node name to wrap Xml in. If null,
     *                                Xml is not wrapped
     * @return string                 Resulting Xml
     */
    protected function convertToXml(stdClass $object, $rootName = FALSE)
    {
        if (isset($object->billTo->customerID) && ($this->customerId == null)) {
            $this->customerId = $object->billTo->customerID;
        } elseif (($this->customerId == null)) {
            $this->customerId = '****';
        }
        $result = $rootName ? '<' . $rootName . '>' : '';
        foreach ($object as $key => $value) {
            if ($value instanceof stdClass || is_array($value)) {
                $result .= $this->convertToXml((object) $value, $key);
            } else {
                $result .= '<' . $key . '>';
                if ($result == '<customerID>') {
                    $customerId = $value;
                }

                if ($this->isDataPrivate($key)) {
                    $result .= '****';
                } else {
                    if ($this->isNameChange($key)) {
                        $result .= $this->customerId;
                    } else {
                        if ($this->isAccountNumber($key)) {
                            $result .= substr($value, 12);
                        } else {
                            $result .= $value;
                        }
                    }
                }
                $result .= '</' . $key . '>';
            }
        }
        $result .= $rootName ? '</' . $rootName . '>' : '';
        return $result;
    }

    /**
     * Check if $key is a name property
     *
     * @param  string  $key Key to check
     * @return boolean      True if $key is a name property
     */
    protected function isNameChange($key)
    {
        $data = array(
            'firstName',
            'lastName'
        );
        return in_array($key, $data);
    }

    /**
     * Check if $key is an account number
     *
     * @param  string  $key Key to check
     * @return boolean      True if $key is an account number
     */
    protected function isAccountNumber($key)
    {
        $data = array(
            'accountNumber'
        );
        return in_array($key, $data);
    }

    /**
     * Check if $key is private data
     *
     * @param  string  $key Key to check
     * @return boolean      True if $key is private
     */
    protected function isDataPrivate($key)
    {
        $data = array(
            'merchantID',
            'expirationYear',
            'expirationMonth',
            'cvNumber'
        );
        return in_array($key, $data);
    }

    /**
     * Set request
     *
     * @param mixed $value Request object or string. This may be refactored in
     *                     the future to accept only an
     *                     Cybersource_PaymentManagement_Model_Soap_Request object.
     */
    public function setRequest($value)
    {
        if ($this->isLogEnabled()) {
            if ($value instanceof stdClass) {
                $this->setTags($value);
            }
            $value = $this->convertToXml($value);
            parent::setRequest($value);
        }
        return $this;
    }

    /**
     * Set response
     *
     * @param mixed $value Response object or string. This may be refactored in
     *                     the future to accept only an
     *                     Cybersource_PaymentManagement_Model_Soap_Response object.
     */
    public function setResponse($value)
    {
        if ($this->isLogEnabled()) {
            if ($value instanceof stdClass) {
                /**
                 * @ todo Check if setTags should be called here. It was commented
                 * out in the "dav" branch
                 */
                $this->setTags($value);
                $value = $this->convertToXml($value);
            }
            parent::setResponse($value);
        }
        return $this;
    }

    /**
     * Set tags
     *
     * Converts to string of comma separated tags if argument passed is an object.
     * This is done via the createTagsForTransaction() method
     *
     * @param string|stdClass $value Comma separated string of tags or an object
     *                               of a "request" generated from
     *                               Cybersource_PaymentManagement_Model_Soap_Request->getRequest()
     */
    public function setTags($value)
    {
        if (is_object($value)) {
            $tags = $this->createTagsForTransaction($value);
        } elseif (is_string($value)) {
            $tags = $value;
        } else {
            Mage::throwException('Invalid argument for $value. Must be string or stdClass');
        }

        $this->setData('tags', $tags);
        return $this;
    }

    /**
     * Save log entry
     *
     * @return self Provides a fluent interface
     */
    public function save()
    {
        /**
         * @todo Add better exception handling for save() if log is not enabled.
         *       The current handling would not notify user in any way.
         */
        if ($this->isLogEnabled()) {
            return parent::save();
        }
        return $this;
    }

    /**
     * Create string of comma separated tags from object returned from
     * Cybersource_PaymentManagement_Model_Soap_Request->getRequest()
     *
     * @param  stdClass $object Object returned from
     *                          Cybersource_PaymentManagement_Model_Soap_Request->getRequest()
     * @return string           Comma separated list of tags
     */
    protected function createTagsForTransaction(stdClass $object)
    {
        $tags = array();

        if (isset($object->ccAuthService)
            || isset($object->ccCaptureService)
            || isset($object->ccCreditService)
            || isset($object->ccAuthReversalService)
            || isset($object->voidService)
            || isset($object->ccCaptureReply)
            || isset($object->ccAuthReply)
            || isset($object->ccAuthReversalReply)
            || isset($object->voidReply)
        ) {
            $tags[] = 'payment';
        }
        if (isset($object->davService) || isset($object->davReply)) {
            $tags[] = 'dav';
        }
        if (isset($object->paySubscriptionCreateService)
            || isset($object->paySubscriptionCreateReply)
        ) {
            $tags[] = 'token';
        }
        if (isset($object->decision)) {
            if ($object->decision == Cybersource_PaymentManagement_Helper_Decision::REVIEW
                || $object->decision == Cybersource_PaymentManagement_Helper_Decision::REJECT
                || $object->decision == Cybersource_PaymentManagement_Helper_Decision::ACCEPT
            ) {
                $tags[] = 'decision';
            }
        }

        return implode(',', $tags);
    }

}
