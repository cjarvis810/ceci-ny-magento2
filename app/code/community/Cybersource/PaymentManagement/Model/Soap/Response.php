<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 *
 * @todo Review why response classes "speak of themselves in the third party". It
 *       seems like the response data should be set directly in this object's
 *       _data array (since it's a Varien_Object) rather than in a child "property"
 *       (really a key in data array) "response".
 */
class Cybersource_PaymentManagement_Model_Soap_Response extends Varien_Object
{

    /**
     * Reason codes
     */
    const CODE_SUCCESS      = 100;
    const CODE_AVS_DECLINE  = 200;
    const CODE_CVN_DECLINE  = 230;
    const CODE_DECISION_REVIEW = 480;

    /**
     * Response field names
     */
    const REQUEST_ID        = 'requestID';
    const REQUEST_TOKEN     = 'requestToken';

    /**
     * Check if response is success
     *
     * @return boolean True if response is success
     */
    public function isSuccess()
    {
        return $this->getResponse()->reasonCode == self::CODE_SUCCESS;
    }

    /**
     * Get reason code from response
     *
     * @return int Reason code
     */
    public function getReasonCode()
    {
        return $this->getResponse()->reasonCode;
    }

    /**
     * Get decision
     *
     * @return string Decision string "ACCEPT", "REVIEW", or "REJECT"
     */
    public function getDecision()
    {
        return $this->getResponse()->decision;
    }

    /**
     * Get request ID
     *
     * @todo Document purpose of request ID
     * @return int Request ID
     */
    public function getRequestId()
    {
        return $this->getResponse()->{self::REQUEST_ID};
    }

    /**
     * Get request token
     *
     * @todo Document purpose of request token
     * @return string Request token
     */
    public function getRequestToken()
    {
        return $this->getResponse()->{self::REQUEST_TOKEN};
    }

    /**
     * Get message
     *
     * @todo Document purpose of message and where this is used
     * @return string Message
     */
    public function getMessage()
    {
        return $this->__($this->getResponse()->decision) . ': '
            . $this->getReasonMessage()
            . Mage::helper('cybersource')->prepareSignature();
    }

    /**
     * Get text message for specific reason code
     *
     * @internal TALK-MESSAGE
     * @return string Message
     */
    public function getReasonMessage()
    {
        /* @link http://apps.cybersource.com/library/documentation/dev_guides/CC_Svcs_SO_API/html/wwhelp/wwhimpl/js/html/wwhelp.htm See Reason Codes section */
        switch ($this->getResponse()->reasonCode) {
            case self::CODE_SUCCESS:
                return $this->__('Successful transaction.');
            case 101:
                return $this->__('The request is missing one or more required fields.');
            case 102:
                return $this->__('One or more fields in the request contains invalid data.');
            case 110:
                return $this->__('Only a partial amount was approved.');
            case 150:
                return $this->__('General system failure.');
            case 151:
                return $this->__('The request was received but there was a server timeout. This error does not include timeouts between the client and the server.');
            case 152:
                return $this->__('The request was received, but a service did not finish running in time.');
            case 200:
                return $this->__('The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the Address Verification System (AVS) check.');
            case 201:
                return $this->__('The issuing bank has questions about the request. You do not receive an authorization code programmatically, but you might receive one verbally by calling the processor.');
            case 202:
                return $this->__('Expired card. You might also receive this if the expiration date you provided does not match the date the issuing bank has on file.');
            case 203:
                return $this->__('General decline of the card. No other information was provided by the issuing bank.');
            case 204:
                return $this->__('Insufficient funds in the account.');
            case 205:
                return $this->__('Stolen or lost card.');
            case 207:
                return $this->__('Issuing bank unavailable.');
            case 208:
                return $this->__('Inactive card or card not authorized for card-not-present transactions.');
            case 209:
                return $this->__('American Express Card Identification Digits (CID) did not match.');
            case 210:
                return $this->__('The card has reached the credit limit.');
            case 211:
                return $this->__('Invalid CVN.');
            case 221:
                return $this->__('The customer matched an entry on the processor’s negative file.');
            case 230:
                return $this->__('The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the CVN check.');
            case 231:
                return $this->__('Invalid account number.');
            case 232:
                return $this->__('The card type is not accepted by the payment processor.');
            case 233:
                return $this->__('General decline by the processor.');
            case 234:
                return $this->__('There is a problem with the information in your CyberSource account.');
            case 235:
                return $this->__('The requested capture amount exceeds the originally authorized amount.');
            case 236:
                return $this->__('Processor failure.');
            case 237:
                return $this->__('The authorization has already been reversed.');
            case 238:
                return $this->__('The authorization has already been captured.');
            case 239:
                return $this->__('The requested transaction amount must match the previous transaction ');
            case 240:
                return $this->__('The card type sent is invalid or does not correlate with the credit card number.');
            case 241:
                return $this->__('The request ID is invalid.');
            case 242:
                return $this->__('You requested a capture, but there is no corresponding, unused authorization record. Occurs if there was not a previously successful authorization request or if the previously successful authorization has already been used by another capture request.');
            case 243:
                return $this->__('The transaction has already been settled or reversed.');
            case 246:
                return $this->__("One of the following:\n* - The capture or credit is not voidable because the capture or credit information has already been submitted to your processor.\n- or -\n* - You requested a void for a type of transaction that cannot be voided.");
            case 247:
                return $this->__('You requested a credit for a capture that was previously voided.');
            case 250:
                return $this->__('The request was received, but there was a timeout at the payment processor.');
            default:
                return $this->__('Unknown error');
        }
    }

    /**
     * Alias to translate method
     *
     * @param  string $message String to translate
     * @return string          Translated string
     */
    protected function __($message)
    {
        return Mage::helper('cybersource')->__($message);
    }

}
