<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 *
 */
class Cybersource_PaymentManagement_Model_Soap_Request extends Varien_Object
{
    /**
     * Get request. Initialize if not set yet.
     *
     * @return stdClass Request object
     */
    public function getRequest()

    {
        if (!$this->hasData('request')) {
            $request = new stdClass();
            $request->merchantID = Mage::getStoreConfig('cybersource/account/merchant');

            /**
             *     @todo Review logic for $orderId in getRequest(). Parts of the
             *       method's code seem to expect order_id to be set in this object.
             */
            $orderId = Mage::app()->getFrontController()->getRequest()->getParam('order_id');
            if($orderId)
            {
                $order = Mage::getModel('sales/order')->load($orderId);
                $request->merchantID = Mage::getStoreConfig('cybersource/account/merchant', $order->getStoreId());
            }

            /**
              * @todo Add check where we are
              * */
            $adminQuote = Mage::getSingleton('adminhtml/session_quote')->getQuote();
            $quote = Mage::getSingleton('checkout/session')->getQuote();

            if ($quote && $quote->hasReservedOrderId()) {
                $request->merchantReferenceCode = $quote->getReservedOrderId();
            } elseif ($adminQuote && $adminQuote->hasReservedOrderId()) {
                $request->merchantReferenceCode = $adminQuote->getReservedOrderId();
            } elseif ($this->hasData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID) &&
                    $this->hasData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN) && $this->hasData('merchant_ref_number')
            ) {
                $request->merchantReferenceCode = $this->getData('merchant_ref_number');
            }
            elseif ($this->hasData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID) &&
                $this->hasData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN) && $this->hasData('order_id')
            ) {
                $request->merchantReferenceCode = $this->getData('order_id');
            }elseif (Mage::registry('current_creditmemo')) {
                $creditMemoOrderId = Mage::registry('current_creditmemo')->getOrderId();
                $request->merchantReferenceCode = Mage::getModel('sales/order')->load($creditMemoOrderId)->getData('increment_id');
            } else {
                $request->merchantReferenceCode = Mage::helper('core')->uniqHash();
            }

            if (Mage::getStoreConfig('payment/cybersource_card_processing/df_enabled')) {
                $request->deviceFingerprintID = session_id();
            }

            ///// DO NOT CHANGE STARTS ////////////////////////////////////////////////////////////////
            $version = Mage::getConfig()->getModuleConfig("Cybersource_PaymentManagement")->version;
            $request->clientLibrary = "Magento Extension " . $version;
            $request->clientEnvironment = php_uname();
            ///// DO NOT CHANGE ENDS //////////////////////////////////////////////////////////////////


            $this->setData('request', $request);
        }
        return $this->getData('request');
    }

    /**
     * Add payment information
     *
     * @param Varien_Object $payment Payment
     * @return self Provides a fluent interface
     */
    public function addPayment(Varien_Object $payment)
    {
        $this->setData('payment', $payment);
        return $this;
    }

    /**
     * Prepare request for tokenization SOAP call
     *
     * @param  Cybersource_PaymentManagement_Model_Soap_Response  $response Response returned from capture SOAP request
     * @return self                                     Provides a fluent interface
     */
    public function tokenizationRequest(Cybersource_PaymentManagement_Model_Soap_Response $response)
    {

            $request                                                    = $this->getRequest();
            $request->ccAuthService                                     = null;
            $request->billTo                                            = null;
            $request->shipTo                                            = null;
            $request->card                                              = null;
            $request->purchaseTotals                                    = null;
            $request->businessRules                                     = null;
            $request->item                                              = null;
            $request->merchantDefinedData                               = null;
            $request->ccCaptureService                                  = null;
            $request->davService                                        = null;
            $request->merchantReferenceCode                             = $request->merchantReferenceCode;
            $request->paySubscriptionCreateService                      = new stdClass();
            $request->paySubscriptionCreateService->run                 = 'true';
            $request->paySubscriptionCreateService->paymentRequestID    = $response->getResponse()->requestID;
            $request->recurringSubscriptionInfo                         = new stdClass();
            $request->recurringSubscriptionInfo->frequency              = 'on-demand';
        return $this;
    }

}
