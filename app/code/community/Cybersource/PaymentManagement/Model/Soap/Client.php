<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Client extends SoapClient
{

    const WSDL_URL_TEST = 'https://ics2wstest.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.70.wsdl';
    const WSDL_URL_LIVE = 'https://ics2ws.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.70.wsdl';

    /**
     * Set WSDL and options
     *
     * @param string|null $wsdl    WSDL URL
     * @param array       $options Options
     */
    public function __construct($wsdl = null, $options = array())
    {
        if (!$wsdl) {
            $wsdl = $this->getWsdlUrl();
        }
        parent::SoapClient($wsdl, $options);
    }

    /**
     * Do SOAP request
     *
     * @param  string  $request  XML SOAP request
     * @param  string  $location URL to request
     * @param  string  $action   SOAP action
     * @param  int      $version SOAP version
     * @param  int      $one_way If one_way is set to 1, this method returns nothing. Use this where a response is not expected.
     * @return string            XML SOAP response
     */
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $username = $this->getConfig('merchant');
        $password = $this->getConfig('key');

        $soapHeader = '<SOAP-ENV:Header '
                    . 'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" '
                    . 'xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
                    . '<wsse:Security SOAP-ENV:mustUnderstand="1">'
                    . '<wsse:UsernameToken>' . '<wsse:Username>' . $username
                    . '</wsse:Username>'
                    . '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'
                    . $password . '</wsse:Password>' . '</wsse:UsernameToken>'
                    . '</wsse:Security>' . '</SOAP-ENV:Header>';

        $requestDOM = new DOMDocument('1.0');
        $soapHeaderDOM = new DOMDocument('1.0');

        try {

            $requestDOM->loadXML($request);
            $soapHeaderDOM->loadXML($soapHeader);

            $node = $requestDOM->importNode($soapHeaderDOM->firstChild, true);
            $requestDOM->firstChild->insertBefore($node, $requestDOM->firstChild->firstChild);

            $request = $requestDOM->saveXML();
        } catch (DOMException $e) {
            die('Error adding UsernameToken: ' . $e->code);
        }
        return parent::__doRequest($request, $location, $action, $version);
    }

    /**
     * Get config value
     *
     * @param  string $key Config key to get
     * @return string|int  Value of key in config
     */
    protected function getConfig($key)
    {
        $store = $this->_getStore();
        return Mage::getStoreConfig('cybersource/account/' . $key, $store) ?:
            Mage::getStoreConfig('cybersource/account/' . $key);
    }

    /**
     * Send request and return response
     *
     * @param Cybersource_PaymentManagement_Model_Soap_Request $request Request to send
     * @return Cybersource_PaymentManagement_Model_Soap_Response Response returned
     */
    public function getResponse(Cybersource_PaymentManagement_Model_Soap_Request $request)
    {
        $_request = $request->getRequest();
        $_response = $this->runTransaction($_request);
        //Checking debug
        if (Mage::getStoreConfig('payment/support/log')) {
            $this->getLog()->setRequest($_request)->setResponse($_response)->save(); //@TODO TALK-LOG
        }
        return Mage::getModel('cybersource/soap_response')->setResponse($_response);
    }

    /**
     * Get cybersource log
     *
     * @return Cybersource_PaymentManagement_Model_Soap_Log Cybersource log
     */
    protected function getLog()
    {
        return Mage::getModel('cybersource/soap_log');
    }

    /**
     * Get WSDL URL
     *
     * @return string WSDL URL
     */
    protected function getWsdlUrl()
    {
        $store = $this->_getStore();
        if (Mage::getStoreConfig('payment/cybersource_card_processing/test', $store) || Mage::getStoreConfig('payment/cybersource_card_processing/test')) {
            return self::WSDL_URL_TEST;
        } else {
            return self::WSDL_URL_LIVE;
        }
    }

    protected function _getStore()
    {
        $orderId = Mage::app()->getFrontController()->getRequest()->getParam('order_id');
        if($orderId)
        {
            return Mage::getModel('sales/order')->load($orderId)->getStoreId();
        }
        else
        {
            return null;
        }
    }

}
