<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Request_Capture
    extends Cybersource_PaymentManagement_Model_Soap_Request_Authorize
{

    /**
     * Initialize ccCaptureService
     *
     * @return void
     */
    public function _construct()
    {
        $request = $this->getRequest();
        $request->ccCaptureService = new stdClass();
        $request->ccCaptureService->run = 'true';

        /**
         * @todo  Review why parent::_construct() is only called if request ID
         *        and request token are not set. Document this here.
         */
        if (!$this->hasData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID) &&
            !$this->hasData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN)
        ) {
            parent::_construct();
        } else {
            $request->ccCaptureService->authRequestID = $this->getData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID);
            $request->ccCaptureService->authRequestToken = $this->getData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN);
        }
    }

    /**
     * Add amount data
     *
     *
     * @param float $amount Payment amount
     * @return self  Provides a fluent interface
     */
    public function addAmountData($amount)
    {
        if (!$this->hasData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID) &&
            !$this->hasData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN)
        ) {
            return parent::addAmountData($amount);
        } else {
            $request = $this->getRequest();
            if (!isset($request->purchaseTotals)) {
                $request->purchaseTotals = new stdClass();
            }
            $request->purchaseTotals->grandTotalAmount = $amount;
        }

        return $this;
    }
}

