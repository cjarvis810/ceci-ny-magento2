<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Request_Decision
    extends Cybersource_PaymentManagement_Model_Soap_Request
{
    /**
     * Appends merchant defined data fields to API request
     *
     * @link http://support.cybersource.com/cybskb/index?page=content&id=C173&actp=RSS
     * @param  Varien_Object $payment Payment information object
     * @return void
     */
    protected function tempappendMerchantDefinedDataFields($payment)
    {

        if (Mage::getStoreConfig('cybersource/decision_manager/mddf_enable')) {

            $order = $payment->getOrder();
            if (is_null($order->getData('customer'))) {
                $customerId = $order->getData('customer_id');
                $hashPassword = Mage::getModel('customer/customer')->load($customerId)->getData('password_hash');
            } else {
                $hashPassword = $order->getData('customer')->getData('password_hash');
            }

            $bin                = substr_replace($payment->getData('cc_number'), "", 5, 10);
            $couponCode         = $order->getCouponCode();
            $paymentMethod      = $payment->getData('method');
            $languageInfo       = Mage::app()->getLocale()->getLocaleCode();
            $isGuestCheckout      = $order->getCustomerIsGuest();
            if($isGuestCheckout) {
                $customerGuest  = 'Yes' ;
            } else {
                $customerGuest  = 'No' ;
            }
            $brand              = $order->getdata('store_name');
            $shippingMethod     = $order->getShippingMethod();

            if ((bool)$order->getRemoteIp()) {
                $createdFrom = "Frontend order";

            } else {
                $createdFrom = "Admin order";
            }

            $modelSession   = Mage::getSingleton('core/session');
            $information    = $modelSession->getData('_session_validator_data');
            $modelVisitor   = Mage::getSingleton('log/visitor');

            $userInformationSession     = $modelVisitor->getFirstVisitAt() . "->" . $modelVisitor->getLastVisitAt();
            $userInformation            = $information['remote_addr'] . " " . $information['http_user_agent'];

            if (is_null($order->getdata('customer'))) {
                $customerEntityId   = Mage::getModel('customer/customer')->load($customerId)->getData('entity_id');
                $customerCreated    = Mage::getModel('customer/customer')->load($customerId)->getData('created_at');
            } else {
                $customerEntityId   = $order->getdata('customer')->getdata('entity_id');
                $customerCreated    = $order->getData('customer')->getData('created_at');

            }
            $modelFilter = Mage::getModel('sales/order')
            ->getCollection()
            ->addFieldToFilter('customer_id', array('eq' => $customerEntityId))
            ->load();
            $countOrders = $modelFilter->count(); //send this 1
            if ($countOrders == 0) {
                $firstP = 'N/A';
                $allId = 0;
                $preLastId = 0;
                $lastP = 'N/A';
            } elseif ($countOrders == 1) {
                $firstP = $modelFilter->getFirstItem()->getCreatedAtFormated("full");
                $lastP = $firstP;
            } elseif ($countOrders > 1) {
                $firstP = $modelFilter->getFirstItem()->getCreatedAtFormated("full");
                $allId = $modelFilter->getAllIds();
                $preLastId = $allId[$countOrders - 2];
                $lastP = $modelFilter->getItemById($preLastId)->getCreatedAtFormated("full");
            }
            $modelGift = Mage::getModel('giftmessage/message');

            $giftId = $order->getGiftMessageId();
            $giftMessage = $modelGift->load($giftId)->getData('message');
            if ($giftMessage == null) $giftMessage = 'N/A';

            //Merchant Defined Data Fields
            $merchantDefinedData = new stdClass();
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_countOrders')) {
                $merchantDefinedData->field1 = $countOrders;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_bin')) {
                $merchantDefinedData->field2 = $bin;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_paymentMethod')) {
                $merchantDefinedData->field3 = $paymentMethod;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_languageInfo')) {
                $merchantDefinedData->field4 = $languageInfo;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_brand')) {
                $merchantDefinedData->field5 = (string)$brand;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_shippingMethod')) {
                $merchantDefinedData->field6 = $shippingMethod;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_customerGuest')) {
                $merchantDefinedData->field7 = $customerGuest;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_hashPassword')) {
                $merchantDefinedData->field8 = $hashPassword;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_createdFrom')) {
                if ($couponCode) {
                    $merchantDefinedData->field9 = $couponCode;
                } else {
                    $merchantDefinedData->field9 = "N/A";
                }
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_couponCode')) {
                $merchantDefinedData->field10 = $createdFrom;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_userInformation')) {
                $merchantDefinedData->field11 = $userInformation;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_userInformationSession')) {
                $merchantDefinedData->field12 = $userInformationSession;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_firstP')) {
                $merchantDefinedData->field13 = $firstP;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_lastP')) {
                $merchantDefinedData->field14 = $lastP;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_customerCreated')) {
                $merchantDefinedData->field15 = $customerCreated;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_gift_message')) {
                $merchantDefinedData->field16 = $giftMessage;
            }
            //if(Mage::getStoreConfig('cybersource/decision_manager/mddf_gift_method')) {
            //    $request->merchantDefinedData->field17 = "need to addd";
            //}

            $this->getRequest()->merchantDefinedData = $merchantDefinedData;

        }
    }

}