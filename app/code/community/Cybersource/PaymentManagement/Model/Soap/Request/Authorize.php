<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Request_Authorize
    extends Cybersource_PaymentManagement_Model_Soap_Request
{

    /**
     * Set request and ccAuthService
     *
     * @return void
     */
    protected function _construct()
    {
        $request = $this->getRequest();
        $request->ccAuthService = new stdClass();
        $request->ccAuthService->run = 'true';
    }

    /**
     * Handle MDDF
     *
     * @link http://support.cybersource.com/cybskb/index?page=content&id=C173&actp=RSS
     * @param  Varien_Object $payment Payment information object
     * @return void
     */
    public function appendMerchantDefinedDataFields($payment)
    {

        if (Mage::getStoreConfig('cybersource/decision_manager/mddf_enable')) {

            $order = $payment->getOrder();
            if (is_null($order->getData('customer'))) {
                $customerId = $order->getData('customer_id');
                $hashPassword = Mage::getModel('customer/customer')->load($customerId)->getData('password_hash');
            } else {
                $hashPassword = $order->getData('customer')->getData('password_hash');
            }

            $bin                = substr_replace($payment->getData('cc_number'), "", 5, 10);
            $couponCode         = $order->getCouponCode();
            $paymentMethod      = $payment->getData('method');
            $languageInfo       = Mage::app()->getLocale()->getLocaleCode();
            $isGuestCheckout      = $order->getCustomerIsGuest();
            if($isGuestCheckout) {
                $customerGuest  = 'Yes' ;
            } else {
                $customerGuest  = 'No' ;
            }
            $brand              = $order->getdata('store_name');
            $shippingMethod     = $order->getShippingMethod();

            if ((bool)$order->getRemoteIp()) {
                $createdFrom = "Frontend order";

            } else {
                $createdFrom = "Admin order";
            }

            $modelSession   = Mage::getSingleton('core/session');
            $information    = $modelSession->getData('_session_validator_data');
            $modelVisitor   = Mage::getSingleton('log/visitor');

            $userInformationSession     = $modelVisitor->getFirstVisitAt() . "->" . $modelVisitor->getLastVisitAt();
            $userInformation            = $information['remote_addr'] . " " . $information['http_user_agent'];

            if (is_null($order->getdata('customer'))) {
                $customerEntityId   = Mage::getModel('customer/customer')->load($customerId)->getData('entity_id');
                $customerCreated    = Mage::getModel('customer/customer')->load($customerId)->getData('created_at');
            } else {
                $customerEntityId   = $order->getdata('customer')->getdata('entity_id');
                $customerCreated    = $order->getData('customer')->getData('created_at');

            }
            $modelFilter = Mage::getModel('sales/order')
                         ->getCollection()
                         ->addFieldToFilter('customer_id', array('eq' => $customerEntityId))
                         ->load();
            $countOrders = $modelFilter->count(); //send this 1
            if ($countOrders == 0) {
                $firstP = 'N/A';
                $allId = 0;
                $preLastId = 0;
                $lastP = 'N/A';
            } elseif ($countOrders == 1) {
                $firstP = $modelFilter->getFirstItem()->getCreatedAtFormated("full");
                $lastP = $firstP;
            } elseif ($countOrders > 1) {
                $firstP = $modelFilter->getFirstItem()->getCreatedAtFormated("full");
                $allId = $modelFilter->getAllIds();
                $preLastId = $allId[$countOrders - 2];
                $lastP = $modelFilter->getItemById($preLastId)->getCreatedAtFormated("full");
            }
            $modelGift = Mage::getModel('giftmessage/message');

            $giftId = $order->getGiftMessageId();
            $giftMessage = $modelGift->load($giftId)->getData('message');
            if ($giftMessage == null) $giftMessage = 'N/A';

            //Merchant Defined Data Fields
            $merchantDefinedData = new stdClass();
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_countOrders')) {
                $merchantDefinedData->field1 = $countOrders;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_bin')) {
                $merchantDefinedData->field2 = $bin;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_paymentMethod')) {
                $merchantDefinedData->field3 = $paymentMethod;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_languageInfo')) {
                $merchantDefinedData->field4 = $languageInfo;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_brand')) {
                $merchantDefinedData->field5 = (string)$brand;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_shippingMethod')) {
                $merchantDefinedData->field6 = $shippingMethod;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_customerGuest')) {
                $merchantDefinedData->field7 = $customerGuest;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_hashPassword')) {
                $merchantDefinedData->field8 = $hashPassword;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_createdFrom')) {
                if ($couponCode) {
                    $merchantDefinedData->field9 = $couponCode;
                } else {
                    $merchantDefinedData->field9 = "N/A";
                }
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_couponCode')) {
                $merchantDefinedData->field10 = $createdFrom;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_userInformation')) {
                $merchantDefinedData->field11 = $userInformation;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_userInformationSession')) {
                $merchantDefinedData->field12 = $userInformationSession;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_firstP')) {
                $merchantDefinedData->field13 = $firstP;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_lastP')) {
                $merchantDefinedData->field14 = $lastP;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_customerCreated')) {
                $merchantDefinedData->field15 = $customerCreated;
            }
            if (Mage::getStoreConfig('cybersource/decision_manager/mddf_gift_message')) {
                $merchantDefinedData->field16 = $giftMessage;
            }
            //if(Mage::getStoreConfig('cybersource/decision_manager/mddf_gift_method')) {
            //    $request->merchantDefinedData->field17 = "need to addd";
            //}

            $this->getRequest()->merchantDefinedData = $merchantDefinedData;

        }
    }

    /**
     * Add payment
     *
     * @param Varien_Object $payment Payment information object
     * @return self                  Provides a fluent interface
     */
    public function addPayment(Varien_Object $payment)
    {
        $data = $this->createBillingAddressData($payment);
        $this->addBillingAddressData($data);

        $data = $this->createShippingAddressData($payment);
        $this->addShippingAddressData($data);

        $data = $this->createCreditCardData($payment);
        $this->addCreditCardData($data);

        $data = $this->createPurchaseTotalsData($payment);
        $this->addPurchaseTotalsData($data);

        $data = $this->createBusinessRulesData();
        $this->addBusinessRulesData($data);

        //if customer logged added it to request
        $customerId = $payment->getOrder()->getCustomerID();
        if (isset($customerId) && !empty($customerId)) {
            $request = $this->getRequest();
            $request->billTo->customerID = $customerId;
        }

        parent::addPayment($payment);
        return $this;
    }

    /**
     * Add billing address data
     *
     * @param array $address Billing address data generated from
     *                       self::createBillingAddressData()
     * @return self          Provides a fluent interface
     */
    public function addBillingAddressData(array $address)
    {
        $request = $this->getRequest();
        $request->billTo = new stdClass();
        foreach ($address as $key => $value) {
            $request->billTo->$key = $value;
        }
        return $this;
    }

    /**
     * Add shipping address data
     *
     * @param array $address Shipping address data generated from
     *                       self::createShippingAddressData()
     * @return self          Provides a fluent interface
     */
    public function addShippingAddressData(array $address)
    {
        $request = $this->getRequest();
        $request->shipTo = new stdClass();
        foreach ($address as $key => $value) {
            $request->shipTo->$key = $value;
        }
        return $this;
    }

    /**
     * Add credit card data
     *
     * @param array $info Credit card data generated from
     *                    self::createCreditCardData()
     * @return self       Provides a fluent interface
     */
    public function addCreditCardData(array $info)
    {
        $request = $this->getRequest();
        $request->card = new stdClass();
        foreach ($info as $key => $value) {
            $request->card->$key = $value;
        }
        return $this;
    }

    /**
     * Add purchase totals data
     *
     * @param array $totals Purchase totals data generated from
     *                      self::createPurchaseTotalsData()
     * @return self         Provides a fluent interface
     */
    public function addPurchaseTotalsData(array $totals)
    {
        $request = $this->getRequest();
        if (!isset($request->purchaseTotals)) {
            $request->purchaseTotals = new stdClass();
        }
        foreach ($totals as $key => $value) {
            $request->purchaseTotals->$key = $value;
        }
        return $this;
    }

    /**
     * Add amount data
     *
     * @param float $amount Payment amount
     * @return self         Provides a fluent interface
     */
    public function addAmountData($amount)
    {
        $request = $this->getRequest();
        if (empty($request->purchaseTotals)) {
            $request->purchaseTotals = new stdClass();
        }
        $request->purchaseTotals->grandTotalAmount = $amount;
        return $this;
    }

    /**
     * Add items data
     *
     * @param array $items  Items to add in format returned by $order->getAllItems()
     * @return self         Provides a fluent interface
     */
    public function addItemsData(array $items)
    {
        if (count($items) > 0) {
            $request = $this->getRequest();
            $request->item = array();
            foreach ($items as $key => $product) {
                $request->item[$key]                = new stdClass();
                $request->item[$key]->id            = $product->getItemId();
                $request->item[$key]->unitPrice     = $product->getPrice();
                $request->item[$key]->quantity      = $product->getQtyOrdered();
                $request->item[$key]->productName   = $product->getName();
                $request->item[$key]->productSKU    = $product->getSku();
                $request->item[$key]->taxAmount     = $product->getTaxAmount();
            }
        }
        /* $request = $this->getRequest();
      if (!isset($request->purchaseTotals))
          $request->purchaseTotals = new stdClass();
      $request->purchaseTotals->grandTotalAmount = $amount;*/
        return $this;
    }

    /**
     * Add business rules data
     *
     * @param array $rules Business rules data generated from
     *                     self::createBusinessRulesData()
     * @return self        Provides a fluent interface
     */
    public function addBusinessRulesData(array $rules)
    {
        $request = $this->getRequest();
        $request->businessRules = new stdClass();
        foreach ($rules as $key => $value) {
            $request->businessRules->$key = $value;
        }
        return $this;
    }

    /**
     * Create billing address data from payment
     *
     * @param  Varien_Object $payment Payment information object
     * @return array                  Billing address data
     */
    protected function createBillingAddressData(Varien_Object $payment)
    {
        $result = array();
        $address = $payment->getOrder()->getBillingAddress();

        if (!$address) {
            $result['firstName']    = 'NOREAL';
            $result['lastName']     = 'NAME';
            $result['email']        = 'null@cybersource.com';
            $result['street1']      = '1295 Charleston Road';
            $result['city']         = 'Mountain View';
            $result['state']        = 'CA';
            $result['postalCode']   = '94043';
            $result['country']      = 'US';
        } else {
            $result['firstName']    = $address->getFirstname();
            $result['lastName']     = $address->getLastname();
            $result['email']        = $payment->getOrder()->getCustomerEmail();
            $result['company']      = $address->getCompany();
            $result['street1']      = $address->getStreet(1);
            $result['street2']      = $address->getStreet(2);
            $result['city']         = $address->getCity();
            $result['state']        = $this->getStateCodeFromRegionName($address->getRegion());
            $result['postalCode']   = $address->getPostcode();
            $result['country']      = $address->getCountry();
            if (Mage::getStoreConfig('payment/cybersource_card_processing/phone_number')) {
                $result['phoneNumber'] = $address->getTelephone();
            }
        }
        $result['ipAddress'] = $payment->getOrder()->getRemoteIp();

        $result = $this->unsetEmptyValues($result);
        return $result;
    }

    /**
     * Create shipping address data from payment
     *
     * @param  Varien_Object $payment Payment information object
     * @return array                  Shipping address data
     */
    protected function createShippingAddressData(Varien_Object $payment)
    {
        $result = array();
        $address = $payment->getOrder()->getShippingAddress();

        $result['firstName']    = $address->getFirstname();
        $result['lastName']     = $address->getLastname();
        $result['company']      = $address->getCompany();
        $result['street1']      = $address->getStreet(1);
        $result['street2']      = $address->getStreet(2);
        $result['city']         = $address->getCity();
        $result['state']        = $this->getStateCodeFromRegionName($address->getRegion());
        $result['postalCode']   = $address->getPostcode();
        $result['country']      = $address->getCountry();
        if (Mage::getStoreConfig('payment/cybersource_card_processing/phone_number')) {
            $result['phoneNumber'] = $address->getTelephone();
        }

        $result = $this->unsetEmptyValues($result);
        return $result;
    }

    /**
     * Create credit card data from payment
     *
     * @param  Varien_Object $payment Payment information object
     * @return array                  Credit card data
     */
    protected function createCreditCardData(Varien_Object $payment)
    {
        $result = array();

        $result['fullName']         = $payment->getCcOwner();
        $result['accountNumber']    = $payment->getCcNumber();
        $result['expirationMonth']  = $payment->getCcExpMonth();
        $result['expirationYear']   = $payment->getCcExpYear();
        if ($cardCode = Mage::getModel('cybersource/system_config_source_cctype')->getCardCode($payment->getCcType())) {
            $result['cardType'] = $cardCode;
        }
        if ($cvn = $payment->getCcCid()) {
            $result['cvNumber'] = $cvn;
        }

        $result = $this->unsetEmptyValues($result);
        return $result;
    }

    /**
     * Create purchase totals data from payment
     *
     * @param  Varien_Object $payment Payment information object
     * @return array                  Purchase totals data
     */
    protected function createPurchaseTotalsData(Varien_Object $payment)
    {
        $result = array();
        $_cur = $payment->getOrder()->getBaseCurrencyCode();
        if (!empty($_cur)) {
            $result['currency'] = $_cur;
        }

        return $result;
    }

    /**
     * Create business rules data from config
     *
     * @return array Business rules data
     */
    protected function createBusinessRulesData()
    {
        $result = array();

        if ($this->getConfigValue('avs')) {
            $result['ignoreAVSResult'] = 'false';
        } else {
            $result['ignoreAVSResult'] = 'true';
        }

        if ($this->getConfigValue('useccv')) {
            $result['ignoreCVResult'] = 'false';
        } else {
            $result['ignoreCVResult'] = 'true';
        }

        return $result;
    }

    /**
     * Alias to Helper_Data::getConfigValue()
     *
     * @param  string $key Key to get config value for
     * @return stinrg|int  Valud for key in config
     */
    protected function getConfigValue($key)
    {
        return Mage::helper('cybersource')->getConfigValue($key);
    }

    /**
     * Unset empty values in an array
     *
     * @param array $data
     * @return array
     */
    protected function unsetEmptyValues(array $data)
    {
        foreach ($data as $key => $value) {
            if (empty($value)) {
                unset ($data[$key]);
            }
        }
        return $data;
    }


    /**
     * Get state code from full region name
     * Example: Return state code "TX" from region name "Texas"
     *
     * @param string $region Region name (e.g. "Texas")
     * @return string        State code (e.g. "TX")
     */
    public function getStateCodeFromRegionName($regionName)
    {
        $regionNameToStateCode = array(
            'ALABAMA'                           => 'AL',
            'ALASKA'                            => 'AK',
            'AMERICAN SAMOA'                    => 'AS',
            'ARIZONA'                           => 'AZ',
            'ARKANSAS'                          => 'AR',
            'CALIFORNIA'                        => 'CA',
            'COLORADO'                          => 'CO',
            'CONNECTICUT'                       => 'CT',
            'DELAWARE'                          => 'DE',
            'DISTRICT OF COLUMBIA'              => 'DC',
            'FEDERATED STATES OF MICRONESIA'    => 'FM',
            'FLORIDA'                           => 'FL',
            'GEORGIA'                           => 'GA',
            'GUAM GU'                           => 'GU',
            'HAWAII'                            => 'HI',
            'IDAHO'                             => 'ID',
            'ILLINOIS'                          => 'IL',
            'INDIANA'                           => 'IN',
            'IOWA'                              => 'IA',
            'KANSAS'                            => 'KS',
            'KENTUCKY'                          => 'KY',
            'LOUISIANA'                         => 'LA',
            'MAINE'                             => 'ME',
            'MARSHALL ISLANDS'                  => 'MH',
            'MARYLAND'                          => 'MD',
            'MASSACHUSETTS'                     => 'MA',
            'MICHIGAN'                          => 'MI',
            'MINNESOTA'                         => 'MN',
            'MISSISSIPPI'                       => 'MS',
            'MISSOURI'                          => 'MO',
            'MONTANA'                           => 'MT',
            'NEBRASKA'                          => 'NE',
            'NEVADA'                            => 'NV',
            'NEW HAMPSHIRE'                     => 'NH',
            'NEW JERSEY'                        => 'NJ',
            'NEW MEXICO'                        => 'NM',
            'NEW YORK'                          => 'NY',
            'NORTH CAROLINA'                    => 'NC',
            'NORTH DAKOTA'                      => 'ND',
            'NORTHERN MARIANA ISLANDS'          => 'MP',
            'OHIO'                              => 'OH',
            'OKLAHOMA'                          => 'OK',
            'OREGON'                            => 'OR',
            'PALAU'                             => 'PW',
            'PENNSYLVANIA'                      => 'PA',
            'PUERTO RICO'                       => 'PR',
            'RHODE ISLAND'                      => 'RI',
            'SOUTH CAROLINA'                    => 'SC',
            'SOUTH DAKOTA'                      => 'SD',
            'TENNESSEE'                         => 'TN',
            'TEXAS'                             => 'TX',
            'UTAH'                              => 'UT',
            'VERMONT'                           => 'VT',
            'VIRGIN ISLANDS'                    => 'VI',
            'VIRGINIA'                          => 'VA',
            'WASHINGTON'                        => 'WA',
            'WEST VIRGINIA'                     => 'WV',
            'WISCONSIN'                         => 'WI',
            'WYOMING'                           => 'WY',
            'ARMED FORSES AFRICA'               => 'AE',
            'ARMED FORCES AMERICAS'             => 'AA',
            'ARMED FORCES CANADA'               => 'AE',
            'ARMED FORCES EUROPE'               => 'AE',
            'ARMED FORCES MIDDLE EAST'          => 'AE',
            'ARMED FORCES PACIFIC'              => 'AP',

            'ALBERTA'                           => 'AB',
            'BRITISH COLUMBIA'                  => 'BC',
            'MANITOBA'                          => 'MB',
            'NEW BRUNSWICK'                     => 'NB',
            'NEWFOUNDLAND AND LABRADOR'         => 'NL',
            'NORTHWEST TERRITORIES'             => 'NT',
            'NOVA SCOTIA'                       => 'NS',
            'NUNAVUT'                           => 'NU',
            'ONTARIO'                           => 'ON',
            'PRINCE EDWARD ISLAND'              => 'PE',
            'QUEBEC'                            => 'QC',
            'SASKATCHEWAN'                      => 'SK',
            'YUKON'                             => 'YT',

            'ALBERTA'                           => 'AB',
            'COLUMBIE-BRITANNIQUE'              => 'BC',
            'MANITOBA'                          => 'MB',
            'NOUVEAU-BRUNSWICK'                 => 'NB',
            'TERRE-NEUVE-ET-LABRADOR'           => 'NL',
            'TERRITOIRES DU NORD-QUEST'         => 'NT',
            'NOUVELLE-ÉCOSSE'                   => 'NS',
            'NUNAVUT'                           => 'NU',
            'ONTARIO'                           => 'ON',
            'ÎLE-DU-PRINCE-ÉDOUARD'             => 'PE',
            'QUéBEC'                            => 'QC',
            'SASKATCHEWAN'                      => 'SK',
            'YUKON'                             => 'YT',
        );

        $uppercaseRegionName = strtoupper($regionName);
        /**
         * @todo Review logic for getStateCodeFromRegionName() in_array($uppercaseRegion, $country).
         *       Pretty sure this should be using isset($country[$uppercaseRegion]) instead
         */
        if (in_array($uppercaseRegionName, $regionNameToStateCode)) {
            return $regionNameToStateCode[$uppercaseRegionName];
        } else {
            /**
             * @todo Add better exception handling in getStateCodeFromRegionName()
             * for when region name is not found in list
             */
            return $regionName;
        }
    }

    /**
     * Add currency data from payment information
     *
     * @param Varien_Object $payment Payment information object
     */
    public function addCurrencyData(Varien_Object $payment)
    {
        $data = $this->createPurchaseTotalsData($payment);
        $this->addPurchaseTotalsData($data);
        return $this;
    }


}
