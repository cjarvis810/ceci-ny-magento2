<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Request_Void
    extends Cybersource_PaymentManagement_Model_Soap_Request_Authorize
{

    /**
     * Initialize voidService. Called by on __construct()
     *
     * @return void
     */
    protected function _construct()
    {
        $request = $this->getRequest();
        $request->voidService = new stdClass();
        $request->voidService->run = 'true';
        $request->voidService->voidRequestID = $this->getData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID);
        $request->voidService->orderRequestToken = $this->getData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN);
    }

}
