<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Request_Reversal
    extends Cybersource_PaymentManagement_Model_Soap_Request_Authorize
{

    /**
     * Initialize ccAuthReversalService. Called by on __construct()
     *
     * @return void
     */
    protected function _construct()
    {
        $request = $this->getRequest();
        $request->ccAuthReversalService = new stdClass();
        $request->ccAuthReversalService->run = 'true';
        $request->ccAuthReversalService->authRequestID = $this->getData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID);
        $request->merchantReferenceCode = $this->getData('order_id');
        //$request->ccAuthReversalService->orderRequestToken = $this->getData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN);
    }

}
