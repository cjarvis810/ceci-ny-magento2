<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Request_Credit
    extends Cybersource_PaymentManagement_Model_Soap_Request_Authorize
{

    /**
     * Initialize ccCreditService
     * @return void
     */
    protected function _construct()
    {
        $request = $this->getRequest();
        $request->ccCreditService = new stdClass();
        $request->ccCreditService->run = 'true';
        $request->ccCreditService->captureRequestID = $this->getData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID);
        $request->ccCreditService->orderRequestToken = $this->getData(Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN);
    }

    /**
     * Add items data
     *
     * @param array $items  Items to add in format returned by $order->getAllItems()
     * @return self         Provides a fluent interface
     */
    public function addItemsData(array $items)
    {
        if (is_array($items) && count($items) >= 1) {
            $request = $this->getRequest();
            $request->item = array();
            foreach ($items as $key => $product) {
                $request->item[$key]                = new stdClass();
                $request->item[$key]->id            = $product->getOrderItemId();
                $request->item[$key]->unitPrice     = $product->getPrice();
                $request->item[$key]->quantity      = $product->getQty();
                $request->item[$key]->productName   = $product->getName();
                $request->item[$key]->productSKU    = $product->getSku();
                $request->item[$key]->taxAmount     = $product->getTaxAmount();
            }
        }
    }

}
