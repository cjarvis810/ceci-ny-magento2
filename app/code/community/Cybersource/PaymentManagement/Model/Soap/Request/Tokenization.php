<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Soap_Request_Tokenization
    extends Cybersource_PaymentManagement_Model_Soap_Request
{

    /**
     * Set request
     *
     * @return void
     */
    protected function _construct()
    {
        $request = $this->getRequest();
    }

    /**
     * Appends payment tokenization fields to API request
     *
     * @param  Cybersource_PaymentManagement_Model_Soap_Response  $response Response returned from capture SOAP request
     * @return self
     */
    public function appendTokenizationFields(Cybersource_PaymentManagement_Model_Soap_Response $response)
    {

       // $request                                                    = $this->getRequest();
        $request->ccAuthService                                     = null;
        $request->billTo                                            = null;
        $request->billTo->email                                     = 'null@cybersource.com';
        $request->shipTo                                            = null;
        $request->card                                              = null;
        $request->purchaseTotals                                    = null;
        $request->businessRules                                     = null;
        $request->item                                              = null;
        $request->merchantDefinedData                               = null;
        $request->ccCaptureService                                  = null;
        $request->davService                                        = null;
        $request->merchantReferenceCode                             = $request->merchantReferenceCode;
        $request->paySubscriptionCreateService                      = new stdClass();
        $request->paySubscriptionCreateService->run                 = 'true';
        $request->paySubscriptionCreateService->paymentRequestID    = $response->getResponse()->requestID;
        $request->recurringSubscriptionInfo                         = new stdClass();
        $request->recurringSubscriptionInfo->frequency              = 'on-demand';
        return $this;
    }

}