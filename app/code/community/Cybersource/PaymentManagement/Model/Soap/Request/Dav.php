<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 *
 */
class Cybersource_PaymentManagement_Model_Soap_Request_Dav
    extends Cybersource_PaymentManagement_Model_Soap_Request_Authorize
{

    /**
     *
     * @deprecated
     * @var string
     */
    protected $method;


    /**
     * Create standalone Delivery Address Verification request
     *
     * @param  int                                  $customerId         Customer ID
     * @param  Mage_Customer_Model_Address_Abstract $billingAddress     Billing Address
     * @param  Mage_Customer_Model_Address_Abstract $shippingAddress    Shipping Address
     * @return string                                                   XML response from DAV Request
     */
    public function fetchAddressVerificationResponse(
            Mage_Customer_Model_Address_Abstract $billingAddress,
            Mage_Customer_Model_Address_Abstract $shippingAddress
    ) {
        // create DAV request
        $this->createDavRequest($billingAddress, $shippingAddress);
        $response = $this->getSoapClient()->getResponse($this);
        // $this->setData('response', $response->getResponse());
        return $response;
    }



    /**
     * Get soap client
     *
     * @return Cybersource_PaymentManagement_Model_Soap_Client
     */
    protected function getSoapClient()
    {
        return Mage::getModel('cybersource/soap_client');
    }

    /**
     * Create request object for Delivery Address Verification SOAP call
     *
     * Set request property of this object. Does not return the request
     *
     * @todo  Review the createDavRequest() method name. "create" implies that it will return what it created.
     * @param  Mage_Customer_Model_Address_Abstract $billingAddress     Billing address
     * @param  Mage_Customer_Model_Address_Abstract $shippingAddress    Shipping address
     * @return void
     */
    protected function createDavRequest(Mage_Customer_Model_Address_Abstract $billingAddress, Mage_Customer_Model_Address_Abstract $shippingAddress)
    {
        if (isset($this->getRequest()->ccAuthService)) {
            $this->getRequest()->ccAuthService = null;
        }
        $this->addBillingAddressData($this->createBillingAddressDataForDav($billingAddress));
        $this->addShippingAddressData($this->createShippingAddressDataForDav($shippingAddress));
        $this->getRequest()->davService = new stdClass();
        $this->getRequest()->davService->run = 'true';
    }


    /**
     * Compile billing address data for request from billing address
     *
     * @param  Mage_Customer_Model_Address_Abstract $billingAddress Billing address
     * @return array                                                Billing address data for request
     */
    protected function createBillingAddressDataForDav(Mage_Customer_Model_Address_Abstract $billingAddress)
    {
        $result                 = array();
        $result['firstName']    = $billingAddress->getFirstname();
        $result['lastName']     = $billingAddress->getLastname();
        $result['company']      = $billingAddress->getCompany();
        $result['street1']      = $billingAddress->getStreet(1);
        $result['street2']      = $billingAddress->getStreet(2);
        $result['city']         = $billingAddress->getCity();
        $result['state']        = $this->getStateCodeFromRegionName($billingAddress->getRegion());
        $result['postalCode']   = $billingAddress->getPostcode();
        $result['country']      = $billingAddress->getCountry();
        if (Mage::getStoreConfig('payment/cybersource_card_processing/phone_number')) {
            $result['phoneNumber'] = $billingAddress->getTelephone();
        }
        return $result;
    }

    /**
     * Compile shipping address data for request from shipping address
     *
     * @param  Mage_Customer_Model_Address_Abstract $shippingAddress    Shipping address
     * @return array                                                    Shipping address data for request
     */
    protected function createShippingAddressDataForDav(Mage_Customer_Model_Address_Abstract $shippingAddress)
    {
        $result                 = array();
        $result['firstName']    = $shippingAddress->getFirstname();
        $result['lastName']     = $shippingAddress->getLastname();
        $result['company']      = $shippingAddress->getCompany();
        $result['street1']      = $shippingAddress->getStreet(1);
        $result['street2']      = $shippingAddress->getStreet(2);
        $result['city']         = $shippingAddress->getCity();
        $result['state']        = $this->getStateCodeFromRegionName($shippingAddress->getRegion());
        $result['postalCode']   = $shippingAddress->getPostcode();
        $result['country']      = $shippingAddress->getCountry();
        if (Mage::getStoreConfig('payment/cybersource_card_processing/phone_number')) {
            $result['phoneNumber'] = $shippingAddress->getTelephone();
        }
        return $result;
    }

}
