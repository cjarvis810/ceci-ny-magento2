<?php

/**
 * CyberSource PaymentManagement Credit Card Processing (Payment) Model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Payment extends Cybersource_PaymentManagement_Model_Cybersource
{

    /*
    * Assembles the Auhtorization Request for CyberSource API
    *
    * @param $payment Varien_Object payment object
    * @param $amount
    *
    * @return Request object
    */
    protected function assembleAuthorizeRequest(Varien_Object $payment, $amount) {
        $request = Mage::getModel('cybersource/soap_request_authorize');
        $request->addPayment($payment);
        $request->addAmountData($amount);
        $request->addItemsData($payment->getOrder()->getAllVisibleItems());
        return $request;
    }

    /*
    * Assembles the Capture Request for CyberSource API
    *
    * @param $payment
    * @param amount
    */
    protected function assembleCaptureRequest(Varien_Object $payment, $amount) {

        /**
         * Clarification:
         * Request Token is a required API field that you will  receive
         * in the reply of all services and send in the API request of most
         * follow-on services. It has nothing to do with payment tokenization.
         * The data used for payment tokenization is called Subscription ID.
         * */
        $cybersourceRequestToken = $payment->getCybersourceRequestToken();
        $parentTransactionId = $payment->getParentTransactionId();

        /**
         * If there is already an authorization
         * Else if there is not any authorization
         * */
        if ($parentTransactionId && $cybersourceRequestToken) {
            $request = Mage::getModel('cybersource/soap_request_capture', array(
                    Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_ID => $parentTransactionId,
                    Cybersource_PaymentManagement_Model_Soap_Response::REQUEST_TOKEN => $cybersourceRequestToken,
                    'order_id' => $payment->getOrder()->getIncrementId(),
                    'merchant_ref_number' => Mage::registry('merchant_ref_number')? : null

            ));
            $request->addCurrencyData($payment);
        } else {
            $request = Mage::getModel('cybersource/soap_request_capture');
            $request->addPayment($payment);
            $request->addItemsData($payment->getOrder()->getAllVisibleItems()); 
        }
        $request->addAmountData($amount);
        return $request;
    }


    /*
    * Save the details of the last transaction to the database
    * @param $payment
    * @param amount
    */
    protected function saveTransactionDetails($payment, $response) {
        $payment->setLastTransId($response->getRequestId());
        $payment->setLastCybersourceRequestToken($response->getRequestToken());
        $payment->setCcTransId($response->getRequestId());
        $payment->setTransactionId($response->getRequestId());
        $payment->setIsTransactionClosed(0);
        $payment->setCybersourceRequestToken($response->getRequestToken());
    }



    /**
     * Check whether the credit card processing is set to test mode
     *
     * @return bool
     */
    protected function isTestMode()
    {
        $mode = Mage::getStoreConfig('payment/cybersource_card_processing/test');
        return (bool) $mode;
    }

    /**
     * Check whether Address Verification Service is in use
     *
     * @return bool True if set to Test Mode
     */
    protected function isAvsInUse()
    {
        $mode = Mage::getStoreConfig('payment/cybersource_card_processing/avs_order');
        return (bool) $mode;
    }


    /**
     * Check whether Credit Card Verification Number is in use
     *
     * @return bool
     */
    protected function isCvnInUse()
    {
        $mode = Mage::getStoreConfig('payment/cybersource_card_processing/cvn_order');
        return (bool) $mode;
    }

    /**
     * Get whether payment action is set to authorize only
     *
     * @return bool True if set to authorize only
     */

    public function isAuthorizeOnly()
    {
        $paymentAction = Mage::getStoreConfig('payment/cybersource_card_processing/payment_action') == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE ;
        return (bool) $paymentAction;
    }

}