<?php
/**
 * Reporting API Model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Reports extends Mage_Core_Model_Abstract
{
    /**
     * Report types
     */
    const REPORT_TYPE_ONDEMAND  = 'ondemand';
    const REPORT_TYPE_DAILY  = 'daily';


    /**
     * Retreive on-demand (decision manager) conversion report
     *
     * @param null $date
     * @return bool|object
     */
    public function fetchOnDemandConversionReport()
    {
        return $this->downloadConversionReport(self::REPORT_TYPE_ONDEMAND);
    }

    /**
     * Retreive daily (decision manager) conversion report
     *
     * @param null $date
     * @return bool|object
     */
    public function fetchDailyConversionReport()
    {
        return $this->downloadConversionReport(self::REPORT_TYPE_DAILY);
    }



    /**
     * Return true if Reporting API is enabled
     * @depracted
     * @see Cybersource_PaymentManagement_Helper_Reports::isEnabled
     * @return bool
     */
    protected function isReportingApiEnabled()
    {
        $reportingApiSetting = Mage::getStoreConfig('payment/cybersource_card_processing/dm_reporting_query');
        return (bool) $reportingApiSetting;
    }

    /**
     * Return true if the conversion report is applicable
     *
     * @return bool
     */
    protected function isConversionReportApplicable()
    {
        $last_cron_run = Mage::getResourceModel('cron/schedule_collection')
        ->setOrder('schedule_id')
        ->setPageSize(1)
        ->addFieldToFilter('job_code', array('eq' => 'cybersource_paymentmanagement'))
        ->addFieldToSelect('executed_at')
        ->addFieldToFilter('status', array('eq' => 'success'))
        ->getFirstItem();
        if ($last_cron_run->getExecutedAt()) {
            $executed = new DateTime($last_cron_run->getExecutedAt());
        } else {
            $executed = new DateTime(Varien_Date::now());
        }
        $now = new DateTime(Varien_Date::now());
        $interval = $now->diff($executed);
        if ($interval->format('%h') < Mage::getStoreConfig('payment/cybersource_card_processing/dm_reporting_query')) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Download conversion report.
     *
     * @return bool|object
     */
    protected function downloadConversionReport($type)
    {
        $httpClient = $this->formulateQuery($type);
        if($httpClient) {
            $response = $httpClient->request(Zend_Http_Client::POST);
            if($response->isSuccessful()) {
                $httpStatusCode = $response->getStatus();
                if($httpStatusCode == 200) {
                    $responseBody = $response->getRawBody();
                    if ($responseBody) {
                        $conversionReport = simplexml_load_string($responseBody,null, LIBXML_NOERROR);
                        if($conversionReport) {
                            return $conversionReport;
                        } else {
                            if (preg_match('/Invalid date.*/', (string) $responseBody) ? true : false) {
                                // When there are no conversions in the report this misleaing message is returned
                                $issue = Mage::helper('cybersource')->__(' No conversions available. ') ;
                            } else {
                                $issue = $responseBody;
                            }
                            throw new Cybersource_PaymentManagement_Exception($issue);
                        }
                    } else {
                        $issue =  Mage::helper('cybersource')->__(' No response received. Please try again. ');
                        throw new Cybersource_PaymentManagement_Exception($issue);
                    }
                } else {
                    $issue = Mage::helper('cybersource')->__(' HTTP succesful but unexpected Reporting API response. Please try again. ');
                    if($httpStatusCode) {
                        $issue .= $httpStatusCode;
                    }
                    throw new Cybersource_PaymentManagement_Exception($issue);
                }
            } else {
                $issue =  Mage::helper('cybersource')->__(' HTTP Error.  Please try again. ');
                if($httpStatusCode) {
                    $issue .= $httpStatusCode;
                }
                throw new Cybersource_PaymentManagement_Exception($issue);
            }
        }
    }

    /**
     * Prepares the query for POST request
     *
     * @return bool|object
     */
    protected function formulateQuery($type, $date = null) {

        if (Mage::getStoreConfig('cybersource/decision_manager/reporting_api_username')) {
            $reportingApiUser = Mage::getStoreConfig('cybersource/decision_manager/reporting_api_username');
        }
        if (Mage::getStoreConfig('cybersource/decision_manager/reporting_api_password')) {
            $reportingApiPassword = Mage::getStoreConfig('cybersource/decision_manager/reporting_api_password');
        }
        if (Mage::getStoreConfig('cybersource/decision_manager/reporting_api_mode') == 'production') {
            $reportingApiModeServer = 'https://ebc.cybersource.com/ebc/';
        } elseif (Mage::getStoreConfig('cybersource/decision_manager/reporting_api_mode') == 'test') {
            $reportingApiModeServer = 'https://ebctest.cybersource.com/ebctest/';
        }
        $params = array('merchantID' => Mage::getStoreConfig('cybersource/account/merchant'),
                'format' => 'xml',
                'username' => $reportingApiUser,
                'password' => $reportingApiPassword);
        switch ($type) {
            case self::REPORT_TYPE_ONDEMAND:
                // The interval must not exceed 24 hours, and
                // you must use the UTC (GMT) time
                $gmtNow = Mage::getModel('core/date')->gmtDate();
                $nowDateTime = new DateTime($gmtNow);
                $params['endDate'] = $nowDateTime->format("Y-m-d");
                $params['endTime'] = $nowDateTime->format("H:i:s");
                $yesterdayDateTime = $nowDateTime->modify('-1 day')->modify('+1 minute');
                $params['startDate'] = $yesterdayDateTime->format("Y-m-d");
                $params['startTime'] = $yesterdayDateTime->format("H:i:s");
                $serverAddress = $reportingApiModeServer . 'ConversionDetailReportRequest.do';
                $uri = Zend_Uri::factory($serverAddress);
                $uri->setQuery($params);
                $httpClient = new Varien_Http_Client($uri);
                return $httpClient;
                break;
            case self::REPORT_TYPE_DAILY:
                if (is_null($date)) {
                    $date = date('Y/m/d', strtotime('yesterday'));
                }
                $serverAddress = $reportingApiModeServer . 'DownloadReport' . DS . $date . DS
                . trim($_merchant_id) . DS . 'ConversionDetailReport.xml';
                $uri = Zend_Uri::factory($serverAddress);
                $httpClient = new Varien_Http_Client($uri);
                return $httpClient;
                break;
            default:
                return false;
                break;
        }

    }
}