<?php

/**
 *  Handle the lack of ionCube Loader rather gracefully
 *  If ionCube loader not available then fallback to the harmless Fallback.php
 *  so that it does not break the website
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
if (extension_loaded('ionCube Loader')) {
    include 'SourceCode/Observer/Encoded.php';
}
else {
    include 'SourceCode/Observer/Fallback.php';
}
