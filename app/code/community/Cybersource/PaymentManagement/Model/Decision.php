<?php

/**
 * Decision manager model
 *
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Model_Decision extends Cybersource_PaymentManagement_Model_Cybersource
{

    /**
     * Return true if Decision Manager is enabled
     *
     * @return bool
     */
    public function isDecisionManagerEnabled() {
        return (bool) Mage::getStoreConfig('payment/cybersource_card_processing/dm_enabled');
    }

    /**
     * Process the decision made for the order by CyberSource Decision Manager rules.
     * If there is a locally stored verification result then uses it instead of calling CyberSource
     *
     * @param $response
     * @param $payment
     * @return bool
     */
    public function manageDecision(
            Cybersource_PaymentManagement_Model_Soap_Response $response,
            Varien_Object $payment,
            $action
    ){
        $isDecisionManagerEnabled = $this->isDecisionManagerEnabled();
        if($isDecisionManagerEnabled) {
            $cybersourceDecision = $response->getDecision();
            switch ($cybersourceDecision) {
                case Cybersource_PaymentManagement_Helper_Decision::REVIEW:
                    if($action == 'capture') {
                        if ($this->getConfigData('dm_comments')) {
                            // @todo make translatable
                            $comment = 'Decision Manager Update: '
                                    . Cybersource_PaymentManagement_Helper_Decision::REVIEW . ' '
                                            . Mage::helper('cybersource')->prepareSignature();
                            $comment = Mage::helper('core')->__($comment);
                        } else {
                            $comment = false;
                        }
                        $payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH, null, null, $comment);
                        $payment->setSkipTransactionCreation(true);
                        $payment->setIsTransactionPending(true);
                    } elseif ($action == 'authorize') {
                        if ($this->getConfigData('dm_comments')) {
                            $comment = Mage::helper('core')->__('Decision Manager '
                                    . 'Update: ' . Cybersource_PaymentManagement_Helper_Decision::REVIEW . ' '
                                    . Mage::helper('cybersource')->prepareSignature());
                            $payment->getOrder()->addStatusHistoryComment($comment);
                        }
                        $payment->setIsTransactionPending(true);
                        Mage::register('dm-decision', 'dm-review');
                    }
                    break;
                case Cybersource_PaymentManagement_Helper_Decision::REJECT:
                    if($action == 'capture') {
                        if(Mage::getModel('cybersource/payment')->isAuthorizeOnly()) {
                            // @todo make translatable
                            if ($this->getConfigData('dm_comments')) {
                                $comment = Mage::helper('core')->__('Decision Manager Update: '
                                        . Cybersource_PaymentManagement_Helper_Decision::REJECT . ' '
                                        . Mage::helper('cybersource')->prepareSignature());
                                $payment->getOrder()->addStatusHistoryComment($comment);
                            }
                            $payment->setSkipTransactionCreation(true);
                            $payment->getOrder()->cancel();
							$payment->getOrder()->setCanSendNewEmailFlag(false);
							Mage::helper('pixcybersource')->sendPaymentFailedEmail($payment->getOrder()->getQuote(), Mage::helper('cybersource')->__('Payment rejected.
                                    Please try again after reviewing your address and credit card information.'), 'onepage', $payment->getOrder()->getIncrementId());

                        } else {
                            $error = Mage::helper('cybersource')->__('Payment rejected.
                                    Please try again after reviewing your address and credit card information.');
                            return $error;
                        }
                    } elseif ($action == 'authorize') {
                        if ($this->getConfigData('dm_comments')) {
                             // @todo make translatable
                            $comment = Mage::helper('core')->__('Decision Manager '
                                    . 'Update: ' . Cybersource_PaymentManagement_Helper_Decision::REJECT . ' '
                                    . Mage::helper('cybersource')->prepareSignature());
                            $payment->getOrder()->addStatusHistoryComment($comment);
                        }
//						Mage::helper('pixcybersource')->sendPaymentFailedEmail($payment->getOrder()->getQuote(), Mage::helper('cybersource')->__('Payment rejected.
//                                    Please try again after reviewing your address and credit card information.'), 'onepage', $payment->getOrder()->getIncrementId());
//						$payment->getOrder()->setCanSendNewEmailFlag(false);
//                        $payment->setSkipTransactionCreation(true);
                        Mage::helper('cybersource/decision')->markDecisionReject();
						$error = Mage::helper('cybersource')->__("\n\nREASON: Payment rejected. Please review your address and credit card information");
						return $error;
                    }
                    break;
                case Cybersource_PaymentManagement_Helper_Decision::ACCEPT:
                    break;
                default:
                    $error = Mage::helper('cybersource')->__('Payment error.
                            Please try again after reviewing your address and credit card information.');
                     return $error;
                break;
            }
        }
    }
}