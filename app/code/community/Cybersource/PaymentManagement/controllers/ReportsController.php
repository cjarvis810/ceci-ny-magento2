<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */

require_once  'Mage/Adminhtml/controllers/Sales/OrderController.php';

class Cybersource_PaymentManagement_ReportsController extends Mage_Adminhtml_Sales_OrderController
{

    /**
     * Download the conversion report for the order
     *
     * @see reviewPaymentAction method of the core OrderController class
     * @return void
     */
    public function downloadAction()
    {
        try {
            $orderId = (int) $this->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
            if($orderId) {
                Mage::getModel('cybersource/conversion')->getDecisionConversion($orderId);
            }
            $order->save();
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Failed to download the conversion report.'));
            Mage::logException($e);
        }
        Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl("adminhtml/sales_order/view", array('order_id'=> $orderId)));

    }


    /**
     * Download the conversion report for order all payment_review orders
     *
     * @see reviewPaymentAction method of the core OrderController class
     * @return void
     */
    public function allAction()
    {
        try {
             Mage::getModel('cybersource/observer')->getAllDecisionConversions();
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
        }
        $orderId = (int) $this->getRequest()->getParam('order_id');
        Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl("adminhtml/sales_order/view", array('order_id'=> $orderId)));

    }

    /**
     * Accept o reject payment_review order manually.
     * Use only if necessary since this action will
     * not be reflected on CyberSource Business Center.
     *
     * @return void
     */
    public function acceptAction()
    {
        try {
            $orderId = (int) $this->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
            if($orderId) {
                Mage::getModel('cybersource/conversion')->forceCybersourcePayment($orderId, Cybersource_PaymentManagement_Helper_Decision::ACCEPT);
            }
            $order->save();
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
        }
        $orderId = (int) $this->getRequest()->getParam('order_id');
        Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl("adminhtml/sales_order/view", array('order_id'=> $orderId)));
    }
    /**
     * Accept o reject payment_review order manually.
     * Use only if necessary since this action will
     * not be reflected on CyberSource Business Center.
     *
     * @return void
     */
    public function rejectAction()
    {
        try {
            $orderId = (int) $this->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
            if($orderId) {
                Mage::getModel('cybersource/conversion')->forceCybersourcePayment($orderId, Cybersource_PaymentManagement_Helper_Decision::REJECT);
            }
            $order->save();
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
        }
        $orderId = (int) $this->getRequest()->getParam('order_id');
        Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl("adminhtml/sales_order/view", array('order_id'=> $orderId)));
    }


}