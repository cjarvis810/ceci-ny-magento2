<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_LogController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Log action
     *
     * @return void
     */
    public function logAction()
    {
        $fileName = 'CyberSourceLog.txt';
        $fileDir = ((string) Mage::getBaseDir('log')) . '/' . $fileName;
        $period = $this->getRequest()->getParam('p');

        $dateEnd = new DateTime();
        $dateEnd->modify('+1days');
        $dateStart = new DateTime();

        switch ($period) {
            case 1:
                $dateStart->modify('-1days');
                break;

            case 2:
                $dateStart->modify('-7days');
                break;

            case 3:
                $dateStart->modify('-1 month');
                break;

            case 4:
                $dateStart->modify('-3 month');
                break;

            case 5:
                $dateStart->modify('-6 month');
                break;

            case 6:
                $dateStart->modify('-12 month');
                break;

            default:
                break;
        }

        $collectionLog = Mage::getModel('cybersource/soap_log')->getCollection();
        $countLog = $collectionLog->getLastItem()->getId();

        //defined 500 record in 1 page
        /**
         * @todo hard coded limit of 500 records per page,
         *       limits should be configurable
         */
        if ($countLog < 500) {
            $collectionLog->setPageSize(500)->setCurPage(1);
            $maxPage = 1;
        } else {
            $maxPage = (int) ($countLog / 500) + 1;
            if (($countLog % 500) == 0) {
                $maxPage = $maxPage - 1;
            }
            $collectionLog->setPageSize(500)->setCurPage(1);
        }

        for ($page = 1; $page <= $maxPage; $page++) {
            $collectionLog->setCurPage($page);
            if ($countLog < 500) {
                $maxItem = $countLog;
                $startItem = 1;
            } else {
                if ($maxPage == ($page - 1)) {
                    $maxItem = $countLog - $page * 500;
                    $startItem = ($page - 1) * 500 + 1;
                    if ($page == 1)
                        $startItem = $page * 500 + 1;
                } else {
                    $startItem = ($page - 1) * 500 + 1;
                    $maxItem = $page * 500;
                    if ($maxItem > $countLog) {
                        $maxItem = $countLog;
                    }
                    if ($page == 1)
                        $startItem = 1;
                }
            }

            //walk on values and find item that we need
            for ($numItem = $startItem; $numItem <= $maxItem; $numItem++) {
                $objItem = $collectionLog->getItemById($numItem);
                if ($objItem != NULL) {
                    $objItemDate = new DateTime($objItem->getData('created_at'));
                    $searchStart = $dateStart->diff($objItemDate);
                    $searchEnd = $objItemDate->diff($dateEnd);
                    if (!$searchStart->invert) {
                        if (!$searchEnd->invert) {
                            Mage::log(serialize($objItem->getData()), null, $fileName);
                        }
                    }
                    $collectionLog->removeItemByKey($objItem->getData('entity_id'));
                }
            }
        }

        if (file_exists($fileDir)) {
            /**
             * @todo Check implode(file($fileDir), " ") logic. This looks like a
             *       backwards use of implode. The glue goes on the left.
             */
            $data = (implode(file($fileDir), " "));
        } else {
            $data = 'No logs for that period!';
        }

        $response = $this->getResponse();
        $response->clearHeaders();
        $response->setHeader('Content-Description', 'File Transfer');
        $response->setHeader('Content-type', 'text/plain');
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Content-Transfer-Encoding', 'binary');

        $response->setBody($data);

        if (file_exists($fileDir)) {
            unlink($fileDir);
        }
    }

}
