<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Block_Form extends Mage_Payment_Block_Form_Cc
{
    /**
     * Set template
     *
     * Prefixed with underscore to match parent method.
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('cybersource/form.phtml');
    }

    /**
     * Get credit card verification exceptions from config
     *
     * @return string Credit card verification exceptions
     */
    public function getVerificationExceptions()
    {
        return $this->getMethod()->getConfigData('ccv_exception');
    }

}
