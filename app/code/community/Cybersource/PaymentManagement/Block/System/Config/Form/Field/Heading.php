<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Block_System_Config_Form_Field_Heading
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    /**
     * Render heading element as html
     *
     * @param  Varien_Data_Form_Element_Abstract $element Heading element to render
     * @return string                                     Rendered html
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $useContainerId = $element->getData('use_container_id');
        $message = '<tr class="system-fieldset-sub-head" id="row_%s">'
                 . '<td colspan="5"><h4 id="%s">%s</h4>%s</td></tr>';
        return sprintf($message, $element->getHtmlId(), $element->getHtmlId(),
                       $element->getLabel(), $this->renderComment($element));
    }

    /**
     * Render comment part of heading as html
     * 
     * @param  Varien_Data_Form_Element_Abstract $element Heading element
     * @return string                                     Rendered comment
     */
    protected function renderComment(Varien_Data_Form_Element_Abstract $element)
    {
        if (!$element->hasComment()) {
            return '';
        }
        return sprintf('<p class="note">%s</p>', $element->getComment());
    }

}
