<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Block_System_Config_Cvn_Exception
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    /**
     * Render exception element as html
     *
     * Prefixed with underscore to match parent method.
     *
     * @param  Varien_Data_Form_Element_Abstract $element Exception element to render
     * @return string                                     Rendered html
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        if (!Mage::getStoreConfig('payment/cybersource_card_processing/useccv'))
            $element->setDisabled(TRUE);
        return parent::_getElementHtml($element) . $this->getJs($element->getHtmlId());
    }

    /**
     * Render js part of exception as html
     *
     * @param  string $id Element html id
     * @return string     Rendered js
     */
    protected function getJs($id)
    {
        return "
        <script type=\"text/javascript\">
        Event.observe('cybersource_card_processing_cvn', 'change', function(){
            specific=$('cybersource_card_processing_cvn').value;
            $('{$id}').disabled = (!specific || specific!=1);
        });
        </script>";
    }
}
