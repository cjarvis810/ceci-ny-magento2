<?php
/**
 * Extends Adminhtml sales order view
*
* @package Cybersource
* @subpackage PaymentManagement
* @author ----

*/
class Cybersource_PaymentManagement_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View {

    /**
     * False to disable "Get All CyberSource Updates" button
     * on adminhtml sales order view page. Unless otherwise
     * necessary keep disabled since this is taken care of
     * by the scheduled cron jobs.
     */
    protected $review_all_enabled = false;

    /**
     * Adds custom button to Adminhtml sales order view page
     *
     * @param Mage_Payment_Model_Info $payment
     *
     * @return bool
     */
    public function  __construct() {
        parent::__construct();
        $orderId = (int) $this->getRequest()->getParam('order_id');
        if($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            $orderStatus = $order->getStatus();
            if($orderStatus == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
                $paymentMethod = $order->getPayment()->getMethod();
                if($paymentMethod == 'cybersource_card_processing') {
                    $this->_addButton('cybersource_update', array(
                        'label'     => Mage::helper('cybersource')->__('Get CyberSource Update'),
                        'onclick'   => 'setLocation(\'' . $this->getReportsControllerActionUrl('download') . '\')',
                    ), 0, 100, 'header', 'header');
                    if($this->review_all_enabled) {
                        $this->_addButton('cybersource_update_all', array(
                            'label'     => Mage::helper('cybersource')->__('Get All CyberSource Updates'),
                            'onclick'   => 'setLocation(\'' . $this->getReportsControllerActionUrl('all') . '\')',
                        ), 0, 200, 'header', 'header');
                     }
                     if(Mage::getStoreConfig('payment/cybersource_card_processing/review_enabled')) {
                         $this->_addButton('cybersource_manual_accept', array(
                             'label'     => Mage::helper('cybersource')->__('Accept CyberSource Payment'),
                             'onclick'   => 'setLocation(\'' . $this->getReportsControllerActionUrl('accept') . '\')',
                          ), 0, 300, 'header', 'header');
                          $this->_addButton('cybersource_manual_reject', array(
                              'label'     => Mage::helper('cybersource')->__('Reject CyberSource Payment'),
                              'onclick'   => 'setLocation(\'' . $this->getReportsControllerActionUrl('reject') . '\')',
                          ), 0, 400, 'header', 'header');
                    }
                }

             }
         }
     }

   /**
    * Return back url for downloading cybversource conversion report
    *
    * @see  Cybersource_PaymentManagement_ReportsController::downloadAction
    * @return string
    */
    protected function getReportsControllerActionUrl($action)
    {
        $orderId = (int) $this->getRequest()->getParam('order_id');
        $key = Mage::getModel('adminhtml/url');
        $url = $key->getUrl('cybersource/reports/' . $action . '/', array('order_id' => $orderId));
        return $url;
    }


}