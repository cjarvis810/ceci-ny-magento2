<?php

/**
 * @package Cybersource
 * @subpackage PaymentManagement
 * @author ----
 */
class Cybersource_PaymentManagement_Block_Fingerprint extends Mage_Core_Block_Template
{

    /**
     * CyberSource provides a unique Organization ID for device fingerprinting 
     *
     * @var string
     */
    protected $organizationId;

    /**
     * CyberSource provides a unique server URL for device fingerprinting 
     *
     * @var string
     */
    protected $url;

    /**
     * Set properties from config
     *
     * Prefixed with underscore to match parent method.
     */
    public function _construct()
    {
        if (Mage::getStoreConfig('cybersource/decision_manager/df_mode') == 'production') {
            $this->organizationId = Mage::getStoreConfig('cybersource/decision_manager/df_org_prod');
        } elseif (Mage::getStoreConfig('cybersource/decision_manager/df_mode') == 'test') {
            $this->organizationId = Mage::getStoreConfig('cybersource/decision_manager/df_org_test');
        }

        if (Mage::getStoreConfig('cybersource/decision_manager/df_server_dns') == 'remote') {
            $this->url = 'https://h.online-metrix.net/fp';
        } elseif (Mage::getStoreConfig('cybersource/decision_manager/df_server_dns') == 'local') {
            $this->url = Mage::getStoreConfig('cybersource/decision_manager/df_local_url') . '/fp';
        }
    }

    /**
     * Render html
     *
     * Prefixed with underscore to match parent method.
     *
     * @return string
     */
    protected function _toHtml()
    {
        return $this->prepareHtml('png') . $this->prepareHtml('js')
             . $this->prepareHtml('swf');
    }

    /**
     * Render part of html
     *
     * @param  string $type Type of html to render ('png','js', or 'swf')
     * @return string       Rendered html
     */
    protected function prepareHtml($type)
    {
        $html = '';
        $sessionId = Mage::getStoreConfig('cybersource/account/merchant')
                   . session_id();

        switch ($type) {

            case 'png':
                $url = $this->url . '/clear.png?org_id=' . $this->organizationId
                     . '&amp;session_id='
                     . Mage::getStoreConfig('cybersource/account/merchant')
                     . session_id() . '&amp;';
                $html = '<p style="background:url(' . $url . 'm=1)"></p>'
                      . '<img src="' . $url . 'm=2" alt="">';
                break;

            case 'js':

                $html = '<script src="' . $this->url . '/check.js?org_id='
                      . $this->organizationId . '&amp;session_id=' . $sessionId
                      . '" type="text/javascript"></script>';
                break;

            case 'swf':
                $html = '<object type="application/x-shockwave-flash" data="'
                      . $this->url . '/fp.swf?org_id=' . $this->organizationId
                      . '&amp;session_id=' . $sessionId . '" width="1" '
                      . 'height="1" id="thm_fp"><param name="movie" value="'
                      . $this->url . '/fp.swf?org_id=' . $this->organizationId
                      . '&amp;session_id=' . $sessionId . '"/><div></div>'
                      . '</object>';
                break;

            default:
                break;
        }

        return $html;
    }

}
